package bizplay.kosign.com.bizplay;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.webcash.sws.pref.MemoryPreferenceDelegator;
import com.webcash.sws.pref.PreferenceDelegator;

import bizplay.kosign.com.bizplay.conf.Conf;
import bizplay.kosign.com.bizplay.constant.Constants;
import bizplay.kosign.com.bizplay.tran.ComTran;
import bizplay.kosign.com.bizplay.tx.TX_IBK_MG_RES;
import bizplay.kosign.com.bizplay.tx.TX_IBK_MG_RES_REC1;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_P001_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_P001_RES;

public class IntroActivity extends AppCompatActivity implements ComTran.OnComTranListener  {

    private ComTran mComTran;
    private static final String REQ_MG_DATA = "MG_DATA";    // MG 전문 구분 코드

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        mComTran = new ComTran(this, this);

        mComTran.requestData(REQ_MG_DATA, Conf.SITE_MG_URL + "/MgGate?master_id=" + Conf.MASTER_ID);
    }

    @Override
    public void onTranResponse(String tranCode, Object object) throws Exception {
        switch (tranCode) {
            case REQ_MG_DATA:
                TX_IBK_MG_RES mgRes = new TX_IBK_MG_RES(object);
                boolean strService = mgRes.getAvailableService();
                String serviceReason = mgRes.getAvailableAct();
                if (serviceReason.equals("")) {
                    serviceReason = "안정된 서비스 제공을 위해 시스템 업그레이드가 진행중입니다. 업그레이드 중에는 서비스 이용이 제한됩니다.";
                }
                //+서비스 여부 체크
                if (strService) {

                    TX_IBK_MG_RES_REC1 menuInfo = mgRes.getMenuInfo();

                    //+  MG 정보 저장..
                    MemoryPreferenceDelegator.getInstance().put("MENU_INFO", menuInfo.getMessageToString()); // 앱 메뉴 정보
                    MemoryPreferenceDelegator.getInstance().put("BIZ_URL", mgRes.getBizURL());            // 전문 URL
                    MemoryPreferenceDelegator.getInstance().put("START_URL", mgRes.getStartURL());        // app 시작 URL
                    MemoryPreferenceDelegator.getInstance().put("CHANNEL_ID", mgRes.getChannelID());    // 전문 채널 ID
                    MemoryPreferenceDelegator.getInstance().put("PORTAL_ID", mgRes.getPortalId());        // Portal ID
                    MemoryPreferenceDelegator.getInstance().put("ALIM_URL", mgRes.getALIMURL());        // 알림UR
                    MemoryPreferenceDelegator.getInstance().put("MEMBER_URL", mgRes.getMemberUrl());    // Member URL
                    MemoryPreferenceDelegator.getInstance().put("LOGIN_FORGET_ID", mgRes.getForgetIdURL());    // 로그인 > 아이디 찾기 URL
                    MemoryPreferenceDelegator.getInstance().put("LOGIN_FORGET_PW", mgRes.getForgetPwURL());    // 로그인 > 패스워드 찾기 URL
                    MemoryPreferenceDelegator.getInstance().put("CLOUDE_URI", mgRes.getCloudeUri());    // cloude uri
                    MemoryPreferenceDelegator.getInstance().put("CLOUDE_KEY", mgRes.getCloudeKey());    // cloude api key
                    MemoryPreferenceDelegator.getInstance().put("QNA_URL", mgRes.getQanUrl());            // qna rul
                    MemoryPreferenceDelegator.getInstance().put("EVENT_YN", mgRes.getEventYN());        // event yn
                    MemoryPreferenceDelegator.getInstance().put("EVENT_URL", mgRes.getEventUrl());        // event rul


                    if (rememberMe()) {
                        Log.d("REMEBER: ", "REMMMM");
                        try {
                            TX_MYCD_MBL_P001_REQ req = new TX_MYCD_MBL_P001_REQ();
                            req.setUserID(PreferenceDelegator.getInstance(IntroActivity.this).get(Constants.LoginInfo.USER_ID));
                            req.setUserPassword(PreferenceDelegator.getInstance(IntroActivity.this).get(Constants.LoginInfo.USER_PWD));

                            mComTran.requestData(TX_MYCD_MBL_P001_REQ.TXNO, req.getSendMessage(), true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {

                        /* New Handler to start the Menu-Activity
                        * and close this Splash-Screen after some seconds.*/
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                        /* Create an Intent that will start the Menu-Activity. */
                                Intent mainIntent = new Intent(IntroActivity.this, TutorialActivity.class);
                                IntroActivity.this.startActivity(mainIntent);
                                IntroActivity.this.finish();
                            }
                        }, 3000);

                    }

                }

                break;
            case TX_MYCD_MBL_P001_REQ.TXNO:
                TX_MYCD_MBL_P001_RES res = new TX_MYCD_MBL_P001_RES(object);


                Log.d("Login Res: ", res.getMessageToString());

                PreferenceDelegator.getInstance(this).put(Constants.LoginInfo.USER_ID, res.getUSER_ID());

                MemoryPreferenceDelegator.getInstance().put(Constants.LoginInfo.BSNN_NM, res.getBSNN_NM());
                MemoryPreferenceDelegator.getInstance().put(Constants.LoginInfo.CRTC_PATH, res.getCRTC_PATH());
                MemoryPreferenceDelegator.getInstance().put(Constants.LoginInfo.USER_IMG_PATH, res.getUSER_IMG_PATH());
                MemoryPreferenceDelegator.getInstance().put(Constants.LoginInfo.USER_NM, res.getUSER_NM());

                Constants.ReceiptInfo.WILL_RELOAD = true;
                Intent mainIntent = new Intent(IntroActivity.this, MainActivity.class);
                IntroActivity.this.startActivity(mainIntent);
                IntroActivity.this.finish();
                break;
        }
    }

    private boolean rememberMe() {
        if (PreferenceDelegator.getInstance(this).get(Constants.LoginInfo.USER_PWD).equals("") || PreferenceDelegator.getInstance(this).get(Constants.LoginInfo.USER_PWD).equals(null)) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }
}
