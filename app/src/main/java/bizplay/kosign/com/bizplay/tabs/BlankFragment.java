package bizplay.kosign.com.bizplay.tabs;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import bizplay.kosign.com.bizplay.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment {

    private static BlankFragment instance = null;
    public BlankFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_blank, container, false);
    }

    public static BlankFragment getInstance() {
        try {
            if (instance == null) {
                // new instance
                instance = new BlankFragment();

                // sets data to bundle
                Bundle bundle = new Bundle();

                // set data to fragment
                instance.setArguments(bundle);

                return instance;
            } else {

                return instance;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
