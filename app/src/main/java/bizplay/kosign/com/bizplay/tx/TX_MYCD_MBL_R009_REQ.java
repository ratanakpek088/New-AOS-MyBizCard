package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 *  Created by Huo Chhunleng on 15/Feb/2017.
 */

public class TX_MYCD_MBL_R009_REQ extends TxMessage {
    public static final String TXNO = "MYCD_MBL_R009";
    private TX_MYCD_MBL_R009_REQ_DATA mTxKeyData;

    public TX_MYCD_MBL_R009_REQ() throws Exception {
        mTxKeyData = new TX_MYCD_MBL_R009_REQ_DATA();
        super.initSendMessage();
    }

    private class TX_MYCD_MBL_R009_REQ_DATA{
        private String CD_USER_NO   = "CD_USER_NO";     // 카드사용자번호
        private String SEND_DTM     = "SEND_DTM";       //  전송희망일시
        private String NOTI_SEQ     = "NOTI_SEQ";       // 일련번호
    }

    public void setCD_USER_NO(String value) throws Exception{
        mSendMessage.put(mTxKeyData.CD_USER_NO, value);
    }

    public void setSEND_DTM(String value) throws Exception{
        mSendMessage.put(mTxKeyData.SEND_DTM, value);
    }

    public void setNOTI_SEQ(String value) throws Exception{
        mSendMessage.put(mTxKeyData.NOTI_SEQ, value);
    }

}
