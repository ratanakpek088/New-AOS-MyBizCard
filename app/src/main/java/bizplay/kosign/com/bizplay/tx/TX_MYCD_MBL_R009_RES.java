package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * @author Huo Chhunleng on 15/Feb/2017.
 */
public class TX_MYCD_MBL_R009_RES extends TxMessage {

    private TX_MYCD_MBL_R009_RES_DATA mTxKeyData;

    private class TX_MYCD_MBL_R009_RES_DATA{
        private String CARD_NO          = "CARD_NO";
        private String APV_DT           = "APV_DT";
        private String APV_NO           = "APV_NO";
        private String TX_SEQ           = "TX_SEQ";
        private String APV_GB           = "APV_GB";
        private String APV_TM           = "APV_TM";
        private String TAX_TYPE_CD      = "TAX_TYPE_CD";
        private String VAT_RFND_STS     = "VAT_RFND_STS";
        private String CARD_CORP_CD     = "CARD_CORP_CD";
        private String CARD_CORP_NM     = "CARD_CORP_NM";
        private String APV_AMT          = "APV_AMT";
        private String SUPL_AMT         = "SUPL_AMT";
        private String VAT_AMT          = "VAT_AMT";
        private String BSNS_DSNC_CD     = "BSNS_DSNC_CD";
        private String BSNS_DSNC_NM     = "BSNS_DSNC_NM";
        private String TRAN_KIND_CD     = "TRAN_KIND_CD";
        private String TRAN_KIND_NM     = "TRAN_KIND_NM";
        private String MGMT1            = "MGMT1";
        private String MGMT1_NM         = "MGMT1_NM";
        private String MGMT2            = "MGMT2";
        private String MGMT2_NM         = "MGMT2_NM";
        private String MGMT3            = "MGMT3";
        private String MGMT3_NM         = "MGMT3_NM";
        private String MGMT4            = "MGMT4";
        private String MGMT4_NM         = "MGMT4_NM";
        private String MGMT5            = "MGMT5";
        private String MGMT5_NM         = "MGMT5_NM";
        private String BP_EMPL_REC      = "BP_EMPL_REC";
        private String BP_CUST_REC      = "BP_CUST_REC";
        private String MEST_BIZ_NO      = "MEST_BIZ_NO";
        private String MEST_NO          = "MEST_NO";
        private String MEST_REPR_NM     = "MEST_REPR_NM";
        private String MEST_NM          = "MEST_NM";
        private String MEST_TEL_NO      = "MEST_TEL_NO";
        private String MEST_ZIP_CD      = "MEST_ZIP_CD";
        private String MEST_ADDR        = "MEST_ADDR";
        private String CARD_TPBZ_CD     = "CARD_TPBZ_CD";
        private String CARD_TPBZ_NM     = "CARD_TPBZ_NM";
        private String RCPT_USER_REC    = "RCPT_USER_REC";
        private String RCPT_IMG_REC     = "RCPT_IMG_REC";
    }

    public TX_MYCD_MBL_R009_RES(Object obj) throws Exception {
        mTxKeyData = new TX_MYCD_MBL_R009_RES_DATA();
        super.initRecvMessage(obj);
    }

    public String getCARD_NO() throws JSONException {
        return getString(mTxKeyData.CARD_NO);
    }

    public String getAPV_DT() throws JSONException {
        return getString(mTxKeyData.APV_DT);
    }

    public String getAPV_NO() throws JSONException {
        return getString(mTxKeyData.APV_NO);
    }

    public String getTX_SEQ() throws JSONException {
        return getString(mTxKeyData.TX_SEQ);
    }

    public String getAPV_GB() throws JSONException {
        return getString(mTxKeyData.APV_GB);
    }

    public String getAPV_TM() throws JSONException {
        return getString(mTxKeyData.APV_TM);
    }

    public String getTAX_TYPE_CD() throws JSONException{
        return getString(mTxKeyData.TAX_TYPE_CD);
    }

    public String getVAT_RFND_STS() throws JSONException{
        return getString(mTxKeyData.VAT_RFND_STS);
    }

    public String getCARD_CORP_CD() throws JSONException{
        return getString(mTxKeyData.CARD_CORP_CD);
    }

    public String getCARD_CORP_NM() throws JSONException{
        return getString(mTxKeyData.CARD_CORP_NM);
    }

    public String getAPV_AMT() throws JSONException{
        return getString(mTxKeyData.APV_AMT);
    }

    public String getSUPL_AMT() throws JSONException{
        return getString(mTxKeyData.SUPL_AMT);
    }

    public String getVAT_AMT() throws JSONException{
        return getString(mTxKeyData.VAT_AMT);
    }

    public String getBSNS_DSNC_CD() throws JSONException{
        return getString(mTxKeyData.BSNS_DSNC_CD);
    }

    public String getBSNS_DSNC_NM() throws JSONException{
        return getString(mTxKeyData.BSNS_DSNC_NM);
    }

    public String getTRAN_KIND_CD() throws JSONException{
        return getString(mTxKeyData.TRAN_KIND_CD);
    }

    public String getTRAN_KIND_NM() throws JSONException{
        return getString(mTxKeyData.TRAN_KIND_NM);
    }

    public String getMGMT1() throws JSONException{
        return getString(mTxKeyData.MGMT1);
    }

    public String getMGMT1_NM() throws JSONException{
        return getString(mTxKeyData.MGMT1_NM);
    }

    public String getMGMT2() throws JSONException{
        return getString(mTxKeyData.MGMT2);
    }

    public String getMGMT2_NM() throws JSONException{
        return getString(mTxKeyData.MGMT2_NM);
    }

    public String getMGMT3() throws JSONException{
        return getString(mTxKeyData.MGMT3);
    }

    public String getMGMT3_NM() throws JSONException{
        return getString(mTxKeyData.MGMT3_NM);
    }

    public String getMGMT4() throws JSONException{
        return getString(mTxKeyData.MGMT4);
    }

    public String getMGMT4_NM() throws JSONException{
        return getString(mTxKeyData.MGMT4_NM);
    }

    public String getMGMT5() throws JSONException{
        return getString(mTxKeyData.MGMT5);
    }

    public String getMGMT5_NM() throws JSONException{
        return getString(mTxKeyData.MGMT5_NM);
    }

    public TX_MYCD_MBL_R003_RES_BP_EMPL_REC getBP_EMPL_REC() throws Exception{
        return new TX_MYCD_MBL_R003_RES_BP_EMPL_REC(getRecord(mTxKeyData.BP_EMPL_REC));
    }

    public TX_MYCD_MBL_R003_RES_BP_CUST_REC getBP_CUST_REC() throws Exception{
        return new TX_MYCD_MBL_R003_RES_BP_CUST_REC(getRecord(mTxKeyData.BP_CUST_REC));
    }

    public String getMEST_BIZ_NO() throws JSONException{
        return getString(mTxKeyData.MEST_BIZ_NO);
    }

    public String getMEST_NO() throws JSONException{
        return getString(mTxKeyData.MEST_NO);
    }

    public String getMEST_REPR_NM() throws JSONException{
        return getString(mTxKeyData.MEST_REPR_NM);
    }

    public String getMEST_NM() throws JSONException{
        return getString(mTxKeyData.MEST_NM);
    }

    public String getMEST_TEL_NO() throws JSONException{
        return getString(mTxKeyData.MEST_TEL_NO);
    }

    public String getMEST_ZIP_CD() throws JSONException{
        return getString(mTxKeyData.MEST_ZIP_CD);
    }

    public String getMEST_ADDR() throws JSONException{
        return getString(mTxKeyData.MEST_ADDR);
    }

    public String getCARD_TPBZ_CD() throws JSONException{
        return getString(mTxKeyData.CARD_TPBZ_CD);
    }

    public String getCARD_TPBZ_NM() throws JSONException{
        return getString(mTxKeyData.CARD_TPBZ_NM);
    }

    public TX_MYCD_MBL_R003_RES_RCPT_USER_REC getRCPT_USER_REC() throws Exception{
        return new TX_MYCD_MBL_R003_RES_RCPT_USER_REC(getRecord(mTxKeyData.RCPT_USER_REC));
    }

    public TX_MYCD_MBL_R003_RES_REC_RCPT_IMG getRCPT_IMG_REC() throws Exception{
        return new TX_MYCD_MBL_R003_RES_REC_RCPT_IMG(getRecord(mTxKeyData.RCPT_IMG_REC));
    }
}
