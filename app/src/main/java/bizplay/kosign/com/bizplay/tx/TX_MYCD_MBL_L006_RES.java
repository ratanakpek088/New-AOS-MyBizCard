package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by Daravuth on 3/3/2017.
 */

public class TX_MYCD_MBL_L006_RES extends TxMessage {

    private TX_MYCD_MBL_L006_RES_DATA mTxKeyData;		// 전문명세 필드

    public TX_MYCD_MBL_L006_RES(Object obj) throws Exception{
        mTxKeyData = new TX_MYCD_MBL_L006_RES_DATA();
        super.initRecvMessage(obj);
    }

    /**
     * 전문 Data 필드 설정
     */
    private class TX_MYCD_MBL_L006_RES_DATA {
        private String BSNS_DSNC_ACVT_YN	=	"BSNS_DSNC_ACVT_YN";	//사업부문 사용여부
        private String BSNS_DSNC_REC	    =	"BSNS_DSNC_REC";		//사업부문 내역
        private String TRAN_KIND_ACVT_YN	=	"TRAN_KIND_ACVT_YN";	//용도코드 사용여부
        private String TRAN_KIND_REC	    =	"TRAN_KIND_REC";		//용도코드 내역
        private String MGMT1_ACVT_YN	    =	"MGMT1_ACVT_YN";		//항목1 사용여부
        private String MGMT1_NM	            =	"MGMT1_NM";		        //항목1 명칭
        private String MGMT1_METH	        =	"MGMT1_METH";		    //항목1 입력방식
        private String MGMT1_REC	        =	"MGMT1_REC";		    //항목1 내역
        private String MGMT2_ACVT_YN	    =	"MGMT2_ACVT_YN";		//항목2 사용여부
        private String MGMT2_NM	            =	"MGMT2_NM";		        //항목2 명칭
        private String MGMT2_METH	        =	"MGMT2_METH";		    //항목2 입력방식
        private String MGMT2_REC	        =	"MGMT2_REC";		    //항목2 내역
        private String MGMT3_ACVT_YN	    =	"MGMT3_ACVT_YN";		//항목3 사용여부
        private String MGMT3_NM	            =	"MGMT3_NM";		        //항목3 명칭
        private String MGMT3_METH	        =	"MGMT3_METH";		    //항목3 입력방식
        private String MGMT3_REC	        =	"MGMT3_REC";		    //항목3 내역
        private String MGMT4_ACVT_YN	    =	"MGMT4_ACVT_YN";		//항목4 사용여부
        private String MGMT4_NM	            =	"MGMT4_NM";		        //항목4 명칭
        private String MGMT4_METH	        =	"MGMT4_METH";		    //항목4 입력방식
        private String MGMT4_REC	        =	"MGMT4_REC";		    //항목4 내역
        private String MGMT5_ACVT_YN	    =	"MGMT5_ACVT_YN";		//항목5 사용여부
        private String MGMT5_NM	            =	"MGMT5_NM";		        //항목5 명칭
        private String MGMT5_METH	        =	"MGMT5_METH";		    //항목5 입력방식
        private String MGMT5_REC	        =	"MGMT5_REC";		    //항목5 내역
        private String EMPL_ACVT_YN	        =	"EMPL_ACVT_YN";		    //경비관리 직원 사용여부
        private String CUST_ACVT_YN	        =	"CUST_ACVT_YN";		    //경비관리 거래처 사용여부
    }

    public String getBSNS_DSNC_ACVT_YN()throws Exception {
        return getString(mTxKeyData.BSNS_DSNC_ACVT_YN);
    }

    public TX_MYCD_MBL_L006_RES_BSNS_DSNC_REC getBSNS_DSNC_REC()throws Exception {
        return new TX_MYCD_MBL_L006_RES_BSNS_DSNC_REC(getRecord(mTxKeyData.BSNS_DSNC_REC));
    }

    public String getTRAN_KIND_ACVT_YN()throws Exception {
        return getString(mTxKeyData.TRAN_KIND_ACVT_YN);
    }

    public TX_MYCD_MBL_L006_RES_TRAN_KIND_REC getTRAN_KIND_REC()throws Exception {
        return new TX_MYCD_MBL_L006_RES_TRAN_KIND_REC(getRecord(mTxKeyData.TRAN_KIND_REC));
    }

    public String getMGMT1_ACVT_YN()throws Exception {
        return getString(mTxKeyData.MGMT1_ACVT_YN);
    }

    public String getMGMT1_NM()throws Exception {
        return getString(mTxKeyData.MGMT1_NM);
    }

    public String getMGMT1_METH()throws Exception {
        return getString(mTxKeyData.MGMT1_METH);
    }

    public TX_MYCD_MBL_L006_RES_MGMT1_REC getMGMT1_REC()throws Exception {
        return new TX_MYCD_MBL_L006_RES_MGMT1_REC(getRecord(mTxKeyData.MGMT1_REC));
    }

    public String getMGMT2_ACVT_YN()throws Exception {
        return getString(mTxKeyData.MGMT2_ACVT_YN);
    }

    public String getMGMT2_NM()throws Exception {
        return getString(mTxKeyData.MGMT2_NM);
    }

    public String getMGMT2_METH()throws Exception {
        return getString(mTxKeyData.MGMT2_METH);
    }

    public TX_MYCD_MBL_L006_RES_MGMT2_REC getMGMT2_REC()throws Exception {
        return new TX_MYCD_MBL_L006_RES_MGMT2_REC(getRecord(mTxKeyData.MGMT2_REC));
    }

    public String getMGMT3_ACVT_YN()throws Exception {
        return getString(mTxKeyData.MGMT3_ACVT_YN);
    }

    public String getMGMT3_NM()throws Exception {
        return getString(mTxKeyData.MGMT3_NM);
    }

    public String getMGMT3_METH()throws Exception {
        return getString(mTxKeyData.MGMT3_METH);
    }

    public TX_MYCD_MBL_L006_RES_MGMT3_REC getMGMT3_REC()throws Exception {
        return new TX_MYCD_MBL_L006_RES_MGMT3_REC(getRecord(mTxKeyData.MGMT3_REC));
    }

    public String getMGMT4_ACVT_YN()throws Exception {
        return getString(mTxKeyData.MGMT4_ACVT_YN);
    }

    public String getMGMT4_NM()throws Exception {
        return getString(mTxKeyData.MGMT4_NM);
    }

    public String getMGMT4_METH()throws Exception {
        return getString(mTxKeyData.MGMT4_METH);
    }

    public TX_MYCD_MBL_L006_RES_MGMT4_REC getMGMT4_REC()throws Exception {
        return new TX_MYCD_MBL_L006_RES_MGMT4_REC(getRecord(mTxKeyData.MGMT4_REC));
    }

    public String getMGMT5_ACVT_YN()throws Exception {
        return getString(mTxKeyData.MGMT5_ACVT_YN);
    }

    public String getMGMT5_NM()throws Exception {
        return getString(mTxKeyData.MGMT5_NM);
    }

    public String getMGMT5_METH()throws Exception {
        return getString(mTxKeyData.MGMT5_METH);
    }

    public TX_MYCD_MBL_L006_RES_MGMT5_REC getMGMT5_REC()throws Exception {
        return new TX_MYCD_MBL_L006_RES_MGMT5_REC(getRecord(mTxKeyData.MGMT5_REC));
    }

    public String getEMPL_ACVT_YN()throws Exception {
        return getString(mTxKeyData.EMPL_ACVT_YN);
    }

    public String getCUST_ACVT_YN()throws Exception {
        return getString(mTxKeyData.CUST_ACVT_YN);
    }
}
