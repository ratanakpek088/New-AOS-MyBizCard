package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 *  Created by Huo Chhunleng on 15/Feb/2017.
 */
public class TX_MYCD_MBL_L006_RES_TRAN_KIND_REC extends TxMessage {

    private TX_MYCD_MBL_L006_RES_TRAN_KIND_REC_DATA mTxKeyData;		// 전문명세 필드

    public TX_MYCD_MBL_L006_RES_TRAN_KIND_REC(Object obj) throws Exception{
        mTxKeyData = new TX_MYCD_MBL_L006_RES_TRAN_KIND_REC_DATA();
        super.initRecvMessage(obj);

    }

    /**
     * 전문 Data 필드 설정
    */
    private class TX_MYCD_MBL_L006_RES_TRAN_KIND_REC_DATA {
        private String TRAN_KIND_CD	=	"TRAN_KIND_CD";		//용도코드
        private String TRAN_KIND_NM	=	"TRAN_KIND_NM";		//용도명
    }

    /**
     * 용도코드
     * @throws Exception
     */
    public String getTRAN_KIND_CD() throws JSONException,Exception{
        return getString(mTxKeyData.TRAN_KIND_CD);
    }

    /**
     * 용도명
     * @throws Exception
     */
    public String getTRAN_KIND_NM() throws JSONException,Exception{
        return getString(mTxKeyData.TRAN_KIND_NM);
    }

}
