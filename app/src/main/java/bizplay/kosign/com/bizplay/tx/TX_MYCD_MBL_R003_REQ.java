package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by Ratanakpek on 1/10/2017.
 */

public class TX_MYCD_MBL_R003_REQ extends TxMessage {
    //Not R003 update R008

    public static final String TXNO = "MYCD_MBL_R008";
    private TX_MYCD_MBL_R003_REQ_DATA mTxKeyData;

    public TX_MYCD_MBL_R003_REQ() throws Exception {
        mTxKeyData = new TX_MYCD_MBL_R003_REQ_DATA();
        super.initSendMessage();
    }

    public void setRCPT_TX_STS(String value) {
        mSendMessage.put(this.mTxKeyData.RCPT_TX_STS, value);
    }

    public void setCARD_CORP_CD(String value) {
        mSendMessage.put(this.mTxKeyData.CARD_CORP_CD, value);
    }

    public void setCARD_NO(String value) {
        mSendMessage.put(this.mTxKeyData.CARD_NO, value);
    }

    public void setAPV_DT(String value) {
        mSendMessage.put(this.mTxKeyData.APV_DT, value);
    }

    public void setAPV_SEQ(String value) {
        mSendMessage.put(this.mTxKeyData.APV_SEQ, value);
    }

    public void setAPV_TM(String value) {
        mSendMessage.put(this.mTxKeyData.APV_TM, value);
    }

    public void setAPV_NO(String value) {
        mSendMessage.put(this.mTxKeyData.APV_NO, value);
    }

    public void setMEST_BIZ_NO(String value) {
        mSendMessage.put(this.mTxKeyData.MEST_BIZ_NO, value);
    }

    public void setMEST_NO(String value) {
        mSendMessage.put(this.mTxKeyData.MEST_NO, value);
    }

    public void setAPV_AMT(String value) {
        mSendMessage.put(this.mTxKeyData.APV_AMT, value);
    }

    public void setAPV_CAN_YN(String value) {
        mSendMessage.put(this.mTxKeyData.APV_CAN_YN, value);
    }

    public void setAPV_CAN_DT(String value) {
        mSendMessage.put(this.mTxKeyData.APV_CAN_DT, value);
    }

    private class TX_MYCD_MBL_R003_REQ_DATA {
        private String RCPT_TX_STS = "RCPT_TX_STS";
        private String CARD_CORP_CD = "CARD_CORP_CD";
        private String CARD_NO = "CARD_NO";
        private String APV_DT = "APV_DT";
        private String APV_SEQ = "APV_SEQ";
        private String APV_TM = "APV_TM";
        private String APV_NO = "APV_NO";
        private String MEST_BIZ_NO = "MEST_BIZ_NO";
        private String MEST_NO = "MEST_NO";
        private String APV_AMT = "APV_AMT";
        private String APV_CAN_YN = "APV_CAN_YN";
        private String APV_CAN_DT = "APV_CAN_DT";

    }
}
