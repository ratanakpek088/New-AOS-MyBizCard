package bizplay.kosign.com.bizplay.extras;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.webcash.sws.picture.extras.Extras;

/**
 * Created by Torn Sokly on 1/11/2017.
 */

public class NotifExatra extends Extras{
    public NotifExatra(Context ctx) {
        super(ctx);
    }

    public NotifExatra(Activity act, Intent intent) {
        super(act, intent);
    }

    public NotifExatra(Activity act, Bundle bundle) {
        super(act, bundle);
    }

    private final String KEY_SND_DTM = "SND_DTM";
    private final String KEY_NOTI_SEQ = "NOTI_SEQ";


    public _Param Param = new _Param();

    public class _Param {

        public void setKEY_SND_DTM(String value) {
            setString(KEY_SND_DTM, value);
        }

        public String getKEY_SND_DTM() {
            return getString(KEY_SND_DTM);
        }

        public void setKEY_NOTI_SEQ(String value) {
            setString(KEY_NOTI_SEQ, value);
        }

        public String getKEY_NOTI_SEQ() {
            return getString(KEY_NOTI_SEQ);
        }


    }
}
