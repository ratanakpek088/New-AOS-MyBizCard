package bizplay.kosign.com.bizplay.popup;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SnackBar {

    public static void show(Context context, View containerLayout, String message){
        try{
            // Snackbar default duration is 3.5s
            Snackbar snackbar = Snackbar.make(containerLayout, "", Snackbar.LENGTH_LONG);
            // Get the Snackbar's layout view

            Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();

            /* Note default padding are (24, 0, 24, 0)
                Set default padding left and right to 0
            */
            layout.setPadding(0, 0, 0, 0);

            TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
            textView.setText(message);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,15);
            textView.setTextColor(Color.rgb(255, 255, 255));

            float sizeInDP = 18.5f;
            int marginInDp = (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, sizeInDP, context.getResources()
                            .getDisplayMetrics());

            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llp.setMargins(marginInDp, 0, marginInDp, 0); // llp.setMargins(left, top, right, bottom);*/
            textView.setLayoutParams(llp);

            layout.setBackgroundColor(Color.rgb(47, 48, 48));

            // Show the Snackbar
            snackbar.show();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
