package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by RatanakPek on 1/17/2017.
 */

public class TX_MYCD_MBL_C002_RES extends TxMessage {
    private TX_MYCD_MBL_C002_RES_DATA mTxKeyData; // 전문명세 필드

    public TX_MYCD_MBL_C002_RES(Object obj) throws Exception {
        mTxKeyData = new TX_MYCD_MBL_C002_RES_DATA();
        super.initRecvMessage(obj);
    }
    public String getRCPT_SEQNO() throws JSONException,Exception {
        return getString(mTxKeyData.RCPT_SEQNO);
    }
    private class TX_MYCD_MBL_C002_RES_DATA{
        private String RCPT_SEQNO="RCPT_SEQNO";
    }
}
