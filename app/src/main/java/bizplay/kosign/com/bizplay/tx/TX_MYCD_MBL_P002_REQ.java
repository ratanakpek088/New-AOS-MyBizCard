package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by User on 2016-12-28.
 */

public class TX_MYCD_MBL_P002_REQ extends TxMessage {

    public static final String TXNO = "MYCD_MBL_P002";
    private TX_PRVCD_MBL_P002_DATA mTxKeyData;

    public TX_MYCD_MBL_P002_REQ() throws Exception {
        this.mTxKeyData = new TX_PRVCD_MBL_P002_DATA();
        super.initSendMessage();
    }

    public void setUSER_ID(String value){
        mSendMessage.put(mTxKeyData.USER_ID, value);
    }

    private class TX_PRVCD_MBL_P002_DATA{
        private String USER_ID = "USER_ID";
    }
}
