package bizplay.kosign.com.bizplay.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;
import com.webcash.sws.log.DevLog;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import bizplay.kosign.com.bizplay.A007_1;
import bizplay.kosign.com.bizplay.A008_2Activity;
import bizplay.kosign.com.bizplay.R;
import bizplay.kosign.com.bizplay.common.ui.UIConstant;
import bizplay.kosign.com.bizplay.common.ui.util.ByteUtils;
import bizplay.kosign.com.bizplay.common.ui.util.FormatUtil;
import bizplay.kosign.com.bizplay.extras.Extra_Receipt;
import bizplay.kosign.com.bizplay.item.NoticeItem;
import bizplay.kosign.com.bizplay.item.Receipt;
import bizplay.kosign.com.bizplay.tabs.ReceiptFragment;

/**
 * Created by ViSAL MSi on 1/8/2017.
 */

public class NoticeAdapter extends BaseAdapter {

    private Context mContext;
    private List<NoticeItem> mNotList;

    public NoticeAdapter(Context context, List<NoticeItem> list) {
        this.mContext = context;
        this.mNotList = list;

    }

    @Override
    public int getCount() {
        return mNotList.size();
    }

    @Override
    public Object getItem(int i) {
        return mNotList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        NoticeViewHolder noticeViewHolder = null;
        // custom recycling listview for more faster whith ViewHolder
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.notice_item, viewGroup, false);
            noticeViewHolder = new NoticeViewHolder(v);
            v.setTag(noticeViewHolder);
        } else {
            noticeViewHolder = (NoticeViewHolder) v.getTag();

        }


        final NoticeItem noticeItem = mNotList.get(i);


        Log.e("rtk", noticeItem.getIMG_URL() + "Adapter");
        /*set image detial*/
        if (!noticeItem.getNOTI_ORG_IMG().equals("")) {
            // ImageLoader.getInstance().displayImage(noticeItem.getIMG_URL(), noticeViewHolder.iv_notice_detail, options, animateFirstListener);
            Glide.with(mContext)
                    .load(noticeItem.getNOTI_ORG_IMG())
                    .centerCrop()
                    .override(40, 40)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(noticeViewHolder.iv_noti_org_img);

        } else noticeViewHolder.iv_noti_org_img.setImageDrawable(null);

        if (!noticeItem.getIMG_URL().equals("")) {
            Glide.with(mContext)
                    .load(noticeItem.getIMG_URL())
                    .centerCrop()
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(noticeViewHolder.iv_notice_detail);
        } else noticeViewHolder.iv_notice_detail.setImageDrawable(null);
        /*set item title*/

        if (!noticeItem.getSEND_DTM().equals("")) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
            Log.e("rtk", "sdf date : " + sdf);
            Date mDate = null;
            try {
                mDate = sdf.parse(noticeItem.getSEND_DTM());
                noticeViewHolder.tv_date_3.setText(FormatUtil.getTimeString(mDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else noticeViewHolder.tv_date_3.setText("");

        if (noticeItem.getNOTI_GB().equals("1")) {
            Log.e("rtk", "NOTO");
            noticeViewHolder.iv_notice_detail.setVisibility(View.GONE);
            noticeViewHolder.btn_detail.setVisibility(View.VISIBLE);
            noticeViewHolder.tv_item_title_2.setVisibility(View.VISIBLE);


            if (noticeItem.getTITLE_NM().equals("") || noticeItem.getTITLE_NM().length() < 50) {
                noticeViewHolder.tv_item_title_1.setText(noticeItem.getTITLE_NM());
                noticeViewHolder.tv_item_title_2.setVisibility(View.GONE);
            } else {
                noticeViewHolder.tv_item_title_1.setText(subByteString(noticeItem.getTITLE_NM(), 0, 50, "UTF-8"));
                noticeViewHolder.tv_item_title_2.setText(subByteString(noticeItem.getTITLE_NM(), 50, 100, "UTF-8"));
            }
            noticeViewHolder.btn_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Just need object to store and for request from API R009
                    Extra_Receipt receipt = new Extra_Receipt(mContext);
                    receipt.Param.setKEY_CD_USER_NO(noticeItem.getCD_USER_NO());
                    receipt.Param.setKEY_SEND_DTM(noticeItem.getSEND_DTM());
                    receipt.Param.setKEY_NOTI_SEQ(noticeItem.getNOTI_SEQ());
                    receipt.Param.setAPI_KEY("MYCD_MBL_R009");
                    Log.e("rtk", "Intent : " + noticeItem.getCD_USER_NO() + "->" + noticeItem.getSEND_DTM() + "->" + noticeItem.getNOTI_SEQ());
                    Intent intent = new Intent(mContext, A007_1.class);
                    intent.putExtras(receipt.getBundle());
                    ((Activity) mContext).startActivityForResult(intent, UIConstant.ActivityRequestCode.REQEST_CODE6);

                }
            });

            noticeViewHolder.ll_list_notice.setOnClickListener(null);
        } else {
            noticeViewHolder.iv_notice_detail.setVisibility(View.VISIBLE);
            noticeViewHolder.btn_detail.setVisibility(View.GONE);
            noticeViewHolder.tv_item_title_2.setVisibility(View.GONE);

            noticeViewHolder.tv_item_title_1.setText(noticeItem.getTITLE_NM());
            noticeViewHolder.tv_item_title_2.setText("");
            noticeViewHolder.ll_list_notice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Extra_Receipt receipt = new Extra_Receipt(mContext);
                        receipt.Param.setKEY_CD_USER_NO(noticeItem.getCD_USER_NO());
                        receipt.Param.setKEY_SEND_DTM(noticeItem.getSEND_DTM());
                        receipt.Param.setKEY_NOTI_SEQ(noticeItem.getNOTI_SEQ());

                        Log.e("rtk", "Intent : " + noticeItem.getCD_USER_NO() + "->" + noticeItem.getSEND_DTM() + "->" + noticeItem.getNOTI_SEQ());

                        Intent intent = new Intent(mContext, A008_2Activity.class);
                        intent.putExtras(receipt.getBundle());
                        ((Activity) mContext).startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        return v;
    }

    public static String subByteString(String value, int start, int end, String charset) {
        try {
            byte[] valueInBytes = value.getBytes(charset);
            byte[] newByte = null;
            if (end > 50) {
                newByte = ByteUtils.subbytes(valueInBytes, start, valueInBytes.length);
            } else {
                newByte = ByteUtils.subbytes(valueInBytes, start, end);
            }
            value = new String(newByte, charset);
            DevLog.devLog("Original String Length>>>>", "" + valueInBytes.length);
            DevLog.devLog("Concatnate String Length>>>>", "" + newByte.length);
            DevLog.devLog("Concatnate String>>>>", "" + value);
            return value.trim();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }

    // ViewHolder Pattern
    class NoticeViewHolder {

        TextView tv_item_title_1, tv_item_title_2;
        TextView tv_date_3;
        ImageView iv_notice_detail, iv_noti_org_img;
        Button btn_detail;
        LinearLayout ll_list_notice;

        NoticeViewHolder(View v) {


            tv_item_title_1 = (TextView) v.findViewById(R.id.tv_item_title_1);
            tv_item_title_2 = (TextView) v.findViewById(R.id.tv_item_title_2);
            tv_date_3 = (TextView) v.findViewById(R.id.tv_date_3);

            iv_notice_detail = (ImageView) v.findViewById(R.id.iv_notice_detail);
            btn_detail = (Button) v.findViewById(R.id.btn_detail);

            iv_noti_org_img = (ImageView) v.findViewById(R.id.iv_noti_org_img);
            ll_list_notice = (LinearLayout) v.findViewById(R.id.ll_list_notice);


        }
    }
}
