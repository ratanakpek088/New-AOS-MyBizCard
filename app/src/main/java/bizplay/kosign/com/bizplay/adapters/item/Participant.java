package bizplay.kosign.com.bizplay.adapters.item;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Voneat-pc on 03/06/2016.
 */
public class Participant implements Parcelable{


//    String id;
//    String name;
//    String des;
    int ID;
    String CLPH_NO = "";
    String FLNM = "";
    String BSNN_NM = "";
    String DVSN_NM = "";
    String JBCL_NM = "";
    String RSPT_NM = "";
    String EML = "";
    String PRFL_PHTG = "";
    boolean IS_SELECTED;

    public Participant() {
    }


/*    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }*/

    public int getID(){return  ID;}

    public void setID(int value){ this.ID = value;}

    public String getCLPH_NO() {
        return CLPH_NO;
    }

    public void setCLPH_NO(String CLPH_NO) {
        this.CLPH_NO = CLPH_NO;
    }

    public String getFLNM() {
        return FLNM;
    }

    public void setFLNM(String FLNM) {
        this.FLNM = FLNM;
    }

    public String getBSNN_NM() {
        return BSNN_NM;
    }

    public void setBSNN_NM(String BSNN_NM) {
        this.BSNN_NM = BSNN_NM;
    }

    public String getDVSN_NM() {
        return DVSN_NM;
    }

    public void setDVSN_NM(String DVSN_NM) {
        this.DVSN_NM = DVSN_NM;
    }

    public String getJBCL_NM() {
        return JBCL_NM;
    }

    public void setJBCL_NM(String JBCL_NM) {
        this.JBCL_NM = JBCL_NM;
    }

    public String getRSPT_NM() {
        return RSPT_NM;
    }

    public void setRSPT_NM(String RSPT_NM) {
        this.RSPT_NM = RSPT_NM;
    }

    public String getEML() {
        return EML;
    }

    public void setEML(String EML) {
        this.EML = EML;
    }

    public String getPRFL_PHTG() {
        return PRFL_PHTG;
    }

    public void setPRFL_PHTG(String PRFL_PHTG) {
        this.PRFL_PHTG = PRFL_PHTG;
    }

    public boolean IS_SELECTED() {
        return IS_SELECTED;
    }

    public void setIS_SELECTED(boolean IS_SELECTED) {
        this.IS_SELECTED = IS_SELECTED;
    }


    /**
     * =============================================================================================
     * Parcel Functions
     * @param in
     * =============================================================================================
     */

    public Participant(Parcel in) {
        readFromParcel(in);
    }
    private void readFromParcel(Parcel in) {

        // We just need to read back each
        // field in the order that it was
        // written to the parcel
//        id = in.readString();
//        name = in.readString();
//        des = in.readString();
        ID = in.readInt();
        CLPH_NO = in.readString();
        FLNM = in.readString();
        BSNN_NM = in.readString();
        DVSN_NM = in.readString();
        JBCL_NM = in.readString();
        RSPT_NM = in.readString();
        EML = in.readString();
        PRFL_PHTG = in.readString();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(id);
//        dest.writeString(name);
//        dest.writeString(des);
        dest.writeInt(ID);
        dest.writeString(CLPH_NO);
        dest.writeString(FLNM);
        dest.writeString(BSNN_NM);
        dest.writeString(DVSN_NM);
        dest.writeString(JBCL_NM);
        dest.writeString(RSPT_NM);
        dest.writeString(EML);
        dest.writeString(PRFL_PHTG);
    }

    public static final Creator CREATOR =
            new Creator() {
                public Participant createFromParcel(Parcel in) {
                    return new Participant(in);
                }

                public Participant[] newArray(int size) {
                    return new Participant[size];
                }
            };
}
