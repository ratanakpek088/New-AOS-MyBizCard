package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by user on 1/13/2017.
 */

public class TX_MYCD_MBL_P005_REQ extends TxMessage{

    public static final String TXNO= "MYCD_MBL_P005";
    private TX_MYCD_MBL_P005_REQ_DATA mTxKeyData;

    public TX_MYCD_MBL_P005_REQ() throws Exception {
        mTxKeyData = new TX_MYCD_MBL_P005_REQ_DATA();
        super.initSendMessage();
    }

    public void setNOTI_STS(String value)throws JSONException { mSendMessage.put(mTxKeyData.NOTI_STS,value);}
    public void setLIMIT_STS(String value)throws JSONException { mSendMessage.put(mTxKeyData.LIMIT_STS,value);}
    public void setLIMIT_STRT_AMPM(String value)throws JSONException { mSendMessage.put(mTxKeyData.LIMIT_STRT_AMPM,value);}
    public void setLIMIT_STRT_TM(String value)throws JSONException { mSendMessage.put(mTxKeyData.LIMIT_STRT_TM,value);}
    public void setLIMIT_END_GB(String value)throws JSONException { mSendMessage.put(mTxKeyData.LIMIT_END_GB,value);}
    public void setLIMIT_END_AMPM(String value)throws JSONException { mSendMessage.put(mTxKeyData.LIMIT_END_AMPM,value);}
    public void setLIMIT_END_TM(String value)throws JSONException { mSendMessage.put(mTxKeyData.LIMIT_END_TM,value);}

    private class TX_MYCD_MBL_P005_REQ_DATA{
        private String NOTI_STS = "NOTI_STS";
        private String LIMIT_STS = "LIMIT_STS";
        private String LIMIT_STRT_AMPM = "LIMIT_STRT_AMPM";
        private String LIMIT_STRT_TM = "LIMIT_STRT_TM";
        private String LIMIT_END_GB = "LIMIT_END_GB";
        private String LIMIT_END_AMPM = "LIMIT_END_AMPM";
        private String LIMIT_END_TM = "LIMIT_END_TM";
    }
}
