package bizplay.kosign.com.bizplay;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.webcash.sws.pref.PreferenceDelegator;

import bizplay.kosign.com.bizplay.adapters.MainPagerAdapter;
import bizplay.kosign.com.bizplay.common.ui.CommonTitleBar;
import bizplay.kosign.com.bizplay.constant.Constants;
import bizplay.kosign.com.bizplay.item.Card;
import bizplay.kosign.com.bizplay.tabs.MyCardFragment;
import bizplay.kosign.com.bizplay.tabs.NoticeFragment;
import bizplay.kosign.com.bizplay.tabs.ReceiptFragment;

public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, View.OnClickListener {

    private MainPagerAdapter mMainPagerAdapter;
    private ViewPager mViewPager;
    private View tabHeader;
    private int cardPosition = 0;
    private OnReceiptReload mOnReceiptReload;
    private OnMyCardReload myCardReload;
    private OnNoticeReload mOnNoticeReload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mOnReceiptReload = (OnReceiptReload) ReceiptFragment.getInstance(); // get current object
        myCardReload = (OnMyCardReload) MyCardFragment.getInstance(); //get current object
        mOnNoticeReload = (OnNoticeReload) NoticeFragment.getInstance();

        tabHeader = findViewById(R.id.tab_header);
        mMainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager(), 4);
        mViewPager = (ViewPager) findViewById(R.id.vp_pager);
        mViewPager.setOffscreenPageLimit(4);
        mViewPager.setAdapter(mMainPagerAdapter);
        mViewPager.addOnPageChangeListener(this);

        RelativeLayout myCard = (RelativeLayout) findViewById(R.id.my_card_tab);
        RelativeLayout myReceipt = (RelativeLayout) findViewById(R.id.rcp_tab);
        RelativeLayout myNotice = (RelativeLayout) findViewById(R.id.noti_tab);
        RelativeLayout myMoreSetting = (RelativeLayout) findViewById(R.id.more_tab);

        myCard.setOnClickListener(this);
        myReceipt.setOnClickListener(this);
        myNotice.setOnClickListener(this);
        myMoreSetting.setOnClickListener(this);

        Log.d("SCRREN", getResources().getDisplayMetrics().density+ "");

//        mViewPager.setCurrentItem(0);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        CommonTitleBar.whenMainChangeTab(position);
        onHeaderChange(position);
        switch (position) {
            case 0:
                myCardReload.reloadCard(cardPosition);
                myCardReload.onChangeCardPosition(cardPosition);
                break;
            case 1:
                mOnReceiptReload.reloadSelectedCardLists(cardPosition);
                //           mOnReceiptReload.reloadReceiptListsOfCard(Card.mCardList.get(cardPosition));
                break;
            case 2:
                mOnNoticeReload.reloadNotice();
                break;
            case 3:
                break;
        }

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void onHeaderChange(int position) {
        switch (position) {
            case 0:
                ((ImageView) tabHeader.findViewById(R.id.my_card)).setImageResource(R.drawable.mn_credit_on_icon);
                ((ImageView) tabHeader.findViewById(R.id.bar_mycard)).setVisibility(View.VISIBLE);

                ((ImageView) tabHeader.findViewById(R.id.receipt)).setImageResource(R.drawable.mn_rcpt_icon);
                ((ImageView) tabHeader.findViewById(R.id.bar_rcpt)).setVisibility(View.GONE);

                ((ImageView) tabHeader.findViewById(R.id.notice)).setImageResource(R.drawable.mn_alarm_icon);
                ((ImageView) tabHeader.findViewById(R.id.bar_notice)).setVisibility(View.GONE);

                ((ImageView) tabHeader.findViewById(R.id.more)).setImageResource(R.drawable.mn_more_icon);
                ((ImageView) tabHeader.findViewById(R.id.bar_more)).setVisibility(View.GONE);
                break;
            case 1:
                ((ImageView) tabHeader.findViewById(R.id.receipt)).setImageResource(R.drawable.mn_rcpt_on_icon);
                ((ImageView) tabHeader.findViewById(R.id.bar_rcpt)).setVisibility(View.VISIBLE);

                ((ImageView) tabHeader.findViewById(R.id.my_card)).setImageResource(R.drawable.mn_credit_icon);
                ((ImageView) tabHeader.findViewById(R.id.bar_mycard)).setVisibility(View.GONE);

                ((ImageView) tabHeader.findViewById(R.id.notice)).setImageResource(R.drawable.mn_alarm_icon);
                ((ImageView) tabHeader.findViewById(R.id.bar_notice)).setVisibility(View.GONE);

                ((ImageView) tabHeader.findViewById(R.id.more)).setImageResource(R.drawable.mn_more_icon);
                ((ImageView) tabHeader.findViewById(R.id.bar_more)).setVisibility(View.GONE);
                break;
            case 2:
                ((ImageView) tabHeader.findViewById(R.id.notice)).setImageResource(R.drawable.mn_alarm_on_icon);
                ((ImageView) tabHeader.findViewById(R.id.bar_notice)).setVisibility(View.VISIBLE);

                ((ImageView) tabHeader.findViewById(R.id.my_card)).setImageResource(R.drawable.mn_credit_icon);
                ((ImageView) tabHeader.findViewById(R.id.bar_mycard)).setVisibility(View.GONE);

                ((ImageView) tabHeader.findViewById(R.id.receipt)).setImageResource(R.drawable.mn_rcpt_icon);
                ((ImageView) tabHeader.findViewById(R.id.bar_rcpt)).setVisibility(View.GONE);

                ((ImageView) tabHeader.findViewById(R.id.more)).setImageResource(R.drawable.mn_more_icon);
                ((ImageView) tabHeader.findViewById(R.id.bar_more)).setVisibility(View.GONE);
                break;
            case 3:
                ((ImageView) tabHeader.findViewById(R.id.more)).setImageResource(R.drawable.mn_more_on_icon);
                ((ImageView) tabHeader.findViewById(R.id.bar_more)).setVisibility(View.VISIBLE);

                ((ImageView) tabHeader.findViewById(R.id.my_card)).setImageResource(R.drawable.mn_credit_icon);
                ((ImageView) tabHeader.findViewById(R.id.bar_mycard)).setVisibility(View.GONE);

                ((ImageView) tabHeader.findViewById(R.id.receipt)).setImageResource(R.drawable.mn_rcpt_icon);
                ((ImageView) tabHeader.findViewById(R.id.bar_rcpt)).setVisibility(View.GONE);

                ((ImageView) tabHeader.findViewById(R.id.notice)).setImageResource(R.drawable.mn_alarm_icon);
                ((ImageView) tabHeader.findViewById(R.id.bar_notice)).setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.my_card_tab:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.rcp_tab:
                mViewPager.setCurrentItem(1);
                break;
            case R.id.noti_tab:
                mViewPager.setCurrentItem(2);
                break;
            case R.id.more_tab:
                mViewPager.setCurrentItem(3);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        PreferenceDelegator.getInstance(this).put(Constants.MycardInfo.CARD_NO, "");
        PreferenceDelegator.getInstance(this).put(Constants.MycardInfo.CARD_CORP_CD, "");

        PreferenceDelegator.getInstance(this).put(Constants.MycardInfo.CARD_NO, Card.mCardList.get(cardPosition).getCARD_NO());
        PreferenceDelegator.getInstance(this).put(Constants.MycardInfo.CARD_CORP_CD, Card.mCardList.get(cardPosition).getCARD_CORP_CD());

        String cardNo = PreferenceDelegator.getInstance(this).get(Constants.MycardInfo.CARD_NO);
        String cardCorpCD = PreferenceDelegator.getInstance(this).get(Constants.MycardInfo.CARD_CORP_CD);

        Log.d("CCCC: ", cardNo + " " + cardCorpCD);

//        System.exit(0);
    }

    public void catchPositionCard(int position) {
        cardPosition = position;
    }

    public void setNextPage() {
        mViewPager.setCurrentItem(1);
    }

    public void lookingForNewReceipt(boolean ifExist) {
        if (ifExist) {
            findViewById(R.id.receipt_new).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.receipt_new).setVisibility(View.GONE);
        }
    }

    public void lookingForNewNotice(boolean ifExist) {
        if (ifExist) {
            findViewById(R.id.notice_new).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.notice_new).setVisibility(View.GONE);
        }
    }

    public interface OnMyCardReload {
        public void onChangeCardPosition(int position);

        public void reloadCard(int position);
    }

    public interface OnReceiptReload {
        public void reloadSelectedCardLists(int position);

        public void reloadReceiptListsOfCard(Object object);
    }

    public interface OnNoticeReload {
        public void reloadNotice();
    }
}
