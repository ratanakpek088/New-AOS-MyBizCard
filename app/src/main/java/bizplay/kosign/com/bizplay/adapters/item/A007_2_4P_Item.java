package bizplay.kosign.com.bizplay.adapters.item;

/**
 * Created by User on 6/6/2016.
 */
public class A007_2_4P_Item {
    private String TRAN_KIND_CD;
    private String TRAN_KIND_NM;

    public String getTRAN_KIND_NM() {
        return TRAN_KIND_NM;
    }

    public void setTRAN_KIND_NM(String name) {
        this.TRAN_KIND_NM = name;
    }


    public String getTRAN_KIND_CD() {
        return TRAN_KIND_CD;
    }

    public void setTRAN_KIND_CD(String TRAN_KIND_CD) {
        this.TRAN_KIND_CD = TRAN_KIND_CD;
    }
}
