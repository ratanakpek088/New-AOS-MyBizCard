package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 *  Created by Ratanak on 22/Feb/2017.
 */

public class TX_MYCD_MBL_U003_RES extends TxMessage {

    private TX_MYCD_MBL_U003_RES_DATA mTxKeyData;		// 전문명세 필드

    public TX_MYCD_MBL_U003_RES(Object obj) throws Exception{
        mTxKeyData = new TX_MYCD_MBL_U003_RES_DATA();
        super.initRecvMessage(obj);
    }

    /**
     * 전문 Data 필드 설정
     */
    private class TX_MYCD_MBL_U003_RES_DATA {
        private String RCPT_SEQNO	=	"RCPT_SEQNO";		//영수증등록 일련번호
    }

    /**
     *
     * @return
     * @throws Exception
     */
    public String getRCPT_SEQNO()throws Exception {
        return getString(mTxKeyData.RCPT_SEQNO);
    }

}
