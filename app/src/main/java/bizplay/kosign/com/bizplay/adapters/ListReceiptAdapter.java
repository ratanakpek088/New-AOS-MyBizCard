package bizplay.kosign.com.bizplay.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import bizplay.kosign.com.bizplay.A007_1;
import bizplay.kosign.com.bizplay.R;
import bizplay.kosign.com.bizplay.item.Card;
import bizplay.kosign.com.bizplay.item.Receipt;
import bizplay.kosign.com.bizplay.tabs.ReceiptFragment;

/**
 * Created by ViSAL MSi on 1/9/2017.
 */

public class ListReceiptAdapter extends BaseAdapter {

    private Context mContext;
    private Card currentSelectedCard;
    private Receipt mReceipts;
    private Resources r;

    public ListReceiptAdapter(Context context, Card currentCard, Receipt receipts) {
        this.mContext = context;
        this.r = mContext.getResources();
        this.currentSelectedCard = currentCard;
        this.mReceipts = receipts;
    }

    @Override
    public int getCount() {
        return mReceipts.getReceiptRecs().size();
    }

    @Override
    public Object getItem(int i) {
        return mReceipts.getReceiptRecs().get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        final LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemReceipt = inflater.inflate(R.layout.item_of_receipt, viewGroup, false);


        TextView aptAmt = (TextView) itemReceipt.findViewById(R.id.txt_APV_AMT_rcpt);
        TextView apvAmtCurrentcy = (TextView) itemReceipt.findViewById(R.id.txt_APV_AMT_currency);
        TextView mestNMBold = (TextView) itemReceipt.findViewById(R.id.txt_MEST_NM_rcpt_bold);
        TextView mestNMNormal = (TextView) itemReceipt.findViewById(R.id.txt_MEST_NM_rcpt_normal);
        TextView apvDT = (TextView) itemReceipt.findViewById(R.id.txt_APV_DT_rcpt);
        TextView cardOrgNMAndCardNo = (TextView) itemReceipt.findViewById(R.id.txt_CARD_ORG_NM_N_CARD_NO_rcpt);
        TextView tranKindNM = (TextView) itemReceipt.findViewById(R.id.txt_TRAN_KIND_NM_rcpt);
        TextView cancelOrIndividual = (TextView) itemReceipt.findViewById(R.id.txt_cancellation_or_individual);
        TextView txt_APV_CAN_YN = (TextView) itemReceipt.findViewById(R.id.txt_APV_CAN_YN);

        LinearLayout wrapperAppAmt = (LinearLayout) itemReceipt.findViewById(R.id.wrapper_APV_AMT);
        ImageView strokeAPV_AMT = (ImageView) itemReceipt.findViewById(R.id.stroke_payment);

        aptAmt.setText(new DecimalFormat("#,###").format(Double.parseDouble(mReceipts.getReceiptRecs().get(i).getAPV_AMT())));
        mestNMBold.setText(mReceipts.getReceiptRecs().get(i).getMEST_NM());
        mestNMNormal.setText(mReceipts.getReceiptRecs().get(i).getMEST_NM());

        String string = currentSelectedCard.getCARD_NO();
        String last4 = "";
        if (string.length() == 16) { // if card has 16 digit
            last4 = string.substring(string.length() - 4);
        } else if (string.length() == 15) { // if card has 15 digit
            last4 = string.substring(string.length() - 3);
        } else if (string.length() == 13) { // if card has 13 digit
            last4 = string.substring(string.length() - 2);
        }

        cardOrgNMAndCardNo.setText(currentSelectedCard.getCARD_ORG_NM() + last4);

        if (mReceipts.getReceiptRecs().get(i).getAPV_DT().length() == 8) {
            apvDT.setText(mReceipts.getReceiptRecs().get(i).getAPV_DT().subSequence(4, 6) + "월 " + mReceipts.getReceiptRecs().get(i).getAPV_DT().substring(6, 8) + "일");
        }

        if (mReceipts.getReceiptRecs().get(i).getTRAN_KIND_NM().equals("") || mReceipts.getReceiptRecs().get(i).getTRAN_KIND_NM().equals("null")) {
            tranKindNM.setVisibility(View.GONE);
        } else {
            tranKindNM.setVisibility(View.VISIBLE);
            tranKindNM.setText(mReceipts.getReceiptRecs().get(i).getTRAN_KIND_NM());
        }

        switch (mReceipts.getReceiptRecs().get(i).getRCPT_TX_STS()) {
            case "0":
                cardOrgNMAndCardNo.setTextColor(Color.parseColor("#1565C0"));
                itemReceipt.setBackgroundResource(R.drawable.item_receipt_selector_new);
                mestNMBold.setVisibility(View.VISIBLE);
                break;
            case "1":
                itemReceipt.setBackgroundResource(R.drawable.item_receipt_selector_old);
                mestNMNormal.setVisibility(View.VISIBLE);
                mestNMNormal.setTextColor(Color.parseColor("#a4a4a4"));
                aptAmt.setTextColor(Color.parseColor("#a4a4a4"));
                apvAmtCurrentcy.setTextColor(Color.parseColor("#a4a4a4"));


                break;
            case "2":
                itemReceipt.setBackgroundResource(R.drawable.item_receipt_selector_old);
                mestNMBold.setVisibility(View.VISIBLE);
                aptAmt.setPaintFlags(mestNMBold.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                apvAmtCurrentcy.setPaintFlags(mestNMBold.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                aptAmt.setTextColor(Color.parseColor("#a4a4a4"));
                apvAmtCurrentcy.setTextColor(Color.parseColor("#a4a4a4"));
                tranKindNM.setPaintFlags(tranKindNM.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                cancelOrIndividual.setVisibility(View.VISIBLE);
                break;
            case "3":
                itemReceipt.setBackgroundResource(R.drawable.item_receipt_selector_old);
                cancelOrIndividual.setVisibility(View.VISIBLE);
                cancelOrIndividual.setText("개인");
                mestNMNormal.setVisibility(View.VISIBLE);
                mestNMNormal.setTextColor(Color.parseColor("#a4a4a4"));
                cancelOrIndividual.setBackgroundResource(R.drawable.bg_cancel);
                cancelOrIndividual.setTextColor(Color.parseColor("#9E9E9E"));
                break;
            case "Y":
                break;
        }

        if (mReceipts.getReceiptRecs().get(i).getRCPT_RFS_STS().equalsIgnoreCase("Y")) {
            ImageView stroke = (ImageView) itemReceipt.findViewById(R.id.img_TRAN_KIND_NM_rcpt_stroke);
            stroke.setVisibility(View.VISIBLE);
            ImageView image_reject = (ImageView) itemReceipt.findViewById(R.id.img_receipt_rejected);
            image_reject.setVisibility(View.VISIBLE);
        }

        if (mReceipts.getReceiptRecs().get(i).getAPV_CAN_YN().equalsIgnoreCase("B")) {
            aptAmt.setTextColor(Color.parseColor("#E53935"));
            aptAmt.setText("-" + aptAmt.getText());
            apvAmtCurrentcy.setTextColor(Color.parseColor("#E53935"));
            tranKindNM.setVisibility(View.GONE);
            txt_APV_CAN_YN.setVisibility(View.VISIBLE);
        }
        final int temp = i;

        /*
        to get data of select item from this adapter to A007_1
        1. get position from intent extra
        2. then get it by:
            ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(position);
         */
        itemReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReceiptFragment.getInstance().setSelectedReceiptToDetail(temp, mReceipts.getReceiptRecs().get(temp).getRCPT_TX_STS());

            }
        });

        return itemReceipt;
    }


}
