package bizplay.kosign.com.bizplay.permission;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;



import java.util.List;

import bizplay.kosign.com.bizplay.R;
import bizplay.kosign.com.bizplay.popup.OnPopupWindowListener;

/*
 * Created by chanyouvita on 1/18/16.
 */
public class PermissionPopup extends Dialog {
    private OnPopupWindowListener listener;

    public PermissionPopup(Context context, List<String> permissions,String message){
        super(context);

        try {
            final View mLayout = View.inflate(context, R.layout.permission_dialog, null);
            super.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            super.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            super.setCanceledOnTouchOutside(false);
            super.setCancelable(false);
            super.setContentView(mLayout);
            super.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            LinearLayout cancel = (LinearLayout) mLayout.findViewById(R.id.btnPCancel);
            LinearLayout setting = (LinearLayout) mLayout.findViewById(R.id.btnPSetting);

            // message permission
            TextView tvMessage = (TextView) mLayout.findViewById(R.id.tv_message);
            tvMessage.setText(message);

            // permissions was denied
            for(String str: permissions){
                switch (str) {
                    case Manifest.permission.READ_PHONE_STATE:
                        mLayout.findViewById(R.id.ll_phone).setVisibility(View.VISIBLE);
                        break;
                    case Manifest.permission.READ_CONTACTS:
                        mLayout.findViewById(R.id.ll_contact).setVisibility(View.VISIBLE);
                        break;
                    case Manifest.permission.RECEIVE_SMS:
                        mLayout.findViewById(R.id.ll_sms).setVisibility(View.VISIBLE);
                        break;
                    case Manifest.permission.READ_EXTERNAL_STORAGE:
                        mLayout.findViewById(R.id.ll_storage).setVisibility(View.VISIBLE);
                        break;
                    case Manifest.permission.CAMERA:
                        mLayout.findViewById(R.id.ll_camera).setVisibility(View.VISIBLE);
                        break;
                }
            }

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onPopupClose();
                }
            });

            setting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onPopupPressed();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setOnPopupListener (OnPopupWindowListener listener){
        this.listener = listener;
    }
}
