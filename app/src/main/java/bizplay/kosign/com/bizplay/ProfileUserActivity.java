package bizplay.kosign.com.bizplay;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.webcash.sws.pref.MemoryPreferenceDelegator;
import com.webcash.sws.pref.PreferenceDelegator;

import org.json.JSONException;

import bizplay.kosign.com.bizplay.constant.Constants;
import bizplay.kosign.com.bizplay.tran.ComTran;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_P002_REQ;

public class ProfileUserActivity extends AppCompatActivity implements ComTran.OnComTranListener, View.OnClickListener{

    private TextView txtUserID;
    private ComTran mComTran;
    private Button btn_logout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_user);

        mComTran = new ComTran(this, this);

        txtUserID = (TextView) findViewById(R.id.userID);
        txtUserID.setText(MemoryPreferenceDelegator.getInstance().get(Constants.LoginInfo.USER_NM));
        btn_logout = (Button) findViewById(R.id.btn_logout);
        btn_logout.setOnClickListener(this);
    }

//    public void onLogout(View view) {
//        try{
//            TX_MYCD_MBL_P002_REQ req = new TX_MYCD_MBL_P002_REQ();
//            Toast.makeText(this, txtUserID.getText().toString(), Toast.LENGTH_SHORT).show();
//            req.setUSER_ID(PreferenceDelegator.getInstance(this).get(Constants.LoginInfo.USER_ID));
//
//            mComTran.requestData(TX_MYCD_MBL_P002_REQ.TXNO, req.getSendMessage(), false);
//        }catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    public void onTranResponse(String tranCode, Object object) throws Exception {
        switch (tranCode){
            case TX_MYCD_MBL_P002_REQ.TXNO:
                PreferenceDelegator.getInstance(this).put(Constants.LoginInfo.USER_PWD, "");

                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_logout:
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(this);
                builder.setTitle(getString(R.string.alert_info));
                builder.setMessage("로그아웃을 하게 되면 자동 로그인도 해제 됩니다. 정말로 로그아웃하시겠습니까?");
                builder.setCancelable(false);
                builder.setPositiveButton("확인",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                TX_MYCD_MBL_P002_REQ req = null;
                                try {
                                    req = new TX_MYCD_MBL_P002_REQ();
                                    req.setUSER_ID(PreferenceDelegator.getInstance(getApplicationContext()).get(Constants.LoginInfo.USER_ID));
                                    mComTran.requestData(TX_MYCD_MBL_P002_REQ.TXNO, req.getSendMessage(), false);//Log out
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                builder.setNegativeButton("취소",
                        new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).show();
                break;

        }
    }
}
