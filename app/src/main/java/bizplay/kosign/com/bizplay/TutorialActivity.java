package bizplay.kosign.com.bizplay;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import bizplay.kosign.com.bizplay.adapters.TutorialPagerAdapter;

public class TutorialActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private ViewPager mViewPagerTutorial;
    private TutorialPagerAdapter mTutorialPagerAdapter;
    private LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        mViewPagerTutorial = (ViewPager) findViewById(R.id.vp_turtorial);
        mViewPagerTutorial.addOnPageChangeListener(this);
        mTutorialPagerAdapter = new TutorialPagerAdapter(getSupportFragmentManager());
        mViewPagerTutorial.setAdapter(mTutorialPagerAdapter);
        pager_indicator = (LinearLayout) findViewById(R.id.viewPagerCountDots);

        setUiPageViewController();
    }

    private void setUiPageViewController() {

        dotsCount = mTutorialPagerAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.non_selecteditem_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(0, 0, 9, 0);

            pager_indicator.addView(dots[i], params);

            if (i == 4) {
                break;
            }
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.non_selecteditem_dot));
        }

        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void onGotoLogin(View view) {
        startActivity(new Intent(TutorialActivity.this, LoginActivity.class));
        finish();
    }
}
