package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by korb on 7/20/2016.
 */

public class TX_MYCD_MBL_UPLOAD_RES extends TxMessage {

    private String FILE_IDNT_ID = "FILE_IDNT_ID";
    private String FILE_NM      = "FILE_NM";
    private String FILE_SIZE    = "FILE_SIZE";
    private String IMG_PATH     = "IMG_PATH";

    public TX_MYCD_MBL_UPLOAD_RES(Object obj) throws Exception{
        super.initRecvMessage(obj);
    }

    public String getFILE_IDNT_ID() throws JSONException {
        return getString(FILE_IDNT_ID);
    }

    public String getFILE_NM() throws JSONException{
        return getString(FILE_NM);
    }

    public String getFILE_SIZE() throws JSONException{
        return getString(FILE_SIZE);
    }

    public String getIMG_PATH() throws JSONException{
        return getString(IMG_PATH);
    }
}
