package bizplay.kosign.com.bizplay;

import android.app.TimePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.nostra13.universalimageloader.utils.L;
import com.webcash.sws.log.DevLog;
import com.webcash.sws.pref.PreferenceDelegator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import bizplay.kosign.com.bizplay.constant.Constants;
import bizplay.kosign.com.bizplay.tran.ComTran;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_P005_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_R006_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_R006_RES;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener, ComTran.OnComTranListener{

    ToggleButton tg_alarm,  tg_notification, tg_time_out;
    TextView tv_start_time, tv_end_time,tv_next_day;
    LinearLayout ll_start_time, ll_end_time;
    private ComTran mComTran;
    private SimpleDateFormat fmTime;
    private Calendar mCurrentStartTime;
    private Calendar mCurrentEndTime;
    TX_MYCD_MBL_R006_RES resData = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        mComTran = new ComTran(this,this);

        tg_alarm = (ToggleButton) findViewById(R.id.tg_alarm);
        tg_notification = (ToggleButton) findViewById(R.id.tg_notification);
        tg_time_out = (ToggleButton) findViewById(R.id.tg_time_out);

        tv_start_time = (TextView) findViewById(R.id.tv_start_time);
        tv_end_time = (TextView) findViewById(R.id.tv_end_time);

        ll_start_time = (LinearLayout) findViewById(R.id.ll_start_time);
        ll_end_time = (LinearLayout) findViewById(R.id.ll_end_time);

        tv_start_time = (TextView) findViewById(R.id.tv_start_time);
        tv_end_time = (TextView) findViewById(R.id.tv_end_time);
        tv_next_day = (TextView) findViewById(R.id.tv_next_day);




        ll_start_time.setOnClickListener(this);
        ll_end_time.setOnClickListener(this);

        tg_alarm.setOnClickListener(this);
        tg_notification.setOnClickListener(this);
        tg_time_out.setOnClickListener(this);

        fmTime = new SimpleDateFormat("a hh:mm");
        mCurrentStartTime = Calendar.getInstance();
        mCurrentEndTime = Calendar.getInstance();
        requestPushNotifiaction();

    }

    @Override
    public void onClick(View v) {
        try{
            switch (v.getId()){
                case R.id.ll_start_time:
                    setupTimePicker(tv_start_time, mCurrentStartTime);
                    break;
                case R.id.ll_end_time:
                    setupTimePicker(tv_end_time, mCurrentEndTime);
                    break;
                case R.id.tg_alarm:
                    if(!tg_notification.isChecked()&& !tg_time_out.isChecked()){
                        tg_notification.setChecked(true);
                        tg_time_out.setChecked(true);
                        ll_start_time.setVisibility(View.VISIBLE);
                        ll_end_time.setVisibility(View.VISIBLE);
                        findViewById(R.id.line_timeout).setVisibility(View.VISIBLE);
                        findViewById(R.id.last_line).setVisibility(View.VISIBLE);
                        onTranRequest(TX_MYCD_MBL_P005_REQ.TXNO);

                    }else {
                        tg_notification.setChecked(false);
                        tg_time_out.setChecked(false);
                        ll_start_time.setVisibility(View.GONE);
                        ll_end_time.setVisibility(View.GONE);
                        findViewById(R.id.line_timeout).setVisibility(View.GONE);
                        findViewById(R.id.last_line).setVisibility(View.GONE);
                        onTranRequest(TX_MYCD_MBL_P005_REQ.TXNO);
                    }
                    break;
                case R.id.tg_notification:
                    pushAlarmControl();
                    onTranRequest(TX_MYCD_MBL_P005_REQ.TXNO);
                    break;
                case R.id.tg_time_out:

                    pushAlarmControl();
                    onTranRequest(TX_MYCD_MBL_P005_REQ.TXNO);
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setupTimePicker(final TextView textView, final Calendar calendar) {
        try {
            TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    try {
                        DevLog.devLog("KERB>>>>","Before Set: "+calendar.getTime());
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);
                        DevLog.devLog("KERB>>>>","After Set: "+calendar.getTime());
                        DevLog.devLog("KERB>>>>","Res Before Set: "+resData.getMessageToString());
                        resData.setLIMIT_END_GB(mCurrentEndTime.getTimeInMillis() <= mCurrentStartTime.getTimeInMillis() ? "1" : "0");
                        if (resData.getLIMIT_END_GB().equals("1")) {
                            tv_next_day.setText("익일 ");
                        } else {
                            tv_next_day.setText("금일 ");
                        }
                        if (calendar == mCurrentStartTime) {
                            resData.setLIMIT_STRT_AMPM(getTimeAMPMInKorean(calendar).contains("오전") ? "0" : "1");
                            resData.setLIMIT_STRT_TM(getHourNoDelimiter(calendar));
                        } else {
                            resData.setLIMIT_END_AMPM(getTimeAMPMInKorean(calendar).contains("오전") ? "0" : "1");
                            resData.setLIMIT_END_TM(getHourNoDelimiter(calendar));
                        }
                        textView.setText(getTimeAMPMInKorean(calendar));
                        DevLog.devLog("KERB>>>>","Res After Set: "+resData.getMessageToString());
                        onTranRequest(TX_MYCD_MBL_P005_REQ.TXNO); //Request Notification setting to server
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            };
            TimePickerDialog mTimePicker = new TimePickerDialog(this, mTimeSetListener, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);
            mTimePicker.setCanceledOnTouchOutside(false);
            mTimePicker.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isUncheckedAll(){
        if(!tg_notification.isChecked() && !tg_time_out.isChecked()){
            Log.d("UNCHECK ALL", "TOGGLE");
            PreferenceDelegator.getInstance(this).put(Constants.NotificationSetting.IS_MAIN_ON,"N");
            return true;
        }else if(tg_notification.isChecked() && tg_time_out.isChecked()){
            Log.d("SELECT ALL", "TOGGLE");
            PreferenceDelegator.getInstance(this).put(Constants.NotificationSetting.IS_MAIN_ON,"Y");
            return false;
        }else if(tg_notification.isChecked() || tg_time_out.isChecked()){
            Log.d("SOME SELECTED","TOGGLE");
            PreferenceDelegator.getInstance(this).put(Constants.NotificationSetting.IS_MAIN_ON,"Y");
            return false;
        }
        return false;
    }

    private void pushAlarmControl(){
        if(isUncheckedAll()){
            tg_alarm.setChecked(false);
            tg_notification.setChecked(false);
            tg_time_out.setChecked(false);
            ll_start_time.setVisibility(View.GONE);
            ll_end_time.setVisibility(View.GONE);
            findViewById(R.id.last_line).setVisibility(View.GONE);
            findViewById(R.id.line_timeout).setVisibility(View.GONE);
        }else {
            tg_alarm.setChecked(true);
            if(tg_time_out.isChecked()){
                ll_start_time.setVisibility(View.VISIBLE);
                ll_end_time.setVisibility(View.VISIBLE);
                findViewById(R.id.line_timeout).setVisibility(View.VISIBLE);
                findViewById(R.id.last_line).setVisibility(View.VISIBLE);
            }else {
                ll_start_time.setVisibility(View.GONE);
                ll_end_time.setVisibility(View.GONE);
                findViewById(R.id.line_timeout).setVisibility(View.GONE);
                findViewById(R.id.last_line).setVisibility(View.GONE);
            }
        }
    }

    private String getTimeAMPMInKorean(Calendar cal){
        String time = new SimpleDateFormat("hh:mm").format(cal.getTime());
        int ampm = cal.get(Calendar.AM_PM);
        DevLog.devLog("KERB>>>>","TIME: "+time+", AM_PM: "+ampm);
        String formatted_tm = "";
        if (ampm == 0) formatted_tm = "오전 "+time;
        else formatted_tm = "오후 "+time;
        return formatted_tm;
    }

    private void requestPushNotifiaction(){
        try{
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            String sample = dateFormat.format(date);
            String result = sample.replace("/","");
            DevLog.devLog("msg", result);

            TX_MYCD_MBL_R006_REQ req = new TX_MYCD_MBL_R006_REQ();
            req.setINQ_DT(result);

            mComTran.requestData(TX_MYCD_MBL_R006_REQ.TXNO, req.getSendMessage(), true );
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getHourNoDelimiter(Calendar cal){
        return new SimpleDateFormat("hhmm").format(cal.getTime());
    }

    private void setTimeToCalender(Calendar cal, String time/*, String ampm*/){
        if (time.length()==4) {
            String hour = time.substring(0, 2);
            String minute = time.substring(2, 4);
            if(hour.equals("12")){
                hour = "0";
            }
            DevLog.devLog("KERB>>>>","Update Time: "+hour+", Min: "+minute/*+", AMPM: "+ampm*/);
            cal.set(Calendar.HOUR, Integer.parseInt(hour));
            cal.set(Calendar.MINUTE, Integer.parseInt(minute));
        }
    }

    private void onTranRequest(String tranCD){
        try{
            if (tranCD.equals(TX_MYCD_MBL_P005_REQ.TXNO)){
                TX_MYCD_MBL_P005_REQ reqMSG = new TX_MYCD_MBL_P005_REQ();
                if (resData == null)return;
                if(tg_notification.isChecked()) {
                    reqMSG.setNOTI_STS("Y");
                } else  reqMSG.setNOTI_STS("N");
//                if(tg_notification.isChecked()) reqMSG.setHANDO_STS("Y"); else  reqMSG.setHANDO_STS("N");
                if(tg_time_out.isChecked()) {
                    reqMSG.setLIMIT_STS("Y");
                    tg_alarm.setChecked(true);
                } else  reqMSG.setLIMIT_STS("N");

                reqMSG.setLIMIT_END_GB(resData.getLIMIT_END_GB());
                reqMSG.setLIMIT_STRT_AMPM(resData.getLIMIT_STRT_AMPM());
                reqMSG.setLIMIT_STRT_TM(resData.getLIMIT_STRT_TM());
                reqMSG.setLIMIT_END_AMPM(resData.getLIMIT_END_AMPM());
                reqMSG.setLIMIT_END_TM(resData.getLIMIT_END_TM());
                mComTran.requestData(TX_MYCD_MBL_P005_REQ.TXNO, reqMSG.getSendMessage(), true);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTranResponse(String tranCode, Object object) throws Exception {
        try{
            if(tranCode.equals(TX_MYCD_MBL_R006_REQ.TXNO)) {
                resData = new TX_MYCD_MBL_R006_RES(object);
                JSONArray resDataArray = (JSONArray)object;
                JSONObject jsonObj = resDataArray.getJSONObject(0);
                DevLog.devLog("KERB>>>>","JSONDATA1: "+resDataArray.toString());
                DevLog.devLog("KERB>>>>","JSONDATA2: "+jsonObj.toString());

                if (jsonObj.isNull("NOTI_STS") || TextUtils.isEmpty(resData.getNOTI_STS()))resData.setNOTI_STS("N");

                if (jsonObj.isNull("LIMIT_STS") || TextUtils.isEmpty(resData.getLIMIT_STS()))resData.setLIMIT_STS("N");

                if (jsonObj.isNull("LIMIT_STRT_AMPM") || TextUtils.isEmpty(resData.getLIMIT_STRT_AMPM())){
                    String value = getTimeAMPMInKorean(mCurrentStartTime).contains("오전")? "0" : "1";
                    resData.setLIMIT_STRT_AMPM(value);
                }

                if (jsonObj.isNull("LIMIT_STRT_TM") || TextUtils.isEmpty(resData.getLIMIT_STRT_TM())){
                    String value = getHourNoDelimiter(mCurrentStartTime);
                    resData.setLIMIT_STRT_TM(value);
                }

                if (jsonObj.isNull("LIMIT_END_GB") || TextUtils.isEmpty(resData.getLIMIT_END_GB())){
                    String value = mCurrentEndTime.getTimeInMillis() <= mCurrentStartTime.getTimeInMillis() ? "1" : "0";
                    resData.setLIMIT_END_GB(value);
                }

                if (jsonObj.isNull("LIMIT_END_AMPM") || TextUtils.isEmpty(resData.getLIMIT_END_AMPM())){
                    String value = getTimeAMPMInKorean(mCurrentEndTime).contains("오전")? "0" : "1";
                    resData.setLIMIT_END_AMPM(value);
                }

                if (jsonObj.isNull("LIMIT_END_TM") || TextUtils.isEmpty(resData.getLIMIT_END_TM())){
                    String value = getHourNoDelimiter(mCurrentEndTime);
                    resData.setLIMIT_END_TM(value);
                }

                DevLog.devLog("KERB>>>>","NullEmptyCheck: "+resData.getMessageToString());

                if (resData.getNOTI_STS().equals("Y")){
                    tg_notification.setChecked(true);
                    tg_alarm.setChecked(true);
                }
                else{
                    tg_notification.setChecked(false);
                    pushAlarmControl();
                }


                if (resData.getLIMIT_STS().equals("Y")){
                    tg_time_out.setChecked(true);
                    ll_start_time.setVisibility(View.VISIBLE);
                    ll_end_time.setVisibility(View.VISIBLE);
                    tg_alarm.setChecked(true);
                }
                else {
                    tg_time_out.setChecked(false);
                    ll_start_time.setVisibility(View.GONE);
                    ll_end_time.setVisibility(View.GONE);
                    pushAlarmControl();
                }

                DevLog.devLog("KERB>>>>","Before Start Time: "+mCurrentStartTime.getTime());
                DevLog.devLog("KERB>>>>","Before End Time: "+mCurrentEndTime.getTime());

                setTimeToCalender(mCurrentStartTime,resData.getLIMIT_STRT_TM());
                setTimeToCalender(mCurrentEndTime,resData.getLIMIT_END_TM());

                if (resData.getLIMIT_STRT_AMPM().equals("0"))
                    mCurrentStartTime.set(Calendar.AM_PM, 0);
                else
                    mCurrentStartTime.set(Calendar.AM_PM, 1);

                if (resData.getLIMIT_END_AMPM().equals("0"))
                    mCurrentEndTime.set(Calendar.AM_PM, 0);
                else
                    mCurrentEndTime.set(Calendar.AM_PM, 1);

                resData.setLIMIT_END_GB(mCurrentEndTime.getTimeInMillis() <= mCurrentStartTime.getTimeInMillis() ? "1" : "0");

                if (resData.getLIMIT_END_GB().equals("1")){
                    tv_next_day.setText("익일 ");
                }else{
                    tv_next_day.setText("금일 ");
                }
                DevLog.devLog("KERB>>>>","Next day text: "+tv_next_day.getText());
                tv_start_time.setText(getTimeAMPMInKorean(mCurrentStartTime));
                tv_end_time.setText(getTimeAMPMInKorean(mCurrentEndTime));

                DevLog.devLog("KERB>>>>","After Start Time: "+mCurrentStartTime.getTime());
                DevLog.devLog("KERB>>>>","After Start Time1: "+getTimeAMPMInKorean(mCurrentStartTime));
                DevLog.devLog("KERB>>>>","After End Time: "+mCurrentEndTime.getTime());
                DevLog.devLog("KERB>>>>","After End Time2: "+getTimeAMPMInKorean(mCurrentEndTime));
                DevLog.devLog("KERB>>>>","After Res: "+resData.getMessageToString());
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }
}
