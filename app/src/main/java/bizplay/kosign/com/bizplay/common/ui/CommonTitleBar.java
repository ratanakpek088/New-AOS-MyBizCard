package bizplay.kosign.com.bizplay.common.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import bizplay.kosign.com.bizplay.R;
import bizplay.kosign.com.bizplay.TutorialActivity;
import bizplay.kosign.com.bizplay.popup.CustomPopupMenu3;
import bizplay.kosign.com.bizplay.tabs.ReceiptFragment;

import static android.R.attr.start;

/**
 * Created by ViSAL MSi on 12/25/2016.
 */
public class CommonTitleBar extends RelativeLayout implements View.OnClickListener {
    private Activity mActivity;
    private Context mContext;
    private static View mToolBar;
    private static TextView mTitle;
    private static ImageView imageView1, imageView2;
    private ArrayList<RelativeLayout> mToolBarsContainer;

    //    private CardRaloadable mCardRaloadable;
    public CommonTitleBar(Context context, AttributeSet attrs) {
        super(context);
        this.mContext = context;
        mActivity = (Activity) context;
        mToolBarsContainer = new ArrayList<RelativeLayout>();
        mToolBar = View.inflate(mContext, R.layout.custom_tool_bar, null);
        addView(mToolBar, new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        String type = attrs.getAttributeValue(null, "type");
        String title = attrs.getAttributeValue(null, "title");
        setToolByType(type, title);
    }

    private void setToolByType(String type, String title) {
        switch (type) {
            case "1": //login
                setVisibleViewOnly(R.id.login_tool_bar);
                ImageView imageView = (ImageView) mToolBar.findViewById(R.id.main_back);
                mTitle = (TextView) mToolBar.findViewById(R.id.login_title);
                mTitle.setText(title);
                imageView.setOnClickListener(this);
                break;
            case "2": //main activity
                setVisibleViewOnly(R.id.main_tool_bar);
                mTitle = (TextView) mToolBar.findViewById(R.id.main_title);
                mTitle.setText(title);
                imageView1 = (ImageView) mToolBar.findViewById(R.id.reloadCard);
                imageView2 = (ImageView) mToolBar.findViewById(R.id.filter);
                imageView1.setOnClickListener(this);
                imageView2.setOnClickListener(this);
                break;
            case "3": //profile
                setVisibleViewOnly(R.id.profile_tool_bar);
                ImageView backToSetting = (ImageView) mToolBar.findViewById(R.id.main_back_to_setting_of_profile);
                backToSetting.setOnClickListener(this);
                break;
            case "4": // receipt detail
                setVisibleViewOnly(R.id.receipt_detail_tool_bar);
                ImageView backToDetailCards = (ImageView) mToolBar.findViewById(R.id.main_back_to_setting_of_rcpt);
                backToDetailCards.setOnClickListener(this);


                break;
            case "5": // notice detail
                setVisibleViewOnly(R.id.A008_2_tool_bar);
                ImageView notif_detail_back_to_notif_list = (ImageView) mToolBar.findViewById(R.id.notif_detail_back_to_notif_list);
                notif_detail_back_to_notif_list.setOnClickListener(this);
                break;
            case "6": // setting
                setVisibleViewOnly(R.id.profile_tool_bar);
                ImageView backToSettingOfSetting = (ImageView) mToolBar.findViewById(R.id.main_back_to_setting_of_profile);
                backToSettingOfSetting.setOnClickListener(this);
                TextView textTitleOfSetting = (TextView) mToolBar.findViewById(R.id.tv_profile_tool_bar);
                textTitleOfSetting.setText(title);
                break;
            default:
        }
    }

    private void setVisibleViewOnly(int id) {
        mToolBarsContainer.add((RelativeLayout) mToolBar.findViewById(R.id.profile_tool_bar));
        mToolBarsContainer.add((RelativeLayout) mToolBar.findViewById(R.id.main_tool_bar));
        mToolBarsContainer.add((RelativeLayout) mToolBar.findViewById(R.id.login_tool_bar));
        mToolBarsContainer.add((RelativeLayout) mToolBar.findViewById(R.id.receipt_detail_tool_bar));
        mToolBarsContainer.add((RelativeLayout) mToolBar.findViewById(R.id.A008_2_tool_bar));
        int i = 0;
        while (i < mToolBarsContainer.size()) {
            if (mToolBarsContainer.get(i).getId() == id) {
                mToolBarsContainer.get(i).setVisibility(VISIBLE);
                if (id == R.id.main_tool_bar) {
                    mToolBarsContainer.get(i).setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (view.getId() == R.id.main_tool_bar) {
                                if (ReceiptFragment.getInstance().isShow) {
                                    ReceiptFragment.getInstance().showList.callOnClick();
                                }
                            }
                        }
                    });
                }
            } else {
                mToolBarsContainer.get(i).setVisibility(GONE);
            }
            i++;
        }
    }

    public static void whenMainChangeTab(int position) {
        switch (position) {
            case 0:
                imageView1.setVisibility(GONE);
                imageView2.setVisibility(GONE);
                mTitle.setText("MY카드");
                break;
            case 1:
                imageView1.setVisibility(VISIBLE);
                imageView2.setVisibility(VISIBLE);
                mTitle.setText("사용내역");
                break;
            case 2:
                imageView1.setVisibility(GONE);
                imageView2.setVisibility(GONE);
                mTitle.setText("알림");
                break;
            case 3:
                imageView1.setVisibility(GONE);
                imageView2.setVisibility(GONE);
                mTitle.setText("더보기");
                break;
        }
    }

    public void goTutorials() {
        Intent intent = new Intent(getContext(), TutorialActivity.class);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.main_back:
                mActivity.finish();
                Intent intent = new Intent(mActivity, TutorialActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mActivity.startActivity(intent);
                break;
            case R.id.main_back_to_setting_of_profile:
                mActivity.finish();
                break;
            case R.id.reloadCard:
                ReceiptFragment.getInstance().refreshCard();
                break;
            case R.id.filter:
                if (ReceiptFragment.getInstance().isShow) {
                    ReceiptFragment.getInstance().showList.callOnClick();
                }
                showMenu(view);
                break;
            case R.id.main_back_to_setting_of_rcpt:
                mActivity.finish();
                break;

            case R.id.notif_detail_back_to_notif_list:
                mActivity.finish();
                break;

        }
    }

    private void showMenu(View view) {


        final CustomPopupMenu3 customPopupMenu3 = new CustomPopupMenu3((Activity) mContext, R.layout.custom_popup_menu3);
        customPopupMenu3.showPopupWindow();
        customPopupMenu3.setOnPopuMenuListener(new CustomPopupMenu3.OnPopupMenuListener() {
            @Override
            public void onItem1Press() {
                imageView2.setImageResource(R.drawable.mn_filter_icon);
                ReceiptFragment.getInstance().filterReceipt("0");
                customPopupMenu3.dismiss();
            }

            @Override
            public void onItem2Press() {
                imageView2.setImageResource(R.drawable.mn_sts_new_icon);
                ReceiptFragment.getInstance().filterReceipt("1");
                customPopupMenu3.dismiss();
            }

            @Override
            public void onItem3Press() {
                imageView2.setImageResource(R.drawable.mn_sts_cancel_icon);
                ReceiptFragment.getInstance().filterReceipt("2");
                customPopupMenu3.dismiss();
            }

            @Override
            public void onItem4Press() {
                imageView2.setImageResource(R.drawable.mn_sts_personal_icon);
                ReceiptFragment.getInstance().filterReceipt("3");
                customPopupMenu3.dismiss();
            }
        });
    }

    public interface CardRaloadable {
        void reloadFromTitle();
    }
}