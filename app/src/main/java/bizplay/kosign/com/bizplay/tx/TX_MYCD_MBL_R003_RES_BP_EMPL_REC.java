package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by Daravuth on 3/3/2017.
 */

public class TX_MYCD_MBL_R003_RES_BP_EMPL_REC extends TxMessage{
    private TX_MYCD_MBL_R003_RES_BP_EMPL_REC_DATA mTxKey;

    public TX_MYCD_MBL_R003_RES_BP_EMPL_REC(Object obj) throws Exception{
        mTxKey= new TX_MYCD_MBL_R003_RES_BP_EMPL_REC_DATA();
        super.initRecvMessage(obj);
    }

    public String getBP_EMPL_NO() throws Exception{
        return getString(mTxKey.BP_EMPL_NO);
    }

    public String getBP_EMPL_NM() throws Exception{
        return getString(mTxKey.BP_EMPL_NM);
    }
    private class TX_MYCD_MBL_R003_RES_BP_EMPL_REC_DATA {
        private String BP_EMPL_NO="BP_EMPL_NO";
        private String BP_EMPL_NM="BP_EMPL_NM";
    }
}
