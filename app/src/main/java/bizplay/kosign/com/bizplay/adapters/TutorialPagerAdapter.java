package bizplay.kosign.com.bizplay.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import bizplay.kosign.com.bizplay.tabs.T1Fragment;
import bizplay.kosign.com.bizplay.tabs.T2Fragment;
import bizplay.kosign.com.bizplay.tabs.T3Fragment;
import bizplay.kosign.com.bizplay.tabs.T4Fragment;

/**
 * Created by ViSAL MSi on 1/3/2017.
 */
public class TutorialPagerAdapter extends FragmentPagerAdapter {

    private static int NUM_ITEMS = 4;

    public TutorialPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return T1Fragment.getInstance();
            case 1:
                return T2Fragment.getInstance();
            case 2:
                return T3Fragment.getInstance();
            default:
                return T4Fragment.getInstance();
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }
}
