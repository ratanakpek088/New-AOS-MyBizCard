package bizplay.kosign.com.bizplay.item;

/**
 * Created by Daravuth on 3/8/2017.
 */

public class Cust {
    private String BP_CUST_NO;
    private String BP_MAGR_NO;
    private String BP_CUST_NM;
    private String BP_MAGR_NM;
    private String CLPH_NO;
    private String BP_MEMO;
    private boolean isChecked;
    int ID;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Cust() {

    }

    public Cust( String BP_CUST_NO, String BP_MAGR_NO, String BP_CUST_NM, String BP_MAGR_NM, String CLPH_NO, String BP_MEMO, boolean isChecked) {
        this.BP_CUST_NO = BP_CUST_NO;
        this.BP_MAGR_NO = BP_MAGR_NO;
        this.BP_CUST_NM = BP_CUST_NM;
        this.BP_MAGR_NM = BP_MAGR_NM;
        this.CLPH_NO = CLPH_NO;
        this.BP_MEMO = BP_MEMO;
        this.isChecked = isChecked;

    }

    public String getBP_CUST_NO() {
        return BP_CUST_NO;
    }

    public void setBP_CUST_NO(String BP_CUST_NO) {
        this.BP_CUST_NO = BP_CUST_NO;
    }

    public String getBP_MAGR_NO() {
        return BP_MAGR_NO;
    }

    public void setBP_MAGR_NO(String BP_MAGR_NO) {
        this.BP_MAGR_NO = BP_MAGR_NO;
    }

    public String getBP_CUST_NM() {
        return BP_CUST_NM;
    }

    public void setBP_CUST_NM(String BP_CUST_NM) {
        this.BP_CUST_NM = BP_CUST_NM;
    }

    public String getBP_MAGR_NM() {
        return BP_MAGR_NM;
    }

    public void setBP_MAGR_NM(String BP_MAGR_NM) {
        this.BP_MAGR_NM = BP_MAGR_NM;
    }

    public String getCLPH_NO() {
        return CLPH_NO;
    }

    public void setCLPH_NO(String CLPH_NO) {
        this.CLPH_NO = CLPH_NO;
    }

    public String getBP_MEMO() {
        return BP_MEMO;
    }

    public void setBP_MEMO(String BP_MEMO) {
        this.BP_MEMO = BP_MEMO;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
