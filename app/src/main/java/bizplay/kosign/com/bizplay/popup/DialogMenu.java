package bizplay.kosign.com.bizplay.popup;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import bizplay.kosign.com.bizplay.R;

/**
 * Created by Daravuth on 3/13/2017.
 */

public class DialogMenu implements View.OnClickListener {
    private Dialog dialog;
    private OnDialogMenuListener listener;

    public DialogMenu(Context context) {
        dialog = new Dialog(context, R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_picture_setting);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.show();

        TextView tv_picture = (TextView) dialog.findViewById(R.id.tv_picture);
        TextView tv_gallery = (TextView) dialog.findViewById(R.id.tv_gallery);
        tv_picture.setOnClickListener(this);
        tv_gallery.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_picture:
                listener.onPicturePress();
                dialog.dismiss();
                break;

            case R.id.tv_gallery:
                listener.onGalleryPress();
                dialog.dismiss();
                break;
        }
    }
    public void setOnDialogMenuListener(OnDialogMenuListener listener){
        this.listener = listener;
    }

    public interface OnDialogMenuListener {
        void onPicturePress();

        void onGalleryPress();
    }
}
