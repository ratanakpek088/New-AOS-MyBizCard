package bizplay.kosign.com.bizplay.extras;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


public class Extra_Notif extends Extras {

    public Extra_Notif(Context ctx) {
        super(ctx);
    }

    public Extra_Notif(Activity act, Intent intent) {
        super(act, intent);
    }

    public Extra_Notif(Activity act, Bundle bundle) {
        super(act, bundle);
    }

    private final String KEY_SEQ     = "SEQ";
    private final String KEY_SND_DTM = "SND_DTM";
    private final String KEY_USER_ID = "USER_ID";
    private final String KEY_CARD_NO = "CARD_NO";

    public _Param Param = new _Param();

    public class _Param {

        public void setKEY_SEQ(String value) {
            setString(KEY_SEQ, value);
        }

        public String getKEY_SEQ() {
            return getString(KEY_SEQ);
        }

        public void setKEY_SND_DTM(String value) {
            setString(KEY_SND_DTM, value);
        }

        public String getKEY_SND_DTM() {
            return getString(KEY_SND_DTM);
        }

        public void setKEY_USER_ID(String value) {
            setString(KEY_USER_ID, value);
        }

        public String getKEY_USER_ID() {
            return getString(KEY_USER_ID);
        }

        public void setKEY_CARD_NO(String value) {
            setString(KEY_CARD_NO, value);
        }

        public String getKEY_CARD_NO() {
            return getString(KEY_CARD_NO);
        }

    }

}
