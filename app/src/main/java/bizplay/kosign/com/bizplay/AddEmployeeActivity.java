package bizplay.kosign.com.bizplay;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.webcash.sws.log.DevLog;

import java.util.ArrayList;

import bizplay.kosign.com.bizplay.adapters.item.EmployeeAdapter;
import bizplay.kosign.com.bizplay.common.ui.util.SoundSearcher;
import bizplay.kosign.com.bizplay.item.Employee;
import bizplay.kosign.com.bizplay.tran.ComTran;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L007_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L007_RES;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L007_RES_REC;

import static android.view.View.GONE;

public class AddEmployeeActivity extends AppCompatActivity implements ComTran.OnComTranListener, View.OnClickListener, AdapterView.OnItemClickListener {

    private TextView top_title, navigate_right;
    private ComTran comTran;
    private String CARD_CORP_CD, CARD_NO;
    private ListView listViewEmployee, listViewSearch;
    private ArrayList<Employee> listEmployee, listSearch, listSelected, listEmpExtra;
    private EmployeeAdapter employeeAdapter, searchAdapter;
    private EditText etSearch;
    private InputMethodManager imm;
    private LinearLayout llSelectedList;
    private ImageView iv_left_btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        findViewById(R.id.ll_current_emp).setOnClickListener(this);
        findViewById(R.id.toolbar).setOnClickListener(this);
        loadEmployee();
    }


    private void initView() {
        setContentView(R.layout.activity_add_employee);
        top_title = (TextView) findViewById(R.id.toolbar_title_center);
        top_title.setText(getResources().getString(R.string.AEA_00));
        navigate_right = (TextView) findViewById(R.id.toolbar_title_right);
        navigate_right.setText(getResources().getString(R.string.AEA_02));
        comTran = new ComTran(this, this);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        navigate_right.setOnClickListener(this);
        iv_left_btn = (ImageView) findViewById(R.id.iv_left_btn);
        iv_left_btn.setOnClickListener(this);
        listViewEmployee = (ListView) findViewById(R.id.exp_list);
        listViewSearch = (ListView) findViewById(R.id.exp_list_search);
        listViewSearch.setOnItemClickListener(this);
        llSelectedList = (LinearLayout) findViewById(R.id.ll_selected_emp);
        listViewEmployee.setOnItemClickListener(this);
        etSearch = (EditText) findViewById(R.id.et_search);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.e("rtk", "before");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.e("rtk", "ontextchange");
                if (s.length() == 0) {
                    listViewEmployee.setVisibility(View.VISIBLE);
                    listViewSearch.setVisibility(View.GONE);
                    employeeAdapter.notifyDataSetChanged();
                } else {
                    listViewEmployee.setVisibility(View.GONE);
                    listViewSearch.setVisibility(View.VISIBLE);
                    searchAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.e("rtk", "aftertextchange");
                if (s.length() > 0) {
                    searchResult(s.toString());

                }
            }
        });

        listEmployee = new ArrayList<>();
        listSelected = new ArrayList<>();
        listEmpExtra = new ArrayList<>();
        listSearch = new ArrayList<>();


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            CARD_CORP_CD = extras.getString("CARD_CORP_CD");
            CARD_NO = extras.getString("CARD_NO");
            Log.e("rtk", "CD " + CARD_CORP_CD + "NO " + CARD_NO);
        }

        if (getIntent() != null && getIntent().hasExtra("BP_EMPL_LIST")) {
            listEmpExtra = getIntent().getParcelableArrayListExtra("BP_EMPL_LIST");

            if (listEmpExtra.size() > 0) {
                listSelected.addAll(listEmpExtra);
                DevLog.devLog("DDDD", listSelected.toString());

                for (int i = 0; i < listSelected.size(); i++) {
                    listSelected.get(i).setID(-1);
                    final View itemView = LayoutInflater.from(this).inflate(R.layout.participant_select_item, null);
                    LinearLayout ll_item = (LinearLayout) itemView.findViewById(R.id.ll_participant_select_item);

                    TextView tvName = (TextView) itemView.findViewById(R.id.tv_name);
                    tvName.setText(listSelected.get(i).getBP_EMPL_NM());
                    ll_item.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            removeUserInSelectListLayout(itemView);
                        }
                    });

                    llSelectedList.addView(itemView);
                    // scrollToRight();
                }
                findViewById(R.id.ll_current_emp).setVisibility(View.VISIBLE);
            }
        }
        employeeAdapter = new EmployeeAdapter(this, listEmployee, "");
        listViewEmployee.setAdapter(employeeAdapter);
        searchAdapter = new EmployeeAdapter(this, listSearch, "");
        listViewSearch.setAdapter(searchAdapter);


    }
    private void hideKeyBoard() {
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        etSearch.clearFocus();
    }
    private void searchResult(String search) {
        listSearch.clear();
        searchAdapter.setSearch(search);
        for (int i = 0; i < listEmployee.size(); i++) {
            if (SoundSearcher.matchString(listEmployee.get(i).getBP_EMPL_NM().toLowerCase(), search.toLowerCase())) {
                listSearch.add(listEmployee.get(i));
            } else if (SoundSearcher.matchString(listEmployee.get(i).getBP_DVSN_NM().toLowerCase(), search.toLowerCase())) {
                listSearch.add(listEmployee.get(i));
            } else if (SoundSearcher.matchString(listEmployee.get(i).getCLPH_NO(), search.replace("-", ""))) {
                listSearch.add(listEmployee.get(i));
            }
        }

    }


    public void loadEmployee() {
        try {
            TX_MYCD_MBL_L007_REQ req = new TX_MYCD_MBL_L007_REQ();
            req.setCARD_CORP_CD(CARD_CORP_CD);
            req.setCARD_NO(CARD_NO);
            comTran.requestData(TX_MYCD_MBL_L007_REQ.TXNO, req.getSendMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTranResponse(String tranCode, Object object) throws Exception {
        try {
            switch (tranCode) {
                case TX_MYCD_MBL_L007_REQ.TXNO:
                    TX_MYCD_MBL_L007_RES res = new TX_MYCD_MBL_L007_RES(object);
                    TX_MYCD_MBL_L007_RES_REC rec = res.getBP_EMPL_REC();
                    for (int i = 0; i < rec.getLength(); i++) {
                        Employee emp = new Employee();
                        emp.setID(listEmployee.size());
                        emp.setBP_EMPL_NO(rec.getBP_EMPL_NO());
                        emp.setBP_EMPL_NM(rec.getBP_EMPL_NM());
                        emp.setCLPH_NO(rec.getCLPH_NO());
                        emp.setBP_DVSN_NM(rec.getBP_DVSN_NM());
                        for (int j = 0; j < listSelected.size(); j++) {
                            if (listSelected.get(j).getBP_EMPL_NO().equals(emp.getBP_EMPL_NO()) && listSelected.get(j).getBP_EMPL_NM().equals(emp.getBP_EMPL_NM())) {
                                listSelected.get(j).setID(emp.getID());
                                emp.setIS_SELECTED(true);
                            }
                        }
                        listEmployee.add(emp);
                        rec.moveNext();
                    }
                    employeeAdapter.notifyDataSetChanged();
                    break;
                default:
                    Log.e("rtk", "nothing");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_title_right:
                returnSelectedEmployee();
                break;

            case R.id.iv_left_btn:
                finish();
                break;
            case R.id.ll_current_emp:
            case R.id.toolbar:
                hideKeyBoard();
                break;
            default:
        }

    }

    private void returnSelectedEmployee() {
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra("BP_EMPL_LIST", listSelected);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Employee obj = new Employee();

        if (findViewById(R.id.ll_current_emp).getVisibility() != View.VISIBLE)
            findViewById(R.id.ll_current_emp).setVisibility(View.VISIBLE);
        if (parent.getId() == R.id.exp_list) {
            obj = listEmployee.get(position);
            if (listEmployee.get(position).getIS_SELECTED()) {


                listEmployee.get(position).setIS_SELECTED(false);


                employeeAdapter.notifyDataSetChanged();
                removeFromSelectedList(obj);

                DevLog.devLog(listSelected.size() + "re---------------");
                DevLog.devLog(llSelectedList.getChildCount() + "re---------------");

                return;
            } else {
                listEmployee.get(position).setIS_SELECTED(true);
                listSelected.add(listEmployee.get(position));
                employeeAdapter.notifyDataSetChanged();
                DevLog.devLog(listSelected.size() + "add---------------");
                DevLog.devLog(llSelectedList.getChildCount() + "add---------------");

            }

        } else if (parent.getId() == R.id.exp_list_search) {
            obj = listSearch.get(position);
            if (listSearch.get(position).getIS_SELECTED()) {
                listSearch.get(position).setIS_SELECTED(false);
                // uncheckd from original list too
                listEmployee.get(listSearch.get(position).getID()).setIS_SELECTED(false);
                searchAdapter.notifyDataSetChanged();
                employeeAdapter.notifyDataSetChanged();
                // remove from list selected too
                removeFromSelectedList(listSearch.get(position));
                return;
            } else {
                listSearch.get(position).setIS_SELECTED(true);
                listSelected.add(listSearch.get(position));
                listEmployee.get(listSearch.get(position).getID()).setIS_SELECTED(true);
                searchAdapter.notifyDataSetChanged();
                employeeAdapter.notifyDataSetChanged();
            }
        }
        final View itemview = LayoutInflater.from(this).inflate(R.layout.participant_select_item, null);
        LinearLayout ll_item = (LinearLayout) itemview.findViewById(R.id.ll_participant_select_item);
        TextView tvName = (TextView) itemview.findViewById(R.id.tv_name);
        tvName.setText(obj.getBP_EMPL_NM());
        llSelectedList.addView(itemview);
        ll_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeUserInSelectListLayout(itemview);
            }
        });

    }

    private void removeUserInSelectListLayout(View itemView) {
        Log.e("rtk", "Item cancel");
        int id = listSelected.get(llSelectedList.indexOfChild(itemView)).getID();
        DevLog.devLog("ID", id + "");
        if (id >= 0) {
            if (listViewEmployee.getVisibility() == View.VISIBLE) {
                listEmployee.get(id).setIS_SELECTED(false);
                employeeAdapter.notifyDataSetChanged();
                DevLog.devLog("listViewEmployee is visible");


            } else if (listViewSearch.getVisibility() == View.VISIBLE) {
                for (int i = 0; i < listSearch.size(); i++) {
                    if (listSearch.get(i).getID() == listSelected.get(llSelectedList.indexOfChild(itemView)).getID()) {
                        listSearch.get(i).setIS_SELECTED(false);
                        //listEmployee.get(listSearch.get(i).getID()).setIS_SELECTED(false);
                        searchAdapter.notifyDataSetChanged();
                        break;
                    }
                }
                for (int i = 0; i < listEmployee.size(); i++) {
                    if (listEmployee.get(i).getID() == listSelected.get(llSelectedList.indexOfChild(itemView)).getID()) {
                        listEmployee.get(i).setIS_SELECTED(false);
                        employeeAdapter.notifyDataSetChanged();
                        break;
                    }
                }
            }

            listSelected.remove(llSelectedList.indexOfChild(itemView));
            llSelectedList.removeView(itemView);

            if (listSelected.size() == 0) findViewById(R.id.ll_current_emp).setVisibility(GONE);
        }

    }

    private void removeFromSelectedList(Employee e) {

        for (int i = 0; i < listSelected.size(); i++) {
            if (e.getID() == listSelected.get(i).getID()) {
                listSelected.remove(i);
                DevLog.devLog("Child View", (llSelectedList.getChildAt(i) == null) ? "Null" : "Not null");
                llSelectedList.removeViewAt(i);
                llSelectedList.invalidate();
                break;
            }
        }
        if (llSelectedList.getChildCount() == 0)
            findViewById(R.id.ll_current_emp).setVisibility(GONE);
    }

}
