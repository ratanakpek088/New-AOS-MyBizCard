package bizplay.kosign.com.bizplay.adapters;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;


import java.util.ArrayList;
import java.util.List;

import bizplay.kosign.com.bizplay.cardview.PageFragment;


/**
 * Created by User on 08-Nov-16.
 */

public class CardViewPagerAdapter<T extends Parcelable> extends FragmentStatePagerAdapter {
    private static final String TAG = CardViewPagerAdapter.class.getSimpleName();
    private int mPagesCount;
    private FragmentManager mFragmentManager;
    private List<T> mItems;
    private Class mPageFragmentClass;
    private int mPageLayoutId;

    public CardViewPagerAdapter(FragmentManager fragmentManager,
                                Class pageFragmentClass,
                                int pageLayoutId,
                                List<T> items) {
        super(fragmentManager);
        mFragmentManager = fragmentManager;
        mPageFragmentClass = pageFragmentClass;
        mPageLayoutId = pageLayoutId;
        if (items == null) {
            mItems = new ArrayList<T>(0);
        } else {
            mItems = items;
        }
        mPagesCount = mItems.size();
    }

    @Override
    public Fragment getItem(int position) {
        try {
            PageFragment pf = (PageFragment) mPageFragmentClass.newInstance();
            pf.setArguments(PageFragment.createArgs(mPageLayoutId, mItems.get(position)));
            return pf;
        } catch (IllegalAccessException e) {
           Log.e(TAG, e.getMessage());
        } catch (InstantiationException e) {
           Log.e(TAG, e.getMessage());
        }
        return null;
    }

    @Override
    public int getCount() {
        return mPagesCount;
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}
