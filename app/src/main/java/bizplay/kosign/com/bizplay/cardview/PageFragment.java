/*
 * Copyright 2014 acbelter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bizplay.kosign.com.bizplay.cardview;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public abstract class PageFragment<T extends Parcelable> extends Fragment {
    /**
     * @param pageLayout Layout of page.
     * @param pageItem   Item for customize page content view.
     * @return View of page content.
     */
    public abstract View setupPage(View pageLayout, T pageItem);

    public static Bundle createArgs(int pageLayoutId, Parcelable item) {
        Bundle args = new Bundle();
        args.putInt("page_layout_id", pageLayoutId);
        args.putParcelable("item", item);
        return args;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        int pageLayoutId = getArguments().getInt("page_layout_id");
        View pageLayout = (View) inflater.inflate(pageLayoutId, container, false);
        if (pageLayout == null) {
            throw new IllegalStateException("PageFragment root layout must have id " +
                    "R.id.page.");
        }

        T item = getArguments().getParcelable("item");
        View pageContent = setupPage(pageLayout, item);
        return pageLayout;
    }
}