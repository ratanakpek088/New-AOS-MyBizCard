package bizplay.kosign.com.bizplay.popup;

import android.app.Activity;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import bizplay.kosign.com.bizplay.R;


/*
 * Created by Huo Chhunleng on 6/7/2016.
 */
public class CustomPopupMenu3 extends PopupMenu {
    private OnPopupMenuListener listener = null;

    private FrameLayout item_1, item_2, item_3, item_4;

    public CustomPopupMenu3(Activity activity, int layout) {
        super(activity, layout);
        item_1 = (FrameLayout) anchor.findViewById(R.id.item_1);
        item_2 = (FrameLayout) anchor.findViewById(R.id.item_2);
        item_3 = (FrameLayout) anchor.findViewById(R.id.item_3);
        item_4 = (FrameLayout) anchor.findViewById(R.id.item_4);

        item_1.setOnClickListener(this);
        item_2.setOnClickListener(this);
        item_3.setOnClickListener(this);
        item_4.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.item_1:
                listener.onItem1Press();
                break;
            case R.id.item_2:
                listener.onItem2Press();
                break;
            case R.id.item_3:
                listener.onItem3Press();
                break;
            case R.id.item_4:
                listener.onItem4Press();
                break;
        }
    }

    @Override
    public void showPopupWindow() {
        Rect rect = locateView(anchor);
        showAtLocation(anchor, Gravity.TOP|Gravity.RIGHT, rect.right + getDP(50), rect.top + getDP(30));
        this.showAsDropDown(anchor);
    }

    public interface OnPopupMenuListener{
        void onItem1Press();
        void onItem2Press();
        void onItem3Press();
        void onItem4Press();
    }

    public void setOnPopuMenuListener(OnPopupMenuListener listener) {
        this.listener = listener;
    }

}
