package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONArray;

/**
 * Created by User on 7/15/2016.
 */
public class TX_MYCD_MBL_P004_REQ extends TxMessage {
    public static final String TXNO = "MYCD_MBL_P004";
    private TX_MYCD_MBL_P004_REQ_DATA mTxKeyData;

    public TX_MYCD_MBL_P004_REQ() throws Exception {
        mTxKeyData = new TX_MYCD_MBL_P004_REQ_DATA();
        super.initSendMessage();
    }
    public void setCARD_CORP_CD(String value){
        mSendMessage.put(mTxKeyData.CARD_CORP_CD, value);
    }
    public void setCARD_NO(String value){
        mSendMessage.put(mTxKeyData.CARD_NO, value);
    }
    public void setRCPT_USER_REC(JSONArray value){
        mSendMessage.put(mTxKeyData.RCPT_USER_REC, value);
    }
    public void setAPV_GB(String value){
        mSendMessage.put(mTxKeyData.APV_GB, value);
    }
    public void setAPV_DT(String value){
        mSendMessage.put(mTxKeyData.APV_DT, value);
    }
    public void setAPV_SEQ(String value){
        mSendMessage.put(mTxKeyData.APV_SEQ, value);
    }
    public void setAPV_TM(String value){
        mSendMessage.put(mTxKeyData.APV_TM, value);
    }
    public void setAPV_NO(String value){
        mSendMessage.put(mTxKeyData.APV_NO, value);
    }

    private class TX_MYCD_MBL_P004_REQ_DATA{
        private String CARD_CORP_CD     = "CARD_CORP_CD";
        private String CARD_NO          = "CARD_NO";
        private String RCPT_USER_REC    = "RCPT_USER_REC";
        private String APV_GB           = "APV_GB";
        private String APV_DT           = "APV_DT";
        private String APV_SEQ          = "APV_SEQ";
        private String APV_TM           = "APV_TM";
        private String APV_NO           = "APV_NO";

    }
}
