package bizplay.kosign.com.bizplay.item;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by ViSAL MSi on 1/9/2017.
 */

public class Receipt {

    public static Receipt mReceipt;

    private String INQ_NEXT_APV_DT = "INQ_NEXT_APV_DT";
    private String INQ_NEXT_APV_SEQ = "INQ_NEXT_APV_SEQ";
    private String INQ_NEXT_APV_TM = "INQ_NEXT_APV_TM";
    private String INQ_NEXT_APV_NO = "INQ_NEXT_APV_NO";
    private String INQ_ITRN_SUB_ROWCOUNT = "INQ_ITRN_SUB_ROWCOUNT";
    private String TRAN_CNT = "TRAN_CNT";
    private String NOTI_CNT = "NOTI_CNT";
    private ArrayList<ReceiptRec> receiptRecs;

    public Receipt(){
        receiptRecs = new ArrayList<>();
    }

    public Receipt(String INQ_NEXT_APV_DT, String INQ_NEXT_APV_SEQ, String INQ_NEXT_APV_TM, String INQ_NEXT_APV_NO, String INQ_ITRN_SUB_ROWCOUNT, String TRAN_CNT, String NOTI_CNT, ArrayList<ReceiptRec> receiptRecs) {
        this.INQ_NEXT_APV_DT = INQ_NEXT_APV_DT;
        this.INQ_NEXT_APV_SEQ = INQ_NEXT_APV_SEQ;
        this.INQ_NEXT_APV_TM = INQ_NEXT_APV_TM;
        this.INQ_NEXT_APV_NO = INQ_NEXT_APV_NO;
        this.INQ_ITRN_SUB_ROWCOUNT = INQ_ITRN_SUB_ROWCOUNT;
        this.TRAN_CNT = TRAN_CNT;
        this.NOTI_CNT = NOTI_CNT;
        this.receiptRecs = receiptRecs;
    }

    public String getINQ_NEXT_APV_DT() {
        return INQ_NEXT_APV_DT;
    }

    public void setINQ_NEXT_APV_DT(String INQ_NEXT_APV_DT) {
        this.INQ_NEXT_APV_DT = INQ_NEXT_APV_DT;
    }

    public String getINQ_NEXT_APV_SEQ() {
        return INQ_NEXT_APV_SEQ;
    }

    public void setINQ_NEXT_APV_SEQ(String INQ_NEXT_APV_SEQ) {
        this.INQ_NEXT_APV_SEQ = INQ_NEXT_APV_SEQ;
    }

    public String getINQ_NEXT_APV_TM() {
        return INQ_NEXT_APV_TM;
    }

    public void setINQ_NEXT_APV_TM(String INQ_NEXT_APV_TM) {
        this.INQ_NEXT_APV_TM = INQ_NEXT_APV_TM;
    }

    public String getINQ_NEXT_APV_NO() {
        return INQ_NEXT_APV_NO;
    }

    public void setINQ_NEXT_APV_NO(String INQ_NEXT_APV_NO) {
        this.INQ_NEXT_APV_NO = INQ_NEXT_APV_NO;
    }

    public String getINQ_ITRN_SUB_ROWCOUNT() {
        return INQ_ITRN_SUB_ROWCOUNT;
    }

    public void setINQ_ITRN_SUB_ROWCOUNT(String INQ_ITRN_SUB_ROWCOUNT) {
        this.INQ_ITRN_SUB_ROWCOUNT = INQ_ITRN_SUB_ROWCOUNT;
    }

    public String getTRAN_CNT() {
        return TRAN_CNT;
    }

    public void setTRAN_CNT(String TRAN_CNT) {
        this.TRAN_CNT = TRAN_CNT;
    }

    public String getNOTI_CNT() {
        return NOTI_CNT;
    }

    public void setNOTI_CNT(String NOTI_CNT) {
        this.NOTI_CNT = NOTI_CNT;
    }

    public ArrayList<ReceiptRec> getReceiptRecs() {
        return receiptRecs;
    }

    public void setReceiptRecs(ArrayList<ReceiptRec> receiptRecs) {
        this.receiptRecs = receiptRecs;
    }

}


