package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by RatanakPek on 1/14/2017.
 */

public class TX_MYCD_MBL_C002_REQ_RCPT_USER_REC extends TxMessage {
    private TX_MYCD_MBL_C002_RCPT_USER_REC_DATA mTxKeyData;

    public TX_MYCD_MBL_C002_REQ_RCPT_USER_REC(Object object) throws Exception {
        mTxKeyData = new TX_MYCD_MBL_C002_RCPT_USER_REC_DATA();
        super.initRecvMessage(object);
    }

    public void setRCPT_USER_NO(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.RCPT_USER_NO, value);
    }

    private class TX_MYCD_MBL_C002_RCPT_USER_REC_DATA{
        private String RCPT_USER_NO="RCPT_USER_NO";
    }
}
