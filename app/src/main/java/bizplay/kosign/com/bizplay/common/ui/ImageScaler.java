package bizplay.kosign.com.bizplay.common.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Daravuth on 3/17/2017.
 */

public class ImageScaler extends ImageView {
    public ImageScaler(Context context) {
        super(context);
    }

    public ImageScaler(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageScaler(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        Drawable drawable = getDrawable();
        if(drawable!=null){
            int width = MeasureSpec.getSize(widthMeasureSpec);
            int height = width * drawable.getIntrinsicHeight() / drawable.getIntrinsicWidth();
            setMeasuredDimension(width, height);
        }else super.onMeasure(widthMeasureSpec, heightMeasureSpec);

    }
}
