package bizplay.kosign.com.bizplay.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;


import java.util.ArrayList;

import bizplay.kosign.com.bizplay.R;
import bizplay.kosign.com.bizplay.adapters.item.A007_2_4P_Item;


public class A007_2_4P_Adapter extends BaseAdapter{

    public OnItemClickListener listener = null;

    private Context context;
    private ArrayList<A007_2_4P_Item> data;
    private LayoutInflater inflater=null;
    private int selectedPosition;


    public A007_2_4P_Adapter(){}

    public A007_2_4P_Adapter(Context context, ArrayList<A007_2_4P_Item> data, int selectedPosition) {
        this.context = context;
        this.data=data;
        this.selectedPosition = selectedPosition;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.a007_2_4p_item, null);


        RadioButton radioButton = (RadioButton) vi.findViewById(R.id.rb_item);

        radioButton.setChecked(position == selectedPosition);
        radioButton.setTag(position);
        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPosition = (Integer)view.getTag();
                listener.onItemClick(selectedPosition);
                notifyDataSetChanged();
            }
        });

        A007_2_4P_Item item = data.get(position);
        radioButton.setText(item.getTRAN_KIND_NM());

        return vi;
    }


    public void setOnListItemListener(OnItemClickListener listener){
        this.listener = listener;
    }

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
}