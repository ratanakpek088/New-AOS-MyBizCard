package bizplay.kosign.com.bizplay.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import bizplay.kosign.com.bizplay.R;
import bizplay.kosign.com.bizplay.item.Card;
import bizplay.kosign.com.bizplay.tabs.ReceiptFragment;

/**
 * Created by ViSAL MSi on 1/5/2017.
 */

// unused current using LinearLayout instead

public class ListCardAdapter extends BaseAdapter {

    private Context mContext;
    private int itemCounts;

    public ListCardAdapter(Context context){
        this.mContext = context;



        if(Card.mCardList.size() > 5){
            itemCounts = 5;
        }else{
            itemCounts = Card.mCardList.size();
        }
    }


    public void setItemCount(int total){
        itemCounts = total;
    }

    @Override
    public int getCount() {
        return itemCounts;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item = inflater.inflate(R.layout.item_of_card_list, viewGroup, false);

        TextView textView = (TextView) item.findViewById(R.id.card_no_of_list_card);

        String string = Card.mCardList.get(i).getCARD_NO();
        String last4 = "";
        if(string.length() == 16){ // if card has 16 digit
            last4  = string.substring(string.length() - 4);
        }else if(string.length() == 15){ // if card has 15 digit
            last4  = string.substring(string.length() - 3);
        }else if(string.length() == 13){ // if card has 13 digit
            last4  = string.substring(string.length() - 2);
        }

        String first4 = string.substring(0, Math.min(string.length(), 4));

        textView.setText(first4 + " - **** - **** - " + last4);

        final int position = i;

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReceiptFragment.getInstance().whenItemCardListClick(position);
            }
        });

        return item;
    }


}
