package bizplay.kosign.com.bizplay.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import bizplay.kosign.com.bizplay.R;
import bizplay.kosign.com.bizplay.item.AppInfo_Item;

/**
 * Created by User on 6/25/2016.
 */
public class GridAdapter extends BaseAdapter{

    private Context mContext;
    private ArrayList<AppInfo_Item> arrayList;
    private final int[] Imageid;

    public GridAdapter(Context mContext, ArrayList<AppInfo_Item> arrayList, int[] imageid) {
        this.mContext = mContext;
        this.arrayList = arrayList;
        Imageid = imageid;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            View grid;
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                grid = new View(mContext);
                grid = inflater.inflate(R.layout.setting_item, null);
                TextView textView = (TextView) grid.findViewById(R.id.tv_setting);
                ImageView imageView = (ImageView)grid.findViewById(R.id.setting_icon);

                AppInfo_Item app = arrayList.get(position);

                textView.setText(app.getApp_name());
                imageView.setImageResource(Imageid[position]);
            } else {
                grid = (View) convertView;
            }
            return grid;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
