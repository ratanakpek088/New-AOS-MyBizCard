package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by ViSAL MSi on 12/29/2016.
 */

public class TX_MYCD_MBL_L002_RES extends TxMessage {

    private TX_MYCD_MBL_L002_RES_DATA mTxKeyData; // 전문명세 필드

    public TX_MYCD_MBL_L002_RES(Object obj) throws Exception {
        mTxKeyData = new TX_MYCD_MBL_L002_RES_DATA();
        super.initRecvMessage(obj);
    }


    public TX_MYCD_MBL_L002_RES_REC getINQ_ITRN_SUB() throws JSONException, Exception {
        return new TX_MYCD_MBL_L002_RES_REC(getRecord(mTxKeyData.INQ_ITRN_SUB));
    }

    public String getINQ_NEXT_APV_DT() throws Exception {
        return getString(mTxKeyData.INQ_NEXT_APV_DT);
    }

    public String getINQ_NEXT_APV_SEQ() throws Exception {
        return getString(mTxKeyData.INQ_NEXT_APV_SEQ);
    }

    public String getINQ_NEXT_APV_TM() throws Exception {
        return getString(mTxKeyData.INQ_NEXT_APV_TM);
    }

    public String getINQ_NEXT_APV_NO() throws Exception {
        return getString(mTxKeyData.INQ_NEXT_APV_NO);
    }

    public String getINQ_ITRN_SUB_ROWCOUNT() throws Exception {
        return getString(mTxKeyData.INQ_ITRN_SUB_ROWCOUNT);
    }

    public String getTRAN_CNT() throws Exception {
        return getString(mTxKeyData.TRAN_CNT);
    }

    public String getNOTI_CNT() throws Exception {
        return getString(mTxKeyData.NOTI_CNT);
    }

    /**
     * 전문 Data 필드 설정
     */
    private class TX_MYCD_MBL_L002_RES_DATA {
        private String INQ_ITRN_SUB = "INQ_ITRN_SUB";                //반복부
        private String INQ_NEXT_APV_DT = "INQ_NEXT_APV_DT";
        private String INQ_NEXT_APV_SEQ = "INQ_NEXT_APV_SEQ";
        private String INQ_NEXT_APV_TM = "INQ_NEXT_APV_TM";
        private String INQ_NEXT_APV_NO = "INQ_NEXT_APV_NO";
        private String INQ_ITRN_SUB_ROWCOUNT = "INQ_ITRN_SUB_ROWCOUNT";
        private String TRAN_CNT = "TRAN_CNT";
        private String NOTI_CNT = "NOTI_CNT";

    }
}
