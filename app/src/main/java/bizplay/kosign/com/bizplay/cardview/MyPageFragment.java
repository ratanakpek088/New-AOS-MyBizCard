package bizplay.kosign.com.bizplay.cardview;

import android.graphics.Color;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import bizplay.kosign.com.bizplay.R;


public class MyPageFragment extends PageFragment<MyPageItem> {

    @Override
    public View setupPage(View pageLayout, MyPageItem pageItem) {
        View pageContent = pageLayout.findViewById(R.id.page_content);
        TextView card_org = (TextView) pageContent.findViewById(R.id.tv_card_org);

        LinearLayout ll_card_start, ll_card_end, ll_month, ll_year;

        ll_card_start = (LinearLayout) pageLayout.findViewById(R.id.ll_card_start);
        ll_card_end = (LinearLayout) pageLayout.findViewById(R.id.ll_card_end);
        ll_month = (LinearLayout) pageLayout.findViewById(R.id.ll_month);
        ll_year = (LinearLayout) pageLayout.findViewById(R.id.ll_year);

        String card_num = pageItem.getCARD_NO();

        String card_s = card_num.substring(0, 4);
        String card_e = card_num.substring(12, card_num.length());

        setUpCardView(ll_card_start, card_s, 11, 15);
        setUpCardView(ll_card_end, card_e, 11, 15);

        if (!TextUtils.isEmpty(pageItem.getEXPR_TRM())) {
            String month = pageItem.getEXPR_TRM().substring(0, 2);
            String year = pageItem.getEXPR_TRM().substring(3, pageItem.getEXPR_TRM().length());

            setUpCardView(ll_month, month, 9, 12);
            setUpCardView(ll_year, year, 9, 12);
        }


        TextView card_biz = (TextView) pageContent.findViewById(R.id.tv_card_biz);
        if (pageItem.getCARD_BYNM().equalsIgnoreCase("null") || TextUtils.isEmpty(pageItem.getCARD_BYNM()) == true) {
            card_biz.setText("");
        } else
            card_biz.setText(pageItem.getCARD_BYNM());

        if (pageItem.getCARD_CORP_CD().equals("30000001")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_kb);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_kb, R.drawable.card_kb_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000002")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_hd);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_hd, R.drawable.card_hd_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000003")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_ss);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_ss, R.drawable.card_ss_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000004")) { //new
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_ss);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_ct, R.drawable.card_ct_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000006")) { //new
            card_biz.setTextColor(Color.parseColor("#000000"));
            card_org.setText(pageItem.getCARD_ORG_NM());
            //pageContent.setBackgroundResource(R.drawable.card_ss);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_white, R.drawable.card_white_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000008")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_sh);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_sh, R.drawable.card_sh_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000010")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_hn);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_hn, R.drawable.card_hn_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000012")) { //new
            card_biz.setTextColor(Color.parseColor("#000000"));
            card_org.setText(pageItem.getCARD_ORG_NM());
            //pageContent.setBackgroundResource(R.drawable.card_hd);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_white, R.drawable.card_white_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000013")) { //new
            card_biz.setTextColor(Color.parseColor("#000000"));
            card_org.setText(pageItem.getCARD_ORG_NM());
            //pageContent.setBackgroundResource(R.drawable.card_hd);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_white, R.drawable.card_white_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000014")) { //new
            card_biz.setTextColor(Color.parseColor("#000000"));
            card_org.setText(pageItem.getCARD_ORG_NM());
            //pageContent.setBackgroundResource(R.drawable.card_hd);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_white, R.drawable.card_white_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000015")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_hn);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_hn, R.drawable.card_hn_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000016")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_ct);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_ct, R.drawable.card_ct_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000017")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_lt);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_lt, R.drawable.card_lt_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000018")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_wr);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_wr, R.drawable.card_wr_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000019")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_lt);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_lt, R.drawable.card_lt_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000020")) { //new
            card_biz.setTextColor(Color.parseColor("#000000"));
            card_org.setText(pageItem.getCARD_ORG_NM());
            //pageContent.setBackgroundResource(R.drawable.card_nh);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_white, R.drawable.card_white_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000021")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_nh);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_nh, R.drawable.card_nh_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000060")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_ibk);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_ibk, R.drawable.card_ibk_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000061")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_sc);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_sc, R.drawable.card_sc_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000062")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_bs);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_bs, R.drawable.card_bs_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000063")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_tk);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_tk, R.drawable.card_tk_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000064")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_kn);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_kn, R.drawable.card_kn_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000065")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_wr);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_wr, R.drawable.card_wr_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000066")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_hn);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_hn, R.drawable.card_hn_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000067")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_nh);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_nh, R.drawable.card_nh_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000068")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_kb);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_kb, R.drawable.card_kb_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000069")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_sh);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_sh, R.drawable.card_sh_d);
        } else if (pageItem.getCARD_CORP_CD().equals("30000070")) { //new
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_sh);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_sh, R.drawable.card_sh_d);
        }
        return pageContent;
    }

    public void setCardBackground(String date, View view, int resId1, int resId2) {
        if (TextUtils.isEmpty(date)) {
            view.setBackgroundResource(resId2);
        } else view.setBackgroundResource(resId1);
    }


    // add image number to card layout
    public void setUpCardView(LinearLayout linearLayout, String text, int width, int height) {
        for (int i = 0; i < text.length(); i++) {
            linearLayout.addView(getViewItem(text.substring(i, i + 1), width, height));
        }
    }

    // create view number item
    public View getViewItem(String str, int width, int height) {
        int index = Integer.parseInt(str);
        int[] res = {R.drawable.cnum_0, R.drawable.cnum_1, R.drawable.cnum_2, R.drawable.cnum_3, R.drawable.cnum_4, R.drawable.cnum_5,
                R.drawable.cnum_6, R.drawable.cnum_7, R.drawable.cnum_8, R.drawable.cnum_9};
        return getImageView(res[index], width, height);
    }

    // create image view programmatically
    public ImageView getImageView(int resId, int width, int height) {
        ImageView img = new ImageView(getActivity());
        img.setLayoutParams(new LinearLayout.LayoutParams(toDP(width), toDP(height)));
        img.setImageDrawable(getActivity().getResources().getDrawable(resId));
        return img;
    }

    private int toDP(float dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }


}
