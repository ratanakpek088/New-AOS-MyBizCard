package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by Huo Chhunleng on 12/15/2016.
 */

public class TX_MYCD_MBL_P006_REQ extends TxMessage {
    public static final String TXNO= "MYCD_MBL_P006";	// 전문 구분 코드

    private TX_MYCD_MBL_P006_REQ_DATA mTxKeyData;			// 전문명세 필드

    /**
     * 전문 Data 필드 설정
     */
    private class TX_MYCD_MBL_P006_REQ_DATA {
        private String CD_USER_NO	    =	"CD_USER_NO";   //카드사용자번호
        private String SEND_DTM	        =	"SEND_DTM";		//전송희망일시
        private String NOTI_SEQ	        =	"NOTI_SEQ";		//일련번호
    }

    public TX_MYCD_MBL_P006_REQ() throws Exception{

        mTxKeyData = new TX_MYCD_MBL_P006_REQ_DATA();
        super.initSendMessage();

    }

    /**
     * 카드사용자번호
     * @param value
     * @throws JSONException
     */
    public void setCD_USER_NO(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.CD_USER_NO, value);
    }

    /**
     * 전송희망일시
     * @param value
     * @throws JSONException
     */
    public void setSEND_DTM(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.SEND_DTM, value);
    }

    /**
     * 일련번호
     * @param value
     * @throws JSONException
     */
    public void setNOTI_SEQ(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.NOTI_SEQ, value);
    }
}
