package bizplay.kosign.com.bizplay.common.ui;

/**
 * Created by Voneat-pc on 02/06/2016.
 */
public class UIConstant {

    public static class ActivityRequestCode {

        // Participant
        public static final int REQUEST_PARTICIPANT = 101;

        // Card Register
        public static final int REQEST_CODE1 = 122;
        public static final int REQEST_CODE2 = 123;

        // Card Setting Option
        public static final int REQEST_CODE3 = 124;
        public static final int REQEST_CODE6 = 127;

    }
}
