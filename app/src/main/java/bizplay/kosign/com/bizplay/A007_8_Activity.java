package bizplay.kosign.com.bizplay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

/*
 * Created by Huo Chhunleng on 20/Feb/2017.
 */
public class A007_8_Activity extends AppCompatActivity implements View.OnClickListener {

    private EditText et_content;
    private TextView tv_right;
    private ImageView iv_close;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a007_8);
       // initToolbar("4", getResources().getString(R.string.A007_8_1));
        //init_tv_title2_right(getResources().getString(R.string.A007_8_2));
        et_content = (EditText) findViewById(R.id.et_content);
        iv_close= (ImageView) findViewById(R.id.iv_left_btn);
        iv_close.setOnClickListener(this);
        tv_right= (TextView) findViewById(R.id.toolbar_title_right);

        tv_right.setOnClickListener(this);


        Bundle extras = getIntent().getExtras();
        String content = extras.getString("CONTENT");
        et_content.setText(content);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
                case R.id.toolbar_title_right:
                    Intent intent = new Intent();
                    intent.putExtra("CONTENT", et_content.getText().toString().trim());
                    setResult(RESULT_OK, intent);
                    finish();
                    break;

                case R.id.iv_left_btn:
                    finish();
                    break;
            }
    }

}
