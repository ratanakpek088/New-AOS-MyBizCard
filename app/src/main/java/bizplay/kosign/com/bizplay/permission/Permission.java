package bizplay.kosign.com.bizplay.permission;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by ChanYouvita on 9/20/16.
 */
public class Permission implements Serializable {
    private List<String> slist;

    public Permission(List<String> data) {
        this.slist = data;
    }

    public List<String> getListPermission() {
        return this.slist;
    }

    public static boolean hasPermissions(Context context, String[] permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static List<String> getPermissions(Context context, String[] data){
        final List<String> permissions = new ArrayList<>();
        for (String permission : data) {
            int hasLocationPermission = ActivityCompat.checkSelfPermission(context, permission);
            if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(permission);
            }
        }

        return permissions;
    }
}
