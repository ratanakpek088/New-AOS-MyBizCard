package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by Daravuth on 3/3/2017.
 */
public class TX_MYCD_MBL_L006_RES_BSNS_DSNC_REC extends TxMessage{
    private TX_MYCD_MBL_L006_RES_BSNS_DSNC_REC_DATA mTxKeyData;

    public TX_MYCD_MBL_L006_RES_BSNS_DSNC_REC(Object obj) throws Exception{
        mTxKeyData = new TX_MYCD_MBL_L006_RES_BSNS_DSNC_REC_DATA();
        super.initRecvMessage(obj);

    }

    private class TX_MYCD_MBL_L006_RES_BSNS_DSNC_REC_DATA {
        private String BSNS_DSNC_CD	=	"BSNS_DSNC_CD";		//용도코드
        private String BSNS_DSNC_NM	=	"BSNS_DSNC_NM";		//용도명
    }

    /**
     * 용도코드
     * @throws Exception
     */
    public String getBSNS_DSNC_CD() throws JSONException,Exception{
        return getString(mTxKeyData.BSNS_DSNC_CD);
    }

    /**
     * 용도명
     * @throws Exception
     */
    public String getBSNS_DSNC_NM() throws JSONException,Exception{
        return getString(mTxKeyData.BSNS_DSNC_NM);
    }
}
