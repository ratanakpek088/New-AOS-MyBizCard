package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by ViSAL MSi on 12/29/2016.
 */

public class TX_MYCD_MBL_L001_RES extends TxMessage {

    private  TX_MYCD_MBL_L001_RES_DATA mTxKeyData; // 전문명세 필드

    public TX_MYCD_MBL_L001_RES(Object obj) throws Exception {
        mTxKeyData = new TX_MYCD_MBL_L001_RES_DATA();
        super.initRecvMessage(obj);
    }

    /**
     * 반복부
     * @throws Exception
     */
    public TX_MYCD_MBL_L001_RES_REC getREC() throws JSONException,Exception {
        return new TX_MYCD_MBL_L001_RES_REC(getRecord(mTxKeyData.REC));
    }

    public String getCARD_MAGR_NM() throws Exception{
        return getString(mTxKeyData.CARD_MAGR_NM);
    }

    /**
     * 전문 Data 필드 설정
     */
    private class TX_MYCD_MBL_L001_RES_DATA {
        private String REC ="REC";				//반복부
        private String CARD_MAGR_NM = "CARD_MAGR_NM";
    }
}
