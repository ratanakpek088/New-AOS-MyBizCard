package bizplay.kosign.com.bizplay.item;

/**
 * Created by ViSAL MSi on 1/6/2017.
 */

public class CardDataTest {

    private String CARD_CORP_CD;
    private String CARD_NO;

    private String APPR_SUM_CNT;
    private String APPR_SUM_AMT;
    private String LST_TRAN_DTM;
    private String TRAN_CNT;
    private String NOTI_CNT;


    public CardDataTest(String CARD_CORP_CD, String CARD_NO,
                        String APPR_SUM_CNT, String APPR_SUM_AMT,
                        String LST_TRAN_DTM, String TRAN_CNT,
                        String NOTI_CNT) {
        this.CARD_CORP_CD = CARD_CORP_CD;
        this.CARD_NO = CARD_NO;
        this.APPR_SUM_CNT = APPR_SUM_CNT;
        this.APPR_SUM_AMT = APPR_SUM_AMT;
        this.LST_TRAN_DTM = LST_TRAN_DTM;
        this.TRAN_CNT = TRAN_CNT;
        this.NOTI_CNT = NOTI_CNT;
    }

    public String getCARD_CORP_CD() {
        return CARD_CORP_CD;
    }

    public void setCARD_CORP_CD(String CARD_CORP_CD) {
        this.CARD_CORP_CD = CARD_CORP_CD;
    }

    public String getCARD_NO() {
        return CARD_NO;
    }

    public void setCARD_NO(String CARD_NO) {
        this.CARD_NO = CARD_NO;
    }

    public String getAPPR_SUM_CNT() {
        return APPR_SUM_CNT;
    }

    public void setAPPR_SUM_CNT(String APPR_SUM_CNT) {
        this.APPR_SUM_CNT = APPR_SUM_CNT;
    }

    public String getAPPR_SUM_AMT() {
        return APPR_SUM_AMT;
    }

    public void setAPPR_SUM_AMT(String APPR_SUM_AMT) {
        this.APPR_SUM_AMT = APPR_SUM_AMT;
    }

    public String getLST_TRAN_DTM() {
        return LST_TRAN_DTM;
    }

    public void setLST_TRAN_DTM(String LST_TRAN_DTM) {
        this.LST_TRAN_DTM = LST_TRAN_DTM;
    }

    public String getTRAN_CNT() {
        return TRAN_CNT;
    }

    public void setTRAN_CNT(String TRAN_CNT) {
        this.TRAN_CNT = TRAN_CNT;
    }

    public String getNOTI_CNT() {
        return NOTI_CNT;
    }

    public void setNOTI_CNT(String NOTI_CNT) {
        this.NOTI_CNT = NOTI_CNT;
    }
}
