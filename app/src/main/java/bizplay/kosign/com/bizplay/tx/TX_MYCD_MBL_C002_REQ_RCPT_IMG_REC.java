package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by RatanakPek on 1/14/2017.
 */

public class TX_MYCD_MBL_C002_REQ_RCPT_IMG_REC extends TxMessage {

        private TX_MYCD_MBL_C002_RCPT_IMG_REC_DATA mTxKeyData;

        public TX_MYCD_MBL_C002_REQ_RCPT_IMG_REC(Object object) throws Exception {
            mTxKeyData = new TX_MYCD_MBL_C002_RCPT_IMG_REC_DATA();
            super.initRecvMessage(object);
        }

        private class TX_MYCD_MBL_C002_RCPT_IMG_REC_DATA{
            private String RCPT_IMG_URL="RCPT_IMG_URL";
        }
    public void setRCPT_IMG_URL(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.RCPT_IMG_URL, value);
    }

}
