package bizplay.kosign.com.bizplay.item;

/**
 * Created by Dell on 6/29/2016.
 */
public class RCPT_RFS_REC_ITEM {

    private String RCPT_RFS_HPNO;
    private String RCPT_RFS_NM;
    private String RCPT_RFS_USER_NO;

    public String getRCPT_RFS_HPNO() {
        return RCPT_RFS_HPNO;
    }

    public void setRCPT_RFS_HPNO(String RCPT_RFS_HPNO) {
        this.RCPT_RFS_HPNO = RCPT_RFS_HPNO;
    }

    public String getRCPT_RFS_NM() {
        return RCPT_RFS_NM;
    }

    public void setRCPT_RFS_NM(String RCPT_RFS_NM) {
        this.RCPT_RFS_NM = RCPT_RFS_NM;
    }

    public String getRCPT_RFS_USER_NO() {
        return RCPT_RFS_USER_NO;
    }

    public void setRCPT_RFS_USER_NO(String RCPT_RFS_USER_NO) {
        this.RCPT_RFS_USER_NO = RCPT_RFS_USER_NO;
    }
}
