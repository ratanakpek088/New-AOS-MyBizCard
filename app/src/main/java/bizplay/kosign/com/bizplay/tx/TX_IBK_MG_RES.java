package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 *
 * @title		: 게이트(MG) 전문 응답 클래스
 * @author		: kkb
 * @date		: 2015. 8. 27. 오후 5:38:51
 * @description	:
 */
public class TX_IBK_MG_RES extends TxMessage {


    private TX_BIZCARD_MG_RES_DATA mTxKeyData;	// 전문명세 필드

    public TX_IBK_MG_RES(Object obj)throws Exception {

        mTxKeyData = new TX_BIZCARD_MG_RES_DATA();
        super.initRecvMessage(obj);
    }

    /**
     * 서비스여부
     * @throws Exception
     */
    public boolean getAvailableService() throws JSONException {

        String strService = getString(mTxKeyData.C_AVAILABLE_SERVICE);
        if(strService.equals("")) {
            return false;
        }
        return Boolean.parseBoolean(strService);
    }

    /**
     * 서비스불가 대응방법
     * @throws Exception
     */
    public String getAvailableAct() throws JSONException {

        return getString(mTxKeyData.C_ACT);
    }

    /**
     * 강제 업데이트 true , false
     * @throws Exception
     */
    public boolean getUpdateClose() throws JSONException {

        String strUpdate = getString(mTxKeyData.C_UPDATE_CLOSE);
        if(strUpdate.equals("")) {
            return false;
        }
        return Boolean.parseBoolean(strUpdate);
    }

    /**
     *  하위 버전 체크
     */
    public String getMinVersion() throws JSONException {

        return getString(mTxKeyData.C_MIN_VER);
    }


    /**
     *  현재 버전
     */
    public String getProgramVersion() throws JSONException {

        return getString(mTxKeyData.C_PROGRAM_VER);
    }



    /**
     *  버전 체크 Message
     * @return
     */
    public String getUpateMessage() throws JSONException {

        return getString(mTxKeyData.C_UPDATE_ACT);
    }


    /**
     *  마켓 URL
     * @return
     */
    public String getAppstoreURL() throws JSONException {

        return getString(mTxKeyData.C_APPSTORE_URL);
    }



    /**
     *  버전 체크 Message
     * @return
     */
    public String getBizURL() throws JSONException {

        return getString(mTxKeyData.C_BIZ_URL);
    }

    /**
     *  버전 체크 Message
     * @return
     */
    public String getStartURL() throws JSONException {

        return getString(mTxKeyData.C_START_URL);
    }

    /**
     * 전문 channel id
     * @return
     * @throws JSONException
     */
    public String getChannelID() throws JSONException {

        return getString(mTxKeyData.C_CHANNEL_ID);
    }


    /**
     *  로그인 >  PW 찾기 url
     * @return
     */
    public String getForgetPwURL() throws JSONException {

        return getString(mTxKeyData.C_FORGET_PW_URL);
    }

    /**
     *  로그인 >  ID 찾기 url
     * @return
     * @throws JSONException
     */
    public String getForgetIdURL() throws JSONException {

        return getString(mTxKeyData.C_FORGET_ID_URL);
    }


    /**
     *  Session Time
     * @return
     * @throws JSONException
     */
    public String getSessionTime() throws JSONException {

        return getString(mTxKeyData.C_SESSION_TIME);
    }

    /**
     *  알림 URL
     * @return
     * @throws JSONException
     */
    public String getALIMURL() throws JSONException {

        return getString(mTxKeyData.C_ALIM_URL);
    }


    /**
     *  메뉴 정보
     * @return
     * @throws Exception
     */
    public TX_IBK_MG_RES_REC1 getMenuInfo() throws Exception {

        return new TX_IBK_MG_RES_REC1(getRecord(mTxKeyData.C_MENU_INFO));
    }

    /**
     * 공지 팝업 여부 Y/N
     * @throws Exception
     */
    public boolean getNoticeActYN() throws JSONException {

        String strNoticeYN = getString(mTxKeyData.C_ACT_YN);
        if(strNoticeYN.equals("")) {
            return false;
        }
        return Boolean.parseBoolean(strNoticeYN);
    }

    /**
     * 공지 팝업 후 화면 진입 여부
     * @throws Exception
     */
    public boolean getNoticCloseYN() throws JSONException {

        String strNoticeCloseYN = getString(mTxKeyData.C_ACT_CLOSE);
        if(strNoticeCloseYN.equals("")) {
            return false;
        }
        return Boolean.parseBoolean(strNoticeCloseYN);
    }

    /**
     *  Member Url
     * @return
     * @throws JSONException
     */
    public String getMemberUrl() throws JSONException {

        return getString(mTxKeyData.C_MEMBER_URL);
    }

    /**
     *  Portal Id
     * @return
     * @throws JSONException
     */
    public String getPortalId() throws JSONException {

        return getString(mTxKeyData.C_PORTAL_ID);
    }

    /**
     *  Update Date
     * @return
     * @throws JSONException
     */
    public String getUpdateDate() throws JSONException {

        return getString(mTxKeyData.C_UPDATE_DATE);
    }

    /**
     *  Cloude Uri
     * @return
     * @throws JSONException
     */
    public String getCloudeUri() throws JSONException {

        return getString(mTxKeyData.C_CLOUDE_URI);
    }

    /**
     *  Cloude Key
     * @return
     * @throws JSONException
     */
    public String getCloudeKey() throws JSONException {

        return getString(mTxKeyData.C_CLOUDE_KEY);
    }

    /**
     *  Qna url Key
     * @return
     * @throws JSONException
     */
    public String getQanUrl() throws JSONException {

        return getString(mTxKeyData.C_QNA_URL);
    }

    /**
     *  Event url Key
     * @return
     * @throws JSONException
     */
    public String getEventYN() throws JSONException {

        return getString(mTxKeyData.C_EVENT_YN);
    }

    /**
     *  Event url Key
     * @return
     * @throws JSONException
     */
    public String getEventUrl() throws JSONException {

        return getString(mTxKeyData.C_EVENT_RUL);
    }
    /**
     * 전문 Data 필드 설정
     * @author nryoo
     */
    private class TX_BIZCARD_MG_RES_DATA {

        private String C_AVAILABLE_SERVICE = "c_available_service";  // 서비스여부
        private String C_ACT = "c_act"; 						     // 서비스불가 대응방법
        private String C_MIN_VER = "c_minimum_ver";					 // 프로그램 하위 버젼
        private String C_PROGRAM_VER = "c_program_ver";				// 현재 프로그램 버전
        private String C_MENU_INFO = "_app_info";					 // 프로그램 메뉴 정보

        private String C_UPDATE_CLOSE = "c_update_close";			 // 강제 업데이트
        private String C_UPDATE_ACT = "c_update_act";				 // 버전 체크 Message
        private String C_APPSTORE_URL = "c_appstore_url";			//  업데이트 url


        private String C_BIZ_URL = "c_bizplay_url";					// 전문 URL
        private String C_START_URL = "c_start_url";					// 인트로 후 메인 url
        private String C_CHANNEL_ID = "c_channel_id";				// channel_id

        private String C_FORGET_PW_URL = "c_forget_pw_url";			// 로그인 >  pw 찾기 url
        private String C_FORGET_ID_URL = "c_forget_id_url";			// 로그인 >  id 찾기 url

        private String C_SESSION_TIME ="c_session_time";			// session time
        private String C_ALIM_URL ="c_alim_url";		// c_alim_url

        // [2015.12.09]
        private String C_ACT_YN = "c_act_yn";		// 공지 Y/N
        private String C_ACT_CLOSE = "c_act_close"; // 공지 팝업 후 처리(true: 강제종료 , false : 화면진)

        private String C_MEMBER_URL = "c_member_url";
        private String C_PORTAL_ID = "c_portal_id";
        private String C_UPDATE_DATE = "c_update_date";

        private String C_CLOUDE_URI = "c_cloude_uri";
        private String C_CLOUDE_KEY = "c_cloude_key";
        private String C_QNA_URL = "c_qna_url";

        private String C_EVENT_YN = "c_event_yn";
        private String C_EVENT_RUL = "c_event_url";

    }





}
