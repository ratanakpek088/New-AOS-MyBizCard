package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by User on 7/18/2016.
 */
public class TX_MYCD_MBL_L005_RES extends TxMessage {
    private TX_MYCD_MBL_L005_RES_DATA mTxKeyData;

    public TX_MYCD_MBL_L005_RES(Object object) throws Exception {
        mTxKeyData = new TX_MYCD_MBL_L005_RES_DATA();
        super.initRecvMessage(object);
    }

    public String getTOTL_RSLT_CNT() throws JSONException {
        return getString(mTxKeyData.TOTL_RSLT_CNT);
    }
    public String getTOTL_PAGE_NCNT() throws JSONException{
        return getString(mTxKeyData.TOTL_PAGE_NCNT);
    }
    public TX_MYCD_MBL_L005_RES_REC getTRAN_KIND_REC() throws Exception {
        return new TX_MYCD_MBL_L005_RES_REC(getRecord(mTxKeyData.CONT_INFO_REC));
    }

    private class TX_MYCD_MBL_L005_RES_DATA{
        private String TOTL_RSLT_CNT = "TOTL_RSLT_CNT";
        private String TOTL_PAGE_NCNT = "TOTL_PAGE_NCNT";
        private String CONT_INFO_REC = "CONT_INFO_REC";
    }
}
