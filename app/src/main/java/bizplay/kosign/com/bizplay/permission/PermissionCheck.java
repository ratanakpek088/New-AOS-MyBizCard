package bizplay.kosign.com.bizplay.permission;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;


import java.util.ArrayList;
import java.util.List;

import bizplay.kosign.com.bizplay.popup.OnPopupWindowListener;

/*
 * Created by ChanYouvita on 9/20/16.
 */
public class PermissionCheck extends AppCompatActivity {
    private static final int REQUEST_CODE_SOME_FEATURES_PERMISSIONS = 0;
    private static final int REQUEST_CODE_SOME_SETTINGS_PERMISSIONS = 1;
    private String[] allPermissions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            // here is your permission need to access
            Permission dataWrapper = (Permission) getIntent().getSerializableExtra("PERMISSION");

            final List<String> permissions = new ArrayList<>();

            if (dataWrapper.getListPermission() != null) {
                for (String permission : dataWrapper.getListPermission()) {
                    int hasLocationPermission = ActivityCompat.checkSelfPermission(this, permission);
                    if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
                        permissions.add(permission);
                    }
                }
            }

            // convert list to array
            allPermissions = permissions.toArray(new String[permissions.size()]);

            // checking permission
            if(!permissions.isEmpty()) {
                for (String deny : permissions) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,deny)) {
                        // show when user clicked deny
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (!isFinishing()){
                                    String message = getIntent().getStringExtra("SMS");
                                    final PermissionPopup popup = new PermissionPopup(PermissionCheck.this,permissions,message);
                                    popup.setOnPopupListener(new OnPopupWindowListener() {
                                        @Override
                                        public void onPopupPressed() {
                                            // setting permissions
                                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                    Uri.parse("package:" + getPackageName()));
                                            startActivityForResult(intent,REQUEST_CODE_SOME_SETTINGS_PERMISSIONS);
                                            popup.dismiss();
                                        }

                                        @Override
                                        public void onPopupClose() {
                                            // cancel permissions
                                            setResult(RESULT_CANCELED);
                                            finish();
                                            popup.dismiss();
                                        }
                                    });
                                    popup.show();
                                }
                            }
                        });
                        break;
                    } else{
                        ActivityCompat.requestPermissions(this, permissions.toArray(new String[permissions.size()]), REQUEST_CODE_SOME_FEATURES_PERMISSIONS);
                        break;
                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        try {

            switch (requestCode) {
                case REQUEST_CODE_SOME_FEATURES_PERMISSIONS: {
                    boolean hasPermission = false;
                    for (int i = 0; i < permissions.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.d("Permissions", "Permission Granted: " + permissions[i]);
                            hasPermission = true;
                            break;
                        } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                            Log.d("Permissions", "Permission Denied: " + permissions[i]);
                            hasPermission = false;
                        }
                    }

                    // result check
                    if (hasPermission){
                        setResult(RESULT_OK);
                        finish();
                    }else{
                        setResult(RESULT_CANCELED);
                        finish();
                    }
                }
                break;
                default: {
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SOME_SETTINGS_PERMISSIONS) {
            if (Permission.hasPermissions(this,allPermissions)) {
                setResult(RESULT_OK);
                finish();
            }else{
                finish();
            }
        }
    }
}
