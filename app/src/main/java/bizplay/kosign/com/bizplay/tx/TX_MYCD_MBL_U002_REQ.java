package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by Huo Chhunleng on 7/21/2016.
 */
public class TX_MYCD_MBL_U002_REQ extends TxMessage {

    public static final String TXNO = "MYCD_MBL_U002";	// 전문 구분 코드
    private TX_MYCD_MBL_U002_REQ_DATA mTxKeyData;		// 전문명세 필드

    public TX_MYCD_MBL_U002_REQ() throws Exception{
        mTxKeyData = new TX_MYCD_MBL_U002_REQ_DATA();
        super.initSendMessage();
    }

    /**
     * 전문 Data 필드 설정
     */
    private class TX_MYCD_MBL_U002_REQ_DATA {
        private String CARD_CORP_CD	    =	"CARD_CORP_CD";
        private String CARD_NO	        =	"CARD_NO";
        private String APV_DT	        =	"APV_DT";
        private String APV_SEQ	        =	"APV_SEQ";
        private String APV_TM	        =	"APV_TM";
        private String APV_NO           =   "APV_NO";
    }


    /**
     * @param value
     * @throws JSONException
     */
    public void setCARD_CORP_CD(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.CARD_CORP_CD, value);
    }

    /**
     * @param value
     * @throws JSONException
     */
    public void setCARD_NO(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.CARD_NO, value);
    }

    /**
     * @param value
     * @throws JSONException
     */
    public void setAPV_DT(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.APV_DT, value);
    }

    /**
     * @param value
     * @throws JSONException
     */
    public void setAPV_SEQ(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.APV_SEQ, value);
    }

    /**
     * @param value
     * @throws JSONException
     */
    public void setAPV_TM(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.APV_TM, value);
    }

    /**
     * @param value
     * @throws JSONException
     */
    public void setAPV_NO(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.APV_NO, value);
    }

}
