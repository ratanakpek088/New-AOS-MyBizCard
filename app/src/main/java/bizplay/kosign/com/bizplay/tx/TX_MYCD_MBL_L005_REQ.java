package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by User on 7/18/2016.
 */
public class TX_MYCD_MBL_L005_REQ extends TxMessage {
    public static final String TXNO = "MYCD_MBL_L005";
    private TX_MYCD_MBL_L005_REQ_DATA mTxKeyData;

    public TX_MYCD_MBL_L005_REQ() throws Exception {
        mTxKeyData = new TX_MYCD_MBL_L005_REQ_DATA();
        super.initSendMessage();
    }

    public void setSRCH_WORD(String value){
        mSendMessage.put(mTxKeyData.SRCH_WORD, value);

    }
    public void setPAGE_PER_CNT(String value){
        mSendMessage.put(mTxKeyData.PAGE_PER_CNT, value);
    }
    public void setPAGE_NO(String value){
        mSendMessage.put(mTxKeyData.PAGE_NO, value);
    }

    private class TX_MYCD_MBL_L005_REQ_DATA{
        private String SRCH_WORD = "SRCH_WORD";
        private String PAGE_PER_CNT = "PAGE_PER_CNT";
        private String PAGE_NO = "PAGE_NO";
    }

}
