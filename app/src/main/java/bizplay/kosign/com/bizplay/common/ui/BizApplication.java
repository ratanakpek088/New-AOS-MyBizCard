package bizplay.kosign.com.bizplay.common.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.multidex.MultiDexApplication;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import bizplay.kosign.com.bizplay.conf.Conf;

/*
 * @title		: Application 상속 클래스
 * @author		: nryoo
 * @date		: 2015. 07. 20
 * @description	: Application 생성시 필요한 Data 및 Application전체에서 사용될수 있는 Data 처리.
 * 
 */
public class BizApplication extends MultiDexApplication {

//	private static HashMap<String, Object> mMemoyPrefMap;

	private Bundle gcmBundle = null;

	@Override
	public void onCreate() {
		super.onCreate();
		//+ 앱 최초 구동시 쿠키정보 초기화 한다.
//		initializeCookie(this);
		
//		mMemoyPrefMap = new HashMap<>();

		initImageLoader(getApplicationContext());

		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread thread, Throwable ex) {
				if (Conf.ISRELEASE) {
					/*FirebaseCrash.report(ex);*/
				}
			}
		});
	}


	@Override
	public void onLowMemory() {
		super.onLowMemory();
	}
	
	
//	/**
//	 * Cookie 정보를 초기화 한다.
//	 * @param context Context
//	 */
//	public static void initializeCookie(Context context) {
//		CookieSyncManager.createInstance(context);
//
//		CookieManager cookieManager = CookieManager.getInstance();
//		cookieManager.setAcceptCookie(true);
//		cookieManager.removeAllCookie();
//	}
	
	
	/*
	 *  앱 구동시  메모리영역에 임시  저장하고자 하는 data 
	 * @return
	 */
//	public static HashMap<String, Object> getMemoryPrefMap() {
//
//		return mMemoyPrefMap;
//	}
	
	
	/*
	 *  앱 구동시  메모리영역에 임시  저장하고자 하는 data 
	 * @return
	 */
//	public static void setMap(HashMap<String, Object> map) {
//
//		mMemoyPrefMap = map;
//	}

	public static void initImageLoader(Context context) {
		// This configuration tuning is custom. You can tune every option, you may tune some of them,
		// or you can create default configuration by
		//  ImageLoaderConfiguration.createDefault(this);
		// method.
		ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
		config.threadPriority(Thread.NORM_PRIORITY - 2);
		config.denyCacheImageMultipleSizesInMemory();
		config.tasksProcessingOrder(QueueProcessingType.LIFO);
		config.writeDebugLogs(); // Remove for release app

		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config.build());
	}

	public Bundle getGCMBundle(){
		return this.gcmBundle;
	}

	public void setGCMBundle(Bundle value){
		this.gcmBundle = value;
	}

		
}
