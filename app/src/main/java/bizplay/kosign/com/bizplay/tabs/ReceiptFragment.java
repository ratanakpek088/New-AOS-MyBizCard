package bizplay.kosign.com.bizplay.tabs;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

import bizplay.kosign.com.bizplay.A007_1;
import bizplay.kosign.com.bizplay.A007_2Activity;
import bizplay.kosign.com.bizplay.MainActivity;
import bizplay.kosign.com.bizplay.R;
import bizplay.kosign.com.bizplay.adapters.ListCardAdapter;
import bizplay.kosign.com.bizplay.adapters.ListReceiptAdapter;
import bizplay.kosign.com.bizplay.constant.Constants;
import bizplay.kosign.com.bizplay.item.Card;
import bizplay.kosign.com.bizplay.item.Receipt;
import bizplay.kosign.com.bizplay.item.ReceiptRec;
import bizplay.kosign.com.bizplay.popup.SnackBar;
import bizplay.kosign.com.bizplay.tran.ComTran;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L002_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L002_RES;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L002_RES_REC;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReceiptFragment extends Fragment implements View.OnClickListener, MainActivity.OnReceiptReload, ComTran.OnComTranListener, AbsListView.OnScrollListener, SwipeRefreshLayout.OnRefreshListener {

    private static ReceiptFragment instance = null;
    private RelativeLayout containerCardLists, hasRcpt, noRcpt, cardListWrapper, filterWrapper;
    private ListView mReceiptList;
    private LinearLayout mCarListView;
    public ImageView showList;
    private ImageView showMore;
    private View view;
    private ComTran mComTran;
    public Receipt mReceiptOfCard;
    private ListReceiptAdapter mListReceiptAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private String numToReq = "0";
    Card card;

    private boolean isNoMore = false;
    private boolean isAllowToMove = false;
    private boolean isFirstRequest = true;
    private int preLast;
    static ViewGroup viewGroup;

    public ReceiptFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mComTran = new ComTran(getActivity(), this);
        mReceiptOfCard = new Receipt();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_receipt, container, false);
        viewGroup = container;
//        View testView = view.findViewById(R.id.testDetailReceipt);
        mCarListView = (LinearLayout) view.findViewById(R.id.list_card_of_rcpt);
        containerCardLists = (RelativeLayout) view.findViewById(R.id.container_card_lists);
        mReceiptList = (ListView) view.findViewById(R.id.list_receipt);
        mReceiptList.setOnScrollListener(this);

        hasRcpt = (RelativeLayout) view.findViewById(R.id.receipts_of_card);
        noRcpt = (RelativeLayout) view.findViewById(R.id.no_receipt_of_card);
        noRcpt.setOnClickListener(this);

        cardListWrapper = (RelativeLayout) view.findViewById(R.id.txt_CARD_NICK_NM_wrapper);
        filterWrapper = (RelativeLayout) view.findViewById(R.id.wrapper_filter);
        cardListWrapper.setOnClickListener(this);

        showList = (ImageView) view.findViewById(R.id.show_list);
        showMore = (ImageView) view.findViewById(R.id.show_more);
        showList.setOnClickListener(this);
        showMore.setOnClickListener(this);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.list_receipt_refresh);
        mSwipeRefreshLayout.setColorSchemeColors(Color.parseColor("#4286f5"));
        mSwipeRefreshLayout.setOnRefreshListener(this);

        numToReq = "0";
        isAllowToMove = false;


        view.findViewById(R.id.txt_refresh_card).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshCard();
            }
        });

//        testView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(getActivity(), A007_1.class));
//            }
//        });

        return view;
    }

    public static ReceiptFragment getInstance() {
        try {
            if (instance == null) {
                // new instance
                instance = new ReceiptFragment();

                // sets data to bundle
                Bundle bundle = new Bundle();

                // set data to fragment
                instance.setArguments(bundle);

                return instance;
            } else {

                return instance;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean isShow = false;
//    private int itemCardCount = 5;

    int count;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_CARD_NICK_NM_wrapper:
            case R.id.show_list:
                if (isShow) {
                    isShow = false;
                    showList.setImageResource(R.drawable.listbox_darr_icon);
                    showMore.setImageResource(R.drawable.listbox_expand_icon);
                    mCarListView.removeAllViews();
                    showMore.setVisibility(View.GONE);

                } else {
                    isShow = true;
                    showList.setImageResource(R.drawable.listbox_uarr_icon);
                    mCarListView.removeAllViews();

                    count = Card.mCardList.size();
                    if (count > 5) count = 5;

                    for (int i = 0; i < count; i++) {
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        LinearLayout item = (LinearLayout) inflater.inflate(R.layout.item_of_card_list, null);
                        TextView cardNO = (TextView) item.findViewById(R.id.card_no_of_list_card);
                        TextView cardNickName = (TextView) item.findViewById(R.id.txt_CARD_NICK_NM_card_list);

                        String string = Card.mCardList.get(i).getCARD_NO();
                        String last4 = "";
                        if (string.length() == 16) { // if card has 16 digit
                            last4 = string.substring(string.length() - 4);
                        } else if (string.length() == 15) { // if card has 15 digit
                            last4 = string.substring(string.length() - 3);
                        } else if (string.length() == 13) { // if card has 13 digit
                            last4 = string.substring(string.length() - 2);
                        }
                        String first4 = string.substring(0, Math.min(string.length(), 4));
                        cardNO.setText(first4 + " - **** - **** - " + last4);

                        cardNickName.setText(Card.mCardList.get(i).getCARD_NICK_NM());

                        if (itemClicked == i) {
                            item.setBackgroundResource(R.color.colorPrimaryDark);
                        }

                        final int temp = i;

                        item.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) { // when click item in card list in receipt ...
                                whenItemCardListClick(temp);
                            }
                        });


                        mCarListView.addView(item);
                    }
                    if (Card.mCardList.size() > 5) {
                        showMore.setVisibility(View.VISIBLE);
                    } else {
                        showMore.setImageResource(R.drawable.listbox_collapse_icon);
                        showMore.setVisibility(View.VISIBLE);
                    }

                }
                break;
            case R.id.show_more:
                if (Card.mCardList.size() == count) {
                    showList.setImageResource(R.drawable.listbox_darr_icon);
                    mCarListView.removeAllViews();
                    showMore.setVisibility(View.GONE);
                    showMore.setImageResource(R.drawable.listbox_expand_icon);
                } else {
                    mCarListView.removeAllViews();
                    count += 5;
                    if (count > Card.mCardList.size()) {
                        count = count - 5;
                        count += Card.mCardList.size() - count;
                    }

                    for (int i = 0; i < count; i++) {
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        LinearLayout item = (LinearLayout) inflater.inflate(R.layout.item_of_card_list, null);
                        TextView cardNO = (TextView) item.findViewById(R.id.card_no_of_list_card);
                        TextView cardNickName = (TextView) item.findViewById(R.id.txt_CARD_NICK_NM_card_list);

                        String string = Card.mCardList.get(i).getCARD_NO();
                        String last4 = "";
                        if (string.length() == 16) { // if card has 16 digit
                            last4 = string.substring(string.length() - 4);
                        } else if (string.length() == 15) { // if card has 15 digit
                            last4 = string.substring(string.length() - 3);
                        } else if (string.length() == 13) { // if card has 13 digit
                            last4 = string.substring(string.length() - 2);
                        }
                        String first4 = string.substring(0, Math.min(string.length(), 4));
                        cardNO.setText(first4 + " - **** - **** - " + last4);

                        cardNickName.setText(Card.mCardList.get(i).getCARD_NICK_NM());

                        if (itemClicked == i) {
                            item.setBackgroundResource(R.color.colorPrimaryDark);
                        }

                        final int temp = i;

                        item.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) { // when click item in card list in receipt ...
                                whenItemCardListClick(temp);
                            }
                        });


                        mCarListView.addView(item);
                    }
                    if (Card.mCardList.size() == count) {
                        showMore.setImageResource(R.drawable.listbox_collapse_icon);
                    }

                }
                break;
            case R.id.no_receipt_of_card:
                if (isShow)
                    showList.callOnClick();
                break;
        }
    }

    int itemClicked;

    public void whenItemCardListClick(int position) {
        itemClicked = position;
        card = Card.mCardList.get(position);

        String string = card.getCARD_NO();
        String last4 = "";
        if (string.length() == 16) { // if card has 16 digit
            last4 = string.substring(string.length() - 4);
        } else if (string.length() == 15) { // if card has 15 digit
            last4 = string.substring(string.length() - 3);
        } else if (string.length() == 13) { // if card has 13 digit
            last4 = string.substring(string.length() - 2);
        }

        String first4 = string.substring(0, Math.min(string.length(), 4));

        TextView textNo = (TextView) view.findViewById(R.id.card_no);
        textNo.setText(first4 + " - **** - **** - " + last4);

        TextView textNNM = (TextView) view.findViewById(R.id.txt_CARD_NICK_NM);
        textNNM.setText(Card.mCardList.get(position).getCARD_NICK_NM());

        if (textNNM.getText().toString().equals("")) {
            textNNM.setVisibility(View.GONE);
        } else {
            textNNM.setVisibility(View.VISIBLE);
        }

        isShow = false;
        showList.setImageResource(R.drawable.listbox_darr_icon);
        mCarListView.removeAllViews();
        showMore.setVisibility(View.GONE);
        showMore.setImageResource(R.drawable.listbox_expand_icon);
        if (Card.mCardList.size() > 5) {
            showMore.setVisibility(View.GONE);
        }

        requestReceiptOfSelectedCard(Card.mCardList.get(position)); // request list of receipt

        MainActivity m = (MainActivity) getActivity(); // to go main to change index of card when swipe back to My Card Frag..
        m.catchPositionCard(position);

        preLast = 0;
    }


    @Override
    public void reloadSelectedCardLists(int position) {
        Log.d("Receipt Fragment", "called11");
        if (Card.mCardList.size() != 0) {
            if (Card.mCardList.size() == 1) {
                containerCardLists.setVisibility(View.GONE);
                view.findViewById(R.id.wrapperCheat).setVisibility(View.GONE);
                whenItemCardListClick(position);
            } else {
                view.findViewById(R.id.wrapperCheat).setVisibility(View.VISIBLE);
                containerCardLists.setVisibility(View.VISIBLE);
                whenItemCardListClick(position);
//                String string = Card.mCardList.get(position).getCARD_NO();
//                String last4 = "";
//                if (string.length() == 16) { // if card has 16 digit
//                    last4 = string.substring(string.length() - 4);
//                } else if (string.length() == 15) { // if card has 15 digit
//                    last4 = string.substring(string.length() - 3);
//                } else if (string.length() == 13) { // if card has 13 digit
//                    last4 = string.substring(string.length() - 2);
//                }
//
//                String first4 = string.substring(0, Math.min(string.length(), 4));
//
//                TextView textNo = (TextView) view.findViewById(R.id.card_no);
//                textNo.setText(first4 + " - **** - **** - " + last4);
//
//                TextView textNNM = (TextView) view.findViewById(R.id.txt_CARD_NICK_NM);
//                textNNM.setText(Card.mCardList.get(position).getCARD_NICK_NM());
            }
        }else{
            containerCardLists.setVisibility(View.GONE);
            view.findViewById(R.id.wrapperCheat).setVisibility(View.GONE);
            view.findViewById(R.id.ll_no_card_in_receipt).setVisibility(View.VISIBLE);
            Button btnManager = (Button) view.findViewById(R.id.manager);
            btnManager.setText("관리자: " + Constants.MycardInfo.CARD_MAGR_NM);
        }

    }


    @Override
    public void reloadReceiptListsOfCard(Object object) {
        requestReceiptOfSelectedCard(object);
    }

    public void requestReceiptOfSelectedCard(Object object) {
        Log.d("res", "requestReceiptOfSelectedCard");
        Log.d("Num To Req: ", numToReq + "");
        card = (Card) object;
        isFirstRequest = true;
        try {
            TX_MYCD_MBL_L002_REQ req = new TX_MYCD_MBL_L002_REQ();
            req.setINQ_CD(numToReq);
            req.setCARD_CORP_CD(card.getCARD_CORP_CD());
            req.setCARD_NO(card.getCARD_NO());

            mComTran.requestData(TX_MYCD_MBL_L002_REQ.TXNO, req.getSendMessage(), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void requestReceiptOfSelectedCardByPage(Object object) {
        Log.d("req", "requestReceiptOfSelectedCardByPage");
        Receipt receipt = (Receipt) object;
        isFirstRequest = false;

//        Log.d("REQ DATA: ", "DT: " + receipt.getINQ_NEXT_APV_DT() + "NO: " + receipt.getINQ_NEXT_APV_NO() + "SEQ: " + receipt.getINQ_NEXT_APV_SEQ() + "TM: " + receipt.getINQ_NEXT_APV_TM());

        if (receipt.getINQ_NEXT_APV_DT().equals("")
                || receipt.getINQ_NEXT_APV_NO().equals("")
                || receipt.getINQ_NEXT_APV_SEQ().equals("")
                || receipt.getINQ_NEXT_APV_TM().equals("")) {
        } else {
//            Log.d("REQ DATA: ", " DT: " + receipt.getINQ_NEXT_APV_DT() + " NO: " + receipt.getINQ_NEXT_APV_NO() + " SEQ: " + receipt.getINQ_NEXT_APV_SEQ() + " TM: " + receipt.getINQ_NEXT_APV_TM());
            try {
                TX_MYCD_MBL_L002_REQ req = new TX_MYCD_MBL_L002_REQ();
                Log.d("Num To Req: ", numToReq + "");
                req.setINQ_CD(numToReq);
                req.setCARD_CORP_CD(card.getCARD_CORP_CD());
                req.setCARD_NO(card.getCARD_NO());
                req.setINQ_NEXT_APV_DT(receipt.getINQ_NEXT_APV_DT());
                req.setINQ_NEXT_APV_SEQ(receipt.getINQ_NEXT_APV_SEQ());
                req.setINQ_NEXT_APV_TM(receipt.getINQ_NEXT_APV_TM());
                req.setINQ_NEXT_APV_NO(receipt.getINQ_NEXT_APV_NO());

                mComTran.requestData(TX_MYCD_MBL_L002_REQ.TXNO, req.getSendMessage(), true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("rtk", "Receipt Fragement Pauce");
    }


    @Override
    public void onTranResponse(String tranCode, Object object) throws Exception {
        if (tranCode == TX_MYCD_MBL_L002_REQ.TXNO) {
            TX_MYCD_MBL_L002_RES res = new TX_MYCD_MBL_L002_RES(object);

            //show snackbar
            Log.e("rtk", "SHOW: " + Constants.ReceiptInfo.SHOW_SEND_SUCCESS_SNACKBAR);
            if (Constants.ReceiptInfo.SHOW_SEND_SUCCESS_SNACKBAR == true) {
                showSnackBar();
                Constants.ReceiptInfo.SHOW_SEND_SUCCESS_SNACKBAR = false;
            }

            if (!res.getINQ_ITRN_SUB_ROWCOUNT().equals("0")) {
                TX_MYCD_MBL_L002_RES_REC rec = res.getINQ_ITRN_SUB();

                Log.d("REC: ", rec.getMessageToString());

                mReceiptOfCard.setINQ_NEXT_APV_DT(res.getINQ_NEXT_APV_DT());
                mReceiptOfCard.setINQ_NEXT_APV_TM(res.getINQ_NEXT_APV_TM());
                mReceiptOfCard.setINQ_NEXT_APV_SEQ(res.getINQ_NEXT_APV_SEQ());
                mReceiptOfCard.setINQ_NEXT_APV_NO(res.getINQ_NEXT_APV_NO());
                mReceiptOfCard.setINQ_ITRN_SUB_ROWCOUNT(res.getINQ_ITRN_SUB_ROWCOUNT());
                mReceiptOfCard.setTRAN_CNT(res.getTRAN_CNT());
                mReceiptOfCard.setNOTI_CNT(res.getNOTI_CNT());

                ArrayList<ReceiptRec> receiptRecs = new ArrayList<>();

                for (int i = 0; i < rec.getLength(); i++) {
                    rec.move(i);
                    receiptRecs.add(new ReceiptRec(
                            rec.getCARD_NO().equals("null") ? "" : rec.getCARD_NO(),
                            rec.getAPV_DT().equals("null") ? "" : rec.getAPV_DT(),
                            rec.getAPV_SEQ().equals("null") ? "" : rec.getAPV_SEQ(),
                            rec.getAPV_TM().equals("null") ? "" : rec.getAPV_TM(),
                            rec.getAPV_NO().equals("null") ? "" : rec.getAPV_NO(),
                            rec.getAPV_AMT().equals("null") ? "" : rec.getAPV_AMT(),
                            rec.getAPV_CAN_YN().equals("null") ? "" : rec.getAPV_CAN_YN(),
                            rec.getAPV_CAN_DT().equals("null") ? "" : rec.getAPV_CAN_DT(),
                            rec.getITLM_MMS_CNT().equals("null") ? "" : rec.getITLM_MMS_CNT(),
                            rec.getSETL_SCHE_DT().equals("null") ? "" : rec.getSETL_SCHE_DT(),
                            rec.getMEST_NM().equals("null") ? "" : rec.getMEST_NM(),
                            rec.getMEST_NO().equals("null") ? "" : rec.getMEST_NO(),
                            rec.getMEST_BIZ_NO().equals("null") ? "" : rec.getMEST_BIZ_NO(),
                            rec.getRCPT_TX_STS().equals("null") ? "" : rec.getRCPT_TX_STS(),
                            rec.getRCPT_RFS_STS().equals("null") ? "" : rec.getRCPT_RFS_STS(),
                            rec.getCARD_NICK_NM().equals("null") ? "" : rec.getCARD_NICK_NM(),
                            rec.getDAY_NM().equals("null") ? "" : rec.getDAY_NM(),
                            rec.getTRAN_KIND_CD().equals("null") ? "" : rec.getTRAN_KIND_CD(),
                            rec.getTRAN_KIND_NM().equals("null") ? "" : rec.getTRAN_KIND_NM(),
                            "MYCD_MBL_R008"

                    ));
                }

                MainActivity m = (MainActivity) getActivity();
                m.lookingForNewReceipt(Integer.parseInt(res.getTRAN_CNT()) != 0);

                if (isFirstRequest) {
                    Log.d("req", "who call it");
                    mListReceiptAdapter = new ListReceiptAdapter(getContext(), card, mReceiptOfCard);
                    mReceiptOfCard.setReceiptRecs(receiptRecs);
                    mReceiptList.setDivider(null);
                    mReceiptList.setAdapter(mListReceiptAdapter);

                } else {
                    for (int i = 0; i < receiptRecs.size(); i++) {
                        mReceiptOfCard.getReceiptRecs().add(receiptRecs.get(i));
                    }
                    mListReceiptAdapter.notifyDataSetChanged();
                }

                if (!(res.getINQ_NEXT_APV_DT().equals("")
                        || res.getINQ_NEXT_APV_NO().equals("")
                        || res.getINQ_NEXT_APV_SEQ().equals("")
                        || res.getINQ_NEXT_APV_TM().equals(""))) {

                    isAllowToMove = true;

                } else {
                    isAllowToMove = false;
                }
                Log.d("req", "set Allow To Move " + isAllowToMove);

                hasRcpt.setVisibility(View.VISIBLE);
                noRcpt.setVisibility(View.GONE);

                mSwipeRefreshLayout.setRefreshing(false);
            } else {
                hasRcpt.setVisibility(View.GONE);
                noRcpt.setVisibility(View.VISIBLE);
            }


            //Snack bar action
            if(Constants.ReceiptInfo.SHOW_SEND_SUCCESS_SNACKBAR==true){
                showSnackBar();
                Constants.ReceiptInfo.SHOW_SEND_SUCCESS_SNACKBAR=false;
            }

            if(Constants.ReceiptInfo.SHOW_UPDATE_SNACKBAR==true){
                showUpdatedSnackBar();
                Constants.ReceiptInfo.SHOW_UPDATE_SNACKBAR=false;
            }

            if(Constants.ReceiptInfo.SHOW_MOVE_SUCCESS_SNACKBAR==true){
                showMoveSuccess();
                Constants.ReceiptInfo.SHOW_MOVE_SUCCESS_SNACKBAR=false;
            }

            if(Constants.ReceiptInfo.SHOW_CANCEL_SUCESS_SNACKBAR==true){
                showCancelSucess();
                Constants.ReceiptInfo.SHOW_CANCEL_SUCESS_SNACKBAR=false;
            }
        }
    }

    public void setDataToReceipt() {

    }

    public void setSelectedReceiptToDetail(int position, String RCPT_TX_STS) {
        if (isShow) {
            showList.callOnClick();
        } else {
            if (RCPT_TX_STS.equals("0")) {
                Intent intent;
                intent = new Intent(getActivity(), A007_1.class);
                intent.putExtra("POSITION", position);
                intent.putExtra("KEY_API", "MYCD_MBL_R008");
                getActivity().startActivity(intent);
            } else {
                Intent intent1;
                intent1 = new Intent(getActivity(), A007_2Activity.class);
                intent1.putExtra("POSITION", position);
                intent1.putExtra("KEY_API", "MYCD_MBL_R008");
                getActivity().startActivity(intent1);
            }
        }
    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView absListView, final int firstVisibleItem, final int visibleItemCount, final int totalItemCount) {
        Log.d("req", "requestReceiptOfSelectedCardByPage onScroll");
        switch (absListView.getId()) {
            case R.id.list_receipt:

                if(isShow){
                    showList.callOnClick();
                }

                if (isAllowToMove) {
                    Log.d("req", "requestReceiptOfSelectedCardByPage Allow to move");
                    final int lastItem = firstVisibleItem + visibleItemCount;

                    if (lastItem == totalItemCount) {
                        if (preLast != lastItem) //to avoid multiple calls for last item --> item receipts have no more...
                        {
                            Log.d("Last", "Last");
                            preLast = lastItem;
                            requestReceiptOfSelectedCardByPage(mReceiptOfCard);
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onRefresh() {
        Log.d("req", "onRefresh");
        isFirstRequest = true;
        try {
            TX_MYCD_MBL_L002_REQ req = new TX_MYCD_MBL_L002_REQ();
            Log.d("Num To Req: ", numToReq + "");
            req.setINQ_CD(numToReq);
            req.setCARD_CORP_CD(card.getCARD_CORP_CD());
            req.setCARD_NO(card.getCARD_NO());

            mComTran.requestData(TX_MYCD_MBL_L002_REQ.TXNO, req.getSendMessage(), false);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void refreshCard() {

        if(isShow)
            showList.callOnClick();

        Log.d("req", "onRefresh");
        isFirstRequest = true;
        try {
            TX_MYCD_MBL_L002_REQ req = new TX_MYCD_MBL_L002_REQ();
            Log.d("Num To Req: ", numToReq + "");
            req.setINQ_CD(numToReq);
            req.setCARD_CORP_CD(card.getCARD_CORP_CD());
            req.setCARD_NO(card.getCARD_NO());

            mComTran.requestData(TX_MYCD_MBL_L002_REQ.TXNO, req.getSendMessage(), true);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void filterReceipt(String inCase) {
        switch (inCase) {
            case "0":
                filterWrapper.setVisibility(View.GONE);
                numToReq = "0";
                break;
            case "1":
                filterWrapper.setVisibility(View.VISIBLE);
                ((TextView) view.findViewById(R.id.text_filter)).setText("신규영수증");
                numToReq = "1";
                break;
            case "2":
                filterWrapper.setVisibility(View.VISIBLE);
                ((TextView) view.findViewById(R.id.text_filter)).setText("카드 취소내역");
                numToReq = "2";
                break;
            case "3":
                filterWrapper.setVisibility(View.VISIBLE);
                ((TextView) view.findViewById(R.id.text_filter)).setText("카드 개인사용분");
                numToReq = "3";
                break;
        }

        Log.d("req", "onRefresh");
        isFirstRequest = true;
        try {
            TX_MYCD_MBL_L002_REQ req = new TX_MYCD_MBL_L002_REQ();
            Log.d("Num To Req: ", numToReq + "");
            req.setINQ_CD(numToReq);
            req.setCARD_CORP_CD(card.getCARD_CORP_CD());
            req.setCARD_NO(card.getCARD_NO());

            mComTran.requestData(TX_MYCD_MBL_L002_REQ.TXNO, req.getSendMessage(), false);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onResume() {
        super.onResume();
        try {
            Log.e("rtk", "Value :" + Constants.ReceiptInfo.RELOAD_RECEIPT);
            if (Constants.ReceiptInfo.RELOAD_RECEIPT == true) {
                Log.e("rtk", "Reloaded after success!");
                refreshCard();
                Constants.ReceiptInfo.RELOAD_RECEIPT = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // SEND SUCCESS
    public void showSnackBar() {
        SnackBar.show(getActivity(), viewGroup, getResources().getString(R.string.A007_1P_1));
    }

    //CANCEL SUCCESS
    public void showCancelSucess() {
        SnackBar.show(getActivity(), viewGroup, getResources().getString(R.string.A007_1P_2));
    }

    //Move sucess
    public void showMoveSuccess() {
        SnackBar.show(getActivity(), viewGroup, getResources().getString(R.string.A007_1P_3));
    }
    //UPDATE SUCCESS
    public void showUpdatedSnackBar() {
        SnackBar.show(getActivity(), viewGroup, getResources().getString(R.string.A007_1P_4));
    }
}


