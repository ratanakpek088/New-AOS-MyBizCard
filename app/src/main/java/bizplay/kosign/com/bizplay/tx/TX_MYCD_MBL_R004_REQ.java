package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by Torn Sokly on 1/10/2017.
 */

public class TX_MYCD_MBL_R004_REQ extends TxMessage {

    public static final String TXNO = "MYCD_MBL_R004";
    private TX_MYCD_MBL_R004_REQ_DATA mTxKeyData;

    public TX_MYCD_MBL_R004_REQ() throws Exception {
        mTxKeyData = new TX_MYCD_MBL_R004_REQ_DATA();
        super.initSendMessage();
    }

    public void setSEND_DTM(String value){
        mSendMessage.put(this.mTxKeyData.SEND_DTM, value);
    }
    public void setNOTI_SEQ(String value){
        mSendMessage.put(this.mTxKeyData.NOTI_SEQ, value);
    }
    private class TX_MYCD_MBL_R004_REQ_DATA{
        private String  SEND_DTM = "SEND_DTM";
        private String  NOTI_SEQ = "NOTI_SEQ";
    }
}
