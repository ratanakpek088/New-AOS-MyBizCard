package bizplay.kosign.com.bizplay.item;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
//
///**
// * Created by ViSAL MSi on 12/29/2016.
// */
//
//public class Card implements Parcelable {
//
//    public static ArrayList<Card> mCardList;
//
//    private String CARD_CORP_CD;
//    private String CARD_NO;
//    private String CARD_ORG_NM;
//    private String BIZ_NM;
//    private String CARD_NICK_NM;
//    private String EXPR_TRM;
//
//    public Card(String CARD_CORP_CD, String CARD_NO, String CARD_ORG_NM, String BIZ_NM, String CARD_NICK_NM, String EXPR_TRM) {
//        this.CARD_CORP_CD = CARD_CORP_CD;
//        this.CARD_NO = CARD_NO;
//        this.CARD_ORG_NM = CARD_ORG_NM;
//        this.BIZ_NM = BIZ_NM;
//        this.CARD_NICK_NM = CARD_NICK_NM;
//        this.EXPR_TRM = EXPR_TRM;
//    }
//
//    public String getCARD_CORP_CD() {
//        return CARD_CORP_CD;
//    }
//
//    public void setCARD_CORP_CD(String CARD_CORP_CD) {
//        this.CARD_CORP_CD = CARD_CORP_CD;
//    }
//
//    public String getCARD_NO() {
//        return CARD_NO;
//    }
//
//    public void setCARD_NO(String CARD_NO) {
//        this.CARD_NO = CARD_NO;
//    }
//
//    public String getCARD_ORG_NM() {
//        return CARD_ORG_NM;
//    }
//
//    public void setCARD_ORG_NM(String CARD_ORG_NM) {
//        this.CARD_ORG_NM = CARD_ORG_NM;
//    }
//
//
//    public String getBIZ_NM() {
//        return BIZ_NM;
//    }
//
//    public void setBIZ_NM(String BIZ_NM) {
//        this.BIZ_NM = BIZ_NM;
//    }
//
//    public String getCARD_NICK_NM() {
//        return CARD_NICK_NM;
//    }
//
//    public void setCARD_NICK_NM(String CARD_NICK_NM) {
//        this.CARD_NICK_NM = CARD_NICK_NM;
//    }
//
//    public String getEXPR_TRM() {
//        return EXPR_TRM;
//    }
//
//    public void setEXPR_TRM(String EXPR_TRM) {
//        this.EXPR_TRM = EXPR_TRM;
//    }
//
//    protected Card(Parcel in) {
//        CARD_CORP_CD = in.readString();
//        CARD_NO = in.readString();
//        CARD_ORG_NM = in.readString();
//        BIZ_NM = in.readString();
//        CARD_NICK_NM = in.readString();
//        EXPR_TRM = in.readString();
//    }
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(CARD_CORP_CD);
//        dest.writeString(CARD_NO);
//        dest.writeString(CARD_ORG_NM);
//        dest.writeString(BIZ_NM);
//        dest.writeString(CARD_NICK_NM);
//        dest.writeString(EXPR_TRM);
//    }
//
//    @SuppressWarnings("unused")
//    public static final Parcelable.Creator<Card> CREATOR = new Parcelable.Creator<Card>() {
//        @Override
//        public Card createFromParcel(Parcel in) {
//            return new Card(in);
//        }
//
//        @Override
//        public Card[] newArray(int size) {
//            return new Card[size];
//        }
//    };
//}


public class Card {

    public static ArrayList<Card> mCardList;

    private String CARD_CORP_CD;
    private String CARD_NO;
    private String CARD_ORG_NM;
    private String BIZ_NM;
    private String CARD_NICK_NM;
    private String EXPR_TRM;

    public Card(String CARD_CORP_CD, String CARD_NO, String CARD_ORG_NM, String BIZ_NM, String CARD_NICK_NM, String EXPR_TRM) {
        this.CARD_CORP_CD = CARD_CORP_CD;
        this.CARD_NO = CARD_NO;
        this.CARD_ORG_NM = CARD_ORG_NM;
        this.BIZ_NM = BIZ_NM;
        this.CARD_NICK_NM = CARD_NICK_NM;
        this.EXPR_TRM = EXPR_TRM;
    }

    public String getCARD_CORP_CD() {
        return CARD_CORP_CD;
    }

    public void setCARD_CORP_CD(String CARD_CORP_CD) {
        this.CARD_CORP_CD = CARD_CORP_CD;
    }

    public String getCARD_NO() {
        return CARD_NO;
    }

    public void setCARD_NO(String CARD_NO) {
        this.CARD_NO = CARD_NO;
    }

    public String getCARD_ORG_NM() {
        return CARD_ORG_NM;
    }

    public void setCARD_ORG_NM(String CARD_ORG_NM) {
        this.CARD_ORG_NM = CARD_ORG_NM;
    }


    public String getBIZ_NM() {
        return BIZ_NM;
    }

    public void setBIZ_NM(String BIZ_NM) {
        this.BIZ_NM = BIZ_NM;
    }

    public String getCARD_NICK_NM() {
        return CARD_NICK_NM;
    }

    public void setCARD_NICK_NM(String CARD_NICK_NM) {
        this.CARD_NICK_NM = CARD_NICK_NM;
    }

    public String getEXPR_TRM() {
        return EXPR_TRM;
    }

    public void setEXPR_TRM(String EXPR_TRM) {
        this.EXPR_TRM = EXPR_TRM;
    }
}
