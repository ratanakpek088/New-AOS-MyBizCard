package bizplay.kosign.com.bizplay;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.webcash.sws.define.Msg;
import com.webcash.sws.ui.DlgAlert;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import bizplay.kosign.com.bizplay.adapters.item.Participant;

import bizplay.kosign.com.bizplay.common.ui.CircleTransform;
import bizplay.kosign.com.bizplay.common.ui.util.FormatUtil;
import bizplay.kosign.com.bizplay.common.ui.util.SoundSearcher;
import bizplay.kosign.com.bizplay.item.Pagination;
import bizplay.kosign.com.bizplay.item.ParticipantItem;
import bizplay.kosign.com.bizplay.item.USER_REC_ITEM;
import bizplay.kosign.com.bizplay.permission.Permission;
import bizplay.kosign.com.bizplay.permission.PermissionCheck;
import bizplay.kosign.com.bizplay.tran.ComTran;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L005_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L005_RES;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L005_RES_REC;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_P004_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_P004_RES;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_P004_RES_REC;

public class ViewContactListActivity extends AppCompatActivity implements ComTran.OnComTranListener, View.OnClickListener, AdapterView.OnItemClickListener {
    private ComTran mComTran;
    private static final int REQUEST_CODE_SOME_FEATURES_PERMISSIONS = 0;
    private ArrayList<USER_REC_ITEM> mSelectedList = new ArrayList<>();
    private ArrayList<Participant> mParticipantList = new ArrayList<>();
    private ArrayList<Participant>mSearchList = new ArrayList<>();
    private ParticipantAdapter mParticipantAdapter;
    private String APV_GB, APV_DT, APV_SEQ, APV_TM, APV_NO, CARD_CORP_CD, CARD_NO;
    private TextView title_bar, btn_next;
    private ListView mParticipantListView, mSearchParticipantListView;
    private EditText mStrSearch;
    private LinearLayout llSelectedList,ll_current_cus;
    private Pagination mPage = new Pagination();
    private InputMethodManager imm;
    private ImageView iv_left_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mComTran = new ComTran(this, this);
        initView();
        mPage.initialize();
        mSearchParticipantListView.setAdapter(new ParticipantAdapter(this, mSearchList));
        mSearchParticipantListView.setOnItemClickListener(this);
        mStrSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 0){
                    findViewById(R.id.exp_list).setVisibility(View.VISIBLE);
                    findViewById(R.id.exp_list_search).setVisibility(View.GONE);
                }else {
                    findViewById(R.id.exp_list_search).setVisibility(View.VISIBLE);
                    findViewById(R.id.exp_list).setVisibility(View.GONE);
                }
                ((BaseAdapter)mParticipantListView.getAdapter()).notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {
                mSearchList.clear();

                for (int i = 0; i < mParticipantList.size(); i++) {
                    if (SoundSearcher.matchString(mParticipantList.get(i).getFLNM().toLowerCase(), s.toString().toLowerCase()))
                        mSearchList.add(mParticipantList.get(i));
                    else if (SoundSearcher.matchString(mParticipantList.get(i).getCLPH_NO().replaceAll("[^0-9]",""),s.toString().replace("-","")))
                        mSearchList.add(mParticipantList.get(i));
                    else if (mParticipantList.get(i).getEML() != "" & mParticipantList.get(i).getEML() != null) {
                        if (SoundSearcher.matchString(mParticipantList.get(i).getEML(), s.toString()))
                            mSearchList.add(mParticipantList.get(i));
                    }
                }
                if(mSearchList.size()==0 && s.length()>=7 && s.length()<=11 && TextUtils.isDigitsOnly(s)) {
                    Participant participant = new Participant();
                    participant.setFLNM(FormatUtil.formatePhoneNumber(s.toString()));
                    participant.setCLPH_NO(s.toString());
                    View alreadySelectedUserView = getAlreadySelectedUserView(FormatUtil.formatePhoneNumber(s.toString()), 0);
                    if(alreadySelectedUserView != null){
                        participant.setIS_SELECTED(true);
                    }
                    mSearchList.add(participant);
                }
                ((BaseAdapter) mSearchParticipantListView.getAdapter()).notifyDataSetChanged();
            }
        });
        mParticipantListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                int threshold = 1;
                int count = mParticipantListView.getCount();

                if (scrollState == SCROLL_STATE_IDLE) {
                    if (mParticipantListView.getLastVisiblePosition() >= count - threshold ) {
                        if (mPage.getTrSending())
                            return;

                        // 더보기 전문 송신
                        if (mPage.getMorePageYN()) {
                            mPage.setTrSending(true);
                            msgTrSend(TX_MYCD_MBL_L005_REQ.TXNO);
                        }
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
    }

    private void initView(){
        setContentView(R.layout.activity_view_contact_list);
        title_bar = (TextView) findViewById(R.id.toolbar_title_center);
        title_bar.setText(getResources().getString(R.string.A007_2_1));
        btn_next= (TextView) findViewById(R.id.toolbar_title_right);
        btn_next.setText(getResources().getString(R.string.AEA_02));
        btn_next.setOnClickListener(this);
        mStrSearch= (EditText) findViewById(R.id.et_search);
        ll_current_cus= (LinearLayout) findViewById(R.id.ll_current_emp);
        iv_left_btn= (ImageView) findViewById(R.id.iv_left_btn);
        iv_left_btn.setOnClickListener(this);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        //get String fron Intent
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            APV_GB=bundle.getString("APV_GB");
            APV_DT=bundle.getString("APV_DT");
            APV_SEQ=bundle.getString("APV_SEQ");
            APV_TM=bundle.getString("APV_TM");
            APV_NO=bundle.getString("APV_NO");
            CARD_CORP_CD=bundle.getString("CARD_CORP_CD");
            CARD_NO=bundle.getString("CARD_NO");
            Log.e("rtk", "Get Intent : APV_GB"+ APV_GB+", APV_DT"+ APV_DT+", APV_SEQ"+APV_SEQ + ", APV_TM"+ APV_TM+ ", APV_NO"+APV_NO+ ", CARD_CORP_CD"+ CARD_CORP_CD + ", CARD_NO"+ CARD_NO);
        }

        //get Object List from Intent
        Intent intent = getIntent();
        if(intent!=null){
            if(intent.hasExtra("RCPT_USER_LIST")){
                mSelectedList=intent.getParcelableArrayListExtra("RCPT_USER_LIST");
            }
        }

        //Listview
        mParticipantListView = (ListView) findViewById(R.id.exp_list);
        mSearchParticipantListView = (ListView) findViewById(R.id.exp_list_search);
        llSelectedList = (LinearLayout) findViewById(R.id.ll_selected_emp);

        msgTrSend(TX_MYCD_MBL_L005_REQ.TXNO);

        //initilize adapter
        mParticipantAdapter = new ParticipantAdapter(this, mParticipantList);
        mParticipantListView.setAdapter(mParticipantAdapter);
        mParticipantListView.setOnItemClickListener(this);

        ll_current_cus.setOnClickListener(this);

    }

    private void msgTrSend(String txno) {
        try {
         if(txno.equals(TX_MYCD_MBL_L005_REQ.TXNO)){
             TX_MYCD_MBL_L005_REQ req = new TX_MYCD_MBL_L005_REQ();
             req.setPAGE_NO(mPage.getPageNo());
             req.setPAGE_PER_CNT(mPage.getPageCnt());
             req.setSRCH_WORD("");
             mComTran.requestData(TX_MYCD_MBL_L005_REQ.TXNO,req.getSendMessage());
         }else if(txno.equals(TX_MYCD_MBL_P004_REQ.TXNO)) {
             TX_MYCD_MBL_P004_REQ req = new TX_MYCD_MBL_P004_REQ();
             req.setAPV_GB(APV_GB);
             req.setAPV_DT(APV_DT);
             req.setAPV_SEQ(APV_SEQ);
             req.setAPV_TM(APV_TM);
             req.setAPV_NO(APV_NO);
             req.setCARD_CORP_CD(CARD_CORP_CD);
             req.setCARD_NO(CARD_NO);

             JSONArray SHRNREC = new JSONArray();
             if (makeResult().size() > 0) {
                 for (Participant item : makeResult()) {
                     JSONObject participant = new JSONObject();
                     participant.put("RCPT_USER_HPNO", item.getCLPH_NO());
                     participant.put("RCPT_USER_NM", item.getFLNM());
                     participant.put("RCPT_USER_BSNN_NM", item.getBSNN_NM());
                     participant.put("RCPT_USER_PSN_NM", item.getJBCL_NM());
                     participant.put("RCPT_USER_PSN_IMG", item.getPRFL_PHTG());
                     SHRNREC.put(participant);
                 }
             }
             req.setRCPT_USER_REC(SHRNREC);
             mComTran.requestPost(TX_MYCD_MBL_P004_REQ.TXNO, req.getSendMessage(), true);
         }
        }catch(Exception e){
            DlgAlert.showError(ViewContactListActivity.this, Msg.Exp.DEFAULT, e);
        }
    }

    @Override
    public void onTranResponse(String tranCode, Object object) throws Exception {
        if(tranCode.equals(TX_MYCD_MBL_L005_REQ.TXNO)){
            TX_MYCD_MBL_L005_RES res = new TX_MYCD_MBL_L005_RES(object);
            TX_MYCD_MBL_L005_RES_REC rec = res.getTRAN_KIND_REC();

            for(int i=0; i<rec.getLength(); i++){
                Participant item = new Participant();
                item.setID(mParticipantList.size());
                item.setFLNM(rec.getFLNM());
                item.setBSNN_NM(rec.getBSNN_NM());
                item.setCLPH_NO(rec.getCLPH_NO());
                item.setDVSN_NM(rec.getDVSN_NM());
                item.setJBCL_NM(rec.getJBCL_NM());
                item.setRSPT_NM(rec.getRSPT_NM());
                item.setEML(rec.getEML());
                item.setPRFL_PHTG(rec.getPRFL_PHTG());
                mParticipantList.add(item);
                rec.moveNext();
            }
            mParticipantAdapter.notifyDataSetChanged();


            double totCnt = Double.parseDouble(res.getTOTL_RSLT_CNT());

            int maxPage = (int) Math.ceil(totCnt / Integer.parseInt(mPage.getPageCnt())); //Row count = 100

            if (maxPage > mPage.getPageNoByInt()) {
                mPage.addPageNo();
                mPage.setMorePageYN(true);
            } else {
                mPage.setMorePageYN(false);
                // list permissions
                List<String> permissions = new ArrayList<>();
                permissions.add(android.Manifest.permission.READ_CONTACTS);
                String message = "주소록에 접근하여 영수증담당자를 지정할 수 없습니다. \"설정 > 권한\"에서 권한을 허용하여 다시 시도하세요.";

                if (!Permission.hasPermissions(this,new String[]{android.Manifest.permission.READ_CONTACTS})) {
                    Intent intent = new Intent(this, PermissionCheck.class);
                    intent.putExtra("PERMISSION", new Permission(permissions));
                    intent.putExtra("SMS",message);
                    startActivityForResult(intent, REQUEST_CODE_SOME_FEATURES_PERMISSIONS);
                }else{
                    loadContacts();
                }
            }
            mPage.setTrSending(false);
            mParticipantAdapter.notifyDataSetChanged();
        }else if(tranCode.equals(TX_MYCD_MBL_P004_REQ.TXNO)){
            TX_MYCD_MBL_P004_RES res = new TX_MYCD_MBL_P004_RES(object);
            TX_MYCD_MBL_P004_RES_REC rec = res.getRCPT_USER_REC();
            for(int i=0; i<rec.getLength(); i++){
                //USER_REC_ITEM item = new USER_REC_ITEM();
                USER_REC_ITEM item = new USER_REC_ITEM();
                item.setRCPT_USER_HPNO(rec.getRCPT_RFS_HPNO());
                item.setRCPT_USER_NM(rec.getRCPT_RFS_NM());
                item.setRCPT_USER_NO(rec.getRCPT_RFS_USER_NO());
                item.setRCPT_USER_BSNN_NM(rec.getRCPT_USER_BSNN_NM());
                item.setRCPT_USER_PSN_NM(rec.getRCPT_USER_PSN_NM());
                item.setRCPT_USER_PSN_IMG(rec.getRCPT_USER_PSN_IMG());
                mSelectedList.add(item);
                rec.moveNext();
            }
            mParticipantAdapter.notifyDataSetChanged();
            returnParticipant(getParticipant());
        }
    }
    private void returnParticipant(ArrayList<USER_REC_ITEM> list){
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra("RCPT_USER_LIST", list);
        setResult(RESULT_OK,intent);
        finish();
    }
    @Override
    public void onTranError(String tranCode, Object object) {

    }
    private ArrayList<USER_REC_ITEM> getParticipant(){
        ArrayList<USER_REC_ITEM> myList = mSelectedList;
        Map<String, USER_REC_ITEM> map = new HashMap<>();
        ArrayList<USER_REC_ITEM> newList = new ArrayList<>();
        try {
            for (int i = 0; i < myList.size(); i++) {
                if (map.get(myList.get(i).getRCPT_USER_NO()) == null) {
                    map.put(myList.get(i).getRCPT_USER_NO(), myList.get(i));
                    newList.add(myList.get(i));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return newList;
    }
    private ArrayList<Participant> makeResult() {
        ArrayList<Participant> result = new ArrayList<Participant>();
        try {
            for (int i = 0; i < llSelectedList.getChildCount(); i++) {
                View v = llSelectedList.getChildAt(i).findViewById(R.id.iv_del);
                Participant temp = (Participant) v.getTag();
                result.add(temp);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.iv_left_btn:
                finish();
                break;

            case R.id.toolbar_title_right:
                Log.e("rtk", "click next btn");
                if(makeResult().size()>0) {
                    msgTrSend(TX_MYCD_MBL_P004_REQ.TXNO);
                }

                break;
            case R.id.ll_current_cus:
            case R.id.toolbar:
                hideKeyBoard();
                break;
        }

    }
    private void hideKeyBoard() {
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        mStrSearch.clearFocus();
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Participant item = new Participant();
        if(findViewById(R.id.ll_current_emp).getVisibility() != View.VISIBLE){
            findViewById(R.id.ll_current_emp).setVisibility(View.VISIBLE);
        }

        if(parent.getId() == R.id.exp_list){
            item = mParticipantList.get(position);
        }else if(parent.getId() == R.id.exp_list_search){
            item = mSearchList.get(position);
        }
        Log.e("rtk", item.toString());
        //String clph_no = item.getCLPH_NO();
        String name = item.getFLNM();
        int pid = item.getID();
        //String eml = item.getEML();
        CheckBox chk = (CheckBox)view.findViewById(R.id.cb_participant);
        chk.setChecked(!chk.isChecked());
        item.setIS_SELECTED(chk.isChecked());

        View alreadySelectedUserView = getAlreadySelectedUserView(name, pid);
        if(alreadySelectedUserView != null){
            removeUserInSelectListLayout(alreadySelectedUserView);
            return;
        }

        View v = LayoutInflater.from(this).inflate(
                R.layout.participant_select_item, null);



        LinearLayout ll_item = (LinearLayout) v.findViewById(R.id.ll_participant_select_item);


        final ImageView ivDel = (ImageView) v.findViewById(R.id.iv_del);
        TextView tvName = (TextView) v.findViewById(R.id.tv_name);
        tvName.setText(item.getFLNM());


        ivDel.setTag(item);
        ll_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeUserInSelectListLayout(ivDel);
            }
        });

        llSelectedList.addView(v);
//        scrollToRight();

    }
//    private void scrollToRight() {
//        final HorizontalScrollView hScrollView = (HorizontalScrollView) findViewById(R.id.horizontalScrollView1);
//        if (hScrollView != null) {
//            hScrollView.post(new Runnable() {
//                @Override
//                public void run() {
//                    hScrollView.fullScroll(View.FOCUS_RIGHT);
//                }
//            });
//        }
//    }
    private void removeUserInSelectListLayout(View view){
        View vp = (View) view.getParent().getParent();
        llSelectedList.removeView(vp);
        if(view.getTag() != null && view.getTag() instanceof Participant){
            Participant temp = (Participant) view.getTag();
            if(mParticipantListView.getVisibility() == View.VISIBLE){
                uncheckFromSelectedUser(temp, mParticipantList);
                mParticipantListView.invalidateViews();
            }else if(mSearchParticipantListView.getVisibility() == View.VISIBLE){
                uncheckFromSelectedUser(temp, mSearchList);
                uncheckFromSelectedUser(temp, mParticipantList);
                mSearchParticipantListView.invalidateViews();
            }
        }
        if(llSelectedList.getChildCount() == 0){
            findViewById(R.id.ll_current_emp).setVisibility(View.GONE);
        }
    }
    public void uncheckFromSelectedUser(Participant src, ArrayList<Participant> targetList){
        for (Participant item : targetList) {

            if (!TextUtils.isEmpty(src.getFLNM()) && src.getFLNM().equals(item.getFLNM()) && (src.getID() == item.getID())) {
                item.setIS_SELECTED(false);
                return;
            }
        }
        /*for(Participant item : target){
            if (((!TextUtils.isEmpty(src.getCLPH_NO()) && src.getCLPH_NO().equals(item.getCLPH_NO()))|| ((!TextUtils.isEmpty(src.getEML())) && src.getEML().equals(item.getEML()))) && (!TextUtils.isEmpty(src.getFLNM())) && src.getFLNM().equals(item.getFLNM())) {
                item.setIS_SELECTED(false);
                return;
            }
        }*/
    }
    private View getAlreadySelectedUserView(String flnm, int id){
        for(int i=0;i<llSelectedList.getChildCount();i++){
            View v = llSelectedList.getChildAt(i).findViewById(R.id.iv_del);
            Participant temp = (Participant) v.getTag();
            if(!TextUtils.isEmpty(flnm) && flnm.equals(temp.getFLNM())&& id == temp.getID()) return v;
        }
        return null;
        /*for(int i=0; i<llSelectedList.getChildCount(); i++){
            View v = llSelectedList.getChildAt(i).findViewById(R.id.iv_del);
            Participant temp = (Participant)v.getTag();
            if (!TextUtils.isEmpty(clphno) && clphno.equals(temp.getCLPH_NO()) && !TextUtils.isEmpty(flnm) && flnm.equals(temp.getFLNM()) )
                return v;
            else if (!TextUtils.isEmpty(eml) && eml.equals(temp.getEML()) && !TextUtils.isEmpty(flnm) && flnm.equals(temp.getFLNM()))
                return v;
        }
        return  null;*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SOME_FEATURES_PERMISSIONS) {
            if (resultCode == RESULT_OK) {
                loadContacts(); // read
            }
        }
    }
    public void loadContacts(){
        try {
            Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
            String[] projection = new String[]{
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                    ContactsContract.CommonDataKinds.Phone.HAS_PHONE_NUMBER,
                    ContactsContract.CommonDataKinds.Phone.NUMBER
            };

            Cursor cursor = getContentResolver().query(uri, projection, null, null, null);

            Participant temp;
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    if (cursor.getInt(1) == 1) {
                        temp = new Participant();
                        String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                        String phoneNo = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        temp.setID(mParticipantList.size());
                        temp.setFLNM(contactName);
                        temp.setCLPH_NO(phoneNo);

                        mParticipantList.add(temp);
                    }
                }
            }
            mParticipantAdapter.notifyDataSetChanged();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*
       *==========================Participant Adapter===============================
        */
    public class ParticipantAdapter extends BaseAdapter {
        LayoutInflater inflater;
        Participant item;
        ArrayList<Participant> mList;

        public ParticipantAdapter(Context context, ArrayList<Participant>list){
            inflater = LayoutInflater.from(context);
            mList = list;
        }
        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public Object getItem(int position) {
            return mList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            if(convertView == null || convertView.getTag() == null){
                convertView = inflater.inflate(R.layout.participant_item, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            }else holder = (ViewHolder) convertView.getTag();

            item = mList.get(position);
            holder.cbParticipant.setChecked(item.IS_SELECTED());
            holder.llItem.setTag(R.id.participant_position,position);
            holder.tvName.setText(item.getFLNM());
            holder.tvComNm.setText(item.getBSNN_NM());
            holder.tvDepNm.setText(item.getJBCL_NM());
            if(TextUtils.isEmpty(item.getBSNN_NM())){
                holder.tvComNm.setText(FormatUtil.formatePhoneNumber(item.getCLPH_NO()));
            }
            if(!TextUtils.isEmpty(item.getPRFL_PHTG())){
                Glide.with(ViewContactListActivity.this).load(item.getPRFL_PHTG()).transform(new CircleTransform(ViewContactListActivity.this)).placeholder(R.drawable.user_empty_icon).error(R.drawable.user_empty_icon).into(holder.ivParticipant);
            }else {
                holder.ivParticipant.setImageBitmap(null);
                holder.ivParticipant.setImageResource(R.drawable.user_empty_icon);
            }
            if (TextUtils.isEmpty(item.getJBCL_NM()) || TextUtils.isEmpty(item.getBSNN_NM())) {
                holder.tvDevider.setVisibility(View.GONE);
            } else if(item.getBSNN_NM() == "" || item.getBSNN_NM() == null){
                holder.tvDevider.setVisibility(View.GONE);
            }else {
                holder.tvDevider.setVisibility(View.VISIBLE);
            }
            return convertView;
        }

        class ViewHolder{
            LinearLayout llItem;
            ImageView ivParticipant;
            CheckBox cbParticipant;
            TextView tvName, tvComNm, tvDepNm, tvDevider;
            public ViewHolder(View view){
                llItem = (LinearLayout) view.findViewById(R.id.ll_item_participant);
                ivParticipant = (ImageView) view.findViewById(R.id.iv_participant);
                cbParticipant = (CheckBox) view.findViewById(R.id.cb_participant);
                tvName = (TextView) view.findViewById(R.id.tv_name);
                tvComNm = (TextView) view.findViewById(R.id.tv_companyNm);
                tvDepNm = (TextView) view.findViewById(R.id.tv_departNm);
                tvDevider = (TextView) view.findViewById(R.id.tv_divide);
            }
        }
    }
    /*
    *==========================Request & Response=========================
     */
}
