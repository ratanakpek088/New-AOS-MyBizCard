package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 *  Created by Huo Chhunleng on 15/Feb/2017.
 */
public class TX_MYCD_MBL_C003_RES extends TxMessage {

    private TX_MYCD_MBL_C003_RES_DATA mTxKeyData;

    public TX_MYCD_MBL_C003_RES(Object obj) throws Exception{
        mTxKeyData = new TX_MYCD_MBL_C003_RES_DATA();
        super.initRecvMessage(obj);
    }

    private class TX_MYCD_MBL_C003_RES_DATA{
        private String RCPT_SEQNO   = "RCPT_SEQNO";
    }

    public String getRCPT_SEQNO() throws Exception{
        return getString(mTxKeyData.RCPT_SEQNO);
    }
}
