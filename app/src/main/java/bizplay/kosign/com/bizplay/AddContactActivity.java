package bizplay.kosign.com.bizplay;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.webcash.sws.define.Msg;
import com.webcash.sws.ui.DlgAlert;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import bizplay.kosign.com.bizplay.adapters.item.ParticipantAdapter;
import bizplay.kosign.com.bizplay.common.ui.UIConstant;
import bizplay.kosign.com.bizplay.item.ParticipantItem;
import bizplay.kosign.com.bizplay.item.USER_REC_ITEM;
import bizplay.kosign.com.bizplay.tran.ComTran;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_D001_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_D001_REQ_REC;

public class AddContactActivity extends AppCompatActivity implements View.OnClickListener, ComTran.OnComTranListener {
    private ListView lv_participant;
    private ArrayList<USER_REC_ITEM> mPartList, mSelectList;
    private String CARD_CORP_CD, CARD_NO;
    private ParticipantAdapter mPartAdapter;
    private int pos;
    private ComTran comTran;
    private ImageView iv_close, add_contact_rct;
    private TextView title_contact;
    private Intent intent;
    private LinearLayout ll_next;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        initView();
    }

    private void initView() {
        lv_participant = (ListView) findViewById(R.id.lv_participant);
        comTran = new ComTran(this, this);
        mPartList = new ArrayList<>();
        intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("RCPT_USER_LIST")) {
                mSelectList = intent.getParcelableArrayListExtra("RCPT_USER_LIST");
                Log.e("rtk", "Intent Object : " + mSelectList.toArray().toString());
            }
        }
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            CARD_CORP_CD = bundle.getString("CARD_CORP_CD");
            CARD_NO = bundle.getString("CARD_NO");
        }
        iv_close = (ImageView) findViewById(R.id.main_back_to_setting_of_rcpt);
        iv_close.setOnClickListener(this);
        add_contact_rct = (ImageView) findViewById(R.id.detail_op_rct);
        add_contact_rct.setImageResource(R.drawable.main_add_user_icon);
        add_contact_rct.setOnClickListener(this);
        title_contact = (TextView) findViewById(R.id.title_contact);
        title_contact.setText(String.format(getResources().getString(R.string.A007_2_3_1), mSelectList.size() + ""));
        ll_next= (LinearLayout) findViewById(R.id.ll_next);
        ll_next.setOnClickListener(this);

        mPartAdapter = new ParticipantAdapter(this, mSelectList, this);
        lv_participant.setAdapter(mPartAdapter);
    }

    public void msgTrSend(String tranCd) {
        try {
            comTran = new ComTran(this, this);
            if (tranCd.equals(TX_MYCD_MBL_D001_REQ.TXNO)) {
                TX_MYCD_MBL_D001_REQ req = new TX_MYCD_MBL_D001_REQ();
                req.setCARD_CORP_CD(CARD_CORP_CD);
                req.setCARD_NO(CARD_NO);

                JSONArray REC = new JSONArray();
                JSONObject participant = new JSONObject();
                participant.put("RCPT_USER_HPNO", mSelectList.get(pos).getRCPT_USER_HPNO());
                participant.put("RCPT_USER_NM", mSelectList.get(pos).getRCPT_USER_NM());
                participant.put("RCPT_USER_NO", mSelectList.get(pos).getRCPT_USER_NO());
                REC.put(participant);

                req.setRCPT_USER_REC(REC);
                comTran.requestData(TX_MYCD_MBL_D001_REQ.TXNO, req.getSendMessage());
            }
        } catch (Exception e) {
            DlgAlert.showError(this, Msg.Exp.DEFAULT, e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == UIConstant.ActivityRequestCode.REQEST_CODE1){
            if(resultCode == Activity.RESULT_OK) {
                ArrayList<USER_REC_ITEM> List_RCPT_USER_REC = data.getParcelableArrayListExtra("RCPT_USER_LIST");
                returnParticipant(List_RCPT_USER_REC);

            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_del:

                pos = Integer.parseInt(v.getTag(R.id.participant_position).toString());
                Log.e("rtk", "Delete?" + pos);
                msgTrSend(TX_MYCD_MBL_D001_REQ.TXNO);
                break;
            case R.id.ll_next:
                returnParticipant(mSelectList);
                break;
            case R.id.main_back_to_setting_of_rcpt:
                Log.e("rtk", "Close activity");
                finish();
                break;
            case R.id.detail_op_rct:
                Log.e("rtk", "Add Activity");
                Intent newIntent = new Intent(this, ViewContactListActivity.class);
                newIntent.putExtras(intent);
                startActivityForResult(newIntent, UIConstant.ActivityRequestCode.REQEST_CODE1);
                break;

        }
    }
    private void returnParticipant(ArrayList<USER_REC_ITEM> list){
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra("RCPT_USER_LIST", list);
        Log.e("addContact", list.size()+"");
        setResult(RESULT_OK,intent);
        finish();
    }
    @Override
    public void onTranResponse(String tranCode, Object object) throws Exception {
        try {
            if (tranCode.equals(TX_MYCD_MBL_D001_REQ.TXNO)) {
                mPartAdapter.remove(pos);
                // initToolbar("3", String.format(getString(R.string.A007_2_3_1), +mListSelect.size()));
            }
        } catch (Exception e) {
            DlgAlert.showError(this, Msg.Exp.DEFAULT, e);
        }
    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }
}
