package bizplay.kosign.com.bizplay;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.webcash.sws.log.DevLog;

import java.util.ArrayList;

import bizplay.kosign.com.bizplay.adapters.item.CustomerAdapter;
import bizplay.kosign.com.bizplay.common.ui.util.SoundSearcher;
import bizplay.kosign.com.bizplay.item.CusEmp;
import bizplay.kosign.com.bizplay.item.Cust;
import bizplay.kosign.com.bizplay.item.Customer;
import bizplay.kosign.com.bizplay.item.Employee;
import bizplay.kosign.com.bizplay.tran.ComTran;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L008_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L008_RES;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L008_RES_REC;

import static android.view.View.GONE;

public class AddCustomerActivity extends AppCompatActivity implements ComTran.OnComTranListener, View.OnClickListener, AdapterView.OnItemClickListener {
    private ComTran mComtran;
    private String CARD_CORP_CD, CARD_NO;
    private ListView lv_cus, lv_cus_search;
    private TextView title_toolbar, next_btn;
    private ImageView close_btn;
    private ArrayList<CusEmp> list_customer, list_selected, list_search, list_cust_extra;
    private CustomerAdapter mCustomerAdapter, mSearch;
    private LinearLayout ll_current_cus, ll_selected_cus, ll_participant_select_item;
    private EditText et_search;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer);
        mComtran = new ComTran(this, this);


        initView();
        loadCustomer();

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    lv_cus.setVisibility(View.VISIBLE);
                    lv_cus_search.setVisibility(View.GONE);
                    mCustomerAdapter.notifyDataSetChanged();
                } else {
                    lv_cus.setVisibility(View.GONE);
                    lv_cus_search.setVisibility(View.VISIBLE);
                    mSearch.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    searchResult(s.toString());

                    mSearch.notifyDataSetChanged();
                }
            }


        });
    }

    private void searchResult(String s) {

        list_search.clear();
        mSearch.setmSearch(s.trim());

        for (int i = 0; i < list_customer.size(); i++) {
            if (SoundSearcher.matchString(list_customer.get(i).getBP_CUST_NM().toLowerCase(), s.toLowerCase())) {
                list_search.add(list_customer.get(i));
            } else if (SoundSearcher.matchString(list_customer.get(i).getBP_MAGR_NM().toLowerCase(), s.toLowerCase())) {
                list_search.add(list_customer.get(i));
            } else if (SoundSearcher.matchString(list_customer.get(i).getCLPH_NO(), s.replace("-", ""))) {
                list_search.add(list_customer.get(i));
            }
        }


    }

    private void initView() {

        lv_cus = (ListView) findViewById(R.id.lv_cus);
        lv_cus_search = (ListView) findViewById(R.id.lv_cus_search);
        title_toolbar = (TextView) findViewById(R.id.toolbar_title_center);
        title_toolbar.setText(getResources().getString(R.string.ACA_00));
        close_btn = (ImageView) findViewById(R.id.iv_left_btn);
        close_btn.setOnClickListener(this);
        next_btn = (TextView) findViewById(R.id.toolbar_title_right);
        next_btn.setOnClickListener(this);
        next_btn.setText(getResources().getString(R.string.ACA_02));
        lv_cus = (ListView) findViewById(R.id.lv_cus);
        lv_cus.setOnItemClickListener(this);
        et_search = (EditText) findViewById(R.id.et_search);
        list_customer = new ArrayList<>();
        list_selected = new ArrayList<>();
        list_search = new ArrayList<>();
        list_cust_extra=new ArrayList<>();
        ll_current_cus = (LinearLayout) findViewById(R.id.ll_current_cus);
        ll_selected_cus = (LinearLayout) findViewById(R.id.ll_selected_cus);

        mCustomerAdapter = new CustomerAdapter(list_customer, this, "");
        mSearch = new CustomerAdapter(list_search, this, "");
        lv_cus.setAdapter(mCustomerAdapter);
        lv_cus_search.setOnItemClickListener(this);
        lv_cus_search.setAdapter(mSearch);

        //Get data from A007
        Intent cardinfo = getIntent();
        if (cardinfo != null) {
            CARD_CORP_CD = cardinfo.getStringExtra("CARD_CORP_CD");
            CARD_NO = cardinfo.getStringExtra("CARD_NO");
            DevLog.devLog("1. initialize intent from A007->" + CARD_CORP_CD + ", " + CARD_NO);
        }
       if(cardinfo!=null){
           list_cust_extra=cardinfo.getParcelableArrayListExtra("BP_CUST_LIST");
           if(list_cust_extra.size()>=0){
               ll_current_cus.setVisibility(View.VISIBLE);
               for(int i=0;i<list_cust_extra.size();i++){
                   list_selected.add(list_cust_extra.get(i));
                   list_selected.get(i).setID(-1);;

                   final View itemView = LayoutInflater.from(this).inflate(R.layout.participant_select_item, null);
                   LinearLayout ll_item = (LinearLayout) itemView.findViewById(R.id.ll_participant_select_item);

                   TextView tvName = (TextView) itemView.findViewById(R.id.tv_name);

                   if (list_selected.get(i).getBP_MAGR_NO().equals("0"))
                       tvName.setText(list_selected.get(i).getBP_CUST_NM());
                   else
                       tvName.setText(list_selected.get(i).getBP_CUST_NM() + "(" + list_selected.get(i).getBP_MAGR_NM() + ")");

                   ll_item.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View v) {
                           removeViewOnClick(itemView);
                       }
                   });
                   ll_selected_cus.addView(itemView);
               }
           }
       }

    }



    private void loadCustomer() {
        try {
            TX_MYCD_MBL_L008_REQ res = new TX_MYCD_MBL_L008_REQ();
            res.setCARD_CORP_CD(CARD_CORP_CD);
            res.setCARD_NO(CARD_NO);
            mComtran.requestData(TX_MYCD_MBL_L008_REQ.TXNO, res.getSendMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTranResponse(String tranCode, Object object) throws Exception {
        try {
            if (tranCode.equals(TX_MYCD_MBL_L008_REQ.TXNO)) {
                TX_MYCD_MBL_L008_RES res = new TX_MYCD_MBL_L008_RES(object);
                TX_MYCD_MBL_L008_RES_REC rec = res.getBP_EMPL_REC();
                for (int i = 0; i < rec.getLength(); i++) {
                    CusEmp cusEmp = new CusEmp();
                    cusEmp.setID(list_customer.size());
                    cusEmp.setBP_CUST_NO(rec.getBP_CUST_NO());
                    cusEmp.setBP_MAGR_NO(rec.getBP_MAGR_NO());
                    cusEmp.setBP_CUST_NM(rec.getBP_CUST_NM());
                    cusEmp.setBP_MAGR_NM(rec.getBP_MAGR_NM());
                    cusEmp.setCLPH_NO(rec.getCLPH_NO());
                    cusEmp.setBP_MEMO(rec.getBP_MEMO());

                    for (int j = 0; j < list_selected.size(); j++) {
                        if (list_selected.get(j).getBP_MAGR_NO().equals(cusEmp.getBP_MAGR_NO()) &&
                                list_selected.get(j).getBP_CUST_NO().equals(cusEmp.getBP_CUST_NO()) &&
                                list_selected.get(j).getBP_MAGR_NM().equals(cusEmp.getBP_MAGR_NM()) &&
                                list_selected.get(j).getBP_CUST_NM().equals(cusEmp.getBP_CUST_NM())) {
                            list_selected.get(j).setID(cusEmp.getID());
                            cusEmp.setChecked(true);
                        }
                    }
                    if (!cusEmp.getBP_MAGR_NO().equals("") && !cusEmp.getBP_MAGR_NO().equals("null"))
                        list_customer.add(cusEmp);
                    rec.moveNext();
                }
                mCustomerAdapter.notifyDataSetChanged();


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_left_btn:
                finish();
                break;
            case R.id.toolbar_title_right:
                returnSelectedCustomer();
                break;
            default:

        }
    }

    private void returnSelectedCustomer() {

        DevLog.devLog("Data send back", list_selected.toString());
        Intent intent= new Intent();
        intent.putParcelableArrayListExtra("BP_CUST_LIST", list_selected);
        setResult(RESULT_OK, intent);

        finish();

    }



    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        CusEmp cust = new CusEmp();

        if(lv_cus.getVisibility()==View.VISIBLE){
            if (list_selected.size() >= 0) {
                cust = list_customer.get(position);
                cust.setID(position);
                if (list_customer.get(position).isChecked()) {
                    list_customer.get(position).setChecked(false);
                    mCustomerAdapter.notifyDataSetChanged();
                    mSearch.notifyDataSetChanged();
                    removeFromSelectedList(cust);
                    hideView();
                    return;

                } else {
                    list_customer.get(position).setChecked(true);
//                    mCustomerAdapter.notifyDataSetChanged();
                    // cust=new Cust(list_customer.get(position).getBP_CUST_NO(), list_customer.get(position).getBP_MAGR_NO(), list_customer.get(position).getBP_MAGR_NO(), "","","",false);

                    list_selected.add(cust);
                    mSearch.notifyDataSetChanged();
                    mCustomerAdapter.notifyDataSetChanged();
                    hideView();
                    DevLog.devLog(list_selected.size() + "li");
                }
            } else {
                ll_current_cus.setVisibility(View.GONE);
                return;
            }


        }else if(lv_cus_search.getVisibility()==View.VISIBLE){

            cust = list_customer.get(position);
            if(list_search.get(position).isChecked()){
                list_search.get(position).setChecked(false);


                list_customer.get(list_search.get(position).getID()).setChecked(false);
                mSearch.notifyDataSetChanged();
                mCustomerAdapter.notifyDataSetChanged();
                removeFromSelectedList(list_search.get(position));
                hideView();
                return;
            }else{

                list_search.get(position).setChecked(true);
                list_selected.add(list_search.get(position));
                list_customer.get(list_search.get(position).getID()).setChecked(true);




                mSearch.notifyDataSetChanged();
                mCustomerAdapter.notifyDataSetChanged();
                hideView();
            }
        }

        final View v = LayoutInflater.from(this).inflate(R.layout.participant_select_item, null);
        TextView name = (TextView) v.findViewById(R.id.tv_name);
        if(lv_cus.getVisibility()==View.VISIBLE) {
           if(cust.getBP_MAGR_NO().equals("0")){
               name.setText(list_customer.get(position).getBP_CUST_NM());
           }else{
               name.setText(list_customer.get(position).getBP_CUST_NM()+"("+list_customer.get(position).getBP_MAGR_NM() +")");
           }
        }
        else name.setText(list_search.get(position).getBP_CUST_NM());




        LinearLayout itemSelected = (LinearLayout) v.findViewById(R.id.ll_participant_select_item);
        itemSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View ve) {

                removeViewOnClick(v);
                //DevLog.devLog(ll_selected_cus.getChild + "ln" + ", " + list_selected.size());
            }
        });
        ll_selected_cus.addView(v);
        DevLog.devLog(ll_selected_cus.getChildCount() + "ln" + ", " + list_selected.size());
    }

    private void removeViewOnClick(View v) {
        DevLog.devLog(ll_selected_cus.getChildCount() + "before" + ", " + list_selected.size()+"ddd"+ll_selected_cus.indexOfChild(v));
        int ind= ll_selected_cus.indexOfChild(v);
        if(lv_cus.getVisibility() == View.VISIBLE){
            Log.e("click", "hereh="+ind);
            ll_selected_cus.removeViewAt(ind);
            ll_selected_cus.invalidate();
            int inde=list_selected.get(ind).getID();
            list_customer.get(inde).setChecked(false);
            list_selected.remove(ind);
            mCustomerAdapter.notifyDataSetChanged();
            DevLog.devLog(ll_selected_cus.getChildCount() + "after" + ", " + list_selected.size());

        }else if(lv_cus_search.getVisibility()==View.VISIBLE){

            //int dex = ll_selected_cus.indexOfChild(v);
           // list_customer.get(dex).setChecked(false);
           // ll_selected_cus.removeViewAt(dex);
           // mSearch.notifyDataSetChanged();

            for (int i = 0; i < list_search.size(); i++) {
                Log.e("rtk", list_search.get(i).getID()+"bbbb"+ ", ");
//                +ll_selected_cus.indexOfChild(v)
                if (list_search.get(i).getID() == list_selected.get(ind).getID()) {
                    list_search.get(i).setChecked(false);
                    //listEmployee.get(listSearch.get(i).getID()).setIS_SELECTED(false);
                   // list_selected.remove(ind);
                    //ll_selected_cus.removeViewAt(ind);

                    mSearch.notifyDataSetChanged();
                    break;
                }
            }
            for (int i = 0; i < list_customer.size(); i++) {
                if (list_customer.get(i).getID() == list_selected.get(ind).getID()) {
                    list_customer.get(i).setChecked(false);
                    mCustomerAdapter.notifyDataSetChanged();
                    break;
                }
            }

            list_selected.remove(ind);
            ll_selected_cus.removeView(v);
        }
        hideView();

    }
    private void hideView() {
        if (list_selected.size() == 0)
            ll_current_cus.setVisibility(View.GONE);
        else
            ll_current_cus.setVisibility(View.VISIBLE);
    }

    private void removeFromSelectedList(CusEmp e) {

        for (int i = 0; i < list_selected.size(); i++) {
            if (e.getID() == list_selected.get(i).getID()) {
                DevLog.devLog("I:" + i + ", " + e.getID() + "-->" + list_selected.get(i).getID());
                list_selected.remove(i);
       DevLog.devLog("Child View", i+",,,,");
                ll_selected_cus.removeViewAt(i);
                ll_selected_cus.invalidate();
                // llSelectedList.removeViewAt(i);
                // llSelectedList.invalidate();
                break;
            }
        }

    }
}
