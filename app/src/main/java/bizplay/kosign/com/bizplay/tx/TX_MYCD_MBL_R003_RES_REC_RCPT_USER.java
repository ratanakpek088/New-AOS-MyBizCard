package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by RatanakPek on 1/10/2017.
 *
 */

public class TX_MYCD_MBL_R003_RES_REC_RCPT_USER extends TxMessage {
    private TX_MYCD_MBL_R003_RES_REC_RCPT_USER_DATA mTxKeyData;

    public TX_MYCD_MBL_R003_RES_REC_RCPT_USER(Object obj) throws Exception {
        this.mTxKeyData = new TX_MYCD_MBL_R003_RES_REC_RCPT_USER_DATA();
        super.initRecvMessage(obj);
    }

    public String getRCPT_USER_HPNO() throws Exception {
        return getString(mTxKeyData.RCPT_USER_HPNO);
    }
    public String getRCPT_USER_NM() throws Exception {
        return getString(mTxKeyData.RCPT_USER_NM);
    }
    public String getRCPT_USER_NO() throws Exception {
        return getString(mTxKeyData.RCPT_USER_NO);
    }
    public String getRCPT_USER_BSNN_NM() throws Exception {
        return getString(mTxKeyData.RCPT_USER_BSNN_NM);
    }

    public String getRCPT_USER_PSN_NM() throws Exception {
        return getString(mTxKeyData.RCPT_USER_PSN_NM);
    }
    public String getRCPT_USER_PSN_IMG() throws Exception {
        return getString(mTxKeyData.RCPT_USER_PSN_IMG);
    }


    private class TX_MYCD_MBL_R003_RES_REC_RCPT_USER_DATA{
        private String RCPT_USER_HPNO="RCPT_USER_HPNO";
        private String RCPT_USER_NM="RCPT_USER_NM";
        private String RCPT_USER_NO="RCPT_USER_NO";
        private String RCPT_USER_BSNN_NM="RCPT_USER_BSNN_NM";
        private String RCPT_USER_PSN_NM="RCPT_USER_PSN_NM";
        private String RCPT_USER_PSN_IMG="RCPT_USER_PSN_IMG";
    }
}
