package bizplay.kosign.com.bizplay;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.webcash.sws.log.DevLog;
import com.webcash.sws.picture.PictureConf;
import com.webcash.sws.picture.PictureUtil;
import com.webcash.sws.picture.PictureViewActivity;
import com.webcash.sws.picture.extras.Extras_Picture;
import com.webcash.sws.ui.DlgAlert;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import bizplay.kosign.com.bizplay.adapters.item.A007_2_4P_Item;
import bizplay.kosign.com.bizplay.adapters.item.ParticipantAdapter;
import bizplay.kosign.com.bizplay.common.ui.PercentageCropImageView;
import bizplay.kosign.com.bizplay.common.ui.util.FormatUtil;
import bizplay.kosign.com.bizplay.constant.Constants;
import bizplay.kosign.com.bizplay.item.CusEmp;
import bizplay.kosign.com.bizplay.item.Employee;
import bizplay.kosign.com.bizplay.item.ParticipantItem;
import bizplay.kosign.com.bizplay.item.PhotoItem;
import bizplay.kosign.com.bizplay.item.Photo_Item;
import bizplay.kosign.com.bizplay.item.RCPT_RFS_REC_ITEM;
import bizplay.kosign.com.bizplay.item.USER_REC_ITEM;
import bizplay.kosign.com.bizplay.item.UsageItem;
import bizplay.kosign.com.bizplay.pick.Action;
import bizplay.kosign.com.bizplay.popup.DetailPopupMenu;
import bizplay.kosign.com.bizplay.popup.DialogMenu;
import bizplay.kosign.com.bizplay.popup.ListPopupWindow;
import bizplay.kosign.com.bizplay.popup.ModifyPopup;
import bizplay.kosign.com.bizplay.tabs.ReceiptFragment;
import bizplay.kosign.com.bizplay.tran.ComLoading;
import bizplay.kosign.com.bizplay.tran.ComTran;
import bizplay.kosign.com.bizplay.tran.ComUpload;
import bizplay.kosign.com.bizplay.tran.UploadItems;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_C002_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_C002_RES;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_C003_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_C003_RES;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L003_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L003_RES;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L003_RES_REC;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L006_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L006_RES;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L006_RES_BSNS_DSNC_REC;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L006_RES_MGMT1_REC;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L006_RES_MGMT2_REC;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L006_RES_MGMT3_REC;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L006_RES_MGMT4_REC;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L006_RES_MGMT5_REC;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L006_RES_TRAN_KIND_REC;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_R003_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_R003_RES;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_R003_RES_BP_CUST_REC;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_R003_RES_BP_EMPL_REC;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_R003_RES_REC_RCPT_IMG;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_R003_RES_REC_RCPT_RFS;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_R003_RES_REC_RCPT_USER;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_U002_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_U003_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_U003_RES;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_UPLOAD_RES;

import static android.R.attr.data;

public class A007_2Activity extends AppCompatActivity implements ComTran.OnComTranListener, View.OnClickListener {


    private ComTran comtran;
    private boolean is_send = false;
    private JSONArray RCPT_USER_REC = new JSONArray();
    private JSONArray RCPT_IMG_REC = new JSONArray();
    private ComLoading comLoading;

    private final int A007_2_REQUEST_CODE = 1;
    private final int A007_4_1_REQUEST_CODE = 2;
    private final int A007_8_1_REQUEST_CODE = 3;
    private final int A007_8_REQUEST_CODE1 = 4;
    private final int A007_8_REQUEST_CODE2 = 5;
    private final int A007_8_REQUEST_CODE3 = 6;
    private final int A007_8_REQUEST_CODE4 = 7;
    private final int A007_8_REQUEST_CODE5 = 8;
    private final int EMPLOYEE_REQUEST_CODE = 9;
    private final int CUSTOMER_REQUEST_CODE = 10;
    private ArrayList<CusEmp> BP_CUST_LIST;
    private ArrayList<Employee> BP_EMPL_LIST;
    private ArrayList<UsageItem> TRAN_KIND_LIST, BSNS_DSNC_LIST, MGMT1_LIST, MGMT2_LIST, MGMT3_LIST, MGMT4_LIST, MGMT5_LIST;
    private ArrayList<PhotoItem> RCPT_IMG_LIST;
    ImageView iv_up_down, n_iv_coupon, n_iv_tran_kind_nm, n_iv_content1, n_iv_content2, n_iv_content3, n_iv_content4, n_iv_content5, n_iv_employee, n_iv_cust, iv_cam;
    FrameLayout fl_up_down;
    LinearLayout ln_content1, ln_content3, ln_content4, ll_photo;
    private boolean is_hide_view_default;
    private LinearLayout ll_coupon,
            ll_tran_kind_nm, ll_content1, ll_content2, ll_content3, ll_content4, ll_content5, ll_employee, ll_cust;

    private String BSNS_DSNC_ACVT_YN, TRAN_KIND_ACVT_YN, MGMT1_ACVT_YN, MGMT1_NM, MGMT1_METH, MGMT2_ACVT_YN, MGMT2_NM, MGMT2_METH,
            MGMT3_ACVT_YN, MGMT3_NM, MGMT3_METH, MGMT4_ACVT_YN, MGMT4_NM, MGMT4_METH, MGMT5_ACVT_YN, MGMT5_NM, MGMT5_METH,
            EMPL_ACVT_YN, CUST_ACVT_YN, MGMT1_LIST_NM, MGMT2_LIST_NM, MGMT3_LIST_NM, MGMT4_LIST_NM, MGMT5_LIST_NM;
    //View
    private TextView tv_unapprove, tv_coupon, tv_tran_kind_nm, tv_content_title1, tv_content_title2, tv_content_title3, tv_content_title4,
            tv_content_title5, tv_content1, tv_content2, tv_content3, tv_content4, tv_content5, tv_employee, tv_cust;

    private String CD_RCPT_STS, CARD_CORP_CD, BSNS_DSNC_CD, BSNS_DSNC_NM, TRAN_KIND_CD,
            TRAN_KIND_NM, MGMT1, MGMT2, MGMT3, MGMT4, MGMT5, MEST_REPR_NM, MEST_NM, MEST_TEL_NO, MEST_ADDR, CARD_TPBZ_NM,
            API_KEY, RCPT_SEQNO;
    private LinearLayout ll_box_1, ll_box_2, ll_box_3, ll_box_4, ll_box_5, ll_box_6, ll_box_7, ll_box_8, ll_box_9;

    private TextView tv_img_number, tv_personal_card, txt_APV_DT_N_APV_TM, txt_CARD_CORP_NM_N_CARD_NO, txt_MEST_NM, txt_MEST_NO, txt_CARD_TPBZ_NM, txt_TAX_TYPE_CD, txt_APV_NO_value, txt_APV_AMT_value, txt_SUPL_AMT_value, txt_VAT_AMT_value;
    String ydm, reg_tx_cd;
    private String APV_GB = "0";
    Button btn_sent;
    EditText et_memo;
    String tran_kind_cd, tran_kind_nm;
    FrameLayout fl_img_number;
    PercentageCropImageView iv_img;
    int itemIndex, selected_item1 = -1, recommented = -1, selected_item = -1;
    LinearLayout ll_add_participant_7_2, ll_option_popup, activity_a007_1, lv_rcpt_user_rec, ll_img;


    private String s_rcpt_tx_sts, s_CARD_CORP_CD, s_CARD_NO, s_APV_DT, s_APV_SEQ, s_APV_TM, s_APV_NO, s_MEST_BIZ_NO, s_MEST_NO, s_APV_AMT, s_APV_CAN_YN, s_APV_CAN_DT, s_SUPL_AMT, s_VAT_AMT;
    private String RCPT_TX_STS, s_PV_CAN_YN;
    private String PTL_ID, USE_INTT_ID, CHNL_ID, USER_ID;
    private boolean is_stop = false;
    private int uploaded_item = 0;

    private ArrayList<A007_2_4P_Item> tranList = new ArrayList<>();
    private ArrayList<USER_REC_ITEM> List_RCPT_USER_REC = new ArrayList<>();
    private ArrayList<RCPT_RFS_REC_ITEM> List_RCPT_RFS_REC = new ArrayList<>();
    private String RCMD_TRAN_KIND_CD, RCMD_TRAN_KIND_NM;
    private ArrayList<Photo_Item> mPhotoItemInfo = new ArrayList<>();
    private int selected_content2_index = -1, selected_content3_index = -1, selected_content4_index = -1, selected_content5_index = -1, selected_coupon_index = -1, selected_index = -1, selected_content1_index = -1;
    private ImageView detail_op_rct, iv_add_participant;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a007_2);
        //hide first
        findViewById(R.id.activity_a007_1).setVisibility(View.INVISIBLE);

        comtran = new ComTran(this, this);
        comLoading = new ComLoading(this);
        API_KEY = ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getKEY_API().toString();

        init();

        //s_CARD_CORP_CD->check again here
        Log.e("rtk", "API_KEY : " + API_KEY);

        requestData(TX_MYCD_MBL_R003_REQ.TXNO);

    }

    private void init() {
        // data = new Extra_Receipt_L002(this, getIntent());
        activity_a007_1 = (LinearLayout) findViewById(R.id.activity_a007_1);
        detail_op_rct = (ImageView) findViewById(R.id.detail_op_rct);
        detail_op_rct.setOnClickListener(this);
        txt_APV_DT_N_APV_TM = (TextView) findViewById(R.id.txt_APV_DT_N_APV_TM);
        txt_CARD_CORP_NM_N_CARD_NO = (TextView) findViewById(R.id.txt_CARD_CORP_NM_N_CARD_NO);
        txt_MEST_NM = (TextView) findViewById(R.id.txt_MEST_NM);
        txt_MEST_NO = (TextView) findViewById(R.id.txt_MEST_NO);
        txt_TAX_TYPE_CD = (TextView) findViewById(R.id.txt_TAX_TYPE_CD);
        txt_APV_NO_value = (TextView) findViewById(R.id.txt_APV_NO_value);
        txt_APV_AMT_value = (TextView) findViewById(R.id.txt_APV_AMT_value);
        txt_SUPL_AMT_value = (TextView) findViewById(R.id.txt_SUPL_AMT_value);
        txt_VAT_AMT_value = (TextView) findViewById(R.id.txt_VAT_AMT_value);
        // ll_add_participant_7_2 = (LinearLayout) findViewById(R.id.ll_add_participant_7_2);
        ll_option_popup = (LinearLayout) findViewById(R.id.ll_option_popup);
        ll_option_popup.setOnClickListener(this);
        tv_tran_kind_nm = (TextView) findViewById(R.id.tv_tran_kind_nm);
        lv_rcpt_user_rec = (LinearLayout) findViewById(R.id.lv_rcpt_user_rec);
        tv_unapprove = (TextView) findViewById(R.id.tv_unapproves);
        et_memo = (EditText) findViewById(R.id.et_memo);
        iv_add_participant = (ImageView) findViewById(R.id.iv_add_participant);
        ll_photo = (LinearLayout) findViewById(R.id.ll_photo);
        iv_cam = (ImageView) findViewById(R.id.iv_cam);
        iv_cam.setOnClickListener(this);
        //new
        iv_up_down = (ImageView) findViewById(R.id.iv_up_down);
        fl_up_down = (FrameLayout) findViewById(R.id.fl_up_down);

        ln_content1 = (LinearLayout) findViewById(R.id.ln_content1);
        ln_content3 = (LinearLayout) findViewById(R.id.ln_content3);
        ln_content4 = (LinearLayout) findViewById(R.id.ln_content4);

        tv_content1 = (TextView) findViewById(R.id.n_tv_content1);
        tv_content2 = (TextView) findViewById(R.id.n_tv_content2);
        tv_content3 = (TextView) findViewById(R.id.n_tv_content3);
        tv_content4 = (TextView) findViewById(R.id.n_tv_content4);
        tv_content5 = (TextView) findViewById(R.id.n_tv_content5);

        tv_coupon = (TextView) findViewById(R.id.n_tv_coupon);
        tv_tran_kind_nm = (TextView) findViewById(R.id.n_tv_tran_kind_nm);
        tv_employee = (TextView) findViewById(R.id.n_tv_employee);
        tv_cust = (TextView) findViewById(R.id.n_tv_cust);

        ll_box_1 = (LinearLayout) findViewById(R.id.ll_box_1);
        ll_box_2 = (LinearLayout) findViewById(R.id.ll_box_2);
        ll_box_3 = (LinearLayout) findViewById(R.id.ll_box_3);
        ll_box_4 = (LinearLayout) findViewById(R.id.ll_box_4);
        ll_box_5 = (LinearLayout) findViewById(R.id.ll_box_5);
        ll_box_6 = (LinearLayout) findViewById(R.id.ll_box_6);
        ll_box_7 = (LinearLayout) findViewById(R.id.ll_box_7);
        ll_box_8 = (LinearLayout) findViewById(R.id.ll_box_8);
        ll_box_9 = (LinearLayout) findViewById(R.id.ll_box_9);

        ll_coupon = (LinearLayout) findViewById(R.id.ln_coupon);
        ll_tran_kind_nm = (LinearLayout) findViewById(R.id.ln_tran_kind_nm);
        ll_content1 = (LinearLayout) findViewById(R.id.ln_sec1);
        ll_content2 = (LinearLayout) findViewById(R.id.ln_sec2);
        ll_content3 = (LinearLayout) findViewById(R.id.ln_sec3);
        ll_content4 = (LinearLayout) findViewById(R.id.ln_sec4);
        ll_content5 = (LinearLayout) findViewById(R.id.ln_sec5);
        ll_employee = (LinearLayout) findViewById(R.id.ln_employee);
        ll_cust = (LinearLayout) findViewById(R.id.ln_customer);

        tv_content_title1 = (TextView) findViewById(R.id.tv_content_title1);
        tv_content_title2 = (TextView) findViewById(R.id.tv_content_title2);
        tv_content_title3 = (TextView) findViewById(R.id.tv_content_title3);
        tv_content_title4 = (TextView) findViewById(R.id.tv_content_title4);
        tv_content_title5 = (TextView) findViewById(R.id.tv_content_title5);
        n_iv_coupon = (ImageView) findViewById(R.id.n_iv_coupon);
        n_iv_tran_kind_nm = (ImageView) findViewById(R.id.n_iv_tran_kind_nm);
        n_iv_employee = (ImageView) findViewById(R.id.n_iv_employee);
        n_iv_cust = (ImageView) findViewById(R.id.n_iv_cust);

        //Select Box
        n_iv_content1 = (ImageView) findViewById(R.id.n_iv_content1);
        n_iv_content2 = (ImageView) findViewById(R.id.n_iv_content2);
        n_iv_content3 = (ImageView) findViewById(R.id.n_iv_content3);
        n_iv_content4 = (ImageView) findViewById(R.id.n_iv_content4);
        n_iv_content5 = (ImageView) findViewById(R.id.n_iv_content5);

        //Variable init
        TRAN_KIND_LIST = new ArrayList<>();
        RCPT_IMG_LIST = new ArrayList<>();
        BP_EMPL_LIST = new ArrayList<>();
        BP_CUST_LIST = new ArrayList<>();
        BSNS_DSNC_LIST = new ArrayList<>();
        MGMT1_LIST = new ArrayList<>();
        MGMT2_LIST = new ArrayList<>();
        MGMT3_LIST = new ArrayList<>();
        MGMT4_LIST = new ArrayList<>();
        MGMT5_LIST = new ArrayList<>();


        //switch view
        hideView(true);


        tv_personal_card = (TextView) findViewById(R.id.tv_approval_top);
        ll_img = (LinearLayout) findViewById(R.id.ll_img);
        iv_img = (PercentageCropImageView) findViewById(R.id.iv_img);
        fl_img_number = (FrameLayout) findViewById(R.id.fl_img_number);
        tv_img_number = (TextView) findViewById(R.id.tv_img_number);
        txt_CARD_TPBZ_NM = (TextView) findViewById(R.id.txt_CARD_TPBZ_NM);


        Intent intent = getIntent();
        itemIndex = intent.getIntExtra("POSITION", 0);
        s_CARD_CORP_CD = intent.getStringExtra("CARD_CORP_CD");

        //get data from L002
        s_rcpt_tx_sts = ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getRCPT_TX_STS();
        s_CARD_NO = ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getCARD_NO();
        s_APV_DT = ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getAPV_DT();
        s_APV_SEQ = ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getAPV_SEQ();
        s_APV_TM = ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getAPV_TM();
        s_APV_NO = ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getAPV_NO();
        s_MEST_BIZ_NO = ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getMEST_BIZ_NO();
        s_MEST_NO = ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getMEST_NO();
        s_APV_AMT = ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getAPV_AMT();
        s_APV_CAN_YN = ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getAPV_CAN_YN();
        s_APV_CAN_DT = ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getAPV_CAN_DT();

        s_PV_CAN_YN = ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getAPV_CAN_YN();
        /*Button*/
        btn_sent = (Button) findViewById(R.id.btn_sent);
        btn_sent.setOnClickListener(this);
        fl_up_down.setOnClickListener(this);
        activity_a007_1.setVisibility(View.GONE);

    }

    private void requestData(String trancode) {
        try {
            if (trancode.equals(TX_MYCD_MBL_R003_REQ.TXNO)) {
                TX_MYCD_MBL_R003_REQ req = new TX_MYCD_MBL_R003_REQ();
                req.setRCPT_TX_STS(s_rcpt_tx_sts);
                req.setCARD_CORP_CD(s_CARD_CORP_CD);
                req.setCARD_NO(s_CARD_NO);
                req.setAPV_DT(s_APV_DT);
                req.setAPV_SEQ(s_APV_SEQ);
                req.setAPV_TM(s_APV_TM);
                req.setAPV_NO(s_APV_NO);
                req.setMEST_BIZ_NO(s_MEST_BIZ_NO);
                req.setMEST_NO(s_MEST_NO);
                req.setAPV_AMT(s_APV_AMT);
                req.setAPV_CAN_YN(s_APV_CAN_YN);
                req.setAPV_CAN_DT(s_APV_CAN_DT);
                comtran.requestData(TX_MYCD_MBL_R003_REQ.TXNO, req.getSendMessage(), true);
            } else if (trancode.equals(TX_MYCD_MBL_L003_REQ.TXNO)) {
                //request list of popup
                TX_MYCD_MBL_L003_REQ req = new TX_MYCD_MBL_L003_REQ();
                req.setCARD_NO(s_CARD_NO);
                req.setCARD_CORP_CD(s_CARD_CORP_CD);
                comtran.requestData(TX_MYCD_MBL_L003_REQ.TXNO, req.getSendMessage());

            } else if (trancode.equals(TX_MYCD_MBL_C002_REQ.TXNO)) {
                TX_MYCD_MBL_C002_REQ req = new TX_MYCD_MBL_C002_REQ();
                //1: Normal registration, 2: Cancellation history transfer, 3: Personal use history transfer
                req.setREG_TX_CD(reg_tx_cd);
                req.setCARD_CORP_CD(CARD_CORP_CD);
                req.setCARD_NO(s_CARD_NO);
                req.setAPV_DT(s_APV_DT);
                req.setAPV_SEQ(s_APV_SEQ);
                req.setAPV_TM(s_APV_TM);
                req.setAPV_NO(s_APV_NO);
                req.setAPV_AMT(s_APV_AMT);
                req.setSUPL_AMT(s_SUPL_AMT);
                req.setVAT_AMT(s_VAT_AMT);
                req.setSUMMARY(et_memo.getText().toString());

                if ("".equals(tv_tran_kind_nm.getText().toString())) {
                    req.setTRAN_KIND_CD("");
                    req.setTRAN_KIND_NM("");
                } else {
                    req.setTRAN_KIND_CD(tran_kind_cd);
                    req.setTRAN_KIND_NM(tran_kind_nm);
                }

                if (List_RCPT_USER_REC != null) {
                    for (int i = 0; i < List_RCPT_USER_REC.size(); i++) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("RCPT_USER_NO", List_RCPT_USER_REC.get(i).getRCPT_USER_NO());
                        RCPT_USER_REC.put(jsonObject);
                    }
                }
                req.setRCPT_USER_REC(RCPT_USER_REC);
                if (mPhotoItemInfo != null) {
                    for (int i = 0; i < mPhotoItemInfo.size(); i++) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("RCPT_IMG_URL", mPhotoItemInfo.get(i).getImageUrl());
                        RCPT_IMG_REC.put(jsonObject);
                    }
                }
                req.setRCPT_IMG_REC(RCPT_IMG_REC);

                DevLog.devLog("ooo " + req.getSendMessage());
                comtran.requestData(TX_MYCD_MBL_C002_REQ.TXNO, req.getSendMessage(), true);
            } else if (trancode.equals(TX_MYCD_MBL_L006_REQ.TXNO)) {
                TX_MYCD_MBL_L006_REQ req = new TX_MYCD_MBL_L006_REQ();
                req.setCARD_NO(s_CARD_NO);
                req.setCARD_CORP_CD(s_CARD_CORP_CD);
                req.setCARD_NO(s_CARD_NO);
                comtran.requestData(TX_MYCD_MBL_L006_REQ.TXNO, req.getSendMessage(), false);

            } else if (trancode.equals(TX_MYCD_MBL_U003_REQ.TXNO)) {

                TX_MYCD_MBL_U003_REQ req = new TX_MYCD_MBL_U003_REQ();
                // req.setREG_TX_CD(reg_tx_cd);
                req.setCARD_CORP_CD(CARD_CORP_CD);
                req.setCARD_NO(s_CARD_NO);
                req.setAPV_DT(s_APV_DT);
                req.setAPV_SEQ(s_APV_SEQ);
                req.setAPV_TM(s_APV_TM);
                req.setAPV_NO(s_APV_NO);
                // req.setAPV_AMT(s_APV_AMT);
                req.setSUPL_AMT(s_SUPL_AMT);
                req.setVAT_AMT(s_VAT_AMT);
                req.setBSNS_DSNC_CD(BSNS_DSNC_CD);
                req.setBSNS_DSNC_NM(BSNS_DSNC_NM);
                req.setTRAN_KIND_NM(TRAN_KIND_NM);
                req.setTRAN_KIND_CD(TRAN_KIND_CD);
                req.setMGMT1(MGMT1);
                req.setMGMT1_NM(MGMT1_NM);
                req.setMGMT2(MGMT2);
                req.setMGMT2_NM(MGMT2_NM);
                req.setMGMT3(MGMT3);
                req.setMGMT3_NM(MGMT3_NM);
                req.setMGMT4(MGMT4);
                req.setMGMT4_NM(MGMT4_NM);
                req.setMGMT5(MGMT5);
                req.setMGMT5_NM(MGMT5_NM);


                JSONArray BP_EMPL_REC = new JSONArray();
                if (BP_EMPL_REC != null) {
                    for (int i = 0; i < BP_EMPL_LIST.size(); i++) {
                        JSONObject obj = new JSONObject();
                        obj.put("BP_EMPL_NO", BP_EMPL_LIST.get(i).getBP_EMPL_NO());
                        obj.put("BP_EMPL_NM", BP_EMPL_LIST.get(i).getBP_EMPL_NM());
                        BP_EMPL_REC.put(obj);
                    }
                }
                req.setBP_EMPL_REC(BP_EMPL_REC);

                JSONArray BP_CUST_REC = new JSONArray();
                if (BP_CUST_REC != null) {
                    for (int i = 0; i < BP_CUST_LIST.size(); i++) {
                        JSONObject obj = new JSONObject();
                        obj.put("BP_CUST_NO", BP_CUST_LIST.get(i).getBP_CUST_NO());
                        obj.put("BP_MAGR_NO", BP_CUST_LIST.get(i).getBP_MAGR_NO());
                        obj.put("BP_CUST_NM", BP_CUST_LIST.get(i).getBP_CUST_NM());
                        obj.put("BP_MAGR_NM", BP_CUST_LIST.get(i).getBP_MAGR_NM());
                        BP_CUST_REC.put(obj);
                    }
                }
                req.setBP_CUST_REC(BP_CUST_REC);

                JSONArray RCPT_USER_REC = new JSONArray();
                if (RCPT_USER_REC != null) {
                    for (int i = 0; i < List_RCPT_USER_REC.size(); i++) {
                        JSONObject obj = new JSONObject();
                        obj.put("RCPT_USER_NO", List_RCPT_USER_REC.get(i).getRCPT_USER_NO());
                        RCPT_USER_REC.put(obj);
                    }
                }
                req.setRCPT_USER_REC(RCPT_USER_REC);

                JSONArray RCPT_IMG_REC = new JSONArray();
                if (RCPT_IMG_REC != null) {
                    for (int i = 0; i < RCPT_IMG_LIST.size(); i++) {
                        JSONObject obj = new JSONObject();
                        obj.put("RCPT_IMG_URL", RCPT_IMG_LIST.get(i).getImageUrl());
                        RCPT_IMG_REC.put(obj);
                    }
                }
                req.setRCPT_IMG_REC(RCPT_IMG_REC);
                comtran.requestData(TX_MYCD_MBL_C003_REQ.TXNO, req.getSendMessage(), true);
            } else if (trancode.equals(TX_MYCD_MBL_U002_REQ.TXNO)) {
                TX_MYCD_MBL_U002_REQ u002_req = new TX_MYCD_MBL_U002_REQ();
                u002_req.setCARD_CORP_CD(CARD_CORP_CD);
                u002_req.setCARD_NO(s_CARD_NO);
                u002_req.setAPV_DT(s_APV_DT);
                u002_req.setAPV_SEQ(s_APV_SEQ);
                u002_req.setAPV_TM(s_APV_TM);
                u002_req.setAPV_NO(s_APV_NO);
                comtran.requestData(TX_MYCD_MBL_U002_REQ.TXNO, u002_req.getSendMessage(), true);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onMoreInfo(View view) {
        new RecipientInfo(this);
    }

    private class RecipientInfo extends Dialog {
        public RecipientInfo(Context context) {
            super(context);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.popup_a007_2p);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            TextView tv1 = (TextView) findViewById(R.id.tv_1);
            TextView tv2 = (TextView) findViewById(R.id.tv_2);
            TextView tv3 = (TextView) findViewById(R.id.tv_3);
            final TextView tv4 = (TextView) findViewById(R.id.tv_4);
            TextView tv5 = (TextView) findViewById(R.id.tv_5);

            tv1.setText(MEST_NM);
            tv2.setText(CARD_TPBZ_NM);
            tv3.setText(MEST_REPR_NM);
            if (!"".equals(MEST_TEL_NO)) {
                findViewById(R.id.iv_call).setVisibility(View.VISIBLE);
            }
            tv4.setText(FormatUtil.formatePhoneNumber(MEST_TEL_NO));
            tv5.setText(MEST_ADDR);

            findViewById(R.id.ll_confirm).setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    dismiss();
                }
            });
            findViewById(R.id.ll_close).setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    dismiss();
                }
            });
            findViewById(R.id.iv_call).setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    makeCall(tv4.getText().toString());
                }
            });

            show();
        }

    }

    public void makeCall(String phoneNumber) {
        //Intent to dial activity
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber));
        startActivity(intent);
    }

    @Override
    public void onTranResponse(String tranCode, Object object) throws Exception {
        if (tranCode.equals(TX_MYCD_MBL_R003_REQ.TXNO)) {
            TX_MYCD_MBL_R003_RES res = new TX_MYCD_MBL_R003_RES(object);
            DevLog.devLog(res.toString());
            findViewById(R.id.activity_a007_1).setVisibility(View.VISIBLE);

            MEST_NM = ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getMEST_NM();
            MEST_REPR_NM = res.getMEST_REPR_NM().equals("null") ? "" : res.getMEST_REPR_NM();
            MEST_TEL_NO = res.getMEST_TEL_NO().equals("null") ? "" : res.getMEST_TEL_NO();
            MEST_ADDR = res.getMEST_ADDR().equals("null") ? "" : res.getMEST_ADDR();
            CARD_TPBZ_NM = res.getCARD_TPBZ_NM().equals("null") ? "" : res.getCARD_TPBZ_NM();

            String day = ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getAPV_DT();
            String time = ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getAPV_TM();
            ydm = FormatUtil.getReceiptDateTime(day, time);
            String card_start = ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getCARD_NO().substring(0, 4);
            String card_end = ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getCARD_NO().substring(12, ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getCARD_NO().length());

            txt_APV_DT_N_APV_TM.setText(ydm);
            txt_CARD_CORP_NM_N_CARD_NO.setText(res.getCARD_CORP_NM() == null ? "" : res.getCARD_CORP_NM() + " " + card_start + " - **** - **** - " + card_end);
            txt_MEST_NM.setText(MEST_NM);
            txt_MEST_NO.setText(ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getMEST_NO());
            txt_TAX_TYPE_CD.setText(getTaxName(res.getTAX_TYPE_CD()));
            txt_APV_NO_value.setText(ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getAPV_NO());
            txt_CARD_TPBZ_NM.setText(res.getCARD_TPBZ_NM().equals("null") ? "" : res.getCARD_TPBZ_NM());

            String s_APV_ATM = res.getCARD_CORP_CD().equals("null") ? "" : res.getAPV_AMT();


            //for upload image
            // private String PTL_ID, USE_INTT_ID, CHNL_ID, USER_ID;
            PTL_ID = res.getPTL_ID().equals("null") ? "" : res.getPTL_ID();
            USE_INTT_ID = res.getUSE_INTT_ID().equals("null") ? "" : res.getUSE_INTT_ID();
            CHNL_ID = res.getCHNL_ID().equals("null") ? "" : res.getCHNL_ID();
            USER_ID = res.getUSER_ID().equals("null") ? "" : res.getUSER_ID();


            //Upload to server
            CARD_CORP_CD = res.getCARD_CORP_CD().equals("null") ? "" : res.getCARD_CORP_CD();
            s_VAT_AMT = res.getVAT_AMT().equals("null") ? "" : res.getVAT_AMT();
            s_SUPL_AMT = res.getSUPL_AMT().equals("null") ? "" : res.getSUPL_AMT();


            String f_apn_amt = FormatUtil.getCommaFormat(s_APV_ATM) + " 원";
            String f_supl_amt = FormatUtil.getCommaFormat(s_SUPL_AMT) + " 원";
            String f_vat_amt = FormatUtil.getCommaFormat(s_VAT_AMT) + " 원";

            txt_APV_AMT_value.setText(f_apn_amt);
            txt_SUPL_AMT_value.setText(f_supl_amt);
            txt_VAT_AMT_value.setText(f_vat_amt);

            TRAN_KIND_CD = res.getTRAN_KIND_CD().equals("null") ? "" : res.getTRAN_KIND_CD();
            BSNS_DSNC_CD = res.getBSNS_DSNC_CD().equals("null") ? "" : res.getBSNS_DSNC_CD();
            BSNS_DSNC_NM = res.getBSNS_DSNC_NM().equals("null") ? "" : res.getBSNS_DSNC_NM();

            tran_kind_nm = res.getTRAN_KIND_NM().equals("null") ? "" : res.getTRAN_KIND_NM();


            tv_coupon.setText(BSNS_DSNC_NM);
            tv_tran_kind_nm.setText(tran_kind_nm);

            s_rcpt_tx_sts = ReceiptFragment.getInstance().mReceiptOfCard.getReceiptRecs().get(itemIndex).getRCPT_TX_STS().toLowerCase().toString();
            CD_RCPT_STS = res.getCD_RCPT_STS();


            TX_MYCD_MBL_R003_RES_REC_RCPT_USER recUser = res.getRCPT_USER_REC();

            for (int i = 0; i < recUser.getLength(); i++) {
                USER_REC_ITEM item = new USER_REC_ITEM();
                item.setRCPT_USER_NO(recUser.getRCPT_USER_NO().equals("null") ? "" : recUser.getRCPT_USER_NO());
                item.setRCPT_USER_HPNO(recUser.getRCPT_USER_HPNO().equals("null") ? "" : recUser.getRCPT_USER_HPNO());
                item.setRCPT_USER_PSN_NM(recUser.getRCPT_USER_PSN_NM().equals("null") ? "" : recUser.getRCPT_USER_PSN_NM());
                item.setRCPT_USER_NM(recUser.getRCPT_USER_NM().equals("null") ? "" : recUser.getRCPT_USER_NM());
                item.setRCPT_USER_BSNN_NM(recUser.getRCPT_USER_BSNN_NM().equals("null") ? "" : recUser.getRCPT_USER_BSNN_NM());
                item.setRCPT_USER_PSN_IMG(recUser.getRCPT_USER_PSN_IMG().equals("null") ? "" : recUser.getRCPT_USER_PSN_IMG());
                List_RCPT_USER_REC.add(item);
                recUser.moveNext();
            }
            addParticipan(lv_rcpt_user_rec, List_RCPT_USER_REC);

            TX_MYCD_MBL_R003_RES_BP_EMPL_REC BP_EMPL_REC = res.getBP_EMPL_REC();
            for (int i = 0; i < BP_EMPL_REC.getLength(); i++) {
                Employee item = new Employee();
                item.setBP_EMPL_NO(BP_EMPL_REC.getBP_EMPL_NO());
                item.setBP_EMPL_NM(BP_EMPL_REC.getBP_EMPL_NM());
                BP_EMPL_LIST.add(item);
                BP_EMPL_REC.moveNext();
            }


            TX_MYCD_MBL_R003_RES_BP_CUST_REC BP_CUST_REC = res.getBP_CUST_REC();
            for (int i = 0; i < BP_CUST_REC.getLength(); i++) {
                CusEmp item = new CusEmp();
                item.setBP_CUST_NO(BP_CUST_REC.getBP_CUST_NO());
                item.setBP_MAGR_NO(BP_CUST_REC.getBP_MAGR_NO());
                item.setBP_CUST_NM(BP_CUST_REC.getBP_CUST_NM());
                item.setBP_MAGR_NM(BP_CUST_REC.getBP_MAGR_NM());
                BP_CUST_LIST.add(item);
                BP_CUST_REC.moveNext();
            }


            TX_MYCD_MBL_R003_RES_REC_RCPT_IMG recImg = res.getRCPT_IMG_REC();
            for (int i = 0; i < recImg.getLength(); i++) {
                Photo_Item item = new Photo_Item();
                item.setImageUrl(recImg.getRCPT_IMG_URL());
                mPhotoItemInfo.add(item);
                recImg.moveNext();
            }


            TX_MYCD_MBL_R003_RES_REC_RCPT_RFS recRFS = res.getRCPT_RFS_REC();
            for (int i = 0; i < recRFS.getLength(); i++) {
                RCPT_RFS_REC_ITEM item = new RCPT_RFS_REC_ITEM();
                item.setRCPT_RFS_HPNO(recRFS.getRCPT_RFS_HPNO());
                item.setRCPT_RFS_NM(recRFS.getRCPT_RFS_NM());
                item.setRCPT_RFS_USER_NO(recRFS.getRCPT_RFS_USER_NO());
                List_RCPT_RFS_REC.add(item);
                recRFS.moveNext();
            }

            if ("B".equals(s_PV_CAN_YN)) {
                tv_unapprove.setVisibility(View.VISIBLE);
                //tv_apn_amt.setPaintFlags(tv_apn_amt.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                String NE_APV_AMT = "-" + f_apn_amt;
                txt_APV_AMT_value.setText(NE_APV_AMT);
                txt_APV_AMT_value.setTextColor(Color.RED);


            }

            if (RCPT_IMG_LIST.size() > 0) {
                showPhoto(true);
            }
            txt_VAT_AMT_value.setText(f_vat_amt);
            et_memo.setText(res.getSUMMARY());

            //New
            MGMT1 = res.getMGMT1().equals("null") ? "" : res.getMGMT1();
            MGMT2 = res.getMGMT2().equals("null") ? "" : res.getMGMT2();
            MGMT3 = res.getMGMT3().equals("null") ? "" : res.getMGMT3();
            MGMT4 = res.getMGMT4().equals("null") ? "" : res.getMGMT4();
            MGMT5 = res.getMGMT5().equals("null") ? "" : res.getMGMT5();

            tv_content1.setText(MGMT1);
            tv_content2.setText(MGMT2);
            tv_content3.setText(MGMT3);
            tv_content4.setText(MGMT4);
            tv_content5.setText(MGMT5);

            String employees = "";
            if (null != BP_EMPL_LIST) {
                for (int i = 0; i < BP_EMPL_LIST.size(); i++) {
                    if (BP_EMPL_LIST.size() == 1 || BP_EMPL_LIST.size() - 1 == i) {
                        employees += BP_EMPL_LIST.get(i).getBP_EMPL_NM();
                    } else {
                        employees += BP_EMPL_LIST.get(i).getBP_EMPL_NM() + ", ";
                    }
                }
                tv_employee.setText(employees);
            }

            String customer = "";
            if (null != BP_CUST_LIST) {
                for (int i = 0; i < BP_CUST_LIST.size(); i++) {
                    if (BP_CUST_LIST.size() == 1 || BP_CUST_LIST.size() - 1 == i) {
                        if ("0".equals(BP_CUST_LIST.get(i).getBP_MAGR_NO())) {
                            customer += BP_CUST_LIST.get(i).getBP_CUST_NM();
                        } else {
                            customer += BP_CUST_LIST.get(i).getBP_CUST_NM() + " (" + BP_CUST_LIST.get(i).getBP_MAGR_NM() + ")";
                        }
                    } else {
                        if ("0".equals(BP_CUST_LIST.get(i).getBP_MAGR_NO())) {
                            customer += BP_CUST_LIST.get(i).getBP_CUST_NM() + ", ";
                        } else {
                            customer += BP_CUST_LIST.get(i).getBP_CUST_NM() + " (" + BP_CUST_LIST.get(i).getBP_MAGR_NM() + "), ";
                        }
                    }
                }
                tv_cust.setText(customer);
            }

            requestData(TX_MYCD_MBL_L006_REQ.TXNO);


        }else if(tranCode.equals(TX_MYCD_MBL_U002_REQ.TXNO)){
                Constants.ReceiptInfo.RELOAD_RECEIPT = true;
                Constants.ReceiptInfo.SHOW_CANCEL_SUCESS_SNACKBAR = true;
                finish();
        }else if(tranCode.equals(TX_MYCD_MBL_L003_REQ.TXNO)) {

            TX_MYCD_MBL_L003_RES res = new TX_MYCD_MBL_L003_RES(object);
            TX_MYCD_MBL_L003_RES_REC res_rec = res.getREC();
            RCMD_TRAN_KIND_CD = res_rec.getTRAN_KIND_CD();
            RCMD_TRAN_KIND_NM = res_rec.getTRAN_KIND_NM();

            tranList = new ArrayList<>();
            for (int i = 0; i < res_rec.getLength(); i++) {
                if (RCMD_TRAN_KIND_CD.equals(res_rec.getTRAN_KIND_CD())) {
                    selected_item1 = i;
                    recommented = i;
                }
                if (tran_kind_cd.equals(res_rec.getTRAN_KIND_CD())) {
                    selected_item = i;
                }
                A007_2_4P_Item item = new A007_2_4P_Item();
                item.setTRAN_KIND_CD(res_rec.getTRAN_KIND_CD());
                item.setTRAN_KIND_NM(res_rec.getTRAN_KIND_NM());
                tranList.add(item);
                res_rec.moveNext();
            }

            addParticipan(lv_rcpt_user_rec, List_RCPT_USER_REC);

            if ("0".equals(s_rcpt_tx_sts)) {
                if ("".equals(RCMD_TRAN_KIND_CD)) {
                    findViewById(R.id.iv_recomment).setVisibility(View.GONE);
                    findViewById(R.id.no_hav_recom).setVisibility(View.VISIBLE);
                    findViewById(R.id.hav_recom).setVisibility(View.GONE);
                } else {
                    findViewById(R.id.iv_recomment).setVisibility(View.VISIBLE);
                    findViewById(R.id.no_hav_recom).setVisibility(View.GONE);
                    findViewById(R.id.hav_recom).setVisibility(View.VISIBLE);
                    tv_tran_kind_nm.setText(RCMD_TRAN_KIND_NM);
                    tran_kind_cd = RCMD_TRAN_KIND_CD;
                    tran_kind_nm = RCMD_TRAN_KIND_NM;

                }
            }


        } else if (tranCode.equals(TX_MYCD_MBL_U003_REQ.TXNO)) {
            Log.e("rtk", "C003 working");
            TX_MYCD_MBL_U003_RES res = new TX_MYCD_MBL_U003_RES(object);
            RCPT_SEQNO = res.getRCPT_SEQNO();
            Constants.ReceiptInfo.RELOAD_RECEIPT = true;
            Constants.ReceiptInfo.SHOW_UPDATE_SNACKBAR = true;
            finish();

        } else if (tranCode.equals(TX_MYCD_MBL_L006_REQ.TXNO)) {

            TX_MYCD_MBL_L006_RES res = new TX_MYCD_MBL_L006_RES(object);
            BSNS_DSNC_ACVT_YN = res.getBSNS_DSNC_ACVT_YN();
            TRAN_KIND_ACVT_YN = res.getTRAN_KIND_ACVT_YN();
            MGMT1_ACVT_YN = res.getMGMT1_ACVT_YN();
            MGMT1_NM = res.getMGMT1_NM();
            MGMT1_METH = res.getMGMT1_METH();
            MGMT2_ACVT_YN = res.getMGMT2_ACVT_YN();
            MGMT2_NM = res.getMGMT2_NM();
            MGMT2_METH = res.getMGMT2_METH();
            MGMT3_ACVT_YN = res.getMGMT3_ACVT_YN();
            MGMT3_NM = res.getMGMT3_NM();
            MGMT3_METH = res.getMGMT3_METH();
            MGMT4_ACVT_YN = res.getMGMT4_ACVT_YN();
            MGMT4_NM = res.getMGMT4_NM();
            MGMT4_METH = res.getMGMT4_METH();
            MGMT5_ACVT_YN = res.getMGMT5_ACVT_YN();
            MGMT5_NM = res.getMGMT5_NM();
            MGMT5_METH = res.getMGMT5_METH();
            EMPL_ACVT_YN = res.getEMPL_ACVT_YN();
            CUST_ACVT_YN = res.getCUST_ACVT_YN();


            if ("MYCD_MBL_R009".equals(API_KEY)) {
                BSNS_DSNC_ACVT_YN = "N";
                TRAN_KIND_ACVT_YN = "N";
                MGMT1_ACVT_YN = "N";
                MGMT2_ACVT_YN = "N";
                MGMT3_ACVT_YN = "N";
                MGMT4_ACVT_YN = "N";
                MGMT5_ACVT_YN = "N";
                EMPL_ACVT_YN = "N";
                CUST_ACVT_YN = "N";
            }


            tv_content_title1.setText(MGMT1_NM);
            tv_content_title2.setText(MGMT2_NM);
            tv_content_title3.setText(MGMT3_NM);
            tv_content_title4.setText(MGMT4_NM);
            tv_content_title5.setText(MGMT5_NM);

            TX_MYCD_MBL_L006_RES_BSNS_DSNC_REC BSNS_DSNC_REC = res.getBSNS_DSNC_REC();
            for (int i = 0; i < BSNS_DSNC_REC.getLength(); i++) {
                if (BSNS_DSNC_CD.equals(BSNS_DSNC_REC.getBSNS_DSNC_CD()) && BSNS_DSNC_NM.equals(BSNS_DSNC_REC.getBSNS_DSNC_NM())) {
                    selected_coupon_index = i;
                }
                UsageItem item = new UsageItem();
                item.setITEM_CODE(BSNS_DSNC_REC.getBSNS_DSNC_CD());
                item.setITEM_NAME(BSNS_DSNC_REC.getBSNS_DSNC_NM());
                BSNS_DSNC_LIST.add(item);
                BSNS_DSNC_REC.moveNext();
            }

            TX_MYCD_MBL_L006_RES_TRAN_KIND_REC TRAN_KIND_REC = res.getTRAN_KIND_REC();
            Log.e("rtk", "KINDCD:" + TRAN_KIND_REC.getTRAN_KIND_CD() + "KINDNM:" + TRAN_KIND_REC.getTRAN_KIND_NM() + "->" + "STringCD" + TRAN_KIND_CD + "StringNM" + tran_kind_nm);

            for (int i = 0; i < TRAN_KIND_REC.getLength(); i++) {
                try {
                    if (TRAN_KIND_CD.equals(TRAN_KIND_REC.getTRAN_KIND_CD()) && tran_kind_nm.equals(TRAN_KIND_REC.getTRAN_KIND_NM())) {
                        selected_index = i;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                UsageItem item = new UsageItem();
                item.setITEM_CODE(TRAN_KIND_REC.getTRAN_KIND_CD());
                item.setITEM_NAME(TRAN_KIND_REC.getTRAN_KIND_NM());
                TRAN_KIND_LIST.add(item);
                TRAN_KIND_REC.moveNext();
            }

            TX_MYCD_MBL_L006_RES_MGMT1_REC MGMT1_REC = res.getMGMT1_REC();
            for (int i = 0; i < MGMT1_REC.getLength(); i++) {
                if (MGMT1 != null && MGMT1_NM.equals(MGMT1_REC.getMGMT1_LIST_NM())) {
                    selected_content1_index = i;
                }
                UsageItem item = new UsageItem();
                item.setITEM_NAME(MGMT1_REC.getMGMT1_LIST_NM());
                MGMT1_LIST.add(item);
                MGMT1_REC.moveNext();
            }

            TX_MYCD_MBL_L006_RES_MGMT2_REC MGMT2_REC = res.getMGMT2_REC();
            for (int i = 0; i < MGMT2_REC.getLength(); i++) {
                if (MGMT2 != null && MGMT2_NM.equals(MGMT2_REC.getMGMT2_LIST_NM())) {
                    selected_content2_index = i;
                }
                UsageItem item = new UsageItem();
                item.setITEM_NAME(MGMT2_REC.getMGMT2_LIST_NM());
                MGMT2_LIST.add(item);
                MGMT2_REC.moveNext();
            }
            TX_MYCD_MBL_L006_RES_MGMT3_REC MGMT3_REC = res.getMGMT3_REC();
            for (int i = 0; i < MGMT3_REC.getLength(); i++) {
                if (MGMT3 != null && MGMT3_NM.equals(MGMT3_REC.getMGMT3_LIST_NM())) {
                    selected_content3_index = i;
                }
                UsageItem item = new UsageItem();
                item.setITEM_NAME(MGMT3_REC.getMGMT3_LIST_NM());
                MGMT3_LIST.add(item);
                MGMT3_REC.moveNext();
            }

            TX_MYCD_MBL_L006_RES_MGMT4_REC MGMT4_REC = res.getMGMT4_REC();
            for (int i = 0; i < MGMT4_REC.getLength(); i++) {
                if (MGMT4 != null && MGMT4_NM.equals(MGMT4_REC.getMGMT4_LIST_NM())) {
                    selected_content4_index = i;
                }
                UsageItem item = new UsageItem();
                item.setITEM_NAME(MGMT4_REC.getMGMT4_LIST_NM());
                MGMT4_LIST.add(item);
                MGMT4_REC.moveNext();
            }
            TX_MYCD_MBL_L006_RES_MGMT5_REC MGMT5_REC = res.getMGMT5_REC();
            for (int i = 0; i < MGMT5_REC.getLength(); i++) {
                if (MGMT5 != null && MGMT5_NM.equals(MGMT5_REC.getMGMT5_LIST_NM())) {
                    selected_content5_index = i;
                }
                UsageItem item = new UsageItem();
                item.setITEM_NAME(MGMT5_REC.getMGMT5_LIST_NM());
                MGMT5_LIST.add(item);
                MGMT5_REC.moveNext();
            }

            if ("".equals(BSNS_DSNC_NM))
                ll_box_1.setVisibility(View.GONE);

            if ("".equals(TRAN_KIND_NM))
                ll_box_2.setVisibility(View.GONE);

            if ("".equals(MGMT1))
                ll_box_3.setVisibility(View.GONE);

            if ("".equals(MGMT2))
                ll_box_4.setVisibility(View.GONE);

            if ("".equals(MGMT3))
                ll_box_5.setVisibility(View.GONE);

            if ("".equals(MGMT4))
                ll_box_6.setVisibility(View.GONE);

            if ("".equals(MGMT5))
                ll_box_7.setVisibility(View.GONE);

            if ("".equals(BP_EMPL_LIST.size() == 0))
                ll_box_8.setVisibility(View.GONE);

            if ("".equals(BP_CUST_LIST.size() == 0))
                ll_box_9.setVisibility(View.GONE);

            activity_a007_1.setVisibility(View.VISIBLE);

        }


    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("rtk", "A007_1  Pauce");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("rtk", "A007_1  Resume");
    }

    private String getTaxName(String tax_type_cd) {
        String tax_name = "";
        if (tax_type_cd.equals("00")) {
            tax_name = "미등록";
        } else if (tax_type_cd.equals("01")) {
            tax_name = "일반";
        } else if (tax_type_cd.equals("02")) {
            tax_name = "간이";
        } else if (tax_type_cd.equals("03")) {
            tax_name = "면세";
        } else if (tax_type_cd.equals("04")) {
            tax_name = "비영리";
        } else if (tax_type_cd.equals("09")) {
            tax_name = "휴업";
        } else if (tax_type_cd.equals("10")) {
            tax_name = "폐업";
        } else {
            tax_name = "";
        }
        return tax_name;
    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }

    public void addParticipan(LinearLayout linearLayout, ArrayList<USER_REC_ITEM> listParticipant) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        linearLayout.removeAllViews();
        for (int i = 0; i < listParticipant.size(); i++) {
            View myView = inflater.inflate(R.layout.a007_4_list_item, null);

            TextView tv_rcpt_user_hpno = (TextView) myView.findViewById(R.id.tv_rcpt_user_hpno);


            USER_REC_ITEM item = listParticipant.get(i);


//
            tv_rcpt_user_hpno.setText(item.getRCPT_USER_NM() + "(" + FormatUtil.formatePhoneNumber(item.getRCPT_USER_HPNO()) + ")");


            linearLayout.addView(myView);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_cam:
                Log.e("rtk", "IV Camera");
                if (RCPT_IMG_LIST.size() >= 3) {
                    DlgAlert.showAlert(this, "한 번에 최대 3장까지만 등록이 가능합니다.", true);
                    return;
                }

                DialogMenu dialogMenu = new DialogMenu(this);
                dialogMenu.setOnDialogMenuListener(new DialogMenu.OnDialogMenuListener() {
                    @Override
                    public void onPicturePress() {
                        Log.e("rtk", "Take Picture");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                PictureUtil.doTakePhotoAction(A007_2Activity.this);
                            }
                        }, 300);
                    }

                    @Override
                    public void onGalleryPress() {
                        Log.e("rtk", "Take from Gallery");
                        Intent intent = new Intent(Action.ACTION_MULTIPLE_PICK_WC);
                        intent.putExtra("cnt", 3 - RCPT_IMG_LIST.size());
                        startActivityForResult(intent, PictureConf.ReqCd.PICTURE.GALLERY);
                    }
                });
                break;
            case R.id.iv_add_participant:
                Log.e("rtk", "add participant");
                Intent a007_4_1Activity = new Intent(this, AddContactActivity.class);
                a007_4_1Activity.putExtra("CARD_CORP_CD", CARD_CORP_CD);
                a007_4_1Activity.putExtra("CARD_NO", s_CARD_NO);
                a007_4_1Activity.putParcelableArrayListExtra("RCPT_USER_LIST", List_RCPT_USER_REC);
                a007_4_1Activity.putExtra("APV_GB", APV_GB);
                a007_4_1Activity.putExtra("APV_DT", s_APV_DT);
                a007_4_1Activity.putExtra("APV_SEQ", s_APV_SEQ);
                a007_4_1Activity.putExtra("APV_TM", s_APV_TM);
                a007_4_1Activity.putExtra("APV_NO", s_APV_NO);
                startActivityForResult(a007_4_1Activity, A007_4_1_REQUEST_CODE);
                break;
            case R.id.detail_op_rct:
                Log.e("rtk", "RCPT_TX_STS:" + s_rcpt_tx_sts);
                if ("1".equals(s_rcpt_tx_sts)) {
                    final ModifyPopup popup = new ModifyPopup(this);
                    popup.setListener(new ModifyPopup.OnePopupMenu() {
                        @Override
                        public void itemClick() {
                            detail_op_rct.setVisibility(View.GONE);
                            popup.dismiss();
                            letEditable(true);

                        }
                    });
                } else if ("2".equals(s_rcpt_tx_sts) || "3".equals(s_rcpt_tx_sts)) {
                    final ModifyPopup popup = new ModifyPopup(this);
                    popup.setListener(new ModifyPopup.OnePopupMenu() {
                        @Override
                        public void itemClick() {
                            // Api
                            requestData(TX_MYCD_MBL_U002_REQ.TXNO);
                            popup.dismiss();
                        }
                    });
                }
                break;
            case R.id.btn_sent:
                //Upload to server
                requestData(TX_MYCD_MBL_U003_REQ.TXNO);

                break;
            case R.id.fl_up_down:
                hideView(is_hide_view_default);
                break;
            case R.id.ln_coupon:
                ListPopupWindow coupoun_popup = new ListPopupWindow(this, BSNS_DSNC_LIST, selected_coupon_index);
                coupoun_popup.setListPopupTitle(getResources().getString(R.string.A007_1_20));
                coupoun_popup.setOnListItemListener(new ListPopupWindow.OnListItemListener() {
                    @Override
                    public void onItemSelected(int position) {
                        if (position != -1) {
                            BSNS_DSNC_CD = BSNS_DSNC_LIST.get(position).getITEM_CODE();
                            BSNS_DSNC_NM = BSNS_DSNC_LIST.get(position).getITEM_NAME();
                            tv_coupon.setText(BSNS_DSNC_NM);
                            ll_coupon.setBackgroundResource(R.drawable.content_box_active);
                            selected_coupon_index = position;
                        }
                    }
                });
                break;
            case R.id.ln_tran_kind_nm:
                ListPopupWindow tran_kind_popup = new ListPopupWindow(this, TRAN_KIND_LIST, selected_index);
                final String popup_title = getResources().getString(R.string.A007_2_4P_1, MEST_NM);
                tran_kind_popup.setOnListItemListener(new ListPopupWindow.OnListItemListener() {
                    @Override
                    public void onItemSelected(int position) {
                        if (position != -1) {
                            TRAN_KIND_CD = TRAN_KIND_LIST.get(position).getITEM_CODE();
                            TRAN_KIND_NM = TRAN_KIND_LIST.get(position).getITEM_NAME();
                            tv_tran_kind_nm.setText(TRAN_KIND_NM);
                            ll_tran_kind_nm.setBackgroundResource(R.drawable.content_box_active);
                            selected_index = position;
                        }
                    }
                });
                break;
            case R.id.ln_sec1:
                if ("1".equals(MGMT1_METH)) {
                    Intent intent1 = new Intent(A007_2Activity.this, A007_8_Activity.class);
                    intent1.putExtra("CONTENT", MGMT1);
                    startActivityForResult(intent1, A007_8_REQUEST_CODE1);
                } else if ("2".equals(MGMT1_METH)) {
                    ListPopupWindow content1_pop = new ListPopupWindow(this, MGMT1_LIST, selected_content1_index);
                    content1_pop.setListPopupTitle(MGMT1_NM);
                    content1_pop.setOnListItemListener(new ListPopupWindow.OnListItemListener() {
                        @Override
                        public void onItemSelected(int position) {
                            if (position != -1) {
                                MGMT1_LIST_NM = MGMT1_LIST.get(position).getITEM_NAME();
                                tv_content1.setText(MGMT1_LIST_NM);
                                ll_content1.setBackgroundResource(R.drawable.content_box_active);
                                selected_content1_index = position;
                            }
                        }
                    });
                }
                break;
            case R.id.ln_sec2:
                if ("1".equals(MGMT2_METH)) {
                    Intent intent2 = new Intent(A007_2Activity.this, A007_8_Activity.class);
                    intent2.putExtra("CONTENT", MGMT2);
                    startActivityForResult(intent2, A007_8_REQUEST_CODE2);
                } else if ("2".equals(MGMT2_METH)) {
                    ListPopupWindow content2_pop = new ListPopupWindow(this, MGMT2_LIST, selected_content2_index);
                    content2_pop.setListPopupTitle(MGMT2_NM);
                    content2_pop.setOnListItemListener(new ListPopupWindow.OnListItemListener() {
                        @Override
                        public void onItemSelected(int position) {
                            if (position != -1) {
                                MGMT2_LIST_NM = MGMT2_LIST.get(position).getITEM_NAME();
                                tv_content2.setText(MGMT2_LIST_NM);
                                ll_content2.setBackgroundResource(R.drawable.content_box_active);
                                selected_content2_index = position;
                            }
                        }
                    });
                }
                break;
            case R.id.ln_sec3:
                if ("1".equals(MGMT3_METH)) {
                    Intent intent3 = new Intent(A007_2Activity.this, A007_8_Activity.class);
                    intent3.putExtra("CONTENT", MGMT3);
                    startActivityForResult(intent3, A007_8_REQUEST_CODE3);

                } else if ("2".equals(MGMT3_METH)) {
                    ListPopupWindow content3_pop = new ListPopupWindow(this, MGMT3_LIST, selected_content3_index);
                    content3_pop.setListPopupTitle(MGMT3_NM);
                    content3_pop.setOnListItemListener(new ListPopupWindow.OnListItemListener() {
                        @Override
                        public void onItemSelected(int position) {
                            if (position != -1) {
                                MGMT3_LIST_NM = MGMT3_LIST.get(position).getITEM_NAME();
                                tv_content3.setText(MGMT3_LIST_NM);
                                ll_content3.setBackgroundResource(R.drawable.content_box_active);
                                selected_content3_index = position;
                            }
                        }
                    });
                }
                break;

            case R.id.ln_sec4:
                Log.e("rtk", "errrosd");
                if ("1".equals(MGMT4_METH)) {
                    Intent intent4 = new Intent(A007_2Activity.this, A007_8_Activity.class);
                    intent4.putExtra("CONTENT", MGMT4);
                    startActivityForResult(intent4, A007_8_REQUEST_CODE4);

                } else if ("2".equals(MGMT4_METH)) {
                    ListPopupWindow content4_pop = new ListPopupWindow(this, MGMT4_LIST, selected_content4_index);
                    content4_pop.setListPopupTitle(MGMT4_NM);
                    content4_pop.setOnListItemListener(new ListPopupWindow.OnListItemListener() {
                        @Override
                        public void onItemSelected(int position) {
                            if (position != -1) {
                                MGMT4_LIST_NM = MGMT4_LIST.get(position).getITEM_NAME();
                                tv_content4.setText(MGMT4_LIST_NM);
                                ll_content4.setBackgroundResource(R.drawable.content_box_active);
                                selected_content4_index = position;
                            }
                        }
                    });
                }
                break;

            case R.id.ln_sec5:
                if ("1".equals(MGMT5_METH)) {
                    Intent intent5 = new Intent(A007_2Activity.this, A007_8_Activity.class);
                    intent5.putExtra("CONTENT", MGMT5);
                    startActivityForResult(intent5, A007_8_REQUEST_CODE5);

                } else if ("2".equals(MGMT5_METH)) {
                    ListPopupWindow content5_pop = new ListPopupWindow(this, MGMT5_LIST, selected_content5_index);
                    content5_pop.setListPopupTitle(MGMT5_NM);
                    content5_pop.setOnListItemListener(new ListPopupWindow.OnListItemListener() {
                        @Override
                        public void onItemSelected(int position) {
                            if (position != -1) {
                                MGMT5_LIST_NM = MGMT5_LIST.get(position).getITEM_NAME();
                                tv_content5.setText(MGMT5_LIST_NM);
                                ll_content5.setBackgroundResource(R.drawable.content_box_active);
                                selected_content5_index = position;
                            }
                        }
                    });
                }
                break;
            case R.id.ln_employee:
                Intent add_emp = new Intent(getApplicationContext(), AddEmployeeActivity.class);
                add_emp.putExtra("CARD_CORP_CD", CARD_CORP_CD);
                add_emp.putExtra("CARD_NO", s_CARD_NO);
                add_emp.putParcelableArrayListExtra("BP_EMPL_LIST", BP_EMPL_LIST);
                startActivityForResult(add_emp, EMPLOYEE_REQUEST_CODE);
                break;
            case R.id.ln_customer:
                Intent add_cust = new Intent(getApplicationContext(), AddCustomerActivity.class);
                add_cust.putExtra("CARD_CORP_CD", CARD_CORP_CD);
                add_cust.putExtra("CARD_NO", s_CARD_NO);
                add_cust.putParcelableArrayListExtra("BP_CUST_LIST", BP_CUST_LIST);
                startActivityForResult(add_cust, CUSTOMER_REQUEST_CODE);
                break;

            default:
                Log.e("rtk", "No Click");
        }

    }

    private void letEditable(boolean isEditable) {
        //set bordor
        if (isEditable) {

            ll_coupon.setBackgroundResource(R.drawable.content_box);
            ll_tran_kind_nm.setBackgroundResource(R.drawable.content_box);
            ll_content1.setBackgroundResource(R.drawable.content_box);
            ll_content2.setBackgroundResource(R.drawable.content_box);
            ll_content3.setBackgroundResource(R.drawable.content_box);
            ll_content4.setBackgroundResource(R.drawable.content_box);
            ll_content5.setBackgroundResource(R.drawable.content_box);
            ll_employee.setBackgroundResource(R.drawable.content_box);
            ll_cust.setBackgroundResource(R.drawable.content_box);
            iv_add_participant.setVisibility(View.VISIBLE);
            iv_add_participant.setOnClickListener(this);
            findViewById(R.id.ln_submit).setVisibility(View.VISIBLE);
//            n_iv_coupon = (ImageView) findViewById(R.id.n_iv_coupon);
//            n_iv_tran_kind_nm = (ImageView) findViewById(R.id.n_iv_tran_kind_nm);
//            n_iv_employee = (ImageView) findViewById(R.id.n_iv_employee);
//            n_iv_cust = (ImageView) findViewById(R.id.n_iv_cust);
//
//            //Select Box
//            n_iv_content1 = (ImageView) findViewById(R.id.n_iv_content1);
//            n_iv_content2 = (ImageView) findViewById(R.id.n_iv_content2);
//            n_iv_content3 = (ImageView) findViewById(R.id.n_iv_content3);
//            n_iv_content4 = (ImageView) findViewById(R.id.n_iv_content4);
//            n_iv_content5 = (ImageView) findViewById(R.id.n_iv_content5);

            if ("Y".equals(BSNS_DSNC_ACVT_YN)) {
                ll_box_1.setVisibility(View.VISIBLE);
                ll_coupon.setOnClickListener(this);
                if (!"".equals(BSNS_DSNC_NM)) {
                    ll_coupon.setBackgroundResource(R.drawable.content_box_active);
                }
                tv_coupon.setHint(getResources().getString(R.string.A007_1_19));
                n_iv_coupon.setVisibility(View.VISIBLE);
            } else {
                ll_box_1.setVisibility(View.GONE);
            }

            if ("Y".equals(TRAN_KIND_ACVT_YN)) {
                ll_box_2.setVisibility(View.VISIBLE);
                ll_tran_kind_nm.setOnClickListener(this);
                if (!"".equals(TRAN_KIND_NM)) {
                    ll_tran_kind_nm.setBackgroundResource(R.drawable.content_box_active);
                }
                n_iv_tran_kind_nm.setVisibility(View.VISIBLE);
                tv_tran_kind_nm.setHint(getResources().getString(R.string.A007_1_19));

            } else {
                ll_box_2.setVisibility(View.GONE);
            }

            if ("Y".equals(MGMT1_ACVT_YN)) {
                ll_box_3.setVisibility(View.VISIBLE);
                ll_content1.setOnClickListener(this);
                if (!"".equals(MGMT1)) {
                    ll_content1.setBackgroundResource(R.drawable.content_box_active);
                }
                if ("2".equals(MGMT1_METH)) {
                    n_iv_content1.setVisibility(View.VISIBLE);
                    tv_content1.setHint(getResources().getString(R.string.A007_1_19));
                } else {
                    tv_content1.setHint(getResources().getString(R.string.A007_1_18));
                }
            } else {
                ll_box_3.setVisibility(View.GONE);
            }

            if ("Y".equals(MGMT2_ACVT_YN)) {
                ll_box_4.setVisibility(View.VISIBLE);
                ll_content2.setOnClickListener(this);
                if (!"".equals(MGMT2)) {
                    ll_content2.setBackgroundResource(R.drawable.content_box_active);
                }
                if ("2".equals(MGMT2_METH)) {
                    n_iv_content2.setVisibility(View.VISIBLE);
                    tv_content2.setHint(getResources().getString(R.string.A007_1_19));
                } else {
                    tv_content2.setHint(getResources().getString(R.string.A007_1_18));
                }
            } else {
                ll_box_4.setVisibility(View.GONE);
            }

            if ("Y".equals(MGMT3_ACVT_YN)) {
                ll_box_5.setVisibility(View.VISIBLE);
                ll_content3.setOnClickListener(this);
                if (!"".equals(MGMT3)) {
                    ll_content3.setBackgroundResource(R.drawable.content_box_active);
                }
                if ("2".equals(MGMT3_METH)) {
                    n_iv_content3.setVisibility(View.VISIBLE);
                    tv_content3.setHint(getResources().getString(R.string.A007_1_19));
                } else {
                    tv_content3.setHint(getResources().getString(R.string.A007_1_18));
                }
            } else {
                ll_box_3.setVisibility(View.GONE);
            }

            if ("Y".equals(MGMT4_ACVT_YN)) {
                ll_box_6.setVisibility(View.VISIBLE);
                ll_content4.setOnClickListener(this);
                if (!"".equals(MGMT4)) {
                    ll_content4.setBackgroundResource(R.drawable.content_box_active);
                }
                if ("2".equals(MGMT4_METH)) {
                    n_iv_content4.setVisibility(View.VISIBLE);
                    tv_content4.setHint(getResources().getString(R.string.A007_1_19));
                } else {
                    tv_content4.setHint(getResources().getString(R.string.A007_1_18));
                }
            } else {
                ll_box_6.setVisibility(View.GONE);
            }

            if ("Y".equals(MGMT5_ACVT_YN)) {
                ll_box_7.setVisibility(View.VISIBLE);
                ll_content5.setOnClickListener(this);
                if (!"".equals(MGMT5)) {
                    ll_content5.setBackgroundResource(R.drawable.content_box_active);
                }
                if ("2".equals(MGMT5_METH)) {
                    n_iv_content5.setVisibility(View.VISIBLE);
                    tv_content5.setHint(getResources().getString(R.string.A007_1_19));
                } else {
                    tv_content5.setHint(getResources().getString(R.string.A007_1_18));
                }
            } else {
                ll_box_7.setVisibility(View.GONE);
            }

            if ("Y".equals(EMPL_ACVT_YN)) {
                ll_box_8.setVisibility(View.VISIBLE);
                ll_employee.setOnClickListener(this);
                if (BP_EMPL_LIST.size() != 0) {
                    ll_employee.setBackgroundResource(R.drawable.content_box_active);
                }
                n_iv_employee.setVisibility(View.VISIBLE);
                tv_employee.setHint(getResources().getString(R.string.A007_1_19));
            } else {
                ll_box_8.setVisibility(View.GONE);
            }

            if ("Y".equals(CUST_ACVT_YN)) {
                ll_box_9.setVisibility(View.VISIBLE);
                ll_cust.setOnClickListener(this);
                if (BP_CUST_LIST.size() > 0) {
                    ll_cust.setBackgroundResource(R.drawable.content_box_active);
                }
                n_iv_cust.setVisibility(View.VISIBLE);
                tv_cust.setHint(getResources().getString(R.string.A007_1_19));
            }

        }
    }

    private void hideView(boolean is_hide) {
        if (is_hide) {

            ln_content1.setVisibility(View.GONE);
            ln_content3.setVisibility(View.GONE);
            ln_content4.setVisibility(View.GONE);

            iv_up_down.setBackgroundResource(R.drawable.listbox_dark_uarr_icon);
        } else {
            ln_content1.setVisibility(View.VISIBLE);
            ln_content3.setVisibility(View.VISIBLE);
            ln_content4.setVisibility(View.VISIBLE);

            iv_up_down.setBackgroundResource(R.drawable.listbox_dark_arr_icon);
        }
        //switch data false or true
        is_hide_view_default = !is_hide;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case A007_8_REQUEST_CODE1:
                if (resultCode == RESULT_OK) {
                    MGMT1 = data.getStringExtra("CONTENT").trim();
                    if (!"".equals(MGMT1)) {
                        ll_content1.setBackgroundResource(R.drawable.content_box_active);
                    } else {
                        ll_content1.setBackgroundResource(R.drawable.content_box);
                    }
                    tv_content1.setText(MGMT1);
                }
                break;
            case A007_8_REQUEST_CODE2:
                if (resultCode == RESULT_OK) {
                    MGMT2 = data.getStringExtra("CONTENT").trim();
                    if (!"".equals(MGMT2)) {
                        ll_content2.setBackgroundResource(R.drawable.content_box_active);
                    } else {
                        ll_content2.setBackgroundResource(R.drawable.content_box);
                    }
                    tv_content2.setText(MGMT2);
                }
                break;
            case A007_8_REQUEST_CODE3:
                if (resultCode == RESULT_OK) {
                    MGMT3 = data.getStringExtra("CONTENT").trim();
                    if (!"".equals(MGMT3)) {
                        ll_content3.setBackgroundResource(R.drawable.content_box_active);
                    } else {
                        ll_content3.setBackgroundResource(R.drawable.content_box);
                    }
                    tv_content3.setText(MGMT3);
                }
                break;
            case A007_8_REQUEST_CODE4:
                if (resultCode == RESULT_OK) {
                    MGMT4 = data.getStringExtra("CONTENT").trim();
                    if (!"".equals(MGMT4)) {
                        ll_content4.setBackgroundResource(R.drawable.content_box_active);
                    } else {
                        ll_content4.setBackgroundResource(R.drawable.content_box);
                    }
                    tv_content4.setText(MGMT3);
                }
                break;
            case A007_8_REQUEST_CODE5:
                if (resultCode == RESULT_OK) {
                    MGMT5 = data.getStringExtra("CONTENT").trim();
                    if (!"".equals(MGMT5)) {
                        ll_content5.setBackgroundResource(R.drawable.content_box_active);
                    } else {
                        ll_content5.setBackgroundResource(R.drawable.content_box);
                    }
                    tv_content5.setText(MGMT5);
                }
                break;
            case EMPLOYEE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    String employees = "";
                    BP_EMPL_LIST = data.getParcelableArrayListExtra("BP_EMPL_LIST");
                    for (int i = 0; i < BP_EMPL_LIST.size(); i++) {
                        if (BP_EMPL_LIST.size() == 1 || BP_EMPL_LIST.size() - 1 == i) {
                            employees += BP_EMPL_LIST.get(i).getBP_EMPL_NM();
                        } else {
                            employees += BP_EMPL_LIST.get(i).getBP_EMPL_NM() + ", ";
                        }
                    }
                    tv_employee.setText(employees);
                    Log.e("rtk", "Employ:" + employees);
                    if (BP_EMPL_LIST.size() > 0) {
                        ll_employee.setBackgroundResource(R.drawable.content_box_active);
                    } else {
                        ll_employee.setBackgroundResource(R.drawable.content_box);
                    }
                }

                break;
            case CUSTOMER_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Log.e("rtk", "data 1");
                    String info = "";
                    BP_CUST_LIST = data.getParcelableArrayListExtra("BP_CUST_LIST");
                    Log.e("rtk", "data value =" + BP_CUST_LIST.size());
                    if (BP_CUST_LIST != null) {
                        Log.e("rtk", "data 3");
                        for (int i = 0; i < BP_CUST_LIST.size(); i++) {
                            if (BP_CUST_LIST.size() == 1 || BP_CUST_LIST.size() - 1 == i) {
                                if ("0".equals(BP_CUST_LIST.get(i).getBP_MAGR_NO()))
                                    info += BP_CUST_LIST.get(i).getBP_CUST_NM();
                                else
                                    info += BP_CUST_LIST.get(i).getBP_CUST_NM() + "(" + BP_CUST_LIST.get(i).getBP_MAGR_NM() + ")";
                            } else {

                                if ("0".equals(BP_CUST_LIST.get(i).getBP_MAGR_NO()))
                                    info += BP_CUST_LIST.get(i).getBP_CUST_NM() + ", ";
                                else
                                    info += BP_CUST_LIST.get(i).getBP_CUST_NM() + "(" + BP_CUST_LIST.get(i).getBP_MAGR_NM() + "), ";
                            }
                        }
                        Log.e("rtk", info + "data");
                        tv_cust.setText(info);
                        if (BP_CUST_LIST.size() > 0) {
                            ll_cust.setBackgroundResource(R.drawable.content_box_active);
                        } else {
                            ll_cust.setBackgroundResource(R.drawable.content_box);
                        }
                    }
                }
                break;
            case A007_4_1_REQUEST_CODE:
                Log.e("rtkgggg", List_RCPT_RFS_REC.toString() + List_RCPT_USER_REC.size());

                if (resultCode == RESULT_OK) {
                    List_RCPT_USER_REC = data.getParcelableArrayListExtra("RCPT_USER_LIST");
                    Log.e("rtkgggg", List_RCPT_RFS_REC.toString() + List_RCPT_USER_REC.size());
                    findViewById(R.id.ln_section2).setVisibility(View.VISIBLE);
                    addParticipan(lv_rcpt_user_rec, List_RCPT_USER_REC);
                    is_send = true;
                }
                break;
            case PictureConf.ReqCd.PICTURE.CAMERA:

                if (resultCode == RESULT_OK) {
                    Bitmap bm = PictureUtil.getBitmapByCameraAtActivity(data);
                    Uri uri = PictureUtil.getFileName();
                    String fileName = uri.getPath().substring(uri.getPath().lastIndexOf("/") + 1);

                    final PhotoItem photo = new PhotoItem();
                    photo.setFileName(fileName);
                    photo.setBitmap(bm);
                    comLoading.showProgressDialog();
                    uploadToCloud(photo, 1);

                }
                break;
            case PictureConf.ReqCd.PICTURE.GALLERY:
                Log.e("rtk", "Gallery");
                if (resultCode == RESULT_OK) {
                    final String[] all_path = data.getStringArrayExtra("all_path");
                    comLoading.showProgressDialog();

                    for (String str : all_path) {
                        Uri uri = null;
                        try {
                            uri = PictureUtil.getUriFromPath(A007_2Activity.this, str);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Bitmap bm = PictureUtil.getBitmapByGalleryAtFragment(A007_2Activity.this, uri);
                        String filename = str.substring(str.lastIndexOf("/") + 1);
                        PhotoItem photo = new PhotoItem();
                        photo.setBitmap(bm);
                        photo.setFileName(filename);

                        uploadToCloud(photo, all_path.length);

                    }
                }
                break;


        }
    }

    private void uploadToCloud(final PhotoItem photo, int i) {
        Log.e("rtk", photo.toString());
        ComUpload comUpload = new ComUpload(this);
        final UploadItems uploadItems = new UploadItems();
        uploadItems.setCHNL_ID(CHNL_ID);
        uploadItems.setPTL_ID(PTL_ID);
        uploadItems.setUSER_ID(USER_ID);
        uploadItems.setUSE_INTT_ID(USE_INTT_ID);
        uploadItems.setFILE(photo.getBitmap());
        uploadItems.setFILE_NM(photo.getFileName());

        comUpload.uploadImage(uploadItems);
        comUpload.setOnCompleted(new ComUpload.OnUploadListener() {
            @Override
            public void onUploadCompleted(Object obj) {
                try {
                    TX_MYCD_MBL_UPLOAD_RES res = new TX_MYCD_MBL_UPLOAD_RES(obj);
                    comLoading.dismissProgressDialog();
                    photo.setImageUrl(res.getIMG_PATH());
                    RCPT_IMG_LIST.add(photo);
                    uploaded_item++;
                    if (RCPT_IMG_LIST.size() > 0) {
                        showPhoto(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onUploadError(String errmsg) {
                comLoading.dismissProgressDialog();
                // check to show an error occurred while transferring pictures
                if (!is_stop) {
                    AlertDialog.Builder builder;
                    builder = new AlertDialog.Builder(A007_2Activity.this);
                    builder.setTitle(getResources().getString(R.string.alert_info));
                    builder.setMessage(errmsg);
                    builder.setPositiveButton("확인",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    is_stop = false;
                                }
                            }).show();

                    is_stop = true;
                }
            }
        });
    }

    private void showPhoto(boolean is_removable) {
        try {
            ll_photo.removeAllViews();
            if (RCPT_IMG_LIST.size() > 0) {
                ll_photo.setVisibility(View.VISIBLE);

                for (int idx = 0; idx < RCPT_IMG_LIST.size(); idx++) {
                    PhotoItem item = RCPT_IMG_LIST.get(idx);
                    View view = LayoutInflater.from(this).inflate(R.layout.layout_photo_item, null);
                    LinearLayout ll_photo_item = (LinearLayout) view.findViewById(R.id.ll_photo_item);
                    final ImageView iv_photo = (ImageView) view.findViewById(R.id.iv_photo);
                    final ImageView iv_del = (ImageView) view.findViewById(R.id.btn_delete);

                    iv_del.setTag(idx);

                    if (is_removable) {
                        iv_del.setVisibility(View.VISIBLE);
                    } else {
                        iv_del.setVisibility(View.GONE);
                    }

                    if (item.getBitmap() != null) {
                        iv_del.setVisibility(View.VISIBLE);
                        iv_photo.setImageBitmap(item.getBitmap());

                    } else {
                        Glide.with(this)
                                .load(item.getImageUrl()).listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
                                return false;
                            }
                        })
                                .error(R.drawable.user_empty_icon)
                                .into(iv_photo);
                    }
                    iv_del.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.e("rtk", "you want to delete?");
                            int position = (int) v.getTag();
                            RCPT_IMG_LIST.remove(position);
                            showPhoto(true);
                        }
                    });
                    final int photo_index = idx;
                    ll_photo_item.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Extras_Picture extras_picture = new Extras_Picture(A007_2Activity.this);
                            extras_picture.Param.setPHOTO_CURRENTITEM(photo_index);

                            ArrayList<String> photoUrlList = new ArrayList<String>();
                            for (int i = 0; i < RCPT_IMG_LIST.size(); i++) {
                                photoUrlList.add(RCPT_IMG_LIST.get(i).getImageUrl());
                            }
                            extras_picture.Param.setPHOTOUUIDLIST(photoUrlList);

                            Intent intent = new Intent(A007_2Activity.this, PictureViewActivity.class);
                            intent.putExtras(extras_picture.getBundle());
                            startActivity(intent);

                        }
                    });

                    if (idx <= 3) {
                        ll_photo.addView(ll_photo_item);
                    }

                    LinearLayout.LayoutParams photo_param = new LinearLayout.LayoutParams(getDP(0), ViewGroup.LayoutParams.MATCH_PARENT, 1.0f);
                    if (idx != 2) {
                        photo_param.setMargins(0, 0, getDP(5), 0);
                    }
                    ll_photo_item.setLayoutParams(photo_param);
                }
            } else {
                ll_photo.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getDP(int size) {
        size = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, size, getResources()
                        .getDisplayMetrics());
        return size;
    }
}
