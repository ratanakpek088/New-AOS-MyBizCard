package bizplay.kosign.com.bizplay.extras;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


public class Extra_Card extends Extras {

    public Extra_Card(Context ctx) {
        super(ctx);
    }

    public Extra_Card(Activity act, Intent intent) {
        super(act, intent);
    }

    public Extra_Card(Activity act, Bundle bundle) {
        super(act, bundle);
    }

    private final String KEY_CARD_NO     = "CARD_NO";
    private final String KEY_CARD_BYNM     = "CARD_BYNM";

    public _Param Param = new _Param();

    public class _Param {

        public void setKEY_CARD_NO(String value) {
            setString(KEY_CARD_NO, value);
        }

        public String getKEY_CARD_NO() {
            return getString(KEY_CARD_NO);
        }

        public void setKEY_CARD_BYNM(String value) {
            setString(KEY_CARD_BYNM, value);
        }

        public String getKEY_CARD_BYNM() {
            return getString(KEY_CARD_BYNM);
        }

    }

}
