package bizplay.kosign.com.bizplay.conf;

/**
 * Created by user on 2016-01-05.
 */
public class Conf {

    /**
     * 릴리즈여부
     *   true - 운영
     *   false - 개발
     */
    public static final boolean ISRELEASE	= false;

    /**
     *  위변조 체크 여부
     *     true - 위변조 체크
     *     false - 위변조 체크 No
     */
    public static final boolean ISAPPIRON = false;

    /**
     * Google play store download app url
     */
    public static final String PLAY_STORE_URL = "market://details?id=";

    /**
     * Gate Master ID
     */
    public static final String MASTER_ID = "A_MY_G_1";

    /**
     *  Uset Agent 초기 값
     */
    public static String mUserAgent = "Mozilla/5.0 (Linux; U; Android 2.1; en-us; sdk Build/ERD79) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17";

    /**
     * 환경설정파일 운영/테스트 여부 (운영 : true, 테스트 : false)
     */
    public static final boolean ISCONFIG = ISRELEASE ? true : false;	// 릴리즈 된 경우 무조건 운영

    /**
     * 사이트 도메인 URL
     */
    public static final String SITE_MG_URL = ISRELEASE ? RealConfig.SITE_MG_URL : TestConfig.SITE_MG_URL;

    /**
     * 중계서버 IP
     */
    //public static String RLAY_SERVER_IP = ISCONFIG ? RealConfig.RLAY_SERVER_IP : TestConfig.RLAY_SERVER_IP;

    /**
     * 중계서버 PORT
     */
    //public static int RLAY_SERVER_PORT = ISCONFIG ? RealConfig.RLAY_SERVER_PORT : TestConfig.RLAY_SERVER_PORT;

    /**
     * PUSH Project no
     */
    public static String PUSH_PROJECT_NO = ISCONFIG ? RealConfig.PUSH_PROJECT_NO : TestConfig.PUSH_PROJECT_NO;

    /**
     * 최초 구동 시작하기 URL
     */
   // public static final String APP_START_URL = ISRELEASE ? RealConfig.APP_START_URL : TestConfig.APP_START_URL;


    /**
     * 앱 위변조 체크 서버 URL
     * http://www.duredure.co.kr/appiron-inspect/
     * https://m.checkpay.co.kr/appiron-inspect/
     */
    public static String APPIRON_AUTHCHECK_URL = ISRELEASE ? RealConfig.APPIRON_AUTHCHECK_URL : TestConfig.APPIRON_AUTHCHECK_URL;

    /**
     *  약관동의 타입별 로그인후 Start URL
     */
   // public static final String LOGIN_RESULT_PAGE = ISRELEASE ? RealConfig.LOGIN_RESULT_PAGE : TestConfig.LOGIN_RESULT_PAGE;


    /**
     * 개발시 사용할 환경 설정
     */
    public static class TestConfig {
//        private static final String SITE_MG_URL             = "http://sportal.dev.weplatform.co.kr:19990"; // http://ibkbizpresso.dev.weplatform.co.kr:19990 // 개발서버 사이트 도메인 URL
//        private static final String RLAY_SERVER_IP          = "172.20.50.54";             //중계서버 IP
//        private static final int RLAY_SERVER_PORT           = 19990;                         //중계서버 PORT
//
//        private static final String LOGIN_RESULT_PAGE		= "http://ibkbizpresso.dev.weplatform.co.kr:19990/ibk_mobl_0001_01.act?CRTC_PATH=";
//        private static final String PUSH_PROJECT_NO         = "705645407617";
//        private static final String APPIRON_AUTHCHECK_URL	= "http://183.111.160.141:8480/authCheck.call"; // http://183.111.160.141:8480/authCheck.call
//        private static final String APP_START_URL           = "http://mibkcard.dev.wecontent.co.kr/#/a01"; // URL

        private static final String SITE_MG_URL             = "http://sportal.dev.weplatform.co.kr:19990";  // 개발서버 사이트 도메인 URL
        private static final String PUSH_PROJECT_NO         = "51047732396";
        private static final String APPIRON_AUTHCHECK_URL	= "http://183.111.160.141:8480/authCheck.call";
    }

    /**
     * 운영시 사용할 환경 설정
     */
    public static class RealConfig {
        private static final String SITE_MG_URL             = "https://www.ibkbizpresso.com"; // 개발서버 사이트 도메인 URL
        private static final String RLAY_SERVER_IP          = "172.20.50.54";             //중계서버 IP
        private static final int RLAY_SERVER_PORT           = 19990;                         //중계서버 PORT

        private static final String LOGIN_RESULT_PAGE		= "https://www.ibkbizpresso.com/ibk_mobl_0001_01.act?CRTC_PATH=";
        private static final String PUSH_PROJECT_NO         = "170453195831";
        private static final String APPIRON_AUTHCHECK_URL	= "http://app.coocon.co.kr/authCheck.call";
        private static final String APP_START_URL           = "http://mibkcard.wecontent.co.kr/#/a01"; // 최초 구동 시작하기 URL
    }
}
