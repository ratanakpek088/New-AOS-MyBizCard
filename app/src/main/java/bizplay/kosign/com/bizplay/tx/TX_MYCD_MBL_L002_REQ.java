package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by ViSAL MSi on 12/29/2016.
 */

public class TX_MYCD_MBL_L002_REQ extends TxMessage {

    public static final String TXNO = "MYCD_MBL_L002";
    private TX_PRVCD_MBL_R002_REQ_DATA mTxKeyData;

    public TX_MYCD_MBL_L002_REQ() throws Exception {
        mTxKeyData = new TX_PRVCD_MBL_R002_REQ_DATA();
        super.initSendMessage();
    }

    public void setINQ_CD(String value){
        mSendMessage.put(this.mTxKeyData.INQ_CD, value);
    }

    public void setCARD_CORP_CD(String value){
         mSendMessage.put(this.mTxKeyData.CARD_CORP_CD, value);
    }

    public void setINQ_NEXT_APV_DT(String value){
        mSendMessage.put(this.mTxKeyData.INQ_NEXT_APV_DT, value);
    }

    public void setINQ_NEXT_APV_SEQ(String value){
        mSendMessage.put(this.mTxKeyData.INQ_NEXT_APV_SEQ, value);
    }

    public void setINQ_NEXT_APV_TM(String value){
        mSendMessage.put(this.mTxKeyData.INQ_NEXT_APV_TM, value);
    }

    public void setINQ_NEXT_APV_NO(String value){
        mSendMessage.put(this.mTxKeyData.INQ_NEXT_APV_NO, value);
    }

    public void setCARD_NO(String value){
        mSendMessage.put(this.mTxKeyData.CARD_NO, value);
    }

    private class TX_PRVCD_MBL_R002_REQ_DATA{
        private String INQ_CD = "INQ_CD";
        private String CARD_CORP_CD = "CARD_CORP_CD";
        private String CARD_NO = "CARD_NO";
        private String INQ_NEXT_APV_DT = "INQ_NEXT_APV_DT";
        private String INQ_NEXT_APV_SEQ = "INQ_NEXT_APV_SEQ";
        private String INQ_NEXT_APV_TM = "INQ_NEXT_APV_TM";
        private String INQ_NEXT_APV_NO = "INQ_NEXT_APV_NO";
    }
}
