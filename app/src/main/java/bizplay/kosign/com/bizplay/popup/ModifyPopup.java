package bizplay.kosign.com.bizplay.popup;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import bizplay.kosign.com.bizplay.R;

/**
 * Created by Daravuth on 3/15/2017.
 */

public class ModifyPopup extends PopupMenu {
    private OnePopupMenu listener;
    private TextView title;

    public ModifyPopup(Activity activity) {
        super(activity, R.layout.popup_modify);
        title= (TextView) anchor.findViewById(R.id.item_1);
        title.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if(v.getId()==R.id.item_1){
            listener.itemClick();
        }
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public void setListener(OnePopupMenu listener) {
        this.listener = listener;
    }

    public interface OnePopupMenu{
        void itemClick();
    }
}
