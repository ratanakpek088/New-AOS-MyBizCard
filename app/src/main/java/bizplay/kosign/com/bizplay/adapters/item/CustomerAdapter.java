package bizplay.kosign.com.bizplay.adapters.item;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import bizplay.kosign.com.bizplay.R;
import bizplay.kosign.com.bizplay.common.ui.util.FormatUtil;
import bizplay.kosign.com.bizplay.item.CusEmp;
import bizplay.kosign.com.bizplay.item.Cust;

/**
 * Created by Daravuth on 3/8/2017.
 */

public class CustomerAdapter extends BaseAdapter {
    ArrayList<CusEmp> mCustomer;
    Context mContext;
    String mSearch;

    public CustomerAdapter(ArrayList<CusEmp> customer, Context mContext, String search) {
        this.mCustomer = customer;
        this.mContext = mContext;
        this.mSearch = search;
    }

    public void setmSearch(String mSearch) {
        this.mSearch = mSearch;
    }

    @Override
    public int getCount() {
        return mCustomer.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null || convertView.getTag() == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.new_customer_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        CusEmp cust = mCustomer.get(position);

        if (cust.getBP_MAGR_NO().equals("0")) {
            //display parent item
            convertView.findViewById(R.id.parent_item).setVisibility(View.VISIBLE);
            convertView.findViewById(R.id.child_item).setVisibility(View.GONE);
            //Tick the checkbox
            viewHolder.cusChecked.setChecked(cust.isChecked());
            if(!TextUtils.isEmpty(cust.getBP_CUST_NM()) && !cust.getBP_CUST_NM().equals("null")){
                if(mSearch.equals("")) viewHolder.cusName.setText(cust.getBP_CUST_NM());
                else viewHolder.cusName.setText(Html.fromHtml(cust.getBP_CUST_NM().replace(mSearch, "<b><font color='#4286f5'>"+mSearch+"</font></b>")));
            }else viewHolder.cusName.setText("");

            if(!TextUtils.isEmpty(cust.getBP_MEMO()) && !cust.getBP_MEMO().equals("null"))
                viewHolder.cusMemo.setText(cust.getBP_MEMO());
            else viewHolder.cusMemo.setText("");
        }else{
            //Display Child Item
            convertView.findViewById(R.id.parent_item).setVisibility(View.GONE);
            convertView.findViewById(R.id.child_item).setVisibility(View.VISIBLE);

            viewHolder.empChecked.setChecked(cust.isChecked());
            if(!TextUtils.isEmpty(cust.getBP_MAGR_NM()) && !cust.getBP_MAGR_NM().equals("null"))
                if(mSearch.equals("")) viewHolder.empName.setText(cust.getBP_MAGR_NM());
                else viewHolder.empName.setText(Html.fromHtml(cust.getBP_MAGR_NM().replaceFirst(mSearch, "<b><font color = '#4286f5'>" + mSearch + "</font></b>")));
            else viewHolder.empName.setText("");
            if (!TextUtils.isEmpty(cust.getCLPH_NO()) && (cust.getCLPH_NO().length() >= 9 && cust.getCLPH_NO().length() <= 11))
                viewHolder.empPhone.setText(FormatUtil.formatePhoneNumber(cust.getCLPH_NO()));
            else viewHolder.empPhone.setText("");

            if (!TextUtils.isEmpty(cust.getBP_MEMO()) && !cust.getBP_MEMO().equals("null"))
                viewHolder.empMemo.setText(cust.getBP_MEMO());
            else viewHolder.empMemo.setText("");
        }

        return convertView;
    }


    class ViewHolder {
        // employee
        CheckBox empChecked;
        TextView empName, empPhone, empMemo;
        // customer
        CheckBox cusChecked;
        TextView cusName, cusMemo;

        public ViewHolder(View view) {
            cusChecked = (CheckBox) view.findViewById(R.id.chkParent);
            cusName = (TextView) view.findViewById(R.id.tv_cus_name);
            cusMemo = (TextView) view.findViewById(R.id.tv_cus_memo);

            empChecked = (CheckBox) view.findViewById(R.id.chkSelected);
            empName = (TextView) view.findViewById(R.id.tv_emp_name);
            empPhone = (TextView) view.findViewById(R.id.tv_emp_phone);
            empMemo = (TextView) view.findViewById(R.id.tv_depart);
        }
    }

}
