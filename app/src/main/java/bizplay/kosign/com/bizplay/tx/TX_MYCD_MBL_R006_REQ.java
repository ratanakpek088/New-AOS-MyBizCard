package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by user on 1/13/2017.
 */

public class TX_MYCD_MBL_R006_REQ extends TxMessage{

    public static final String TXNO= "MYCD_MBL_R006";
    private TX_MYCD_MBL_R006_REQ_DATA mTxKeyData;

    public TX_MYCD_MBL_R006_REQ() throws Exception {
        mTxKeyData = new TX_MYCD_MBL_R006_REQ_DATA();
        super.initSendMessage();
    }

    public void setINQ_DT(String value)throws JSONException{
        mSendMessage.put(mTxKeyData.INQ_DT,value);
    }
    private class TX_MYCD_MBL_R006_REQ_DATA{
        private String INQ_DT= "INQ_DT";
    }
}
