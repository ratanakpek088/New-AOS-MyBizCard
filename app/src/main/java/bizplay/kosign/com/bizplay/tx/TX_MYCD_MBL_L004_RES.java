package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;


public class TX_MYCD_MBL_L004_RES extends TxMessage {

    private  TX_MYCD_MBL_L004_RES_DATA mTxKeyData; // 전문명세 필드

    public TX_MYCD_MBL_L004_RES(Object obj) throws Exception{
        mTxKeyData = new TX_MYCD_MBL_L004_RES_DATA();
        super.initRecvMessage(obj);
    }

    /**
     * 반복부
     * @throws Exception
     */
    public TX_MYCD_MBL_L004_RES_REC getREC() throws JSONException,Exception{
        return new TX_MYCD_MBL_L004_RES_REC(getRecord(mTxKeyData.REC));
    }

    /**
     * 전문 Data 필드 설정
     */
    private class TX_MYCD_MBL_L004_RES_DATA {
        private String REC		        = 	"REC";				        //반복부
    }

}
