package bizplay.kosign.com.bizplay.tabs;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import bizplay.kosign.com.bizplay.A008_2Activity;
import bizplay.kosign.com.bizplay.MainActivity;
import bizplay.kosign.com.bizplay.R;
import bizplay.kosign.com.bizplay.adapters.NoticeAdapter;
import bizplay.kosign.com.bizplay.common.ui.BizApplication;
import bizplay.kosign.com.bizplay.extras.NotifExatra;
import bizplay.kosign.com.bizplay.item.NoticeItem;
import bizplay.kosign.com.bizplay.tran.ComTran;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L004_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L004_RES;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L004_RES_REC;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoticeFragment extends Fragment implements ComTran.OnComTranListener,/*AdapterView.OnItemClickListener,*/MainActivity.OnNoticeReload{

    private static NoticeFragment instance = null;
    private ListView mNoticeListView;
    ComTran mComTran;
    NoticeAdapter mNoticeAdapter;
    List<NoticeItem> mNoticeitem;
    RelativeLayout has_notice,rl_no_notice;
    TextView tv_req_notif;

    public NoticeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        BizApplication.initImageLoader(getActivity());

        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(getActivity()));

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notice, container, false);

        mNoticeAdapter = new NoticeAdapter(getActivity(),mNoticeitem);
        tv_req_notif = (TextView) view.findViewById(R.id.tv_req_notif);
        mNoticeListView = (ListView) view.findViewById(R.id.notice_list);
        has_notice = (RelativeLayout) view.findViewById(R.id.has_notice);
        rl_no_notice = (RelativeLayout) view.findViewById(R.id.rl_no_notice);
        mComTran = new ComTran(getActivity(),this);
        tv_req_notif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadNotice();
            }
        });

        return view;
    }

    public void loadNotice(){
        try {
            if(mNoticeitem == null)
            {
                mNoticeitem = new ArrayList<>();
                mNoticeAdapter = new NoticeAdapter(getActivity(),mNoticeitem);
            }else {
                mNoticeitem.clear();
                mNoticeAdapter.notifyDataSetChanged();
            }

           requestData(TX_MYCD_MBL_L004_REQ.TXNO);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void requestData(String code) {
     try{
         switch (code){
             case TX_MYCD_MBL_L004_REQ.TXNO:
                 TX_MYCD_MBL_L004_REQ req = new TX_MYCD_MBL_L004_REQ();
                 java.text.DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

                 Date date = new Date();
                 String sample = dateFormat.format(date);

                 String result = sample.replace("/","");

                 req.setINQ_DT(result);

                 mComTran.requestData(TX_MYCD_MBL_L004_REQ.TXNO,req.getSendMessage());
                 break;
         }
     }catch (Exception e){
        e.printStackTrace();
     }
    }

    public static NoticeFragment getInstance() {
        try {
            if (instance == null) {
                // new instance
                instance = new NoticeFragment();

                // sets data to bundle
                Bundle bundle = new Bundle();

                // set data to fragment
                instance.setArguments(bundle);

                return instance;
            } else {

                return instance;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public void onTranResponse(String tranCode, Object object) throws Exception {
        Log.e("Response",object.toString());
        try{
            if(tranCode.equals(TX_MYCD_MBL_L004_REQ.TXNO)){
                TX_MYCD_MBL_L004_RES res = new TX_MYCD_MBL_L004_RES(object);
                TX_MYCD_MBL_L004_RES_REC rec = res.getREC();

                if(rec.getLength() != 0){
                    has_notice.setVisibility(View.VISIBLE);
                    rl_no_notice.setVisibility(View.GONE);
                }
                rec.moveFirst();
                rec.toString();
                for(int i=0;i<rec.getLength();i++){
                    NoticeItem noticeItem = new NoticeItem();
                    noticeItem.setCD_USER_NO(rec.getCD_USER_NO());
                    noticeItem.setSEND_DTM(rec.getSEND_DTM());
                    noticeItem.setNOTI_SEQ(rec.getNOTI_SEQ());
                    noticeItem.setNOTI_ORG_NM(rec.getNOTI_ORG_NM());
                    noticeItem.setNOTI_ORG_IMG(rec.getNOTI_ORG_IMG());
                    noticeItem.setTITLE_NM(rec.getTITLE_NM());
                    noticeItem.setINQ_DTM(rec.getINQ_DTM());
                    noticeItem.setIMG_URL(rec.getIMG_URL());
                    noticeItem.setTARGET_PAGE_URL(rec.getTARGET_PAGE_URL());
                    noticeItem.setNOTI_GB(rec.getNOTI_GB());
                    mNoticeitem.add(noticeItem);
                    rec.moveNext();
                }
                mNoticeListView.setAdapter(mNoticeAdapter);
               // mNoticeListView.setOnItemClickListener(this);
                //dont use because we want action in adapter for clicking
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }

//    @Override
//    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//        try {
//            NotifExatra notifExatra = new NotifExatra(getActivity());
//            NoticeItem noticeItem = (NoticeItem) adapterView.getItemAtPosition(i);
//            notifExatra.Param.setKEY_SND_DTM(noticeItem.getSEND_DTM());
//            notifExatra.Param.setKEY_NOTI_SEQ(noticeItem.getNOTI_SEQ());
//
//            Intent intent = new Intent(getActivity(), A008_2Activity.class);
//            intent.putExtras(notifExatra.getBundle());
//            startActivity(intent);
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }

    @Override
    public void reloadNotice() {
        loadNotice();
    }
}
