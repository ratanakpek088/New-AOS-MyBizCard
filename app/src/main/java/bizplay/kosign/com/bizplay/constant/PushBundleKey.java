package bizplay.kosign.com.bizplay.constant;

/**
 * Created by ChanYouvita on 7/7/16.
 */
public class PushBundleKey {
    public static String CONTROL_CD        = "_c"; // control cd 01:실시간승인내역알림, 02:카드한도초기화일자알림, 03:일반알림
    public static String DISPLAY_MESSAGE   = "_d"; // display message
    public static String MID               = "_mid";
    public static String DATE              = "_date";
    public static String TYPE              = "_type";
    public static String COLLAPSE_KEY      = "collapse_key";
    public static String PUSH_RECEIVE      = "PUSH_RECEIVE";
}
