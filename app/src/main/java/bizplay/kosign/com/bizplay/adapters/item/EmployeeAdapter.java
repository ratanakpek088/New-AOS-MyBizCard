package bizplay.kosign.com.bizplay.adapters.item;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;



import java.util.ArrayList;

import bizplay.kosign.com.bizplay.R;
import bizplay.kosign.com.bizplay.common.ui.util.FormatUtil;
import bizplay.kosign.com.bizplay.item.Employee;

/**
 * Created by User on 14-Feb-17.
 */

public class EmployeeAdapter extends BaseAdapter {
    private ArrayList<Employee> list;
    private Context context;
    private String search;

    public EmployeeAdapter(Context context, ArrayList<Employee> list, String search) {
        this.context = context;
        this.list = list;
        this.search = search;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null || convertView.getTag() == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.employee_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();
        Employee emp = list.get(position);

        viewHolder.chk_selected.setChecked(emp.getIS_SELECTED());


//        if(!TextUtils.isEmpty(emp.getBP_EMPL_NM()) && !emp.getBP_EMPL_NM().equals("null")){
//            viewHolder.tv_emp_name.setText(emp.getBP_EMPL_NM());
//        }else{
//            viewHolder.tv_emp_name.setText("");
//        }
//
//        if(!TextUtils.isEmpty(emp.getBP_DVSN_NM()) && !emp.getBP_DVSN_NM().equals("null")){
//            viewHolder.tv_emp_depart.setText(emp.getBP_DVSN_NM());
//        }else{
//            viewHolder.tv_emp_depart.setText("");
//        }
//
//        if(!TextUtils.isEmpty(emp.getBP_EMPL_NO()) && !emp.getBP_EMPL_NO().equals("null")){
//            viewHolder.tv_emp_phone.setText(FormatUtil.formatePhoneNumber(emp.getCLPH_NO()));
//        }else{
//            viewHolder.tv_emp_phone.setText("");
//        }

        if (!TextUtils.isEmpty(emp.getBP_EMPL_NM()) && !emp.getBP_EMPL_NM().equals("null"))
            if (search.equals("")) viewHolder.tv_emp_name.setText(emp.getBP_EMPL_NM());
            else
                viewHolder.tv_emp_name.setText(Html.fromHtml( emp.getBP_EMPL_NM().replaceFirst(search, "<b><font color = '#4286f5'>" + search + "</font></b>")));
        else viewHolder.tv_emp_name.setText("");


        if (!TextUtils.isEmpty(emp.getCLPH_NO()) && (emp.getCLPH_NO().length() >= 9 && emp.getCLPH_NO().length() <= 11))
            if (search.equals("")) viewHolder.tv_emp_phone.setText(FormatUtil.formatePhoneNumber(emp.getCLPH_NO()));
            else
                viewHolder.tv_emp_phone.setText(Html.fromHtml(FormatUtil.formatePhoneNumber(emp.getCLPH_NO()).replaceFirst(search, "<b><font color = '#4286f5'>" + search + "</font></b>")));
        else viewHolder.tv_emp_phone.setText("");


        if (!TextUtils.isEmpty(emp.getBP_DVSN_NM()) && !emp.getBP_DVSN_NM().equals("null"))
            if (search.equals("")) viewHolder.tv_emp_depart.setText(emp.getBP_DVSN_NM());
            else
                viewHolder.tv_emp_depart.setText(Html.fromHtml(emp.getBP_DVSN_NM().replaceFirst(search, "<b><font color = '#4286f5'>" + search + "</font></b>")));
        else viewHolder.tv_emp_depart.setText("");

        return convertView;

    }

    class ViewHolder {
        LinearLayout ll_emp_item;
        TextView tv_emp_name, tv_emp_phone, tv_emp_depart;
        CheckBox chk_selected;

        public ViewHolder(View view) {
            chk_selected = (CheckBox) view.findViewById(R.id.chkSelected);
            ll_emp_item = (LinearLayout) view.findViewById(R.id.ll_emp_item);
            tv_emp_name = (TextView) view.findViewById(R.id.tv_emp_name);
            tv_emp_phone = (TextView) view.findViewById(R.id.tv_emp_phone);
            tv_emp_depart = (TextView) view.findViewById(R.id.tv_depart);
        }
    }

    public void setSearch(String value) {
        this.search = value;
    }

}
