package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by User on 7/15/2016.
 */
public class TX_MYCD_MBL_P004_RES extends TxMessage {
    private String RCPT_REC = "RCPT_REC";

    public TX_MYCD_MBL_P004_RES(Object object) throws Exception {
        super.initRecvMessage(object);
    }
    public TX_MYCD_MBL_P004_RES_REC getRCPT_USER_REC() throws Exception {
        return new TX_MYCD_MBL_P004_RES_REC(getRecord(RCPT_REC));
    }
}
