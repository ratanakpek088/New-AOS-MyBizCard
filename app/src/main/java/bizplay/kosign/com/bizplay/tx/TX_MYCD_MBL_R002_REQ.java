package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by ViSAL MSi on 12/29/2016.
 */

public class TX_MYCD_MBL_R002_REQ extends TxMessage {

    public static final String TXNO = "MYCD_MBL_R002";
    private TX_PRVCD_MBL_R002_REQ_DATA mTxKeyData;

    public TX_MYCD_MBL_R002_REQ() throws Exception {
        mTxKeyData = new TX_PRVCD_MBL_R002_REQ_DATA();
        super.initSendMessage();
    }

    public void setCARD_CORP_CD(String value){
         mSendMessage.put(this.mTxKeyData.CARD_CORP_CD, value);
    }

    public void setCARD_NO(String value){
        mSendMessage.put(this.mTxKeyData.CARD_NO, value);
    }

    private class TX_PRVCD_MBL_R002_REQ_DATA{
        private String CARD_CORP_CD = "CARD_CORP_CD";
        private String CARD_NO = "CARD_NO";
    }
}
