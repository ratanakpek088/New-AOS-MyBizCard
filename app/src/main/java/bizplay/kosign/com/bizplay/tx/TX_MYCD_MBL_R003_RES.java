package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by RatanakPek on 1/10/2017.
 */

public class TX_MYCD_MBL_R003_RES extends TxMessage {


    private TX_MYCD_MBL_R003_RES_DATA mTxKeyData;

    public TX_MYCD_MBL_R003_RES(Object obj) throws Exception {
        this.mTxKeyData = new TX_MYCD_MBL_R003_RES_DATA();
        super.initRecvMessage(obj);
    }

    public String getCD_RCPT_STS() throws Exception {
        return getString(mTxKeyData.CD_RCPT_STS);
    }

    public String getTAX_TYPE_CD() throws Exception {
        return getString(mTxKeyData.TAX_TYPE_CD);
    }

    public String getVAT_RFND_STS() throws Exception {
        return getString(mTxKeyData.VAT_RFND_STS);
    }

    public String getCARD_CORP_CD() throws Exception {
        return getString(mTxKeyData.CARD_CORP_CD);
    }

    public String getCARD_CORP_NM() throws Exception {
        return getString(mTxKeyData.CARD_CORP_NM);
    }

    public String getAPV_AMT() throws Exception {
        return getString(mTxKeyData.APV_AMT);
    }

    public String getSUPL_AMT() throws Exception {
        return getString(mTxKeyData.SUPL_AMT);
    }

    public String getVAT_AMT() throws Exception {
        return getString(mTxKeyData.VAT_AMT);
    }

    public String getTRAN_KIND_CD() throws Exception {
        return getString(mTxKeyData.TRAN_KIND_CD);
    }

    public String getTRAN_KIND_NM() throws Exception {

        return getString(mTxKeyData.TRAN_KIND_NM);
    }

    public String getMEST_REPR_NM() throws Exception {

        return getString(mTxKeyData.MEST_REPR_NM);
    }

    public String getSUMMARY() throws Exception {

        return getString(mTxKeyData.SUMMARY);
    }


    public String getMEST_NM() throws Exception {

        return getString(mTxKeyData.MEST_NM);
    }

    public String getMEST_TEL_NO() throws Exception {

        return getString(mTxKeyData.MEST_TEL_NO);
    }

    public String getMEST_ZIP_CD() throws Exception {

        return getString(mTxKeyData.MEST_ZIP_CD);
    }

    public String getMEST_ADDR() throws Exception {

        return getString(mTxKeyData.MEST_ADDR);
    }

    public String getCARD_TPBZ_CD() throws Exception {

        return getString(mTxKeyData.CARD_TPBZ_CD);
    }

    public String getCARD_TPBZ_NM() throws Exception {

        return getString(mTxKeyData.CARD_TPBZ_NM);
    }

    public String getRCPT_RFS_CNT() throws Exception {

        return getString(mTxKeyData.RCPT_RFS_CNT);
    }

    public TX_MYCD_MBL_R003_RES_REC_RCPT_RFS getRCPT_RFS_REC() throws Exception {

        return new TX_MYCD_MBL_R003_RES_REC_RCPT_RFS(getRecord(mTxKeyData.RCPT_RFS_REC));
    }

    public String getRCPT_USER_CNT() throws Exception {

        return getString(mTxKeyData.RCPT_USER_CNT);
    }

    public TX_MYCD_MBL_R003_RES_REC_RCPT_USER getRCPT_USER_REC() throws Exception {

        return new TX_MYCD_MBL_R003_RES_REC_RCPT_USER(getRecord(mTxKeyData.RCPT_USER_REC));
    }

    public TX_MYCD_MBL_R003_RES_REC_RCPT_IMG getRCPT_IMG_REC() throws Exception {

        return new TX_MYCD_MBL_R003_RES_REC_RCPT_IMG(getRecord(mTxKeyData.RCPT_IMG_REC));
    }




    public String getMGMT1() throws Exception {
        return getString(mTxKeyData.MGMT1);
    }

    public String getMGMT2() throws Exception {
        return getString(mTxKeyData.MGMT2);
    }
    public String getMGMT3() throws Exception {
        return getString(mTxKeyData.MGMT3);
    }
    public String getMGMT4() throws Exception {
        return getString(mTxKeyData.MGMT4);
    }
    public String getMGMT5() throws Exception {
        return getString(mTxKeyData.MGMT5);
    }
    public String getBSNS_DSNC_CD() throws JSONException {
        return getString(mTxKeyData.BSNS_DSNC_CD);
    }

    public String getBSNS_DSNC_NM() throws JSONException{
        return getString(mTxKeyData.BSNS_DSNC_NM);
    }

    public TX_MYCD_MBL_R003_RES_BP_EMPL_REC getBP_EMPL_REC() throws Exception{
        return new TX_MYCD_MBL_R003_RES_BP_EMPL_REC(getRecord(mTxKeyData.BP_EMPL_REC));
    }

    public TX_MYCD_MBL_R003_RES_BP_CUST_REC getBP_CUST_REC() throws Exception{
        return new TX_MYCD_MBL_R003_RES_BP_CUST_REC(getRecord(mTxKeyData.BP_CUST_REC));
    }
    private class TX_MYCD_MBL_R003_RES_DATA {
        private String CD_RCPT_STS = "CD_RCPT_STS";
        private String TAX_TYPE_CD = "TAX_TYPE_CD";
        private String VAT_RFND_STS = "VAT_RFND_STS";
        private String CARD_CORP_CD = "CARD_CORP_CD";
        private String CARD_CORP_NM = "CARD_CORP_NM";
        private String APV_AMT = "APV_AMT";
        private String SUPL_AMT = "SUPL_AMT";
        private String VAT_AMT = "VAT_AMT";
        private String TRAN_KIND_CD = "TRAN_KIND_CD";
        private String TRAN_KIND_NM = "TRAN_KIND_NM";
        private String SUMMARY = "SUMMARY";
        private String MEST_REPR_NM = "MEST_REPR_NM";
        private String MEST_NM = "MEST_NM";
        private String MEST_TEL_NO = "MEST_TEL_NO";
        private String MEST_ZIP_CD = "MEST_ZIP_CD";
        private String MEST_ADDR = "MEST_ADDR";
        private String CARD_TPBZ_CD = "CARD_TPBZ_CD";
        private String CARD_TPBZ_NM = "CARD_TPBZ_NM";
        private String RCPT_RFS_CNT = "RCPT_RFS_CNT";
        private String RCPT_RFS_REC = "RCPT_RFS_REC";
        private String RCPT_USER_REC = "RCPT_USER_REC";
        private String RCPT_IMG_REC = "RCPT_IMG_REC";
        private String RCPT_USER_CNT = "RCPT_USER_CNT";





        private String MGMT1="MGMT1";
        private String MGMT2="MGMT2";
        private String MGMT3="MGMT3";
        private String MGMT4="MGMT4";
        private String MGMT5="MGMT5";
        private String BSNS_DSNC_CD="BSNS_DSNC_CD";
        private String BSNS_DSNC_NM="BSNS_DSNC_NM";

        private String BP_EMPL_REC="BP_EMPL_REC";
        private String BP_CUST_REC="BP_CUST_REC";


        private String PTL_ID           = "PTL_ID";
        private String USE_INTT_ID      = "USE_INTT_ID";
        private String CHNL_ID          = "CHNL_ID";
        private String USER_ID          = "USER_ID";

    }
    public String getPTL_ID() throws JSONException{
        return getString(mTxKeyData.PTL_ID);
    }

    public String getUSE_INTT_ID() throws JSONException{
        return getString(mTxKeyData.USE_INTT_ID);
    }

    public String getCHNL_ID() throws JSONException{
        return getString(mTxKeyData.CHNL_ID);
    }

    public String getUSER_ID() throws JSONException{
        return getString(mTxKeyData.USER_ID);
    }
}
