package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by RatanakPek on 1/10/2017.
 */

public class TX_MYCD_MBL_R003_RES_REC_RCPT_IMG extends TxMessage {

    private TX_MYCD_MBL_R003_RES_REC_RCPT_IMG_DATA mTxKeyData;

    public TX_MYCD_MBL_R003_RES_REC_RCPT_IMG(Object obj) throws Exception {
        this.mTxKeyData = new TX_MYCD_MBL_R003_RES_REC_RCPT_IMG_DATA();
        super.initRecvMessage(obj);
    }
    public String getRCPT_IMG_URL() throws Exception {
        return getString(mTxKeyData.RCPT_IMG_URL);
    }

    private class TX_MYCD_MBL_R003_RES_REC_RCPT_IMG_DATA{
        private String RCPT_IMG_URL="RCPT_IMG_URL";
    }
}
