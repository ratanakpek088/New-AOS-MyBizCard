package bizplay.kosign.com.bizplay.common.ui.util;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.text.method.TransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.EditText;

import com.webcash.sws.log.DevLog;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class FormatUtil {

	/*public static String getDateTimeUtil(String value) {
		
		try {
			if (value == null)
				return "";
			if (value.length() < 8)
				return value;
			
			String date = Convert.ComDate.formatDotDelimiter(value.substring(0, 8), "-");
			
			if (value.length() < 12)
				return date;
			
			String time = Convert.ComDate.strTime.formatDelimiter(value.substring(8, 12));
			
			return date + " | " + time;
		} catch (Exception e) {
			e.printStackTrace();
			return value;
		}		
		
	}*/

	/**
	 * byte 단위의 파일 사이즈를 KB, MB, GB 로 변형한다.
	 * @param size : byte
	 * @return
	 */
	/*public static String fileSize(String size) {
		if (size == null)
			return "0";
		if (Convert.getNumber(size).equals(""))
			return "0";
			
		return fileSize(Long.parseLong(size));
	}*/

	/**
	 * byte 단위의 파일 사이즈를 KB, MB, GB 로 변형한다.
	 * @param size : byte
	 * @return
	 */
	public static String fileSize(long size) {
	    if(size <= 0) return "0";
	    final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
	    int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
	    return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}

	public static String getResolutionImage(Bitmap bm) {
		if (bm == null)
			return "";
		return bm.getWidth() + "X" + bm.getHeight();
	}


	/**
	 *
	 * @param ediText
	 * @param seperateNumber
	 * @param textLength
	 * @param delimiter
	 * Ex: ############ => ####-####-#### (seperateNumber = 4, textLength = 12, delimiter = '-')
	 *     #### => ##/## (seperateNumber = 2, textLength = 4, delimiter = '/')
	 */
	public static void editTextWithDelimiter(final EditText ediText, final int seperateNumber, final int textLength, final String delimiter) throws Exception {
		ediText.addTextChangedListener(new TextWatcher() {
			String mText;
			int keyDel;
			int mDelimiterNumber = (textLength/seperateNumber)-1;
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				boolean flag = true;
				String eachBlock[] = ediText.getText().toString().split(delimiter);
				for (int i = 0; i < eachBlock.length; i++) {
					if (eachBlock[i].length() > seperateNumber) {
						flag = false;
					}
				}
				if (flag) {

					ediText.setOnKeyListener(new OnKeyListener() {

						@Override
						public boolean onKey(View v, int keyCode, KeyEvent event) {

							if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN){

								int strLength = ediText.getText().length();
								if(strLength != 0){
									if(strLength %(seperateNumber+1) == 0){
										keyDel = 1;
										String input = ediText.getText().toString();
										ediText.setText(input.substring(0, strLength-1));
									}
								}
								else{
									keyDel = 1;
								}

							}
							return false;
						}
					});

					if (keyDel == 0) {

						if (((ediText.getText().length() + 1) % (seperateNumber+1)) == 0) {

							if (ediText.getText().toString().split(delimiter).length <= mDelimiterNumber) {
								ediText.setText(ediText.getText() + delimiter);
								ediText.setSelection(ediText.getText().length());
							}
						}
						mText = ediText.getText().toString();
					} else {
						mText = ediText.getText().toString();
						ediText.setSelection(ediText.getText().length());
						keyDel = 0;
					}

				} else {
					ediText.setText(mText);
				}
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});

	}



	/**
	 * EditText PassWord Setting
	 */
	/*public static void pwdEditTextWithDot(final EditText etPwd, final ImageView iv_pwd_dot1, final ImageView iv_pwd_dot2) throws Exception {
		etPwd.addTextChangedListener(new TextWatcher() {				
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				

				if(etPwd.getText().length() == 2){
					
					etPwd.setTextColor(Color.parseColor("#FFFFFF"));
					
					iv_pwd_dot1.setImageResource(R.drawable.text_output_icon1);			
					iv_pwd_dot2.setImageResource(R.drawable.text_output_icon1);
					
					iv_pwd_dot1.setVisibility(View.VISIBLE);
					iv_pwd_dot2.setVisibility(View.VISIBLE);	

				} else{
					
					etPwd.setTextColor(Color.parseColor("#000000"));
					
					iv_pwd_dot1.setVisibility(View.GONE);
					iv_pwd_dot2.setVisibility(View.GONE);
				}
			}				
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}				
			@Override
			public void afterTextChanged(Editable s) {}
		});
		
		etPwd.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(etPwd.getText().length() == 0){
					iv_pwd_dot1.setImageResource(R.drawable.text_none1_icon);			
					iv_pwd_dot2.setImageResource(R.drawable.text_none1_icon);
					
					iv_pwd_dot1.setVisibility(View.VISIBLE);
					iv_pwd_dot2.setVisibility(View.VISIBLE);
				}
			}
		});
	}*/

	public static class PasswordTransformationMethod extends android.text.method.PasswordTransformationMethod {

		private char DOT = '\u25CF';

	    @Override
	    public CharSequence getTransformation(CharSequence source, View view) {
	        return new PasswordCharSequence(source);
	    }

	    private class PasswordCharSequence implements CharSequence {
	        private CharSequence mSource;
	        public PasswordCharSequence(CharSequence source) {
	            mSource = source; // Store char sequence
	        }
	        public char charAt(int index) {
	            return DOT; // This is the important part
	        }
	        public int length() {
	            return mSource.length(); // Return default
	        }
	        public CharSequence subSequence(int start, int end) {
	            return mSource.subSequence(start, end); // Return default
	        }
	    }
	};


	public static class HiddenPassTransformationMethod implements TransformationMethod {
		//26AB , u25CF, \u2022
		private char DOT = '\u25CF';

		@Override
		public CharSequence getTransformation(final CharSequence charSequence, final View view) {
			return new PassCharSequence(charSequence);
		}

		@Override
		public void onFocusChanged(final View view, final CharSequence charSequence, final boolean b,
				final int i, final Rect rect) { }

		private class PassCharSequence implements CharSequence {

			private final CharSequence charSequence;

			public PassCharSequence(final CharSequence charSequence) {
				this.charSequence = charSequence;
			}

			@Override
			public char charAt(final int index) {
				if(index <= 9)
					return this.charSequence.charAt(index);
				else{
					if(index == 14)
						return '-';
					else{
						return DOT;
					}
				}
			}

			@Override
			public int length() {
				return charSequence.length();
			}

			@Override
			public CharSequence subSequence(final int start, final int end) {
				return new PassCharSequence(charSequence.subSequence(start, end));
			}
		}
	}

	//Ex. 200,122,333
	public static String getCommaFormat(String text){
		try {
			DecimalFormat formatter = new DecimalFormat("#,###,###");
			text = formatter.format(Double.parseDouble(text));
		}catch (Exception e){
			e.printStackTrace();
		}
		return text;
	}

	//Ex. 2016년 05월09일 13:43
	public static String getReceiptDateTime(String date, String time){
		String date_time = "";
		try {
			if("".equals(time)){
				date_time = date.substring(0, 4) + "년 " + date.substring(4, 6) + "월 " + date.substring(6, 8) + "일 ";
			}else {
				date_time = date.substring(0, 4) + "년 " + date.substring(4, 6) + "월 " + date.substring(6, 8) + "일 " + time.substring(0, 2) + ":" + time.substring(2, 4);
			}
		}catch (Exception e){
			e.printStackTrace();
			date_time = date + " " + time;
		}
		return date_time;
	}

	//Ex. 06/16
	public static String getNotifDate(String date){
		String year_month;
		String date_time = "";
		try {
			year_month = date.substring(0,6);
			date_time = year_month.substring(4, 6) + "/" + year_month.substring(2, 4);
			DevLog.devLog("Date Testing:>>","Date: "+year_month+", date"+date_time);
		}catch (Exception e){
			e.printStackTrace();
			date_time = date;
		}
		return date_time;
	}


	//Ex. 기업BC 1234-****_****-1234
	public static String getReceiptCreditCard(String name, String no){
		String credit_card = "";
		try {
			credit_card = name + " " + no.substring(0, 4) + "-****-****-" + no.substring(12, no.length());
		}catch (Exception e){
			e.printStackTrace();
			credit_card = name + " " + no;
		}
		return credit_card;
	}

	//Ex. 1234 - **** - **** - 1234
	public static String getCreditCard(String no){
		String credit_card = "";
		try {
			credit_card = no.substring(0, 4) + " - **** - **** - " + no.substring(12, no.length());
		}catch (Exception e){
			e.printStackTrace();
			credit_card = no;
		}
		return credit_card;
	}

	//Ex. 123 - 45 - 12345
	public static String getBizNo(String biz_no){
		String credit_card = "";
		try {
			credit_card = biz_no.substring(0, 3) +" - "+ biz_no.substring(3, 5) +" - "+ biz_no.substring(5, 10);
		}catch (Exception e){
			e.printStackTrace();
			credit_card = biz_no;
		}
		return credit_card;
	}

	public static String getReceiptPhone(String phone){
		try {
			phone = phone.substring(0, 2) + "-" + phone.substring(2, 6) + "-" + phone.substring(6, phone.length());
		}catch (Exception e){
			e.printStackTrace();
		}
		return phone;
	}

	public static String getTimeString(Date fromdate) {

		long then;
		then = fromdate.getTime();
		Date date = new Date(then);
		SimpleDateFormat sdfMD = new SimpleDateFormat("MM/dd", Locale.KOREAN);
//		SimpleDateFormat sdfMDY = new SimpleDateFormat("MM/dd/yyyy", Locale.KOREAN);

		StringBuffer dateStr = new StringBuffer();

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		Calendar now = Calendar.getInstance();

		int days = daysBetween(calendar.getTime(), now.getTime());
		int minutes = hoursBetween(calendar.getTime(), now.getTime());
		int hours = minutes / 60;
		if (days == 0) {

			int second = minuteBetween(calendar.getTime(), now.getTime());
			if (minutes > 60) {

				if (hours >= 1 && hours <= 24) {
					dateStr.append(hours).append("시간 전");
					/*if (hours == 1)
						dateStr.append(hours).append(" hour ago");
					else
						dateStr.append(hours).append(" hours ago");*/
				}

			} else {
				if (second < 60) {
					dateStr.append("지금");
				} else if (second >= 60 && minutes < 60) {
					dateStr.append(minutes).append("분 전");
				}

				/*if (second <= 10) {
					dateStr.append("Just now");
				} else if (second > 10 && second <= 30) {
					dateStr.append("Few seconds ago");
				} else if (second > 30 && second <= 60) {
					dateStr.append(second).append(" seconds ago");
				} else if (second >= 60 && minutes <= 60) {
					if (minutes == 1)
						dateStr.append(minutes).append(" minute ago");
					else
						dateStr.append(minutes).append(" minutes ago");
				}*/
			}
		} else
            dateStr.append(sdfMD.format(date));
		/*if (hours > 24 && days <= 7) {
			if (days == 1)
				dateStr.append(days).append(" day ago");
			else
				dateStr.append(days).append(" days ago");
		} else {
			dateStr.append(sdfMD.format(date));
		}*/

		return dateStr.toString();
	}

	public static int minuteBetween(Date d1, Date d2) {
		return (int) ((d2.getTime() - d1.getTime()) / DateUtils.SECOND_IN_MILLIS);
	}

	public static int hoursBetween(Date d1, Date d2) {
		return (int) ((d2.getTime() - d1.getTime()) / DateUtils.MINUTE_IN_MILLIS);
	}

	public static int daysBetween(Date d1, Date d2) {
		return (int) ((d2.getTime() - d1.getTime()) / DateUtils.DAY_IN_MILLIS);
	}

	public static String getDateDifferenceForDisplay(Date inputdate) {
		Calendar now = Calendar.getInstance();
		Calendar then = Calendar.getInstance();

		now.setTime(new Date());
		then.setTime(inputdate);

		// Get the represented date in milliseconds
		long nowMs = now.getTimeInMillis();
		long thenMs = then.getTimeInMillis();

		// Calculate difference in milliseconds
		long diff = nowMs - thenMs;

		// Calculate difference in seconds
		long diffMinutes = diff / (60 * 1000);
		long diffHours = diff / (60 * 60 * 1000);
		long diffDays = diff / (24 * 60 * 60 * 1000);

		if (diffMinutes < 60) {
			return diffMinutes + " m";

		} else if (diffHours < 24) {
			return diffHours + " h";

		} else if (diffDays < 7) {
			return diffDays + " d";

		} else {

			SimpleDateFormat todate = new SimpleDateFormat("MMM dd",
					Locale.ENGLISH);

			return todate.format(inputdate);
		}
	}

	/*
     * format date
     */
	public static String formateDateFromString(String inputDate){
		Date parsed = null;
		String outputDate = "";
		SimpleDateFormat df_input = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
		SimpleDateFormat df_output = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.getDefault());

		try {
			parsed = df_input.parse(inputDate);
			outputDate = df_output.format(parsed);

		} catch (ParseException e) {

		}
		return outputDate;
	}

	public static String formatePhoneNumber(String number){
		//숫자만 남겨놓고 모두 제거
		String clphNo = number.replaceAll("[^0-9]", "");

		try {

			if(clphNo.length() ==  9) {
				clphNo = clphNo.substring(0,2)+ "-" + clphNo.substring(2,5) + "-" + clphNo.substring(5,9);
			}else if(clphNo.length() == 10) {
				if(clphNo.substring(0,2).equals("02")){ // 지역번호 서울인 경우
					clphNo = clphNo.substring(0,2)+ "-" + clphNo.substring(2,6) + "-" + clphNo.substring(6,10);
				}else{
					clphNo = clphNo.substring(0,3)+ "-" + clphNo.substring(3,6) + "-" + clphNo.substring(6,10);
				}
			}else if(clphNo.length() == 11) {
				clphNo = clphNo.substring(0,3)+ "-" + clphNo.substring(3,7) + "-" + clphNo.substring(7,11);
			}

		}catch (Exception e){
			e.printStackTrace();
		}

		return clphNo;
	}
}
