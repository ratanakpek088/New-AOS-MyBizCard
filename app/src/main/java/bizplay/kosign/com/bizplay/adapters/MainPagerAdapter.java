package bizplay.kosign.com.bizplay.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import bizplay.kosign.com.bizplay.tabs.BlankFragment;
import bizplay.kosign.com.bizplay.tabs.MoreFragment;
import bizplay.kosign.com.bizplay.tabs.MyCardFragment;
import bizplay.kosign.com.bizplay.tabs.NoticeFragment;
import bizplay.kosign.com.bizplay.tabs.ReceiptFragment;

/**
 * Created by ViSAL MSi on 1/3/2017.
 */
public class MainPagerAdapter extends FragmentPagerAdapter {

    private static int NUM_ITEMS = 4;

    public MainPagerAdapter(FragmentManager fm,  int numbOfTabs) {
        super(fm);
        this.NUM_ITEMS = numbOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = MyCardFragment.getInstance();
                break;
            case 1:
                fragment = ReceiptFragment.getInstance();
                break;
            case 2:
                fragment =  NoticeFragment.getInstance();
                break;
            case 3:
                fragment = MoreFragment.getInstance();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }
}
