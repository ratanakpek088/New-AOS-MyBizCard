package bizplay.kosign.com.bizplay.item;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by User on 20-Feb-17.
 */

public class CusEmp implements Parcelable{
    private int ID;
    private String BP_CUST_NO;
    private String BP_MAGR_NO;
    private String BP_CUST_NM;
    private String BP_MAGR_NM;
    private String CLPH_NO;
    private String BP_MEMO;
    boolean isChecked;

    public CusEmp(){

    }

    public CusEmp(int ID, String BP_CUST_NO, String BP_MAGR_NO, String BP_CUST_NM, String BP_MAGR_NM, String CLPH_NO, String BP_MEMO){
        this.ID = ID;
        this.BP_CUST_NO = BP_CUST_NO;
        this.BP_CUST_NM = BP_CUST_NM;
        this.BP_MAGR_NO = BP_MAGR_NO;
        this.BP_MAGR_NM = BP_MAGR_NM;
        this.CLPH_NO = CLPH_NO;
        this.BP_MEMO = BP_MEMO;

    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getBP_CUST_NM() {
        return BP_CUST_NM;
    }

    public void setBP_CUST_NM(String BP_CUST_NM) {
        this.BP_CUST_NM = BP_CUST_NM;
    }

    public String getBP_CUST_NO() {
        return BP_CUST_NO;
    }

    public void setBP_CUST_NO(String BP_CUST_NO) {
        this.BP_CUST_NO = BP_CUST_NO;
    }

    public String getBP_MAGR_NM() {
        return BP_MAGR_NM;
    }

    public void setBP_MAGR_NM(String BP_MAGR_NM) {
        this.BP_MAGR_NM = BP_MAGR_NM;
    }

    public String getBP_MAGR_NO() {
        return BP_MAGR_NO;
    }

    public void setBP_MAGR_NO(String BP_MAGR_NO) {
        this.BP_MAGR_NO = BP_MAGR_NO;
    }

    public String getBP_MEMO() {
        return BP_MEMO;
    }

    public void setBP_MEMO(String BP_MEMO) {
        this.BP_MEMO = BP_MEMO;
    }

    public String getCLPH_NO() {
        return CLPH_NO;
    }

    public void setCLPH_NO(String CLPH_NO) {
        this.CLPH_NO = CLPH_NO;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String toString(){
        return getBP_CUST_NO() + ", " + getBP_MAGR_NO() + ", " + getBP_CUST_NM() + ", " + getBP_MAGR_NM()+ "\n";
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(BP_CUST_NO);
        dest.writeString(BP_MAGR_NO);
        dest.writeString(BP_CUST_NM);
        dest.writeString(BP_MAGR_NM);
        dest.writeString(CLPH_NO);
        dest.writeString(BP_MEMO);
    }

    private CusEmp(Parcel in){ readFromParcel(in);}

    private void readFromParcel(Parcel in){
        BP_CUST_NO = in.readString();
        BP_MAGR_NO = in.readString();
        BP_CUST_NM = in.readString();
        BP_MAGR_NM = in.readString();
        CLPH_NO = in.readString();
        BP_MEMO = in.readString();
    }

    public static final Creator CREATOR = new Creator() {
        @Override
        public CusEmp createFromParcel(Parcel source) {
            return new CusEmp(source);
        }

        @Override
        public CusEmp[] newArray(int size) {
            return new CusEmp[size];
        }
    };
}
