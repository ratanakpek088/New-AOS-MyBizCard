package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by Torn Sokly on 1/10/2017.
 */

public class TX_MYCD_MBL_R004_RES extends TxMessage {
    private  TX_MYCD_MBL_R004_RES_DATA mTxKeyData; // 전문명세 필드

    public TX_MYCD_MBL_R004_RES(Object obj) throws Exception {
        mTxKeyData = new TX_MYCD_MBL_R004_RES_DATA();
        super.initRecvMessage(obj);
    }

    public String getNOTI_ORG_NM() throws JSONException,Exception {
        return getString(mTxKeyData.NOTI_ORG_NM);
    }
    public String getNOTI_ORG_IMG() throws JSONException,Exception {
        return getString(mTxKeyData.NOTI_ORG_IMG);
    }
    public String getTITLE_NM() throws JSONException,Exception {
        return getString(mTxKeyData.TITLE_NM);
    }
    public String getINQ_DTM() throws JSONException,Exception {
        return getString(mTxKeyData.INQ_DTM);
    }
    public String getNOTI_MSG() throws JSONException,Exception {
        return getString(mTxKeyData.NOTI_MSG);
    }
    public String getIMG_URL() throws JSONException,Exception {
        return getString(mTxKeyData.IMG_URL);
    }
    public String getTARGET_PAGE_URL() throws JSONException,Exception {
        return getString(mTxKeyData.TARGET_PAGE_URL);
    }
    
    private class TX_MYCD_MBL_R004_RES_DATA {
        private String  NOTI_ORG_NM = "NOTI_ORG_NM";
        private String  NOTI_ORG_IMG = "NOTI_ORG_IMG";
        private String  TITLE_NM = "TITLE_NM";
        private String  INQ_DTM = "INQ_DTM";
        private String  NOTI_MSG = "NOTI_MSG";
        private String  IMG_URL = "IMG_URL";
        private String  TARGET_PAGE_URL = "TARGET_PAGE_URL";
    }
}
