package bizplay.kosign.com.bizplay.item;

/**
 * Created by Torn Sokly on 1/10/2017.
 */

public class NoticeItem {
    private String CD_USER_NO;
    private String SEND_DTM;
    private String NOTI_SEQ;
    private String NOTI_ORG_NM;
    private String NOTI_ORG_IMG;
    private String TITLE_NM;
    private String INQ_DTM;
    private String IMG_URL;
    private String TARGET_PAGE_URL;
    private String NOTI_GB;

    public String getCD_USER_NO() {
        return CD_USER_NO;
    }

    public void setCD_USER_NO(String CD_USER_NO) {
        this.CD_USER_NO = CD_USER_NO;
    }

    public String getSEND_DTM() {
        return SEND_DTM;
    }

    public void setSEND_DTM(String SEND_DTM) {
        this.SEND_DTM = SEND_DTM;
    }

    public String getNOTI_SEQ() {
        return NOTI_SEQ;
    }

    public void setNOTI_SEQ(String NOTI_SEQ) {
        this.NOTI_SEQ = NOTI_SEQ;
    }

    public String getNOTI_ORG_NM() {
        return NOTI_ORG_NM;
    }

    public void setNOTI_ORG_NM(String NOTI_ORG_NM) {
        this.NOTI_ORG_NM = NOTI_ORG_NM;
    }

    public String getNOTI_ORG_IMG() {
        return NOTI_ORG_IMG;
    }

    public void setNOTI_ORG_IMG(String NOTI_ORG_IMG) {
        this.NOTI_ORG_IMG = NOTI_ORG_IMG;
    }

    public String getTITLE_NM() {
        return TITLE_NM;
    }

    public void setTITLE_NM(String TITLE_NM) {
        this.TITLE_NM = TITLE_NM;
    }

    public String getINQ_DTM() {
        return INQ_DTM;
    }

    public void setINQ_DTM(String INQ_DTM) {
        this.INQ_DTM = INQ_DTM;
    }

    public String getIMG_URL() {
        return IMG_URL;
    }

    public void setIMG_URL(String IMG_URL) {
        this.IMG_URL = IMG_URL;
    }

    public String getTARGET_PAGE_URL() {
        return TARGET_PAGE_URL;
    }

    public void setTARGET_PAGE_URL(String TARGET_PAGE_URL) {
        this.TARGET_PAGE_URL = TARGET_PAGE_URL;
    }

    public String getNOTI_GB() {
        return NOTI_GB;
    }

    public void setNOTI_GB(String NOTI_GB) {
        this.NOTI_GB = NOTI_GB;
    }
}
