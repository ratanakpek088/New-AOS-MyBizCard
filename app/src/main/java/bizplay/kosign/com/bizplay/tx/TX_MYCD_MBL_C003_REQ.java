package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONArray;
import org.json.JSONException;


/**
 *  Created by Huo Chhunleng on 15/Feb/2017.
 */
public class TX_MYCD_MBL_C003_REQ extends TxMessage {

    public static final String TXNO = "MYCD_MBL_C003";
    private TX_MYCD_MBL_C003_REQ_DATA mTxKeyData;

    public TX_MYCD_MBL_C003_REQ() throws Exception{
        mTxKeyData = new TX_MYCD_MBL_C003_REQ_DATA();
        super.initSendMessage();
    }

    private class TX_MYCD_MBL_C003_REQ_DATA{
        private String REG_TX_CD        = "REG_TX_CD";
        private String CARD_CORP_CD     = "CARD_CORP_CD";
        private String CARD_NO          = "CARD_NO";
        private String APV_DT           = "APV_DT";
        private String APV_SEQ          = "APV_SEQ";
        private String APV_TM           = "APV_TM";
        private String APV_NO           = "APV_NO";
        private String APV_AMT          = "APV_AMT";
        private String SUPL_AMT         = "SUPL_AMT";
        private String VAT_AMT          = "VAT_AMT";
        private String BSNS_DSNC_CD     = "BSNS_DSNC_CD";
        private String BSNS_DSNC_NM     = "BSNS_DSNC_NM";
        private String TRAN_KIND_CD     = "TRAN_KIND_CD";
        private String TRAN_KIND_NM     = "TRAN_KIND_NM";
        private String MGMT1            = "MGMT1";
        private String MGMT1_NM         = "MGMT1_NM";
        private String MGMT2            = "MGMT2";
        private String MGMT2_NM         = "MGMT2_NM";
        private String MGMT3            = "MGMT3";
        private String MGMT3_NM         = "MGMT3_NM";
        private String MGMT4            = "MGMT4";
        private String MGMT4_NM         = "MGMT4_NM";
        private String MGMT5            = "MGMT5";
        private String MGMT5_NM         = "MGMT5_NM";
        private String BP_EMPL_REC      = "BP_EMPL_REC";
        private String BP_CUST_REC      = "BP_CUST_REC";
        private String RCPT_USER_REC    = "RCPT_USER_REC";
        private String RCPT_IMG_REC     = "RCPT_IMG_REC";
    }

    public void setREG_TX_CD(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.REG_TX_CD, value);
    }

    public void setCARD_CORP_CD(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.CARD_CORP_CD, value);
    }

    public void setCARD_NO(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.CARD_NO, value);
    }

    public void setAPV_DT(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.APV_DT, value);
    }

    public void setAPV_SEQ(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.APV_SEQ, value);
    }

    public void setAPV_TM(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.APV_TM, value);
    }

    public void setAPV_NO(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.APV_NO, value);
    }

    public void setAPV_AMT(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.APV_AMT, value);
    }

    public void setSUPL_AMT(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.SUPL_AMT, value);
    }

    public void setVAT_AMT(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.VAT_AMT, value);
    }

    public void setBSNS_DSNC_CD(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.BSNS_DSNC_CD, value);
    }

    public void setBSNS_DSNC_NM(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.BSNS_DSNC_NM, value);
    }

    public void setTRAN_KIND_CD(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.TRAN_KIND_CD, value);
    }

    public void setTRAN_KIND_NM(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.TRAN_KIND_NM, value);
    }

    public void setMGMT1(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.MGMT1, value);
    }

    public void setMGMT1_NM(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.MGMT1_NM, value);
    }

    public void setMGMT2(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.MGMT2, value);
    }

    public void setMGMT2_NM(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.MGMT2_NM, value);
    }

    public void setMGMT3(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.MGMT3, value);
    }

    public void setMGMT3_NM(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.MGMT3_NM, value);
    }

    public void setMGMT4(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.MGMT4, value);
    }

    public void setMGMT4_NM(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.MGMT4_NM, value);
    }

    public void setMGMT5(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.MGMT5, value);
    }

    public void setMGMT5_NM(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.MGMT5_NM, value);
    }

    public void setBP_EMPL_REC(JSONArray value) throws JSONException{
        mSendMessage.put(mTxKeyData.BP_EMPL_REC, value);
    }

    public void setBP_CUST_REC(JSONArray value) throws JSONException{
        mSendMessage.put(mTxKeyData.BP_CUST_REC, value);
    }

    public void setRCPT_USER_REC(JSONArray value) throws JSONException{
        mSendMessage.put(mTxKeyData.RCPT_USER_REC, value);
    }

    public void setRCPT_IMG_REC(JSONArray value) throws JSONException{
        mSendMessage.put(mTxKeyData.RCPT_IMG_REC, value);
    }

}
