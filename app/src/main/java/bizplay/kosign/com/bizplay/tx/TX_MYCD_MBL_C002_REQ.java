package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by RatanakPek on 1/14/2017.
 */

public class TX_MYCD_MBL_C002_REQ extends TxMessage {
    public static final String TXNO = "MYCD_MBL_C002";
    private TX_MYCD_MBL_C002_REQ_DATA mTxKeyData;

    public TX_MYCD_MBL_C002_REQ() throws Exception {
        mTxKeyData = new TX_MYCD_MBL_C002_REQ_DATA();
        super.initSendMessage();
    }

    public void setREG_TX_CD(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.REG_TX_CD, value);
    }

    public void setCARD_CORP_CD(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.CARD_CORP_CD, value);
    }

    public void setCARD_NO(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.CARD_NO, value);
    }

    public void setAPV_DT(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.APV_DT, value);
    }

    public void setAPV_SEQ(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.APV_SEQ, value);
    }

    public void setAPV_TM(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.APV_TM, value);
    }

    public void setAPV_NO(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.APV_NO, value);
    }


    public void setAPV_AMT(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.APV_AMT, value);
    }

    public void setTRAN_KIND_CD(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.TRAN_KIND_CD, value);
    }

    public void setTRAN_KIND_NM(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.TRAN_KIND_NM, value);
    }

    public void setSUMMARY(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.SUMMARY, value);
    }

    public void setRCPT_USER_REC(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.RCPT_USER_REC, value);
    }

    public void setRCPT_IMG_REC(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.RCPT_IMG_REC, value);
    }

    public void setRCPT_USER_REC(JSONArray value) throws JSONException {
        mSendMessage.put(mTxKeyData.RCPT_USER_REC, value);
    }

    public void setRCPT_IMG_REC(JSONArray value) throws JSONException {
        mSendMessage.put(mTxKeyData.RCPT_IMG_REC, value);
    }

    public void setSUPL_AMT(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.SUPL_AMT, value);
    }

    public void setVAT_AMT(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.VAT_AMT, value);
    }

    private class TX_MYCD_MBL_C002_REQ_DATA {
        private String REG_TX_CD = "REG_TX_CD";
        private String CARD_CORP_CD = "CARD_CORP_CD";
        private String CARD_NO = "CARD_NO";
        private String APV_DT = "APV_DT";
        private String APV_SEQ = "APV_SEQ";
        private String APV_TM = "APV_TM";
        private String APV_NO = "APV_NO";
        private String APV_AMT = "APV_AMT";
        private String SUPL_AMT = "SUPL_AMT";
        private String VAT_AMT="VAT_AMT";
        private String TRAN_KIND_CD = "TRAN_KIND_CD";
        private String TRAN_KIND_NM = "TRAN_KIND_NM";
        private String SUMMARY = "SUMMARY";
        private String RCPT_USER_REC = "RCPT_USER_REC";
        private String RCPT_IMG_REC = "RCPT_IMG_REC";

    }
}
