package bizplay.kosign.com.bizplay.tabs;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.webcash.sws.pref.PreferenceDelegator;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import bizplay.kosign.com.bizplay.MainActivity;
import bizplay.kosign.com.bizplay.R;
import bizplay.kosign.com.bizplay.adapters.CardViewPagerAdapter;
import bizplay.kosign.com.bizplay.cardview.CoverFlow;
import bizplay.kosign.com.bizplay.cardview.MyPageFragment;
import bizplay.kosign.com.bizplay.cardview.MyPageItem;
import bizplay.kosign.com.bizplay.constant.Constants;
import bizplay.kosign.com.bizplay.item.Card;
import bizplay.kosign.com.bizplay.item.CardDataTest;
import bizplay.kosign.com.bizplay.tran.ComTran;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L001_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L001_RES;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_L001_RES_REC;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_P001_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_R002_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_R002_RES;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyCardFragment extends Fragment implements ViewPager.OnPageChangeListener, MainActivity.OnMyCardReload, ComTran.OnComTranListener {

    private static MyCardFragment instance = null;
    private ViewPager mViewPager;
    private CardViewPagerAdapter mPagerAdapter;
    private ArrayList<MyPageItem> mItems;
    private LinearLayout pager_indicator;
    private ImageView[] dots;
    private int dotsCount;
    private ComTran mComTran;
    private RelativeLayout rNoCard, rHaveCard;
    private int cardPosition = 0;

    private TextView txtLST_TRAN_DTM, txtAPPR_SUM_AMT, txtTRAN_CNT;


    private View mView;
    private boolean checkFirstTime = true;

    public MyCardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mComTran = new ComTran(getActivity(), this);

        mItems = new ArrayList<>();
        Card.mCardList = new ArrayList<>();

        checkFirstTime = true;



//
//        Card.mCardList.add(new Card("30000001", "1234567894234567", "My Card", "", "", ""));
//        Card.mCardList.add(new Card("30000002", "234567865456765", "My Card", "", "", ""));
//        Card.mCardList.add(new Card("30000015", "3456789456789123", "My Card", "", "", ""));
//        Card.mCardList.add(new Card("30000001", "1234567894234567", "My Card", "", "", ""));
//        Card.mCardList.add(new Card("30000002", "1234567894234567", "My Card", "", "", ""));
//        Card.mCardList.add(new Card("30000015", "3456789456789123", "My Card", "", "", ""));
//        Card.mCardList.add(new Card("30000001", "3456789456789123", "My Card", "", "", ""));
//        Card.mCardList.add(new Card("30000002", "345678945678942", "My Card", "", "", ""));
//        Card.mCardList.add(new Card("30000015", "3456789456789123", "My Card", "", "", ""));
//        Card.mCardList.add(new Card("30000002", "1234567894234567", "My Card", "", "", ""));
//        Card.mCardList.add(new Card("30000015", "3456789456789123", "My Card", "", "", ""));
//        Card.mCardList.add(new Card("30000001", "3456789456789123", "My Card", "", "", ""));
//        Card.mCardList.add(new Card("30000002", "345678945678942", "My Card", "", "", ""));
//        Card.mCardList.add(new Card("30000015", "3456789456789123", "My Card", "", "", ""));


        //test
//        cardDataTestArrayList = new ArrayList<>();
//
//        cardDataTestArrayList.add(
//                new CardDataTest("30000001", "1234567894234567", "100000000", "1000000", "2016-12-16 12:20", "5", "")
//        );
//        cardDataTestArrayList.add(
//                new CardDataTest("30000002", "234567865456765", "100000000", "2000000", "2016-09-16 12:20", "10", "")
//        );
//        cardDataTestArrayList.add(
//                new CardDataTest("30000015", "3456789456789123", "100000000", "3000000", "2016-05-16 12:20", "20", "")
//        );
//        cardDataTestArrayList.add(
//                new CardDataTest("30000001", "1234567894234567", "100000000", "4000000", "2016-02-16 12:20", "30", "")
//        );
//        cardDataTestArrayList.add(
//                new CardDataTest("30000002", "1234567894234567", "100000000", "5000000", "2016-12-16 12:20", "10", "")
//        );
//        cardDataTestArrayList.add(
//                new CardDataTest("30000015", "3456789456789123", "100000000", "6000000", "2016-11-16 12:20", "3", "")
//        );
//        cardDataTestArrayList.add(
//                new CardDataTest("30000001", "3456789456789123", "100000000", "7000000", "2016-07-16 12:20", "8", "")
//        );
//        cardDataTestArrayList.add(
//                new CardDataTest("30000002", "345678945678942", "100000000", "8000000", "2016-03-16 12:20", "12", "")
//        );
//        cardDataTestArrayList.add(
//                new CardDataTest("30000015", "3456789456789123", "100000000", "18000000", "2016-05-16 12:20", "13", "")
//        );
//        test


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_my_card, container, false);

        //Extra info card
        txtAPPR_SUM_AMT = (TextView) mView.findViewById(R.id.txtAPPR_SUM_AMT);
        txtLST_TRAN_DTM = (TextView) mView.findViewById(R.id.txtLST_TRAN_DTM);
        txtTRAN_CNT = (TextView) mView.findViewById(R.id.txtTRAN_CNT);
        //Extra info card


        rNoCard = (RelativeLayout) mView.findViewById(R.id.rl_no_card);
        rHaveCard = (RelativeLayout) mView.findViewById(R.id.rl_card_exist);

        mViewPager = (ViewPager) mView.findViewById(R.id.carousel_pager);
        mViewPager.setClipToPadding(false);
        mViewPager.setOffscreenPageLimit(3);
        new CoverFlow.Builder()
                .with(mViewPager)
                .scale(0.82f)
                .pagerMargin(getResources().getDimensionPixelSize(R.dimen.margin_pager))
                .spaceSize(getResources().getDimension(R.dimen.page_content_width) / 10);

        mPagerAdapter = new CardViewPagerAdapter<MyPageItem>(getFragmentManager(), MyPageFragment.class, R.layout.page_layout, mItems);
        mViewPager.setAdapter(mPagerAdapter);
        pager_indicator = (LinearLayout) mView.findViewById(R.id.viewPagerCountDots);
        mViewPager.addOnPageChangeListener(this);

        mView.findViewById(R.id.nextWrapper).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity m = (MainActivity) getActivity();
                m.setNextPage();
            }
        });

        requestCard();
//        loadData();
        return mView;
    }

    public void loadData() {

        mItems.clear();
        mViewPager.removeAllViews();

        for (int i = 0; i < Card.mCardList.size(); i++) {
            MyPageItem item = new MyPageItem();
            item.setCARD_CORP_CD(Card.mCardList.get(i).getCARD_CORP_CD());
            item.setCARD_ORG_NM(Card.mCardList.get(i).getCARD_ORG_NM());
            item.setCARD_NO(Card.mCardList.get(i).getCARD_NO());
            item.setCARD_BYNM(Card.mCardList.get(i).getBIZ_NM());
            item.setEXPR_TRM(Card.mCardList.get(i).getEXPR_TRM());
            mItems.add(item);
        }
        mPagerAdapter = new CardViewPagerAdapter(getFragmentManager(), MyPageFragment.class, R.layout.page_layout, mItems);
        mViewPager.setClipToPadding(false);
        mViewPager.setOffscreenPageLimit(3);
        new CoverFlow.Builder()
                .with(mViewPager)
                .scale(0.82f)
                .pagerMargin(getResources().getDimensionPixelSize(R.dimen.margin_pager))
                .spaceSize(getResources().getDimensionPixelSize(R.dimen.page_content_width) / 10)
                .build();
        mPagerAdapter = new CardViewPagerAdapter<MyPageItem>(getFragmentManager(), MyPageFragment.class, R.layout.page_layout, mItems);
        mViewPager.setAdapter(mPagerAdapter);
        setUiPageViewController();
        mViewPager.setCurrentItem(cardPosition);

        //New...
        //Selected Current Card
        selectPreviouseCard();



    }

    public void selectPreviouseCard() {
       if(checkFirstTime){
           String cardNo = PreferenceDelegator.getInstance(getActivity()).get(Constants.MycardInfo.CARD_NO);
           String cardCorpCD = PreferenceDelegator.getInstance(getActivity()).get(Constants.MycardInfo.CARD_CORP_CD);
           boolean ifExist = false;
           for (int i = 0; i < Card.mCardList.size(); i++) {
               if (cardNo.equals(Card.mCardList.get(i).getCARD_NO()) && cardCorpCD.equals(Card.mCardList.get(i).getCARD_CORP_CD())) {
                   ifExist = true;
                   mViewPager.setCurrentItem(i);
                   requestExtraInfoCard(i);
                   break;
               }
           }

           if (!ifExist) {
               mViewPager.setCurrentItem(0);
               requestExtraInfoCard(0);
           }


            checkFirstTime = false;
       }
    }

    private void setUiPageViewController() {

        pager_indicator.removeAllViews();

        dotsCount = mPagerAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(getActivity());
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.non_selecteditem_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(8, 0, 8, 0);

            pager_indicator.addView(dots[i], params);

            if (i == 4) {
                break;
            }
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
    }

    public static MyCardFragment getInstance() {
        try {
            if (instance == null) {
                // new instance
                instance = new MyCardFragment();

                // sets data to bundle
                Bundle bundle = new Bundle();

                // set data to fragment
                instance.setArguments(bundle);

                return instance;
            } else {

                return instance;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void requestExtraInfoCard(int position) {
        try {
            TX_MYCD_MBL_R002_REQ req = new TX_MYCD_MBL_R002_REQ();
            req.setCARD_CORP_CD(Card.mCardList.get(position).getCARD_CORP_CD());
            req.setCARD_NO(Card.mCardList.get(position).getCARD_NO());
            mComTran.requestData(TX_MYCD_MBL_R002_REQ.TXNO, req.getSendMessage(), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String stringFormatToDate(String str) {

        String date = null;
        if (str.equals("") || str.equals("null") || str.equals(null)) {
            date = "";
        } else {
            String mm = str.substring(str.length() - 4, str.length() - 2);
            String h = str.substring(str.length() - 6, str.length() - 4);

            String dd = str.substring(str.length() - 8, str.length() - 6);
            String MM = str.substring(str.length() - 10, str.length() - 8);
            String yyyy = str.substring(str.length() - 14, str.length() - 10);

            date = yyyy + "-" + MM + "-" + dd + " " + h + ":" + mm;
        }

        return date;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        int count = mViewPager.getChildCount();
        if (count < 5) count = count;
        else count = 5;
        for (int i = 0; i < count; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.non_selecteditem_dot));
        }

        if (position >= 5) {
            dots[4].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));

        } else {
            dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));

        }

        //for receipt reload
        MainActivity m = (MainActivity) getActivity();
        m.catchPositionCard(position);

        //for extra info of card testing
        requestExtraInfoCard(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onChangeCardPosition(int position) {
        cardPosition = position;
    }

    @Override
    public void reloadCard(int position) {
        if (Constants.ReceiptInfo.WILL_RELOAD) {
            requestCard();

            //requestExtraInfoCard(position);
        }
    }

    public void requestCard() {
        try {
            TX_MYCD_MBL_L001_REQ req = new TX_MYCD_MBL_L001_REQ();
            mComTran.requestData(TX_MYCD_MBL_L001_REQ.TXNO, req.getSendMessage(), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTranResponse(String tranCode, Object object) throws Exception {
        if (tranCode == TX_MYCD_MBL_L001_REQ.TXNO) {
            TX_MYCD_MBL_L001_RES res = new TX_MYCD_MBL_L001_RES(object);
            TX_MYCD_MBL_L001_RES_REC rec = res.getREC();

            Card.mCardList.clear();

            for (int i = 0; i < rec.getLength(); i++) {
                rec.move(i);
                Card.mCardList.add(
                        new Card(
                                rec.getCARD_CORP_CD(),
                                rec.getCARD_NO(),
                                rec.getCARD_ORG_NM(),
                                rec.getBIZ_NM(),
                                rec.getCARD_NICK_NM(),
                                rec.getEXPR_TRM()
                        )
                );
            }

            if (Card.mCardList.size() == 0) { // if no cards
                rNoCard.setVisibility(View.VISIBLE);
                rHaveCard.setVisibility(View.GONE);

                Button btnManager = (Button) mView.findViewById(R.id.manager);
                btnManager.setText("관리자: " + res.getCARD_MAGR_NM());

                Constants.MycardInfo.CARD_MAGR_NM = res.getCARD_MAGR_NM();


            } else { // if have card(s)

                rHaveCard.setVisibility(View.VISIBLE);
                rNoCard.setVisibility(View.GONE);

                mPagerAdapter.notifyDataSetChanged();

                loadData();
            }

        } else if (tranCode == TX_MYCD_MBL_R002_REQ.TXNO) {
            TX_MYCD_MBL_R002_RES res = new TX_MYCD_MBL_R002_RES(object);
            Log.d("Data Extra info", res.getMessageToString());

            txtTRAN_CNT.setText(res.getTRAN_CNT() + "개");
            txtAPPR_SUM_AMT.setText(new DecimalFormat("#,###").format(Double.parseDouble(res.getAPPR_SUM_AMT())));
            txtLST_TRAN_DTM.setText(stringFormatToDate(res.getLST_TRAN_DTM()));

            MainActivity m = (MainActivity) getActivity();
            m.lookingForNewReceipt(Integer.parseInt(res.getTRAN_CNT()) != 0);
            m.lookingForNewNotice(Integer.parseInt(res.getNOTI_CNT()) != 0);

//                Log.d("DATE:", new Date().toString());
        }
    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }
}
