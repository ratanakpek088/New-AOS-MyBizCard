package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by Huo Chhunleng on 6/14/2016.
 */
public class TX_MYCD_MBL_L005_RES_REC extends TxMessage {

    private TX_MYCD_MBL_L005_RES_REC1_DATA mTxKeyData;		// 전문명세 필드

    public TX_MYCD_MBL_L005_RES_REC(Object obj) throws Exception{
        mTxKeyData = new TX_MYCD_MBL_L005_RES_REC1_DATA();
        super.initRecvMessage(obj);

    }

    /**
     * 전문 Data 필드 설정
    */
    private class TX_MYCD_MBL_L005_RES_REC1_DATA {
        private String CLPH_NO =	"CLPH_NO";
        private String FLNM =	"FLNM";
        private String BSNN_NM = "BSNN_NM";
        private String DVSN_NM = "DVSN_NM";
        private String JBCL_NM = "JBCL_NM";
        private String RSPT_NM = "RSPT_NM";
        private String EML = "EML";
        private String PRFL_PHTG = "PRFL_PHTG";


    }

    public String getCLPH_NO() throws JSONException,Exception {
        return getString(mTxKeyData.CLPH_NO);
    }

    public String getFLNM() throws JSONException,Exception {
        return getString(mTxKeyData.FLNM);
    }

    public String getBSNN_NM() throws JSONException,Exception{
        return getString(mTxKeyData.BSNN_NM);
    }

    public String getDVSN_NM() throws JSONException,Exception{
        return getString(mTxKeyData.DVSN_NM);
    }

    public String getJBCL_NM() throws JSONException,Exception{
        return getString(mTxKeyData.JBCL_NM);
    }

    public String getRSPT_NM() throws JSONException,Exception{
        return getString(mTxKeyData.RSPT_NM);
    }

    public String getEML() throws JSONException,Exception{
        return getString(mTxKeyData.EML);
    }

    public String getPRFL_PHTG() throws JSONException,Exception{
        return getString(mTxKeyData.PRFL_PHTG);
    }

}
