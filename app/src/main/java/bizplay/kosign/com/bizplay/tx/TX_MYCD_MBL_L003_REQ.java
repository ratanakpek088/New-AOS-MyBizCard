package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by Daravuth on 1/13/2017.
 */

public class TX_MYCD_MBL_L003_REQ extends TxMessage {
    public static final String TXNO = "MYCD_MBL_L003";
    private TX_MYCD_MBL_L003_REQ_DATA mTxKeyData;

    public TX_MYCD_MBL_L003_REQ() throws Exception {
        mTxKeyData = new TX_MYCD_MBL_L003_REQ_DATA();
        super.initSendMessage();
    }
    public void setCARD_CORP_CD(String value){
        mSendMessage.put(this.mTxKeyData.CARD_CORP_CD, value);
    }
    public void setCARD_NO(String value){
        mSendMessage.put(this.mTxKeyData.CARD_NO, value);
    }

    private class TX_MYCD_MBL_L003_REQ_DATA{
        private String CARD_CORP_CD="CARD_CORP_CD";
        private String CARD_NO="CARD_NO";
    }
}
