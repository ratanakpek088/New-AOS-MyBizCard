package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by Daravuth on 3/7/2017.
 */

public class TX_MYCD_MBL_L008_RES_REC extends TxMessage {
    private TX_MYCD_MBL_L008_RES_REC_DATA mTxKeyData;

    public TX_MYCD_MBL_L008_RES_REC(Object object) throws Exception {
        mTxKeyData = new TX_MYCD_MBL_L008_RES_REC_DATA();
        super.initRecvMessage(object);
    }
    public String getBP_CUST_NO() throws JSONException {
        return getString(mTxKeyData.BP_CUST_NO);
    }
    public String getBP_MAGR_NO() throws JSONException {
        return getString(mTxKeyData.BP_MAGR_NO);
    }
    public String getBP_CUST_NM() throws JSONException {
        return getString(mTxKeyData.BP_CUST_NM);
    }
    public String getBP_MAGR_NM() throws JSONException {
        return getString(mTxKeyData.BP_MAGR_NM);
    }
    public String getCLPH_NO() throws JSONException {
        return getString(mTxKeyData.CLPH_NO);
    }
    public String getBP_MEMO() throws JSONException {
        return getString(mTxKeyData.BP_MEMO);
    }
    private class TX_MYCD_MBL_L008_RES_REC_DATA {
        private String BP_CUST_NO="BP_CUST_NO";
        private String BP_MAGR_NO="BP_MAGR_NO";
        private String BP_CUST_NM="BP_CUST_NM";
        private String BP_MAGR_NM="BP_MAGR_NM";
        private String CLPH_NO="CLPH_NO";
        private String BP_MEMO="BP_MEMO";
    }
}
