package bizplay.kosign.com.bizplay.tran;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;

import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.webcash.sws.log.DevLog;
import com.webcash.sws.network.VolleyNetwork;
import com.webcash.sws.network.internal.NetworkErrorCode;
import com.webcash.sws.network.internal.OnNetworkListener;
import com.webcash.sws.network.tx.JSONHelper;
import com.webcash.sws.pref.MemoryPreferenceDelegator;
import com.webcash.sws.pref.PreferenceDelegator;
import com.webcash.sws.util.ComUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;

import bizplay.kosign.com.bizplay.IntroActivity;
import bizplay.kosign.com.bizplay.LoginActivity;
import bizplay.kosign.com.bizplay.R;
import bizplay.kosign.com.bizplay.conf.Conf;
import bizplay.kosign.com.bizplay.constant.Constants;

/**
 * Created by user on 2016-01-05.
 * 전문 통신 공통 Class
 */
public class ComTran implements OnNetworkListener {

    private Context mContext;                           // Activity Context
    private OnComTranListener mTranListener;            // UI단 통신 결과 Listener
    private VolleyNetwork mVolleyNetwork;               // 통신 Class
    private GoogleCloudMessaging mGCM = null;
    private boolean mIsDialog;
    private ComLoading mLoading;
    private String errmsg;

    public ComTran(Context context , OnComTranListener listener) {
        mContext = context;
        mTranListener = listener;
        mLoading = new ComLoading(context);
        mVolleyNetwork = new VolleyNetwork(mContext , this);
    }

    public void requestData(String tranCd, HashMap<String, Object> requestData) throws JSONException {
        requestData(tranCd, requestData, true);
    }

    /*
     * MG 전문 호출
     * @param tranCd
     */
    public void requestData(String tranCd , String url) {

        ConnectivityManager connectManager = (ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        //+ 인터넷 연결 상태
        if(connectManager.getActiveNetworkInfo() == null) {
            //+네트워크 연결 오류
            onErrorData(tranCd, NetworkErrorCode.TRNS_ERRCD_INTERNET , null);
            return;
        }
        mVolleyNetwork.requestVolleyNetwork(tranCd, false, url, false);
    }

    /*
     *  전문송신 - JSON Data
     * @param tranCd
     * @param tran_req_data
     */
    @SuppressWarnings("deprecation")
    public void requestData(String tranCd, HashMap<String, Object> tran_req_data, boolean isdialog) throws JSONException {

        String bizURL = MemoryPreferenceDelegator.getInstance().get("BIZ_URL");
        if(bizURL.equals("")) {
            //+ 전문 page url 이 없는경우 리턴
            return;
        }

        mIsDialog = isdialog;

        if(isdialog) {
//            showProgressDialog();
            mLoading.showProgressDialog();
        }

        // 네트워크 연결 확인
        if(ComUtil.getNetworkStatus((Activity)mContext)) {

            JSONObject jsonObj =  (JSONObject) JSONHelper.toJSON(tran_req_data);

            DevLog.devLog("visal log", "jsonObj ::    " + jsonObj.toString());

            DevLog.devLog("nryoo", "jsonObj ::    " + jsonObj.toString());

            JSONObject jobjectInput = new JSONObject();

            jobjectInput.put(ComTranCode.CNTS_CRTS_KEY_CODE  , "");
            jobjectInput.put(ComTranCode.KEY_TRAN_CODE  , tranCd);
            jobjectInput.put(ComTranCode.KEY_REQ_DATA   , jsonObj);

            DevLog.devLog("nryoo", "requestData ::    " + jobjectInput.toString());

            //+header
            HashMap<String, String> headers = new HashMap<>();
            headers.put("charset", "UTF-8");
            headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
            headers.put("User-Agent", Conf.mUserAgent);
            mVolleyNetwork.setComHeaders(headers);

            try {
                DevLog.devLog("nryoo", "send :: " + bizURL + "?JSONData=" + URLEncoder.encode(jobjectInput.toString()));
                mVolleyNetwork.requestVolleyNetwork(tranCd, false, bizURL + "?JSONData=" + URLEncoder.encode(jobjectInput.toString(), "UTF-8"), false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            //+네트워크 연결 오류
            onErrorData(tranCd, NetworkErrorCode.TRNS_ERRCD_INTERNET, null);
        }
    }

    /*
     * 전문송신 - JSON Data
     * @param tranCd
     * @param tran_req_data
     */
    @SuppressWarnings("deprecation")
    public void requestPost(final String tranCd, HashMap<String, Object> tran_req_data, boolean isdialog) throws JSONException {

        String bizURL = MemoryPreferenceDelegator.getInstance().get("BIZ_URL");
        if(bizURL.equals("")) {
            //+ 전문 page url 이 없는경우 리턴
            return;
        }

        mIsDialog = isdialog;

        if(isdialog) {
//            showProgressDialog();
            mLoading.showProgressDialog();
        }

        // 네트워크 연결 확인
        if(ComUtil.getNetworkStatus((Activity)mContext)) {

            JSONObject jsonObj =  (JSONObject) JSONHelper.toJSON(tran_req_data);

            DevLog.devLog("nryoo", "jsonObj ::    " + jsonObj.toString());

            JSONObject jobjectInput = new JSONObject();

            jobjectInput.put(ComTranCode.CNTS_CRTS_KEY_CODE  , "");
            jobjectInput.put(ComTranCode.KEY_TRAN_CODE  , tranCd);
            jobjectInput.put(ComTranCode.KEY_REQ_DATA   , jsonObj);

            DevLog.devLog("nryoo", "requestData ::    " + jobjectInput.toString());

            //+header
            HashMap<String, String> headers = new HashMap<>();
            headers.put("charset", "UTF-8");
            headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
            headers.put("User-Agent", Conf.mUserAgent);
            mVolleyNetwork.setComHeaders(headers);

            try {
                DevLog.devLog("nryoo", "send :: " + bizURL + "?JSONData=" + URLEncoder.encode(jobjectInput.toString()));
                HashMap<String, String> params = new HashMap<>();
                params.put("JSONData", URLEncoder.encode(jobjectInput.toString(), "UTF-8"));
                mVolleyNetwork.requestVolleyNetwork(tranCd, true, bizURL ,params, false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            //+네트워크 연결 오류
            onErrorData(tranCd, NetworkErrorCode.TRNS_ERRCD_INTERNET, null);
        }
    }

    @Override
    public void onNetworkResponse(final String tranCode, Object object) {

        //+ 전문 응답부
        if(mTranListener == null){
            return;
        }

        JSONObject jsonOutput;
        JSONArray jarrayResData   = null;

        try {

            jsonOutput = new JSONObject(URLDecoder.decode(object.toString().replaceAll("%(?![0-9a-fA-F]{2})", "%25"), "UTF-8"));
            DevLog.devLog("nryoo", "onGtinNetworkResponse jsonOutput::" + jsonOutput.toString());


            if(tranCode.equals("MG_DATA")){
                if(!jsonOutput.isNull(ComTranCode.KEY_RES_DATA)) {
                    jsonOutput = jsonOutput.getJSONObject(ComTranCode.KEY_RES_DATA);
                    if(!jsonOutput.isNull(ComTranCode.KEY_TRAN_RES_DATA)) {
                        jarrayResData = jsonOutput.getJSONArray(ComTranCode.KEY_TRAN_RES_DATA);
                    }
                } else {
                    onErrorData(tranCode, NetworkErrorCode.TRNS_ERRCD_PAGEERR, jsonOutput);
                    return;
                }
            } else {
                /* 응답 값 */
                if(!jsonOutput.isNull(ComTranCode.KEY_RSLT_CD)){
                    //* 오류발생 전문 코드값 처리
                    String resultErrorCd = jsonOutput.getString(ComTranCode.KEY_RSLT_CD);
                    if(!resultErrorCd.equals("0000")) {
                        onErrorData(tranCode, resultErrorCd, jsonOutput);
                        return;
                    }
                }

                if(!jsonOutput.isNull(ComTranCode.KEY_RES_DATA)) {
//                    jarrayResData = jsonOutput.getJSONArray(ComTranCode.KEY_RES_DATA);
                    jarrayResData = new JSONArray();
                    jarrayResData.put(jsonOutput.getJSONObject(ComTranCode.KEY_RES_DATA));
                }
            }

            //+ 통신 Dialog Dismiss
//            dismissProgressDialog();
            mLoading.dismissProgressDialog();

            mTranListener.onTranResponse(tranCode, jarrayResData);
            return;



        } catch (JSONException e1) {
            e1.printStackTrace();
            //onNetworkError(tranCode, NetworkErrorCode.TRNS_ERRCD_PASER );
            onErrorData(tranCode, NetworkErrorCode.TRNS_ERRCD_PASER, null);
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            //onNetworkError(tranCode, NetworkErrorCode.TRNS_ERRCD_PASER);
            onErrorData(tranCode, NetworkErrorCode.TRNS_ERRCD_PASER, null);
        }
        catch (Exception e) {
            e.printStackTrace();
            onErrorData(tranCode, NetworkErrorCode.APP_ERRCD_UNKNOWN, null);
            //onNetworkError(tranCode, NetworkErrorCode.APP_ERRCD_UNKNOWN);
        }
    }

    @Override
    public void onNetworkError(String tranCode, Object object) {

        String errorStr;
        //+ 전문 오류
        if(object instanceof VolleyError){
            VolleyError error = (VolleyError)object;
            DevLog.devLog("nryoo", "onNetworkError VolleyError::" + error.toString());
            errorStr = error.toString();
        }else         {
            DevLog.devLog("nryoo", "onNetworkError jsonOutput::" + object.toString());
            errorStr =  object.toString();
        }
        onErrorData(tranCode, NetworkErrorCode.TRNS_ERRCD_PAGEERR, errorStr);
        /*dismissProgressDialog();
        mTranListener.onTranError(tranCode, errorStr);*/

    }


    /*
     * 통신중 에러 처리
     * @param tranCode
     * @param errcd
     * @param error
     */
    private void onErrorData(final String tranCode, final String errcd, Object error) {

        try {
            mLoading.dismissProgressDialog();

            errmsg = "";
            if (errcd.equals(NetworkErrorCode.TRNS_ERRCD_INTERNET)) {

                errmsg = errmsg + "인터넷 연결이 불안정합니다.\n연결 후 이용하시기 바랍니다.";
//                error = errmsg;
            } else if (errcd.equals(NetworkErrorCode.TRNS_ERRCD_MAKE)
                    || errcd.equals(NetworkErrorCode.TRNS_ERRCD_PASER)
                    || errcd.equals(NetworkErrorCode.APP_ERRCD_UNKNOWN)) {
                // 공통에서 전문발송 데이타를 만드는중에러
                // 전문결과 받은후 공통에서 json 파싱중 에러 (전문 형식 잘못됨)
                errmsg = errmsg + "처리중 오류가 발생하였습니다.";
//                error = errmsg;
            } else if (errcd.equals(NetworkErrorCode.TRNS_ERRCD_PAGEERR)) {
//            return;
                errmsg = errmsg + "통신 상태가 불안정합니다.\n잠시 후 이용하시기 바랍니다.";
//                error = errmsg;
            } else if (errcd.equals("E_2001") || errcd.equals("E_2002")) {
                // 세션이 종료되었습니다.
                if (error instanceof JSONObject) {
                    JSONObject jsonObj = (JSONObject) error;
                    if (jsonObj.has(ComTranCode.KEY_RSLT_MSG)) {

                        try {
                            errmsg = jsonObj.getString(ComTranCode.KEY_RSLT_MSG);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    errmsg = errmsg + error.toString();
                }

            } else {

                if (error instanceof JSONObject) {
                    JSONObject jsonObj = (JSONObject) error;
                    if (jsonObj.has(ComTranCode.KEY_RSLT_MSG)) {

                        try {
                            errmsg = jsonObj.getString(ComTranCode.KEY_RSLT_MSG);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    errmsg = errmsg + error.toString();
                }
            }

            final String ErrorTranCode;
            final Object objError = error;
            ErrorTranCode = tranCode;

            ((Activity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!((Activity) mContext).isFinishing()) {

                        AlertDialog.Builder builder;
                        builder = new AlertDialog.Builder(mContext);
                        builder.setTitle(mContext.getString(R.string.alert_info));
                        builder.setCancelable(false);
                        builder.setMessage(errmsg);
                        builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (mTranListener == null) {
                                    return;
                                }
                                // 세션이 종료되었습니다.
                                if (errcd.equals("E_2001") || errcd.equals("E_2002")) {
                                    DevLog.devLog("ComTran", "Session was end!");
                                    MemoryPreferenceDelegator.getInstance().put("KILL_VA", "N"); // not yet to kill mVaccine
                                    Intent intent;
                                    String check_auto_login = PreferenceDelegator.getInstance(mContext).get(Constants.LoginInfo.Auto_LOGIN);
                                        if (check_auto_login.equals("") || check_auto_login.equals("false")) {
                                            intent = new Intent(mContext, LoginActivity.class);
                                        } else {
                                            intent = new Intent(mContext, IntroActivity.class);
                                        }
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                            mContext.startActivity(intent);
                                            ((Activity) mContext).finish();

                                        } else if (errcd.equals("1008") || errcd.equals("5510")) {
                                            PreferenceDelegator.getInstance(mContext).put(Constants.LoginInfo.Auto_LOGIN, "false");
                                            MemoryPreferenceDelegator.getInstance().put("KILL_VA", "N"); // not yet to kill mVaccine

                                            Intent intent = new Intent(mContext, LoginActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                            mContext.startActivity(intent);
                                            ((Activity) mContext).finish();
                                        } else {
                                            mTranListener.onTranError(ErrorTranCode, objError);
                                        }
                                    }
                                }).show();
                            }
                        }
                    });

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     *  쿠키값 설정
     */
//    public void syncCookieManager() {
//        String domainURL = MemoryPreferenceDelegator.getInstance().get("BIZ_URL");
//        mVolleyNetwork.syncSessionWithWebView(domainURL);
//    }

    /**
     * 세션정보를 초기화한다.
     */
//    public void sessionKill() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            CookieManager.getInstance().removeSessionCookies(null);
//            CookieManager.getInstance().removeAllCookies(null);
//        }else {
//
//            CookieManager.getInstance().removeExpiredCookie();
//            CookieManager.getInstance().removeSessionCookie();
//            CookieSyncManager.getInstance().resetSync();
//        }
//
////		Conf.LAST_TRAN_TIME = Conf.AUTO_LOGOUT_TIME;
//
//    }

    /**
     *  통신 처리 UI단 전달 Listener
     */
    public interface OnComTranListener {
        void onTranResponse(String tranCode, Object object) throws Exception;
        void onTranError(String tranCode, Object object);
    }

    private class ComTranCode {

        //+요청
        public static final String KEY_REQ_DATA 				= "REQ_DATA";	// 전문 요청 DATA
        public static final String KEY_TRAN_CODE 				= "TRAN_NO";	// 전문 요청 CODE
        public static final String CNTS_CRTS_KEY_CODE 			= "CNTS_CRTS_KEY";	// 전문 요청 CODE

        //+응답
        public static final String KEY_RSLT_CD 					= "RSLT_CD";	// 결과코드
        public static final String KEY_RSLT_MSG					= "RSLT_MSG";	// 결과메세지
        public static final String KEY_RES_DATA 				= "RESP_DATA";	// 응답데이타
        public static final String KEY_TRAN_RES_DATA 			= "_tran_res_data";	// 정상 응답 전문
    }

    /**
     * Push register
     */
    @SuppressWarnings("deprecation")
    public void requestPushRegister(final String tranCd, boolean isdialog){

        if (isdialog) {
            mLoading.showProgressDialog();
        }

        ConnectivityManager connectManager = (ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        //* 인터넷 연결 상태
        if(connectManager.getActiveNetworkInfo() == null){
            //* 네트워크 연결 오류
            onErrorData(tranCd, NetworkErrorCode.TRNS_ERRCD_INTERNET, null);
            return;
        }

        /**
         * GCM 정보 설정
         */
        //* 1. Google Play Services 사용 가능 여부 체크
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mContext);
        if(resultCode != ConnectionResult.SUCCESS) {

            mLoading.dismissProgressDialog();

            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) mContext, 0);
            if (dialog != null) {
                //This dialog will help the user update to the latest GooglePlayServices
                dialog.show();
            }

//            onErrorData(tranCd, NetworkErrorCode.PUSH_ERRCD_UNKNOWN_GCM, null);
            return;
        }

        new AsyncTask<String, Void, String>() {

            @Override
            protected String doInBackground(String... params) {
                if(mGCM == null){
                    mGCM = GoogleCloudMessaging.getInstance(mContext);
                }

                String regid = "";
                try {
                    regid = mGCM.register(Conf.PUSH_PROJECT_NO);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return regid;
            }

//            @Override
//            protected void onPostExecute(String msg) {
//
//                String regId; // 새로 발급 받은 Registration ID
//                regId = msg;
//                if(!regId.equals("")) {
//                    try {
//                        TX_IBK_PUSH_REG_REQ req = new TX_IBK_PUSH_REG_REQ();
//                        req.setPUSHSERVER_KIND("GCM");
//                        req.setAPPID("com.ibk.bizcard");
//                        req.setPUSH_ID(regId);
//                        req.setMODEL_NAME(Build.MODEL);
//                        req.setOS(Build.VERSION.RELEASE);
//                        req.setCOMPANY_ID("WEBCASH");
//                        req.setDEVICE_ID(Utils.createUUID(mContext));
//                        req.setRELATION_KEY(PreferenceDelegator.getInstance(mContext).get(Constants.LoginInfo.USER_ID));
//                        req.setRETRANS_YN("N");
//                        req.setREMARK("Android");
//                        requestData(tranCd,req.getSendMessage(),false);
//                    } catch(Exception e) {
//                        e.printStackTrace();
//                        onErrorData(tranCd, NetworkErrorCode.PUSH_ERRCD_UNKNOWN, null);
//                    }
//                } else {
//                    onErrorData(tranCd, NetworkErrorCode.PUSH_ERRCD_UNKNOWN, null);
//                }
//            }

        }.execute();

    }

}
