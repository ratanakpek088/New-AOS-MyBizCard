package bizplay.kosign.com.bizplay.cardview;

import android.os.Parcel;
import android.os.Parcelable;

public class MyPageItem implements Parcelable {
    private String mTitle;
    /*
        CARD_CORP_CD
       30000001;"KB"
       30000002;"현대"
       30000003;"삼성"
       30000004;"외환"
       30000006;"비씨"
       30000008;"신한"
       30000010;"하나"
       30000012;"광주"
       30000013;"수협"
       30000014;"전북"
       30000015;"하나"
       30000016;"씨티"
       30000017;"아멕스"
       30000018;"우리"
       30000019;"롯데"
       30000020;"산은"
       30000021;"NH"
       30000060;"기업"
     */
    private String CARD_CORP_CD;            // 카드사코드
    private String CARD_NO;                 // 카드번호
    private String CARD_REG_STS;            // 카드등록구분
    private String CARD_ORG_NM;             // 카드사명
    private String CARD_NM;                 // 카드명
    private String CARD_BYNM;               // 카드별칭
    private String USE_HANDO;               // 이용한도
    private String EXPR_TRM;
    private String CARD_MODULE;

    public MyPageItem() {}

    public MyPageItem(String title) {
        mTitle = title;
    }

    private MyPageItem(Parcel in) {
        mTitle = in.readString();
        CARD_CORP_CD = in.readString();
        CARD_NO = in.readString();
        CARD_REG_STS = in.readString();
        CARD_ORG_NM = in.readString();
        CARD_NM = in.readString();
        CARD_BYNM = in.readString();
        USE_HANDO = in.readString();
        CARD_MODULE = in.readString();
    }

    public MyPageItem(String CARD_CORP_CD, String CARD_NO, String CARD_REG_STS, String CARD_ORG_NM, String CARD_NM, String CARD_BYNM, String USE_HANDO, String EXPR_TRM, String CARD_MODULE) {
        this.CARD_CORP_CD = CARD_CORP_CD;
        this.CARD_NO = CARD_NO;
        this.CARD_REG_STS = CARD_REG_STS;
        this.CARD_ORG_NM = CARD_ORG_NM;
        this.CARD_NM = CARD_NM;
        this.CARD_BYNM = CARD_BYNM;
        this.USE_HANDO = USE_HANDO;
        this.EXPR_TRM = EXPR_TRM;
        this.CARD_MODULE = CARD_MODULE;
    }

    public void setTitle(String value) { this.mTitle = value;}
    public String getTitle() {
        return mTitle;
    }

    /*
        CARD_CORP_CD
       30000001;"KB"
       30000002;"현대"
       30000003;"삼성"
       30000004;"외환"
       30000006;"비씨"
       30000008;"신한"
       30000010;"하나"
       30000012;"광주"
       30000013;"수협"
       30000014;"전북"
       30000015;"하나"
       30000016;"씨티"
       30000017;"아멕스"
       30000018;"우리"
       30000019;"롯데"
       30000020;"산은"
       30000021;"NH"
       30000060;"기업"
     */
    public String getCARD_CORP_CD() {
        return CARD_CORP_CD;
    }

    /*
        CARD_CORP_CD
       30000001;"KB"
       30000002;"현대"
       30000003;"삼성"
       30000004;"외환"
       30000006;"비씨"
       30000008;"신한"
       30000010;"하나"
       30000012;"광주"
       30000013;"수협"
       30000014;"전북"
       30000015;"하나"
       30000016;"씨티"
       30000017;"아멕스"
       30000018;"우리"
       30000019;"롯데"
       30000020;"산은"
       30000021;"NH"
       30000060;"기업"
     */
    public void setCARD_CORP_CD(String CARD_CORP_CD) {
        this.CARD_CORP_CD = CARD_CORP_CD;
    }

    public String getCARD_NO() {
        return CARD_NO;
    }

    public void setCARD_NO(String CARD_NO) {
        this.CARD_NO = CARD_NO;
    }

    public String getCARD_ORG_NM() {
        return CARD_ORG_NM;
    }

    public void setCARD_ORG_NM(String CARD_ORG_NM) {
        this.CARD_ORG_NM = CARD_ORG_NM;
    }

    public String getCARD_NM() {
        return CARD_NM;
    }

    public void setCARD_NM(String CARD_NM) {
        this.CARD_NM = CARD_NM;
    }

    public String getEXPR_TRM() {
        return EXPR_TRM;
    }

    public void setEXPR_TRM(String EXPR_TRM) {
        this.EXPR_TRM = EXPR_TRM;
    }

    public String getCARD_REG_STS() {
        return CARD_REG_STS;
    }

    public void setCARD_REG_STS(String CARD_REG_STS) {
        this.CARD_REG_STS = CARD_REG_STS;
    }

    public String getCARD_BYNM() {
        return CARD_BYNM;
    }

    public void setCARD_BYNM(String CARD_BYNM) {
        this.CARD_BYNM = CARD_BYNM;
    }

    public String getUSE_HANDO() {
        return USE_HANDO;
    }

    public void setUSE_HANDO(String USE_HANDO) {
        this.USE_HANDO = USE_HANDO;
    }

    public String getCARD_MODULE() {return  CARD_MODULE;}

    public void setCARD_MODULE(String CARD_MODULE) { this.CARD_MODULE = CARD_MODULE;}

    public static final Creator<MyPageItem> CREATOR =
            new Creator<MyPageItem>() {
                @Override
                public MyPageItem createFromParcel(Parcel in) {
                    return new MyPageItem(in);
                }

                @Override
                public MyPageItem[] newArray(int size) {
                    return new MyPageItem[size];
                }
            };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(mTitle);
        out.writeString(CARD_CORP_CD);
        out.writeString(CARD_NO);
        out.writeString(CARD_REG_STS);
        out.writeString(CARD_ORG_NM);
        out.writeString(CARD_NM);
        out.writeString(CARD_BYNM);
        out.writeString(USE_HANDO);
        out.writeString(CARD_MODULE);

    }
}

