package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by ViSAL MSi on 12/29/2016.
 */

public class TX_MYCD_MBL_L002_RES_REC extends TxMessage {

    private TX_MYCD_MBL_L002_RES_REC_DATA mTxKeyData;

    public TX_MYCD_MBL_L002_RES_REC(Object object) throws Exception {
        mTxKeyData = new TX_MYCD_MBL_L002_RES_REC_DATA();
        super.initRecvMessage(object);
    }

    public String getCARD_NO() throws Exception, JSONException {
        return getString(mTxKeyData.CARD_NO);
    }

    public String getAPV_DT() throws Exception, JSONException {
        return getString(mTxKeyData.APV_DT);
    }

    public String getAPV_SEQ() throws Exception, JSONException {
        return getString(mTxKeyData.APV_SEQ);
    }

    public String getAPV_TM() throws Exception, JSONException {
        return getString(mTxKeyData.APV_TM);
    }

    public String getAPV_NO() throws Exception, JSONException {
        return getString(mTxKeyData.APV_NO);
    }

    public String getAPV_AMT() throws Exception, JSONException {
        return getString(mTxKeyData.APV_AMT);
    }

    public String getAPV_CAN_YN() throws Exception, JSONException {
        return getString(mTxKeyData.APV_CAN_YN);
    }

    public String getAPV_CAN_DT() throws Exception, JSONException {
        return getString(mTxKeyData.APV_CAN_DT);
    }

    public String getITLM_MMS_CNT() throws Exception, JSONException {
        return getString(mTxKeyData.ITLM_MMS_CNT);
    }

    public String getSETL_SCHE_DT() throws Exception, JSONException {
        return getString(mTxKeyData.SETL_SCHE_DT);
    }

    public String getMEST_NM() throws Exception, JSONException {
        return getString(mTxKeyData.MEST_NM);
    }

    public String getMEST_NO() throws Exception, JSONException {
        return getString(mTxKeyData.MEST_NO);
    }

    public String getMEST_BIZ_NO() throws Exception, JSONException {
        return getString(mTxKeyData.MEST_BIZ_NO);
    }

    public String getRCPT_TX_STS() throws Exception, JSONException {
        return getString(mTxKeyData.RCPT_TX_STS);
    }

    public String getRCPT_RFS_STS() throws Exception, JSONException {
        return getString(mTxKeyData.RCPT_RFS_STS);
    }

    public String getCARD_NICK_NM() throws Exception, JSONException {
        return getString(mTxKeyData.CARD_NICK_NM);
    }

    public String getTRAN_KIND_CD() throws Exception, JSONException {
        return getString(mTxKeyData.TRAN_KIND_CD);
    }

    public String getDAY_NM() throws Exception, JSONException {
        return getString(mTxKeyData.DAY_NM);
    }

    public String getTRAN_KIND_NM() throws Exception, JSONException {
        return getString(mTxKeyData.TRAN_KIND_NM);
    }


    private class TX_MYCD_MBL_L002_RES_REC_DATA {
        private String CARD_NO = "CARD_NO";
        private String APV_DT = "APV_DT";
        private String APV_SEQ = "APV_SEQ";
        private String APV_TM = "APV_TM";
        private String APV_NO = "APV_NO";
        private String APV_AMT = "APV_AMT";
        private String APV_CAN_YN = "APV_CAN_YN";
        private String APV_CAN_DT = "APV_CAN_DT";
        private String ITLM_MMS_CNT = "ITLM_MMS_CNT";
        private String SETL_SCHE_DT = "SETL_SCHE_DT";
        private String MEST_NM = "MEST_NM";
        private String MEST_NO = "MEST_NO";
        private String MEST_BIZ_NO = "MEST_BIZ_NO";
        private String RCPT_TX_STS = "RCPT_TX_STS";
        private String RCPT_RFS_STS = "RCPT_RFS_STS";
        private String CARD_NICK_NM = "CARD_NICK_NM";
        private String DAY_NM = "DAY_NM";
        private String TRAN_KIND_CD = "TRAN_KIND_CD";
        private String TRAN_KIND_NM = "TRAN_KIND_NM";
    }
}

