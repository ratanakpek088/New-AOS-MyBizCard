package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by User on 7/15/2016.
 */
public class TX_MYCD_MBL_P004_REQ_REC extends TxMessage {
    public static final String TXNO = "MYCD_MBL_P004";
    private TX_MYCD_MBL_P004_REQ_REC_DATA mTxKeyData;

    public TX_MYCD_MBL_P004_REQ_REC() throws Exception {
        mTxKeyData = new TX_MYCD_MBL_P004_REQ_REC_DATA();
        super.initSendMessage();
    }
    public void setRCPT_USER_HPNO(String value){
        mSendMessage.put(mTxKeyData.RCPT_USER_HPNO, value);
    }
    public void setRCPT_USER_NM(String value){
        mSendMessage.put(mTxKeyData.RCPT_USER_NM, value);
    }
    public void setRCPT_USER_BSNN_NM(String value){
        mSendMessage.put(mTxKeyData.RCPT_USER_BSNN_NM, value);
    }
    public void setRCPT_USER_PSN_NM(String value){
        mSendMessage.put(mTxKeyData.RCPT_USER_PSN_NM, value);
    }
    public void setRCPT_USER_PSN_IMG(String value){
        mSendMessage.put(mTxKeyData.RCPT_USER_PSN_IMG, value);
    }

    private class TX_MYCD_MBL_P004_REQ_REC_DATA{
        private String RCPT_USER_HPNO       = "RCPT_USER_HPNO";
        private String RCPT_USER_NM         = "RCPT_USER_NM";
        private String RCPT_USER_BSNN_NM    = "RCPT_USER_BSNN_NM";
        private String RCPT_USER_PSN_NM     = "RCPT_USER_PSN_NM";
        private String RCPT_USER_PSN_IMG    = "RCPT_USER_PSN_IMG";
    }
}
