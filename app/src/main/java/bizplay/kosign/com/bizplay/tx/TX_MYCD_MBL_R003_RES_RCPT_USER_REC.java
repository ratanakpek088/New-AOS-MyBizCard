package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * @author Huo Chhunleng on 15/Feb/2017.
 */

public class TX_MYCD_MBL_R003_RES_RCPT_USER_REC extends TxMessage {

    private TX_MYCD_MBL_R008_RES_RCPT_USER_REC_DATA mTxKeyData;

    private class TX_MYCD_MBL_R008_RES_RCPT_USER_REC_DATA{
        private String RCPT_USER_HPNO       = "RCPT_USER_HPNO";
        private String RCPT_USER_NM         = "RCPT_USER_NM";
        private String RCPT_USER_NO         = "RCPT_USER_NO";
        private String RCPT_USER_BSNN_NM    = "RCPT_USER_BSNN_NM";
        private String RCPT_USER_PSN_NM     = "RCPT_USER_PSN_NM";
        private String RCPT_USER_PSN_IMG    = "RCPT_USER_PSN_IMG";
    }

    public TX_MYCD_MBL_R003_RES_RCPT_USER_REC(Object obj) throws Exception{
        mTxKeyData = new TX_MYCD_MBL_R008_RES_RCPT_USER_REC_DATA();
        super.initRecvMessage(obj);
    }


    public String getRCPT_USER_HPNO() throws JSONException {
        return getString(mTxKeyData.RCPT_USER_HPNO);
    }

    public String getRCPT_USER_NM() throws JSONException{
        return getString(mTxKeyData.RCPT_USER_NM);
    }

    public String getRCPT_USER_NO() throws JSONException{
        return getString(mTxKeyData.RCPT_USER_NO);
    }
    public String getRCPT_USER_BSNN_NM() throws JSONException {
        return getString(mTxKeyData.RCPT_USER_BSNN_NM);
    }

    public String getRCPT_USER_PSN_NM() throws JSONException{
        return getString(mTxKeyData.RCPT_USER_PSN_NM);
    }

    public String getRCPT_USER_PSN_IMG() throws JSONException{
        return getString(mTxKeyData.RCPT_USER_PSN_IMG);
    }


}
