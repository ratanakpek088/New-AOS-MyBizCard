package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by Daravuth on 3/3/2017.
 */

public class TX_MYCD_MBL_L006_REQ extends TxMessage {
    public static final String TXNO= "MYCD_MBL_L006";
    private TX_MYCD_MBL_L006_DATA mTxKeyData;

    public TX_MYCD_MBL_L006_REQ() throws Exception{
        mTxKeyData=new TX_MYCD_MBL_L006_DATA();
        super.initSendMessage();
    }
    public void setCARD_CORP_CD(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.CARD_CORP_CD, value);
    }

    public void setCARD_NO(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.CARD_NO, value);
    }

    private class TX_MYCD_MBL_L006_DATA {
        private String CARD_CORP_CD="CARD_CORP_CD";
        private String CARD_NO="CARD_NO";
    }
}
