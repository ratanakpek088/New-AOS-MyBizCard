package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by ViSAL MSi on 12/29/2016.
 */

public class TX_MYCD_MBL_L001_REQ extends TxMessage {

    public static final String TXNO = "MYCD_MBL_L001";
    private TX_MYCD_MBL_L001_REQ_DATA mTxKeyData;

    public TX_MYCD_MBL_L001_REQ() throws Exception {
        mTxKeyData = new TX_MYCD_MBL_L001_REQ_DATA();
        super.initSendMessage();
    }

    public void setINQ_DT(String value){
         mSendMessage.put(this.mTxKeyData.INQ_DT, value);
    }

    private class TX_MYCD_MBL_L001_REQ_DATA{
        private String INQ_DT = "INQ_DT";
    }
}
