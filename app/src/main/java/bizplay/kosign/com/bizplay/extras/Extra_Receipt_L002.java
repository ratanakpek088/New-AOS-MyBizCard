package bizplay.kosign.com.bizplay.extras;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


public class Extra_Receipt_L002 extends Extras {

    public Extra_Receipt_L002(Context ctx) {
        super(ctx);
    }

    public Extra_Receipt_L002(Activity act, Intent intent) {
        super(act, intent);
    }

    public Extra_Receipt_L002(Activity act, Bundle bundle) {
        super(act, bundle);
    }

    private final String CARD_NO = "CARD_NO";
    private final String APV_DT = "APV_DT";
    private final String APV_SEQ = "APV_SEQ";
    private final String APV_TM = "APV_TM";
    private final String APV_NO = "APV_NO";
    private final String APV_AMT = "APV_AMT";
    private final String APV_CAN_YN = "APV_CAN_YN";
    private final String APV_CAN_DT = "APV_CAN_DT";
    private final String ITLM_MMS_CNT = "ITLM_MMS_CNT";
    private final String SETL_SCHE_DT = "SETL_SCHE_DT";
    private final String MEST_NM = "MEST_NM";
    private final String MEST_NO = "MEST_NO";
    private final String MEST_BIZ_NO = "MEST_BIZ_NO";
    private final String RCPT_TX_STS = "RCPT_TX_STS";
    private final String RCPT_RFS_STS = "RCPT_RFS_STS";
    private final String CARD_NICK_NM = "CARD_NICK_NM";
    private final String DAY_NM = "DAY_NM";
    private final String TRAN_KIND_CD = "TRAN_KIND_CD";
    private final String TRAN_KIND_NM = "TRAN_KIND_NM";

    public _Param Param = new _Param();

    public class _Param {
        public void setTRAN_KIND_NM(String value) {
            setString(TRAN_KIND_NM, value);
        }

        public String getTRAN_KIND_NM() {
            return getString(TRAN_KIND_NM);
        }

        public void setTRAN_KIND_CD(String value) {
            setString(TRAN_KIND_CD, value);
        }

        public String getTRAN_KIND_CD() {
            return getString(TRAN_KIND_CD);
        }

        public void setDAY_NM(String value) {
            setString(DAY_NM, value);
        }

        public String getDAY_NM() {
            return getString(DAY_NM);
        }

        public void setCARD_NICK_NM(String value) {
            setString(CARD_NICK_NM, value);
        }

        public String getCARD_NICK_NM() {
            return getString(CARD_NICK_NM);
        }

        public void setMEST_BIZ_NO(String value) {
            setString(MEST_BIZ_NO, value);
        }

        public String getMEST_BIZ_NO() {
            return getString(MEST_BIZ_NO);
        }

        public void setMEST_NO(String value) {
            setString(MEST_NO, value);
        }

        public String getMEST_NO() {
            return getString(MEST_NO);
        }

        public void setMEST_NM(String value) {
            setString(MEST_NM, value);
        }

        public String getMEST_NM() {
            return getString(MEST_NM);
        }


        public void setSETL_SCHE_DT(String value) {
            setString(SETL_SCHE_DT, value);
        }

        public String getSETL_SCHE_DT() {
            return getString(SETL_SCHE_DT);
        }

        public void setITLM_MMS_CNT(String value) {
            setString(ITLM_MMS_CNT, value);
        }

        public String getITLM_MMS_CNT() {
            return getString(ITLM_MMS_CNT);
        }

        public void setAPV_CAN_DT(String value) {
            setString(APV_CAN_DT, value);
        }

        public String getAPV_CAN_DT() {
            return getString(APV_CAN_DT);
        }

        public void setAPV_CAN_YN(String value) {
            setString(APV_CAN_YN, value);
        }

        public String getAPV_CAN_YN() {
            return getString(APV_CAN_YN);
        }

        public void setAPV_AMT(String value) {
            setString(APV_AMT, value);
        }

        public String getAPV_AMT() {
            return getString(APV_AMT);
        }


        public void setAPV_NO(String value) {
            setString(APV_NO, value);
        }

        public String getAPV_NO() {
            return getString(APV_NO);
        }

        public void setAPV_TM(String value) {
            setString(APV_TM, value);
        }

        public String getAPV_TM() {
            return getString(APV_SEQ);
        }

        public void setAPV_SEQ(String value) {
            setString(APV_SEQ, value);
        }

        public String getAPV_SEQ() {
            return getString(APV_SEQ);
        }


        public void setAPV_DT(String value) {
            setString(APV_DT, value);
        }

        public String getAPV_DT() {
            return getString(APV_DT);
        }


        public void setCARD_NO(String value) {
            setString(CARD_NO, value);
        }

        public String getCARD_NO() {
            return getString(CARD_NO);
        }

        public void setRCPT_TX_STS(String value) {
            setString(RCPT_TX_STS, value);
        }

        public String getRCPT_TX_STS() {
            return getString(RCPT_TX_STS);
        }

        public void setRCPT_RFS_STS(String value) {
            setString(RCPT_RFS_STS, value);
        }

        public String getRCPT_RFS_STS() {
            return getString(RCPT_RFS_STS);
        }


    }

}
