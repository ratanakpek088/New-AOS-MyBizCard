package bizplay.kosign.com.bizplay.popup;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import bizplay.kosign.com.bizplay.R;


/*
 * Created by chanyouvita on 1/18/16.
 */
public class PopupWindow extends Dialog {
    private View mLayout;
    private OnPopupWindowListener listener;

    public PopupWindow(Context context, final View layoutId){
        super(context);
        mLayout = layoutId;

        super.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        super.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.setCanceledOnTouchOutside(false);
        super.setContentView(mLayout);
        super.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button cancel = (Button) layoutId.findViewById(R.id.btn_cancel);
        Button save = (Button) layoutId.findViewById(R.id.btn_save);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onPopupClose();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onPopupPressed();
            }
        });
    }

    public void setOnPopupListener (OnPopupWindowListener listener){
        this.listener = listener;
    }
}
