package bizplay.kosign.com.bizplay.popup;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import bizplay.kosign.com.bizplay.R;

/**
 * Created by Daravuth on 3/9/2017.
 */

public class DetailPopupMenu extends PopupMenu {
    private OnPopupMenuListener listener=null;
    private TextView item_1, item_2;

    public DetailPopupMenu(Activity activity) {
        super(activity, R.layout.rct_detail_popup);
        item_1 = (TextView) anchor.findViewById(R.id.item_1);
        item_2 = (TextView) anchor.findViewById(R.id.item_2);
        item_1.setOnClickListener(this);
        item_2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.item_1:
                listener.onItemClick1();
                break;
            case R.id.item_2:
                listener.onItemClick2();
                break;

        }
    }

    public void setListener(OnPopupMenuListener listener) {
        this.listener = listener;
    }

    public void setItem_1(String item_1) {
        this.item_1.setText(item_1);
    }

    public void setItem_2(String item_2) {
       this.item_2.setText(item_2);
    }

    public interface OnPopupMenuListener{
        void onItemClick1();
        void onItemClick2();
    }
}
