package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by Daravuth on 3/3/2017.
 */

public class TX_MYCD_MBL_R003_RES_BP_CUST_REC extends TxMessage{
    private TX_MYCD_MBL_R003_RES_BP_CUST_REC_DATA mTxKeyData;

public TX_MYCD_MBL_R003_RES_BP_CUST_REC(Object obj) throws Exception{
    mTxKeyData= new TX_MYCD_MBL_R003_RES_BP_CUST_REC_DATA();
    super.initRecvMessage(obj);
}
    private class TX_MYCD_MBL_R003_RES_BP_CUST_REC_DATA {
        private String BP_CUST_NO="BP_CUST_NO";
        private String BP_MAGR_NO="BP_MAGR_NO";
        private String BP_CUST_NM="BP_CUST_NM";
        private String BP_MAGR_NM="BP_MAGR_NM";
    }

    public String getBP_CUST_NO() throws Exception{
        return getString(mTxKeyData.BP_CUST_NO);
    }
    public String getBP_MAGR_NO() throws Exception{
        return getString(mTxKeyData.BP_MAGR_NO);
    }
    public String getBP_CUST_NM() throws Exception{
        return getString(mTxKeyData.BP_CUST_NM);
    }
    public String getBP_MAGR_NM() throws Exception{
        return getString(mTxKeyData.BP_MAGR_NM);
    }
}
