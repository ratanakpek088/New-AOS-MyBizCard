package bizplay.kosign.com.bizplay.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;

import java.util.ArrayList;

import bizplay.kosign.com.bizplay.R;
import bizplay.kosign.com.bizplay.item.UsageItem;


public class Usage_Adapter extends BaseAdapter{

    public OnItemClickListener listener = null;
    private ArrayList<UsageItem> data;
    private LayoutInflater inflater=null;
    private int selectedPosition;

    public Usage_Adapter(Context context, ArrayList<UsageItem> data, int selectedPosition) {
        this.data=data;
        this.selectedPosition = selectedPosition;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.sk_receipt_select_row, null);


        RadioButton radioButton = (RadioButton) vi.findViewById(R.id.btnskRadio);

        radioButton.setChecked(position == selectedPosition);
        radioButton.setTag(position);
        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPosition = (Integer)view.getTag();
                listener.onItemClick(selectedPosition);
                notifyDataSetChanged();
            }
        });

        UsageItem item = data.get(position);
        radioButton.setText(item.getITEM_NAME());

        return vi;
    }


    public void setOnListItemListener(OnItemClickListener listener){
        this.listener = listener;
    }

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
}