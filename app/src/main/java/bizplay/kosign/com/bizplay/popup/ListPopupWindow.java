package bizplay.kosign.com.bizplay.popup;

import android.content.Context;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;



import java.util.ArrayList;

import bizplay.kosign.com.bizplay.R;
import bizplay.kosign.com.bizplay.adapters.A007_2_4P_Adapter;
import bizplay.kosign.com.bizplay.adapters.Usage_Adapter;
import bizplay.kosign.com.bizplay.adapters.item.A007_2_4P_Item;
import bizplay.kosign.com.bizplay.item.UsageItem;

/*
 * Created by User on 3/24/2016.
 */
public class ListPopupWindow {

    private TextView listPopupTitle;
    private int item_selected = -1;
    private int item_selected_temp = -1;
    private OnListItemListener onListItemListener;

    public ListPopupWindow(final Context context, ArrayList<UsageItem> list, int selectedItem) {

        this.item_selected = selectedItem;
        this.item_selected_temp = selectedItem;

        final View layout = View.inflate(context, R.layout.a007_2_4p, null);
        listPopupTitle = (TextView) layout.findViewById(R.id.tv_pop_up_title);
        final PopupWindow popupWindow = new PopupWindow(context, layout);
        popupWindow.show();
        popupWindow.setOnPopupListener(new OnPopupWindowListener() {
            @Override
            public void onPopupPressed() {
                item_selected = item_selected_temp;
                try {
                    onListItemListener.onItemSelected(item_selected);
                }catch (Exception e){
                    //Log.d("Exception", "Error implement method setOnListItemListener");
                    //e.printStackTrace();
                }

                popupWindow.dismiss();
            }

            @Override
            public void onPopupClose() {
                popupWindow.dismiss();
            }
        });
        final ListView mListView = (ListView) layout.findViewById(R.id.lv_sk);
        final Usage_Adapter mAdapter = new Usage_Adapter(context, list, item_selected);
        mAdapter.setOnListItemListener(new Usage_Adapter.OnItemClickListener(){
            @Override
            public void onItemClick(int position) {
                item_selected_temp = position;
            }
        });

        mListView.setAdapter(mAdapter);

    }

    public void setListPopupTitle(String title){
        listPopupTitle.setText(title);
    }

    public interface OnListItemListener {
        void onItemSelected(int position);
    }

    public void setOnListItemListener(OnListItemListener onListItemListener){
        this.onListItemListener = onListItemListener;
    }

}
