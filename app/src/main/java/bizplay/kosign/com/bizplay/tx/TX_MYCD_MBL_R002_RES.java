package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by User on 2016-12-28.
 */

public class TX_MYCD_MBL_R002_RES extends TxMessage {


    private TX_PRVCD_MBL_P001_RES_DATA mTxKeyData;

    public TX_MYCD_MBL_R002_RES(Object obj) throws Exception {
        this.mTxKeyData = new TX_PRVCD_MBL_P001_RES_DATA();
        super.initRecvMessage(obj);
    }

    public String getAPPR_SUM_CNT() throws Exception {
        return getString(mTxKeyData.APPR_SUM_CNT);
    }

    public String getAPPR_SUM_AMT() throws Exception {
        return getString(mTxKeyData.APPR_SUM_AMT);
    }

    public String getLST_TRAN_DTM() throws Exception {
        return getString(mTxKeyData.LST_TRAN_DTM);
    }

    public String getTRAN_CNT() throws Exception {
        return getString(mTxKeyData.TRAN_CNT);
    }

    public String getNOTI_CNT() throws Exception {
        return getString(mTxKeyData.NOTI_CNT);
    }

    private class TX_PRVCD_MBL_P001_RES_DATA {
        private String APPR_SUM_CNT = "APPR_SUM_CNT";
        private String APPR_SUM_AMT = "APPR_SUM_AMT";
        private String LST_TRAN_DTM = "LST_TRAN_DTM";
        private String TRAN_CNT = "TRAN_CNT";
        private String NOTI_CNT = "NOTI_CNT";
    }
}
