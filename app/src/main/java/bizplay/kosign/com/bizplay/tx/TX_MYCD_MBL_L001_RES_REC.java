package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by ViSAL MSi on 12/29/2016.
 */

public class TX_MYCD_MBL_L001_RES_REC extends TxMessage {

    private TX_PRVCD_MBL_L001_RES_REC_DATA mTxKeyData;

    public TX_MYCD_MBL_L001_RES_REC(Object object) throws Exception {
        mTxKeyData = new TX_PRVCD_MBL_L001_RES_REC_DATA();
        super.initRecvMessage(object);
    }

    public String getCARD_CORP_CD() throws Exception, JSONException {
        return getString(mTxKeyData.CARD_CORP_CD);
    }

    public String getCARD_NO() throws Exception, JSONException {
        return getString(this.mTxKeyData.CARD_NO);
    }


    public String getCARD_ORG_NM() throws Exception, JSONException {
        return getString(this.mTxKeyData.CARD_ORG_NM);
    }

    public String getBIZ_NM() throws Exception, JSONException {
        return getString(this.mTxKeyData.BIZ_NM);
    }

    public String getCARD_NICK_NM() throws Exception, JSONException {
        return getString(this.mTxKeyData.CARD_NICK_NM);
    }

    public String getEXPR_TRM() throws Exception, JSONException {
        return getString(this.mTxKeyData.EXPR_TRM);
    }

    private class TX_PRVCD_MBL_L001_RES_REC_DATA {
        private String CARD_CORP_CD = "CARD_CORP_CD";
        private String CARD_NO = "CARD_NO";
        private String CARD_ORG_NM = "CARD_ORG_NM";
        private String BIZ_NM = "BIZ_NM";
        private String CARD_NICK_NM = "CARD_NICK_NM";
        private String EXPR_TRM = "EXPR_TRM";
    }
}
