package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by Daravuth on 3/7/2017.
 */

public class TX_MYCD_MBL_L008_RES extends TxMessage{
    private TX_MYCD_MBL_L008_RES_DATA mTxKeyData;

    public TX_MYCD_MBL_L008_RES(Object object) throws Exception {
        mTxKeyData = new TX_MYCD_MBL_L008_RES_DATA();
        super.initRecvMessage(object);
    }

    public TX_MYCD_MBL_L008_RES_REC getBP_EMPL_REC() throws Exception {
        return new TX_MYCD_MBL_L008_RES_REC(getRecord(mTxKeyData.BP_CUST_REC));
    }

    private class TX_MYCD_MBL_L008_RES_DATA {
        private String BP_CUST_REC="BP_CUST_REC";
    }
}
