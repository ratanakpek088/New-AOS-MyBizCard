package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

public class TX_MYCD_MBL_L004_REQ extends TxMessage {


    public static final String TXNO= "MYCD_MBL_L004";	// 전문 구분 코드

    private TX_MYCD_MBL_L004_REQ_DATA mTxKeyData;			// 전문명세 필드

    public TX_MYCD_MBL_L004_REQ() throws Exception{

        mTxKeyData = new TX_MYCD_MBL_L004_REQ_DATA();
        super.initSendMessage();

    }

    /**
     * 조회일자
     * @param value
     * @throws JSONException
     */
    public void setINQ_DT(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.INQ_DT, value);
    }


    /**
     * 전문 Data 필드 설정
     */
    private class TX_MYCD_MBL_L004_REQ_DATA {
        private String INQ_DT	            =	"INQ_DT";		    //조회일자
    }

}
