package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * MG 전문 반복부 1 - 메뉴 정보 
 * @author Administrator
 */
public class TX_IBK_MG_RES_REC1 extends TxMessage {
	
	private TX_BIZCARD_MG_RES_REC1_DATA mTxKeyData;	// 전문명세 필드 

	public TX_IBK_MG_RES_REC1(Object obj)throws Exception {
		mTxKeyData = new TX_BIZCARD_MG_RES_REC1_DATA();
		super.initRecvMessage(obj);
	}
	
	/**
	 * 메뉴 URL 정보 
	 * @return
	 * @throws JSONException
	 */
	public String getMenuUrl()throws JSONException {
		
		return getString(mTxKeyData.C_MENU_URL);
	}
	
	/**
	 *  메뉴 명
	 * @return
	 * @throws JSONException
	 */
	public String getMenuName()throws JSONException {
		
		return getString(mTxKeyData.C_MENU_NAME);
	}
	
	/**
	 *  메뉴 서비스 서비스불가 여부 
	 * @return
	 * @throws JSONException
	 */
	public boolean isMenuAvailable()throws JSONException {
		
		String strService = getString(mTxKeyData.C_MENU_AVAILABLE);
		if(strService.equals("")) {
			return false;
		}
		return true;
	}
	
	/**
	 *  메뉴서비스 불가 이유
	 * @return
	 * @throws JSONException
	 */
	public String getMenuAvailableReson()throws JSONException {
		
		return getString(mTxKeyData.C_MENU_RESON);
	}

	/**
	 *  아이디 app
	 * @return
	 * @throws JSONException
	 */
	public String getMenuID()throws JSONException {

		return getString(mTxKeyData.C_MENU_ID);
	}
	
	private class TX_BIZCARD_MG_RES_REC1_DATA {
		
		private String C_MENU_URL =  "c_url";  // 메뉴 url
		private String C_MENU_NAME = "c_name";  // 메뉴명
		private String C_MENU_AVAILABLE  = "c_available"; // 서비스불가 여부
		private String C_MENU_RESON = "c_reason";	 // 서비스불가 이유
		private String C_MENU_ID = "c_app_id"; // 아이디 app
	}

}
