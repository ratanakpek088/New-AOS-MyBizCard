package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by User on 2016-12-28.
 */

public class TX_MYCD_MBL_P001_REQ extends TxMessage {

    public static final String TXNO = "MYCD_MBL_P001";

    private TX_PRVCD_MBL_P001_REQ_DATA mTxKeyData;

    public TX_MYCD_MBL_P001_REQ() throws Exception {
        mTxKeyData = new TX_PRVCD_MBL_P001_REQ_DATA();
        super.initSendMessage();
    }

    public void setUserID(String value){
        mSendMessage.put(mTxKeyData.USER_ID, value);
    }


    public void setUserPassword(String value){
        mSendMessage.put(mTxKeyData.USER_PW, value);
    }



    private class TX_PRVCD_MBL_P001_REQ_DATA{
        private String USER_ID = "USER_ID";
        private String USER_PW = "USER_PW";
    }

}
