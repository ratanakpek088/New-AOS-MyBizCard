package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;


public class TX_MYCD_MBL_L004_RES_REC extends TxMessage {

    private TX_MYCD_MBL_L004_RES_REC_DATA mTxKeyData;		// 전문명세 필드

    public TX_MYCD_MBL_L004_RES_REC(Object obj) throws Exception{
        mTxKeyData = new TX_MYCD_MBL_L004_RES_REC_DATA();
        super.initRecvMessage(obj);
    }

    /**
     * 카드사용자번호
     * @throws Exception
     */
    public String getCD_USER_NO() throws JSONException,Exception{
        return getString(mTxKeyData.CD_USER_NO);
    }

    /**
     * 전송희망일시
     * @throws Exception
     */
    public String getSEND_DTM() throws JSONException,Exception{
        return getString(mTxKeyData.SEND_DTM);
    }


    /**
     * 일련번호
     * @throws Exception
     */
    public String getNOTI_SEQ() throws JSONException,Exception{
        return getString(mTxKeyData.NOTI_SEQ);
    }

    /**
     * 알림기관명
     * @throws Exception
     */
    public String getNOTI_ORG_NM() throws JSONException,Exception{
        return getString(mTxKeyData.NOTI_ORG_NM);
    }

    /**
     * 일림기관이미지
     * @throws Exception
     */
    public String getNOTI_ORG_IMG() throws JSONException,Exception{
        return getString(mTxKeyData.NOTI_ORG_IMG);
    }

    /**
     * 알림제목
     * @throws Exception
     */
    public String getTITLE_NM() throws JSONException,Exception{
        return getString(mTxKeyData.TITLE_NM);
    }

    /**
     * 조회일시
     * @throws Exception
     */
    public String getINQ_DTM() throws JSONException,Exception{
        return getString(mTxKeyData.INQ_DTM);
    }

    /**
     * 알림 이미지URL
     * @throws Exception
     */
    public String getIMG_URL() throws JSONException,Exception{
        return getString(mTxKeyData.IMG_URL);
    }

    /**
     * 랜딩페이지
     * @throws Exception
     */
    public String getTARGET_PAGE_URL() throws JSONException,Exception{
        return getString(mTxKeyData.TARGET_PAGE_URL);
    }

    /**
     * 랜딩페이지
     * @throws Exception
     */
    public String getNOTI_GB() throws JSONException,Exception{
        return getString(mTxKeyData.NOTI_GB);
    }
    /**
     * 전문 Data 필드 설정
     */
    private class TX_MYCD_MBL_L004_RES_REC_DATA {
        private String CD_USER_NO	            =	"CD_USER_NO";		    //카드사용자번호
        private String SEND_DTM	                =	"SEND_DTM";		        //전송희망일시
        private String NOTI_SEQ	                =	"NOTI_SEQ";		        //일련번호
        private String NOTI_ORG_NM	            =	"NOTI_ORG_NM";		    //알림기관명
        private String NOTI_ORG_IMG	            =	"NOTI_ORG_IMG";		    //일림기관이미지
        private String TITLE_NM	                =	"TITLE_NM";		        //알림제목
        private String INQ_DTM	                =	"INQ_DTM";		        //조회일시
        private String IMG_URL	                =	"IMG_URL";		        //알림 이미지URL
        private String TARGET_PAGE_URL	        =	"TARGET_PAGE_URL";		//랜딩페이지
        private String NOTI_GB		            =   "NOTI_GB";  		    //알림구분
    }

}
