package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONArray;

/**
 * Created by User on 7/15/2016.
 */
public class TX_MYCD_MBL_D001_REQ extends TxMessage {
    public static final String TXNO = "MYCD_MBL_D001";
    private TX_MYCD_MBL_D001_REQ_DATA mTxKeyData;

    public TX_MYCD_MBL_D001_REQ() throws Exception {
        mTxKeyData = new TX_MYCD_MBL_D001_REQ_DATA();
        super.initSendMessage();
    }

    public void setCARD_CORP_CD(String value){
        mSendMessage.put(mTxKeyData.CARD_CORP_CD, value);
    }
    public void setCARD_NO(String value){
        mSendMessage.put(mTxKeyData.CARD_NO, value);
    }
    public void setRCPT_USER_REC(JSONArray value){
        mSendMessage.put(mTxKeyData.RCPT_USER_REC,value);
    }
    private class TX_MYCD_MBL_D001_REQ_DATA{
        private String CARD_CORP_CD     = "CARD_CORP_CD";
        private String CARD_NO          = "CARD_NO";
        private String RCPT_USER_REC    = "RCPT_USER_REC";
    }
}
