package bizplay.kosign.com.bizplay.tran;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import bizplay.kosign.com.bizplay.R;

/*
 * Created by ChanYouvita on 7/9/16.
 */
public class ComLoading {
    private Context mContext;
    private Dialog mProgressDialog;
    private AnimationDrawable mFrameAnimation;

    public ComLoading(Context context) {
        mContext = context;
    }

    /**
     *  통신단 Dialog
     */
    public void showProgressDialog() {

        ((Activity) mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!((Activity) mContext).isFinishing()) {

                    if (mProgressDialog == null) {
                        View loadingView = LayoutInflater.from(mContext).inflate(R.layout.comm_loading_activity, null);
                        ImageView imgView = (ImageView) loadingView.findViewById(R.id.iv_loading);
                        mFrameAnimation = (AnimationDrawable) imgView.getBackground();
                        mFrameAnimation.start();

                        mProgressDialog = new Dialog(mContext);
                        mProgressDialog.setCancelable(false);
                        mProgressDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                        mProgressDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                        mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        mProgressDialog.setContentView(loadingView);
                        mProgressDialog.setCanceledOnTouchOutside(false);
                    }

                    mProgressDialog.show();
                }
            }
        });
    }

    public void dismissProgressDialog() {

        if (mProgressDialog== null) {
            return;
        }

        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}
