package bizplay.kosign.com.bizplay.item;

/**
 * Created by kosign-pc on 18/04/2016.
 */
public class AppInfo_Item {

    String app_id;
    String app_name;
    String c_available;
    String c_reason;

    public AppInfo_Item() {

    }

    public AppInfo_Item(String app_id, String app_name, String c_available, String c_reason){
        this.app_id = app_id;
        this.app_name = app_name;
        this.c_available = c_available;
        this.c_reason = c_reason;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public String getC_available() {
        return c_available;
    }

    public void setC_available(String c_available) {
        this.c_available = c_available;
    }

    public String getC_reason() {
        return c_reason;
    }

    public void setC_reason(String c_reason) {
        this.c_reason = c_reason;
    }
}