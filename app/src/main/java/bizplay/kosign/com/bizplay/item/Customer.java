package bizplay.kosign.com.bizplay.item;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Daravuth on 3/7/2017.
 */

public class Customer implements Parcelable {
    private String BP_CUST_NO;
    private String BP_MAGR_NO;
    private String BP_CUST_NM;
    private String BP_MAGR_NM;
    private String CLPH_NO;
    private String BP_MEMO;
    private boolean isChecked;

    public String getBP_CUST_NO() {
        return BP_CUST_NO;
    }

    public void setBP_CUST_NO(String BP_CUST_NO) {
        this.BP_CUST_NO = BP_CUST_NO;
    }

    public String getBP_MAGR_NO() {
        return BP_MAGR_NO;
    }

    public void setBP_MAGR_NO(String BP_MAGR_NO) {
        this.BP_MAGR_NO = BP_MAGR_NO;
    }

    public String getBP_CUST_NM() {
        return BP_CUST_NM;
    }

    public void setBP_CUST_NM(String BP_CUST_NM) {
        this.BP_CUST_NM = BP_CUST_NM;
    }

    public String getBP_MAGR_NM() {
        return BP_MAGR_NM;
    }

    public void setBP_MAGR_NM(String BP_MAGR_NM) {
        this.BP_MAGR_NM = BP_MAGR_NM;
    }

    public String getCLPH_NO() {
        return CLPH_NO;
    }

    public void setCLPH_NO(String CLPH_NO) {
        this.CLPH_NO = CLPH_NO;
    }

    public String getBP_MEMO() {
        return BP_MEMO;
    }

    public void setBP_MEMO(String BP_MEMO) {
        this.BP_MEMO = BP_MEMO;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    protected Customer(Parcel dest) {
        BP_CUST_NO=dest.readString();
        BP_MAGR_NO=dest.readString();
        BP_CUST_NM=dest.readString();
        BP_MAGR_NM=dest.readString();
        CLPH_NO=dest.readString();
        BP_MEMO=dest.readString();
    }

    public static final Creator<Customer> CREATOR = new Creator<Customer>() {
        @Override
        public Customer createFromParcel(Parcel in) {
            return new Customer(in);
        }

        @Override
        public Customer[] newArray(int size) {
            return new Customer[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(BP_CUST_NO);
        dest.writeString(BP_MAGR_NO);
        dest.writeString(BP_CUST_NM);
        dest.writeString(BP_MAGR_NM);
        dest.writeString(CLPH_NO);
        dest.writeString(BP_MEMO);
    }
}
