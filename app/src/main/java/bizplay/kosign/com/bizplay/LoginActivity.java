package bizplay.kosign.com.bizplay;

import android.content.Intent;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.webcash.sws.pref.MemoryPreferenceDelegator;
import com.webcash.sws.pref.PreferenceDelegator;

import bizplay.kosign.com.bizplay.cardview.Utils;
import bizplay.kosign.com.bizplay.common.ui.CustomEditTextPassword;
import bizplay.kosign.com.bizplay.constant.Constants;
import bizplay.kosign.com.bizplay.tran.ComTran;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_P001_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_P001_RES;

import static android.R.attr.editable;

public class LoginActivity extends AppCompatActivity implements ComTran.OnComTranListener {

    private EditText et_id, et_password;
    private ImageView img_del_id, img_del_password;
    private CheckBox chk_remember;
    private ComTran mComTran;
    private Button btn_forget_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mComTran = new ComTran(this, this);

        btn_forget_id = (Button) findViewById(R.id.btn_forget_id);
        img_del_id = (ImageView) findViewById(R.id.txt_del_id);
        img_del_password = (ImageView) findViewById(R.id.txt_del_password);
        chk_remember = (CheckBox) findViewById(R.id.chk_remember_me);

//        Auto login
        if(PreferenceDelegator.getInstance(this).get(Constants.LoginInfo.Auto_LOGIN).equals("true") &&
                PreferenceDelegator.getInstance(this).get(Constants.LoginInfo.USER_PWD).equals("") == false) {
                et_password.setText(PreferenceDelegator.getInstance(this).get(Constants.LoginInfo.USER_PWD));
                chk_remember.setChecked(true);
            callLogin();
        }




        et_id = (EditText) findViewById(R.id.et_id);
        et_id.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().equals("")) {
                    img_del_id.setVisibility(View.VISIBLE);
                } else {
                    img_del_id.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {            }
        });
        et_id.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus && !et_id.getText().toString().equals("")){
                    img_del_id.setVisibility(View.VISIBLE);
                }else {
                    img_del_id.setVisibility(View.GONE);
                }
            }
        });


        et_password = (EditText) findViewById(R.id.et_password);
        et_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().equals("")) {
                    img_del_password.setVisibility(View.VISIBLE);
                } else {
                    img_del_password.setVisibility(View.GONE);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {            }
        });
        et_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus && !et_password.getText().toString().equals("")){
                    img_del_password.setVisibility(View.VISIBLE);
                }else {
                    img_del_password.setVisibility(View.GONE);
                }
            }
        });

        et_password.addTextChangedListener(new CustomEditTextPassword(this, et_password, 0, CustomEditTextPassword.PASSWORD_NORMAL));

    }

    public void onDeleteID(View view) {       et_id.setText("");}

    public void onDeletePassword(View view) {
        et_password.setText("");
    }

    public void onLogin(View view) {
        callLogin();
    }

    private void callLogin() {
        try {
            TX_MYCD_MBL_P001_REQ req = new TX_MYCD_MBL_P001_REQ();
            req.setUserID(et_id.getText().toString());
            req.setUserPassword(et_password.getText().toString());

            mComTran.requestData(TX_MYCD_MBL_P001_REQ.TXNO, req.getSendMessage(), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(LoginActivity.this, TutorialActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onTranResponse(String tranCode, Object object) throws Exception {
        switch (tranCode) {
            case TX_MYCD_MBL_P001_REQ.TXNO:
                TX_MYCD_MBL_P001_RES res = new TX_MYCD_MBL_P001_RES(object);


                Log.d("Login Res: ", res.getMessageToString());

                PreferenceDelegator.getInstance(this).put(Constants.LoginInfo.USER_ID, res.getUSER_ID());

                MemoryPreferenceDelegator.getInstance().put(Constants.LoginInfo.BSNN_NM, res.getBSNN_NM());
                MemoryPreferenceDelegator.getInstance().put(Constants.LoginInfo.CRTC_PATH, res.getCRTC_PATH());
                MemoryPreferenceDelegator.getInstance().put(Constants.LoginInfo.USER_IMG_PATH, res.getUSER_IMG_PATH());
                MemoryPreferenceDelegator.getInstance().put(Constants.LoginInfo.USER_NM, res.getUSER_NM());

                if (chk_remember.isChecked()) {
                    PreferenceDelegator.getInstance(this).put(Constants.LoginInfo.USER_PWD, et_password.getText().toString());
                } else {
                    PreferenceDelegator.getInstance(this).put(Constants.LoginInfo.USER_PWD, "");
                }

                Constants.ReceiptInfo.WILL_RELOAD = true;
                Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
                LoginActivity.this.startActivity(mainIntent);
                LoginActivity.this.finish();
                break;
        }
    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }
}
