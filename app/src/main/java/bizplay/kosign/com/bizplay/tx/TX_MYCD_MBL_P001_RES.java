package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by User on 2016-12-28.
 */

public class TX_MYCD_MBL_P001_RES extends TxMessage {
    private TX_PRVCD_MBL_R001_RES_DATA mTxKeyData;

    public TX_MYCD_MBL_P001_RES(Object obj) throws Exception {
        this.mTxKeyData = new TX_PRVCD_MBL_R001_RES_DATA();
        super.initRecvMessage(obj);
    }

    public String getUSER_ID() throws Exception {
        return getString(mTxKeyData.USER_ID);
    }

    public String getUSER_NM() throws Exception {
        return getString(mTxKeyData.USER_NM);
    }

    public String getUSER_IMG_PATH()throws Exception {
        return getString(mTxKeyData.USER_IMG_PATH);
    }

    public String getBSNN_NM()throws Exception {
        return getString(mTxKeyData.BSNN_NM);
    }

    public String getCRTC_PATH()throws Exception {
        return getString(mTxKeyData.CRTC_PATH);
    }

    private class TX_PRVCD_MBL_R001_RES_DATA{
        private String USER_ID = "USER_ID";
        private String USER_NM = "USER_NM";
        private String USER_IMG_PATH = "USER_IMG_PATH";
        private String BSNN_NM = "BSNN_NM";
        private String CRTC_PATH = "CRTC_PATH";
    }
}
