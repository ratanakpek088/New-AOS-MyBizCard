package bizplay.kosign.com.bizplay.popup;

import android.app.Activity;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.PopupWindow;

public class PopupMenu extends PopupWindow implements OnClickListener{

	private Activity mActivity;
	protected View anchor;
//	private int layout;
//	private boolean mActivityFinish = false;

	@SuppressWarnings("deprecation")
	public PopupMenu(Activity activity, int layout) {

		mActivity = activity;
//		this.layout = layout;

		anchor = View.inflate(mActivity, layout, null);
		super.setContentView(anchor);
		super.setWidth(LayoutParams.WRAP_CONTENT);
		super.setHeight(LayoutParams.WRAP_CONTENT);
		super.setWindowLayoutMode(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		this.setOutsideTouchable(true);
		this.setBackgroundDrawable(new BitmapDrawable());
		this.setAnimationStyle(-1); // 애니메이션 설정(-1:설정, 0:설정안함)
		this.setTouchInterceptor(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
					dismiss();
					return true;
				}
				return false;
			}
		});
		showPopupWindow();
	}
	
//	public void setActivityFinish(boolean value) {
//		mActivityFinish = value;
//	}
	
	public void showPopupWindow() {
		
		Rect rect = locateView(anchor);		
		showAtLocation(anchor, Gravity.TOP|Gravity.RIGHT, rect.left, rect.bottom);
		
		this.showAsDropDown(anchor);
		
	}
	
	protected  Rect locateView(View v) {
	    int[] loc_int = new int[2];
	    if (v == null) return null;
	    try
	    {
	        v.getLocationOnScreen(loc_int);
	    } catch (NullPointerException npe)
	    {
	        //Happens when the view doesn't exist on screen anymore.
	        return null;
	    }
	    Rect location = new Rect();
	    location.left = loc_int[0];
	    location.top = loc_int[1];
	    location.right = location.left + v.getWidth();
	    location.bottom = location.top + v.getHeight();
	    return location;
	}

	protected int getDP(int size) {
		size = (int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, size, mActivity.getResources()
						.getDisplayMetrics());
		return size;
	}

	@Override
	public void onClick(View v) {

	}

}
