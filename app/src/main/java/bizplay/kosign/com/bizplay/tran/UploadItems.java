package bizplay.kosign.com.bizplay.tran;

import android.graphics.Bitmap;

/*
 * Created by ChanYouvita on 7/5/16.
 */
public class UploadItems {

    private String PTL_ID       = "PTL_ID";
    private String CHNL_ID      = "CHNL_ID";
    private String USE_INTT_ID  = "USE_INTT_ID";
    private String USER_ID      = "USER_ID";
    private String FILE_NM      = "FILE_NM";
    private Bitmap FILE;


    public String getPTL_ID() {
        return PTL_ID;
    }

    public void setPTL_ID(String PTL_ID) {
        this.PTL_ID = PTL_ID;
    }

    public String getCHNL_ID() {
        return CHNL_ID;
    }

    public void setCHNL_ID(String CHNL_ID) {
        this.CHNL_ID = CHNL_ID;
    }

    public String getUSE_INTT_ID() {
        return USE_INTT_ID;
    }

    public void setUSE_INTT_ID(String USE_INTT_ID) {
        this.USE_INTT_ID = USE_INTT_ID;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public Bitmap getFILE() {
        return FILE;
    }

    public void setFILE(Bitmap FILE) {
        this.FILE = FILE;
    }

    public String getFILE_NM() {
        return FILE_NM;
    }

    public void setFILE_NM(String FILE_NM) {
        this.FILE_NM = FILE_NM;
    }
}
