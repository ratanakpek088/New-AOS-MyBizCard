package bizplay.kosign.com.bizplay;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;
import com.webcash.sws.log.DevLog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import bizplay.kosign.com.bizplay.adapters.NoticeAdapter;
import bizplay.kosign.com.bizplay.common.ui.util.FormatUtil;
import bizplay.kosign.com.bizplay.extras.NotifExatra;
import bizplay.kosign.com.bizplay.tran.ComTran;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_R004_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_R004_RES;

public class A008_2Activity extends AppCompatActivity implements ComTran.OnComTranListener {

    private ComTran mComTran;
    ImageView iv_noti_org_img, iv_notice_detail;
    TextView tv_item_title_nm, tv_date, tv_noti_msg;
    NotifExatra mNotifExtra;
    private String KEY_SEND_DTM, KEY_CD_USER_NO, KEY_NOTI_SEQ;
    private String INQ_DTM, NOTI_MSG, TARGET_PAGE_URL, TITLE_NM, NOTI_ORG_NM, IMG_URL,NOTI_ORG_IMG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a008_2);
        mComTran = new ComTran(this, this);




        iv_noti_org_img = (ImageView) findViewById(R.id.iv_noti_org_img);
        tv_item_title_nm = (TextView) findViewById(R.id.tv_item_title_nm);
        tv_date = (TextView) findViewById(R.id.tv_date);
        iv_notice_detail = (ImageView) findViewById(R.id.iv_notice_detail);
        tv_noti_msg = (TextView) findViewById(R.id.tv_noti_msg);
        Bundle bundle= getIntent().getExtras();

       // mNotifExtra = new NotifExatra(this, getIntent());
        if(bundle!=null){
            KEY_SEND_DTM=bundle.getString("KEY_SEND_DTM");
            KEY_NOTI_SEQ=bundle.getString("KEY_NOTI_SEQ");
            KEY_CD_USER_NO=bundle.getString("KEY_CD_USER_NO");
            Log.e("rtkddd", KEY_CD_USER_NO+KEY_SEND_DTM+ KEY_NOTI_SEQ+"");
        }

        onTranRequest(TX_MYCD_MBL_R004_REQ.TXNO);
//        Log.e("EXTRAS", mNotifExtra.toString());



    }

    public void onTranRequest(String tranCD) {
        try {
            if (tranCD.equals(TX_MYCD_MBL_R004_REQ.TXNO)) {
                TX_MYCD_MBL_R004_REQ req = new TX_MYCD_MBL_R004_REQ();
                req.setSEND_DTM(KEY_SEND_DTM);
                req.setNOTI_SEQ(KEY_NOTI_SEQ);
                mComTran.requestData(tranCD, req.getSendMessage(), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTranResponse(String tranCode, Object object) throws Exception {
        try {
            if (tranCode.equals(TX_MYCD_MBL_R004_REQ.TXNO)) {
                TX_MYCD_MBL_R004_RES res = new TX_MYCD_MBL_R004_RES(object);


                DevLog.devLog("Object : "+object);
                JSONArray array = (JSONArray) object;
                DevLog.devLog("Object array: "+array);
                JSONObject jn = array.getJSONObject(0);

                DevLog.devLog("Object json OBject : "+jn);
                // private String INQ_DTM, NOTI_MSG, TARGET_PAGE_URL, TITLE_NM, NOTI_ORG_NM, IMG_URL,NOTI_ORG_IMG;
                //check json object is null
                if(jn.isNull("INQ_DTM")) INQ_DTM="";
                else INQ_DTM=jn.getString("INQ_DTM");

                if(jn.isNull("NOTI_MSG")) NOTI_MSG="";
                else NOTI_MSG=jn.getString("NOTI_MSG");

                if(jn.isNull("TARGET_PAGE_URL")) TARGET_PAGE_URL="";
                else TARGET_PAGE_URL=jn.getString("TARGET_PAGE_URL");

                if(jn.isNull("TITLE_NM")) TITLE_NM="";
                else TITLE_NM=jn.getString("TITLE_NM");

                if(jn.isNull("NOTI_ORG_NM")) NOTI_ORG_NM="";
                else NOTI_ORG_NM=jn.getString("NOTI_ORG_NM");

                if(jn.isNull("IMG_URL")) IMG_URL="";
                else IMG_URL=jn.getString("IMG_URL");

                if(jn.isNull("NOTI_ORG_IMG")) NOTI_ORG_IMG="";
                else NOTI_ORG_IMG=jn.getString("NOTI_ORG_IMG");


                tv_item_title_nm.setText(TITLE_NM);
                tv_noti_msg.setText(NOTI_MSG);
                 /*set date*/
                if (!INQ_DTM.equals("")) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
                    Date mDate = null;
                    try {
                        mDate = sdf.parse(INQ_DTM);
                        tv_date.setText(FormatUtil.getTimeString(mDate));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else tv_date.setText("");

                /*set image iv_noti_org_img*/
                if (!NOTI_ORG_IMG.equals(""))
                  //  ImageLoader.getInstance().displayImage(res.getNOTI_ORG_IMG(), iv_noti_org_img, optionOrg, animateFirstListener);
                    Glide.with(this)
                            .load(res.getNOTI_ORG_IMG())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .centerCrop()
                            .crossFade()
                            .into(iv_noti_org_img);

                else iv_noti_org_img.setImageDrawable(null);


        /*set image detial*/
                if (!IMG_URL.equals("")) {

                    Glide.with(this)
                            .load(res.getIMG_URL())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .centerCrop()
                            .crossFade()
                            .into(iv_notice_detail);
                } else{
                    iv_notice_detail.setImageDrawable(null);
                    findViewById(R.id.ll_image_below).setVisibility(View.GONE);
                }
                Log.e("EXTRASRESPONSE", object.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }
}
