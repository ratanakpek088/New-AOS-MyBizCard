package bizplay.kosign.com.bizplay.item;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;


public class Photo_Item implements Parcelable {


    private String imageUrl = "";
    private String thumbnailImageUrl = "";
    private String fileName = "";
    private String attach_srno = "";
    private String fileUrl = "";
    private int size;
    private Bitmap bitmap;
    private boolean isNewImage = false;
    private boolean isFile = false;

    public Photo_Item() {

    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getThumbnailImageUrl() {
        return thumbnailImageUrl;
    }

    public void setThumbnailImageUrl(String thumbnailImageUrl) {
        this.thumbnailImageUrl = thumbnailImageUrl;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getAttach_srno() {
        return attach_srno;
    }

    public void setAttach_srno(String attach_srno) {
        this.attach_srno = attach_srno;
    }

    public boolean isNewImage() {
        return isNewImage;
    }

    public void setNewImage(boolean newImage) {
        isNewImage = newImage;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isFile() {
        return isFile;
    }

    public void setFile(boolean file) {
        isFile = file;
    }

    /**
     * =============================================================================================
     * Parcel Functions
     * @param in
     * =============================================================================================
     */

    public Photo_Item(Parcel in) {
        readFromParcel(in);
    }

    private void readFromParcel(Parcel in) {
        imageUrl = in.readString();
        thumbnailImageUrl = in.readString();
        fileName = in.readString();
        attach_srno = in.readString();
        fileUrl = in.readString();
        size = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(imageUrl);
        dest.writeString(thumbnailImageUrl);
        dest.writeString(fileName);
        dest.writeString(attach_srno);
        dest.writeString(fileUrl);
        dest.writeInt(size);
    }

    public static final Creator CREATOR =
            new Creator() {
                public Photo_Item createFromParcel(Parcel in) {
                    return new Photo_Item(in);
                }

                public Photo_Item[] newArray(int size) {
                    return new Photo_Item[size];
                }
            };
}
