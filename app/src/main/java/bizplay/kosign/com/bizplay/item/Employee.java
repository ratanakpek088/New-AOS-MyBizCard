package bizplay.kosign.com.bizplay.item;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by User on 14-Feb-17.
 */

public class Employee implements Parcelable {
    int ID;
    String BP_EMPL_NO;
    String BP_EMPL_NM;
    String CLPH_NO;
    String BP_DVSN_NM;
    boolean IS_SELECTED;

    public Employee(){}

    private Employee(Parcel in){
        readFromParcel(in);
    }

    private void readFromParcel(Parcel in){
        // the order need to be same as write to parcel
        ID = in.readInt();
        BP_EMPL_NO = in.readString();
        BP_EMPL_NM = in.readString();
        CLPH_NO = in.readString();
        BP_DVSN_NM = in.readString();

    }
    public String getBP_DVSN_NM() {
        return BP_DVSN_NM;
    }

    public void setBP_DVSN_NM(String BP_DVSN_NM) {
        this.BP_DVSN_NM = BP_DVSN_NM;
    }

    public String getBP_EMPL_NM() {
        return BP_EMPL_NM;
    }

    public void setBP_EMPL_NM(String BP_EMPL_NM) {
        this.BP_EMPL_NM = BP_EMPL_NM;
    }

    public String getBP_EMPL_NO() {
        return BP_EMPL_NO;
    }

    public void setBP_EMPL_NO(String BP_EMPL_NO) {
        this.BP_EMPL_NO = BP_EMPL_NO;
    }

    public String getCLPH_NO() {
        return CLPH_NO;
    }

    public void setCLPH_NO(String CLPH_NO) {
        this.CLPH_NO = CLPH_NO;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public boolean getIS_SELECTED() {
        return IS_SELECTED;
    }

    public void setIS_SELECTED(boolean IS_SELECTED) {
        this.IS_SELECTED = IS_SELECTED;
    }

    public String toString(){
        return getBP_EMPL_NO() + ", " + getBP_EMPL_NM() + ", " + getCLPH_NO() + ", " + getBP_DVSN_NM() + getIS_SELECTED() + "\n";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ID);
        dest.writeString(BP_EMPL_NO);
        dest.writeString(BP_EMPL_NM);
        dest.writeString(CLPH_NO);
        dest.writeString(BP_DVSN_NM);
    }
    public static final Creator CREATOR = new Creator() {
        @Override
        public Employee createFromParcel(Parcel source) {
            return new Employee(source);
        }

        @Override
        public Employee[] newArray(int size) {
            return new Employee[size];
        }
    };
}
