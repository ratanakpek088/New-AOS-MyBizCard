package bizplay.kosign.com.bizplay.tabs;


import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.webcash.sws.log.DevLog;
import com.webcash.sws.pref.MemoryPreferenceDelegator;
import com.webcash.sws.pref.PreferenceDelegator;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import bizplay.kosign.com.bizplay.ProfileUserActivity;
import bizplay.kosign.com.bizplay.R;
import bizplay.kosign.com.bizplay.SettingActivity;
import bizplay.kosign.com.bizplay.adapters.A007_2_4P_Adapter;
import bizplay.kosign.com.bizplay.adapters.GridAdapter;
import bizplay.kosign.com.bizplay.conf.Conf;
import bizplay.kosign.com.bizplay.item.AppInfo_Item;
import bizplay.kosign.com.bizplay.tran.ComTran;
import bizplay.kosign.com.bizplay.tx.TX_IBK_MG_RES_REC1;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_R006_REQ;
import bizplay.kosign.com.bizplay.tx.TX_MYCD_MBL_R006_RES;

import static bizplay.kosign.com.bizplay.R.id.card_more;
import static bizplay.kosign.com.bizplay.R.id.contact_more;
import static bizplay.kosign.com.bizplay.R.id.ll_card_more;
import static bizplay.kosign.com.bizplay.R.id.ll_contact_more;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoreFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener, ComTran.OnComTranListener{

    private static MoreFragment instance = null;
    private GridView gridView;
    private GridAdapter adapter;
    private ArrayList<AppInfo_Item> arrayList;
    private int[] imageId;
    private boolean mPlaystore = true;
    private boolean app_available = false;
    private ComTran mComTran;

    public MoreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_more_two, container, false);
        gridView = (GridView)view.findViewById(R.id.grid_setting);

        mComTran = new ComTran(getActivity(), this);

        recommendedApp();

        LinearLayout profile = (LinearLayout) view.findViewById(R.id.ll_profile_more);
        profile.setOnClickListener(this);

        LinearLayout card = (LinearLayout) view.findViewById(ll_card_more);
        card.setOnClickListener(this);

        LinearLayout contact = (LinearLayout) view.findViewById(ll_contact_more);
        contact.setOnClickListener(this);

        LinearLayout setting = (LinearLayout) view.findViewById(R.id.ll_setting_more);
        setting.setOnClickListener(this);

        return view;
    }

    public static MoreFragment getInstance() {
        try {
            if (instance == null) {
                // new instance
                instance = new MoreFragment();

                // sets data to bundle
                Bundle bundle = new Bundle();

                // set data to fragment
                instance.setArguments(bundle);

                return instance;
            } else {

                return instance;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void intializeData(){
        try {
            imageId = new int[]{R.drawable.ai_payment_icon};
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void recommendedApp(){
        try {
            intializeData();
            TX_IBK_MG_RES_REC1 app_info = new TX_IBK_MG_RES_REC1(new JSONArray(MemoryPreferenceDelegator.getInstance().get("MENU_INFO")));
            arrayList = new ArrayList<>();
            app_info.moveFirst();
            for (int i=0;i<app_info.getLength();i++){
                AppInfo_Item app = new AppInfo_Item();
                app.setApp_id(app_info.getMenuID());
                app.setApp_name(app_info.getMenuName());
                app.setC_available(String.valueOf(app_info.isMenuAvailable()));
                app.setC_reason(app_info.getMenuAvailableReson());
                arrayList.add(app);
                app_info.moveNext();
            }
            adapter = new GridAdapter(getContext(), arrayList, imageId);
            gridView.setAdapter(adapter);

            gridView.setOnItemClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ll_profile_more:
                Intent intent = new Intent(getActivity(), ProfileUserActivity.class);
                startActivity(intent);
                break;

            case ll_contact_more:
                if (!MemoryPreferenceDelegator.getInstance().get("QNA_URL").equals("")) {
                    intent = new Intent(Intent.ACTION_VIEW);
//                        intent.setData(Uri.parse("https://docs.google.com/forms/d/1T10Ev9lzXaWVC7DXcYIgXww58CRwKmZOpN-OO6RhZcU/viewform"));
                    intent.setData(Uri.parse(MemoryPreferenceDelegator.getInstance().get("QNA_URL")));
                    startActivity(intent);
                }
                break;

            case R.id.ll_setting_more:
                startActivity(new Intent(getActivity(), SettingActivity.class));
                break;
        }
    }



    @Override
    public void onTranResponse(String tranCode, Object object) throws Exception {
        try{
            if(tranCode.equals(TX_MYCD_MBL_R006_REQ.TXNO)) {
                DevLog.devLog("msg", "object >>> " + object.toString());
                TX_MYCD_MBL_R006_RES resData = new TX_MYCD_MBL_R006_RES(object);
                PreferenceDelegator.getInstance(getContext()).put("NOTI_STS", resData.getNOTI_STS()); // Real-time alerts if authorization
                PreferenceDelegator.getInstance(getContext()).put("LIMIT_STS", resData.getLIMIT_STS()); //Limit set whether notifications
                PreferenceDelegator.getInstance(getContext()).put("LIMIT_STRT_AMPM", resData.getLIMIT_STRT_AMPM()); //Start time nine minutes AM PM
                PreferenceDelegator.getInstance(getContext()).put("LIMIT_STRT_TM", resData.getLIMIT_STRT_TM()); //Notifications limited in time starts at
                PreferenceDelegator.getInstance(getContext()).put("LIMIT_END_GB", resData.getLIMIT_END_GB());// Notification limit exit nine minutes
                PreferenceDelegator.getInstance(getContext()).put("LIMIT_END_AMPM", resData.getLIMIT_END_AMPM()); // End Time nine minutes AM PM
                PreferenceDelegator.getInstance(getContext()).put("LIMIT_END_TM", resData.getLIMIT_END_TM()); //End Time limit notification
                startActivity(new Intent(getActivity(),SettingActivity.class));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        try {
            //check available app on play store
            if (!"".equals(arrayList.get(position).getApp_id())){
                app_available = true;
            }else {
                app_available = false;
            }

            // check available App in device
            PackageManager pm = getContext().getPackageManager();
            final List<ApplicationInfo> list = pm.getInstalledApplications(0);
            if (app_available) {
                for (int i = 0; i < list.size(); i++) {
                    if (arrayList.get(position).getApp_id().equals(list.get(i).packageName)) {
                        mPlaystore = false;
                        Intent LaunchIntent = getActivity().getPackageManager().getLaunchIntentForPackage(list.get(i).packageName);
                        startActivity(LaunchIntent);
                    }
                }
                if (mPlaystore) {
                    Uri uri = Uri.parse(Conf.PLAY_STORE_URL+ arrayList.get(position).getApp_id());
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
                mPlaystore = true;
                app_available = false;
            } else {
                // App not available
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
