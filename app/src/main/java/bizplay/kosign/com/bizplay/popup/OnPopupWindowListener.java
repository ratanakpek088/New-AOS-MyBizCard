package bizplay.kosign.com.bizplay.popup;

/*
 * Created by User on 6/6/2016.
 */
public interface OnPopupWindowListener {
    void onPopupPressed();
    void onPopupClose();
}
