package bizplay.kosign.com.bizplay.popup;

import android.app.Activity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import bizplay.kosign.com.bizplay.R;


/*
 * Created by User on 6/7/2016.
 */
public class CustomPopupMenu1 extends PopupMenu {
    private OnPopupMenuListener listener = null;
    private FrameLayout item_1, item_2;
    private TextView tv_item_1, tv_item_2;

    public CustomPopupMenu1(Activity activity, int layout) {
        super(activity, layout);
        item_1 = (FrameLayout) anchor.findViewById(R.id.item_1);
        item_2 = (FrameLayout) anchor.findViewById(R.id.item_2);
        item_1.setOnClickListener(this);
        item_2.setOnClickListener(this);

        tv_item_1 = (TextView) anchor.findViewById(R.id.tv_item_1);
        tv_item_2 = (TextView) anchor.findViewById(R.id.tv_item_2);
    }

    public void changeItemText(String item1, String item2){
        tv_item_1.setText(item1);
        tv_item_2.setText(item2);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.item_1:
                listener.onItem1Press();
                break;
            case R.id.item_2:
                listener.onItem2Press();
                break;
        }
    }

    public interface OnPopupMenuListener{
        void onItem1Press();
        void onItem2Press();
    }

    public void setOnPopuMenuListener(OnPopupMenuListener listener) {
        this.listener = listener;
    }

}
