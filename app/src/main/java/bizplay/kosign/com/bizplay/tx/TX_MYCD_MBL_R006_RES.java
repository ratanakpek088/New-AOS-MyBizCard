package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by user on 1/13/2017.
 */

public class TX_MYCD_MBL_R006_RES extends TxMessage {

    private TX_MYCD_MBL_R006_RES_DATA mTxKeyData;

    public TX_MYCD_MBL_R006_RES(Object object) throws Exception {
        mTxKeyData = new TX_MYCD_MBL_R006_RES_DATA();
        super.initRecvMessage(object);
    }



    private class TX_MYCD_MBL_R006_RES_DATA{
        private String NOTI_STS = "NOTI_STS";
        private String LIMIT_STS = "LIMIT_STS";
        private String LIMIT_STRT_AMPM = "LIMIT_STRT_AMPM";
        private String LIMIT_STRT_TM = "LIMIT_STRT_TM";
        private String LIMIT_END_GB = "LIMIT_END_GB";
        private String LIMIT_END_AMPM = "LIMIT_END_AMPM";
        private String LIMIT_END_TM = "LIMIT_END_TM";
    }
    public void setNOTI_STS(String value) throws JSONException{  setString(mTxKeyData.NOTI_STS, value);}
    public void setLIMIT_STS(String value) throws JSONException{  setString(mTxKeyData.LIMIT_STS, value);}
    public void setLIMIT_STRT_AMPM(String value) throws JSONException{  setString(mTxKeyData.LIMIT_STRT_AMPM, value);}
    public void setLIMIT_STRT_TM(String value) throws JSONException{  setString(mTxKeyData.LIMIT_STRT_TM, value);}
    public void setLIMIT_END_GB(String value) throws JSONException{  setString(mTxKeyData.LIMIT_END_GB, value);}
    public void setLIMIT_END_AMPM(String value) throws JSONException{  setString(mTxKeyData.LIMIT_END_AMPM, value);}
    public void setLIMIT_END_TM(String value) throws JSONException{  setString(mTxKeyData.LIMIT_END_TM, value);}

    public String getNOTI_STS() throws JSONException {
        return getString(mTxKeyData.NOTI_STS);
    }
    public String getLIMIT_STS() throws JSONException {
        return getString(mTxKeyData.LIMIT_STS);
    }
    public String getLIMIT_STRT_AMPM() throws JSONException {
        return getString(mTxKeyData.LIMIT_STRT_AMPM);
    }
    public String getLIMIT_STRT_TM() throws JSONException {
        return getString(mTxKeyData.LIMIT_STRT_TM);
    }
    public String getLIMIT_END_GB() throws JSONException {
        return getString(mTxKeyData.LIMIT_END_GB);
    }
    public String getLIMIT_END_AMPM() throws JSONException {
        return getString(mTxKeyData.LIMIT_END_AMPM);
    }
    public String getLIMIT_END_TM() throws JSONException {
        return getString(mTxKeyData.LIMIT_END_TM);
    }

}
