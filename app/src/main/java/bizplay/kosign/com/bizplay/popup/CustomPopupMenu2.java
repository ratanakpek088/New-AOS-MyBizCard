package bizplay.kosign.com.bizplay.popup;

import android.app.Activity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import bizplay.kosign.com.bizplay.R;

/*
 * Created by User on 6/7/2016.
 */
public class CustomPopupMenu2 extends PopupMenu {
    private OnPopupMenuListener listener = null;
    private FrameLayout fl_popup;
    private TextView item_1;

    public CustomPopupMenu2(Activity activity, int layout) {
        super(activity, layout);
        fl_popup = (FrameLayout) anchor.findViewById(R.id.fl_popup);
        item_1 = (TextView) anchor.findViewById(R.id.item_1);
        fl_popup.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.fl_popup:
                listener.onItem1Press();
                break;
        }
    }

    public interface OnPopupMenuListener{
        void onItem1Press();
    }

    public void setItemText(String text){
        item_1.setText(text);
    }

    public void setOnPopuMenuListener(OnPopupMenuListener listener) {
        this.listener = listener;
    }

}
