package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by RatanakPek on 1/10/2017.
 */

public class TX_MYCD_MBL_R003_RES_REC_RCPT_RFS extends TxMessage {


    private TX_MYCD_MBL_R003_RES_REC_RCPT_RFS_DATA mTxKeyData;

    public TX_MYCD_MBL_R003_RES_REC_RCPT_RFS(Object obj) throws Exception {
        this.mTxKeyData = new TX_MYCD_MBL_R003_RES_REC_RCPT_RFS_DATA();
        super.initRecvMessage(obj);
    }

    public String getRCPT_RFS_HPNO() throws Exception {
        return getString(mTxKeyData.RCPT_RFS_HPNO);
    }

    public String getRCPT_RFS_NM() throws Exception {
        return getString(mTxKeyData.RCPT_RFS_NM);
    }

    public String getRCPT_RFS_USER_NO() throws Exception {
        return getString(mTxKeyData.RCPT_RFS_USER_NO);
    }

    private class TX_MYCD_MBL_R003_RES_REC_RCPT_RFS_DATA {
        private String RCPT_RFS_HPNO = "RCPT_RFS_HPNO";
        private String RCPT_RFS_NM = "RCPT_RFS_NM";
        private String RCPT_RFS_USER_NO = "RCPT_RFS_USER_NO";
    }
}
