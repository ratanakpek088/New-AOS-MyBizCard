package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 *  Created by Huo Chhunleng on 27/Feb/2017.
 */
public class TX_MYCD_MBL_L006_RES_MGMT5_REC extends TxMessage {

    private TX_MYCD_MBL_L006_RES_MGMT5_REC_DATA mTxKeyData;		// 전문명세 필드

    public TX_MYCD_MBL_L006_RES_MGMT5_REC(Object obj) throws Exception{
        mTxKeyData = new TX_MYCD_MBL_L006_RES_MGMT5_REC_DATA();
        super.initRecvMessage(obj);

    }

    /**
     * 전문 Data 필드 설정
    */
    private class TX_MYCD_MBL_L006_RES_MGMT5_REC_DATA {
        private String MGMT5_LIST_NM	=	"MGMT5_LIST_NM";		//항목3 목록명칭
    }

    /**
     * 용도코드
     * @throws Exception
     */
    public String getMGMT5_LIST_NM() throws JSONException,Exception{
        return getString(mTxKeyData.MGMT5_LIST_NM);
    }

}
