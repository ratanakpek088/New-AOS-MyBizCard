package bizplay.kosign.com.bizplay.extras;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


public class Extra_Receipt extends Extras {

    public Extra_Receipt(Context ctx) {
        super(ctx);
    }

    public Extra_Receipt(Activity act, Intent intent) {
        super(act, intent);
    }

    public Extra_Receipt(Activity act, Bundle bundle) {
        super(act, bundle);
    }

    private final String RCPT_TX_STS 	= "RCPT_TX_STS";
    private final String CDN 			= "CDN";
    private final String ATHZ_YMD 	= "ATHZ_YMD";
    private final String ATHZ_HMS 	= "ATHZ_HMS";
    private final String APN 			= "APN";
    private final String AFST_BZN 	= "AFST_BZN";
    private final String AFST_NO 		= "AFST_NO";
    private final String ATHZ_AMT 	= "ATHZ_AMT";
    private final String CNCL_YMD 	= "CNCL_YMD";
    private final String CNCL_AMT 	= "CNCL_AMT";
    private final String RCPT_SEQNO 	= "RCPT_SEQNO";
    private final String RCPT_RFS_STS = "RCPT_RFS_STS"; //N:정상(or 미대상),  Y:승인거부(deny)
    private final String AFST_NM 	= "AFST_NM";
    private final String CARD_BYNM 	= "CARD_BYNM";

    //for Notice Adapter only
    private final String KEY_CD_USER_NO="KEY_CD_USER_NO";
    private final String KEY_SEND_DTM="KEY_SEND_DTM";
    private final String KEY_NOTI_SEQ="KEY_NOTI_SEQ";
    private final String API_KEY="API_KEY";

    public _Param Param = new _Param();

    public class _Param {
        public void setAPI_KEY(String value) {
            setString(API_KEY, value);
        }

        public String getAPI_KEY() {
            return getString(API_KEY);
        }
        public void setKEY_NOTI_SEQ(String value) {
            setString(KEY_NOTI_SEQ, value);
        }

        public String getKEY_NOTI_SEQ() {
            return getString(KEY_NOTI_SEQ);
        }

        public void setKEY_SEND_DTM(String value) {
            setString(KEY_SEND_DTM, value);
        }

        public String getKEY_SEND_DTM() {
            return getString(KEY_SEND_DTM);
        }

        public void setKEY_CD_USER_NO(String value) {
            setString(KEY_CD_USER_NO, value);
        }

        public String getKEY_CD_USER_NO() {
            return getString(KEY_CD_USER_NO);
        }

        public void setRCPT_TX_STS(String value) {
            setString(RCPT_TX_STS, value);
        }

        public String getRCPT_TX_STS() {
            return getString(RCPT_TX_STS);
        }

        public void setCDN(String value) {
            setString(CDN, value);
        }

        public String getCDN() {
            return getString(CDN);
        }

        public void setATHZ_YMD(String value) {
            setString(ATHZ_YMD, value);
        }

        public String getATHZ_YMD() {
            return getString(ATHZ_YMD);
        }

        public void setATHZ_HMS(String value) {
            setString(ATHZ_HMS, value);
        }

        public String getATHZ_HMS() {
            return getString(ATHZ_HMS);
        }

        public void setAPN(String value) {
            setString(APN, value);
        }

        public String getAPN() {
            return getString(APN);
        }

        public void setAFST_BZN(String value) {
            setString(AFST_BZN, value);
        }

        public String getAFST_BZN() {
            return getString(AFST_BZN);
        }

        public void setAFST_NO(String value) {
            setString(AFST_NO, value);
        }

        public String getAFST_NO() {
            return getString(AFST_NO);
        }

        public void setATHZ_AMT(String value) {
            setString(ATHZ_AMT, value);
        }

        public String getATHZ_AMT() {
            return getString(ATHZ_AMT);
        }

        public void setCNCL_YMD(String value) {
            setString(CNCL_YMD, value);
        }

        public String getCNCL_YMD() {
            return getString(CNCL_YMD);
        }

        public void setCNCL_AMT(String value) {
            setString(CNCL_AMT, value);
        }

        public String getCNCL_AMT() {
            return getString(CNCL_AMT);
        }

        public void setRCPT_SEQNO(String value) {
            setString(RCPT_SEQNO, value);
        }

        public String getRCPT_SEQNO() {
            return getString(RCPT_SEQNO);
        }

        public void setRCPT_RFS_STS(String value) {
            setString(RCPT_RFS_STS, value);
        }

        public String getRCPT_RFS_STS() {
            return getString(RCPT_RFS_STS);
        }

        public void setAFST_NM(String value) {
            setString(AFST_NM, value);
        }

        public String getAFST_NM() {
            return getString(AFST_NM);
        }

        public void setCARD_BYNM(String value) {
            setString(CARD_BYNM, value);
        }

        public String getCARD_BYNM() {
            return getString(CARD_BYNM);
        }

    }

}
