package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by Daravuth on 1/13/2017.
 */
public class TX_MYCD_MBL_L003_RES_REC  extends TxMessage{
    private TX_MYCD_MBL_L003_RES_REC_DATA mTxKeyData; // 전문명세 필드

    public TX_MYCD_MBL_L003_RES_REC(Object obj) throws Exception {
        mTxKeyData = new TX_MYCD_MBL_L003_RES_REC_DATA();
        super.initRecvMessage(obj);
    }
    public String getTRAN_KIND_CD() throws JSONException,Exception {
        return getString(mTxKeyData.TRAN_KIND_CD);
    }
    public String getTRAN_KIND_NM() throws JSONException,Exception {
        return getString(mTxKeyData.TRAN_KIND_NM);
    }
    private class TX_MYCD_MBL_L003_RES_REC_DATA{
        private String TRAN_KIND_CD="TRAN_KIND_CD";
        private String TRAN_KIND_NM="TRAN_KIND_NM";
    }
}
