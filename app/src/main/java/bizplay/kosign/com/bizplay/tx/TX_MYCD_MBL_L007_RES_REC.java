package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by User on 14-Feb-17.
 */

public class TX_MYCD_MBL_L007_RES_REC extends TxMessage {
    private TX_MYCD_MBL_L007_RES_REC_DATA mTxKeyData;

    public TX_MYCD_MBL_L007_RES_REC(Object object) throws Exception {
        mTxKeyData = new TX_MYCD_MBL_L007_RES_REC_DATA();
        super.initRecvMessage(object);
    }

    public String getBP_EMPL_NO() throws JSONException{
        return getString(mTxKeyData.BP_EMPL_NO);
    }
    public String getBP_EMPL_NM() throws JSONException{
        return getString(mTxKeyData.BP_EMPL_NM);
    }
    public String getCLPH_NO() throws JSONException{
        return getString(mTxKeyData.CLPH_NO);
    }
    public String getBP_DVSN_NM() throws JSONException{
        return getString(mTxKeyData.BP_DVSN_NM);
    }
    private class TX_MYCD_MBL_L007_RES_REC_DATA{
        private String BP_EMPL_NO = "BP_EMPL_NO";
        private String BP_EMPL_NM = "BP_EMPL_NM";
        private String CLPH_NO = "CLPH_NO";
        private String BP_DVSN_NM = "BP_DVSN_NM";
    }
}
