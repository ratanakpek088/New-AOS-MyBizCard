package bizplay.kosign.com.bizplay.item;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Voneat-pc on 03/06/2016.
 */
public class USER_REC_ITEM implements Parcelable{


    String RCPT_USER_HPNO;
    String RCPT_USER_NM;
    String RCPT_USER_NO;
    String RCPT_USER_BSNN_NM;
    String RCPT_USER_PSN_NM;
    String RCPT_USER_PSN_IMG;



    public USER_REC_ITEM() {
    }


    public String getRCPT_USER_HPNO() {
        return RCPT_USER_HPNO;
    }

    public void setRCPT_USER_HPNO(String RCPT_USER_HPNO) {
        this.RCPT_USER_HPNO = RCPT_USER_HPNO;
    }

    public String getRCPT_USER_NM() {
        return RCPT_USER_NM;
    }

    public void setRCPT_USER_NM(String RCPT_USER_NM) {
        this.RCPT_USER_NM = RCPT_USER_NM;
    }

    public String getRCPT_USER_NO() {
        return RCPT_USER_NO;
    }

    public void setRCPT_USER_NO(String RCPT_USER_NO) {
        this.RCPT_USER_NO = RCPT_USER_NO;
    }

    public String getRCPT_USER_BSNN_NM() {
        return RCPT_USER_BSNN_NM;
    }

    public void setRCPT_USER_BSNN_NM(String RCPT_USER_BSNN_NM) {
        this.RCPT_USER_BSNN_NM = RCPT_USER_BSNN_NM;
    }

    public String getRCPT_USER_PSN_NM() {
        return RCPT_USER_PSN_NM;
    }

    public void setRCPT_USER_PSN_NM(String RCPT_USER_PSN_NM) {
        this.RCPT_USER_PSN_NM = RCPT_USER_PSN_NM;
    }

    public String getRCPT_USER_PSN_IMG() {
        return RCPT_USER_PSN_IMG;
    }

    public void setRCPT_USER_PSN_IMG(String RCPT_USER_PSN_IMG) {
        this.RCPT_USER_PSN_IMG = RCPT_USER_PSN_IMG;
    }

    /**
     * =============================================================================================
     * Parcel Functions
     * @param in
     * =============================================================================================
     */

    public USER_REC_ITEM(Parcel in) {
        readFromParcel(in);
    }
    private void readFromParcel(Parcel in) {

        // We just need to read back each
        // field in the order that it was
        // written to the parcel
        RCPT_USER_NO = in.readString();
        RCPT_USER_BSNN_NM = in.readString();
        RCPT_USER_HPNO = in.readString();
        RCPT_USER_NM = in.readString();
        RCPT_USER_PSN_IMG = in.readString();
        RCPT_USER_PSN_NM = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(RCPT_USER_NO);
        dest.writeString(RCPT_USER_BSNN_NM);
        dest.writeString(RCPT_USER_HPNO);
        dest.writeString(RCPT_USER_NM);
        dest.writeString(RCPT_USER_PSN_IMG);
        dest.writeString(RCPT_USER_PSN_NM);
    }

    public static final Creator CREATOR =
            new Creator() {
                public USER_REC_ITEM createFromParcel(Parcel in) {
                    return new USER_REC_ITEM(in);
                }

                public USER_REC_ITEM[] newArray(int size) {
                    return new USER_REC_ITEM[size];
                }
            };
}
