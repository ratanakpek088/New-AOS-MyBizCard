package bizplay.kosign.com.bizplay.item;

import android.text.TextUtils;

/**
 * Created by bunna on 6/5/2016.
 */
public class Pagination {

    /**
     * 페이지 번호를 1부터 시작한다.
     */
    private final int START_PAGE_NO = 1;


    /*
     * 페이지 번호
     */
    private int mPageNo = START_PAGE_NO;

    /*
     * 더보기 여부
     */
    private boolean mIsMorePage = false;

    /*
     * 더보기 플래그 (전문 두번 호출 막기)
     */
    private boolean mIsMsgTrSending = false;

    private String mRowCount = null;

    public Pagination() {
        initialize();
    }

    public Pagination(String rowCount) {
        mRowCount = rowCount;
        initialize();
    }

    public void initialize() {
        mPageNo = START_PAGE_NO;
        mIsMorePage = false;
        mIsMsgTrSending = false;
    }

    public void setPageNo(int value) {
        mPageNo = value;
    }
    public void addPageNo() {
        mPageNo++;
    }
    public String getPageNo() {
        return String.valueOf(mPageNo);
    }
    public int getPageNoByInt() {
        return mPageNo;
    }
    public String getPageCnt() {
        if (TextUtils.isEmpty(mRowCount))
            return "100";
        else
            return mRowCount;
    }

    public void setMorePageYN (boolean value) {
        mIsMorePage = value;
    }
    public boolean getMorePageYN() {
        return mIsMorePage;
    }

    public void setTrSending(boolean value) {
        mIsMsgTrSending = value;
    }
    public boolean getTrSending() {
        return mIsMsgTrSending;
    }

}
