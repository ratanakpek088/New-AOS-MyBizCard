package bizplay.kosign.com.bizplay.adapters.item;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import bizplay.kosign.com.bizplay.R;
import bizplay.kosign.com.bizplay.common.ui.CircleTransform;
import bizplay.kosign.com.bizplay.common.ui.util.FormatUtil;
import bizplay.kosign.com.bizplay.item.ParticipantItem;
import bizplay.kosign.com.bizplay.item.USER_REC_ITEM;

/**
 * Created by Daravuth on 3/10/2017.
 */

public class ParticipantAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<USER_REC_ITEM> mParticipant;
    private View.OnClickListener mClickListener;


    public ParticipantAdapter() {
    }

    public ParticipantAdapter(Context mContext, ArrayList<USER_REC_ITEM> mParticipant, View.OnClickListener mClickListener) {
        this.mContext = mContext;
        this.mParticipant = mParticipant;
        this.mClickListener=mClickListener;
    }

    @Override
    public int getCount() {
        return mParticipant.size();
    }

    @Override
    public Object getItem(int position) {
        return mParticipant.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public void remove(int position) {
        mParticipant.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView==null || convertView.getTag() ==null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.activity_a007_4_1_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else holder = (ViewHolder) convertView.getTag();

        holder.ll_del.setTag(R.id.participant_position, position);
        holder.ll_del.setOnClickListener(mClickListener);
        holder.tv_name.setText(mParticipant.get(position).getRCPT_USER_NM());
        holder.tv_companyNm.setText(mParticipant.get(position).getRCPT_USER_BSNN_NM());
        holder.tv_departNm.setText(mParticipant.get(position).getRCPT_USER_PSN_NM());

        if(mParticipant.get(position).getRCPT_USER_BSNN_NM()==null || mParticipant.get(position).getRCPT_USER_BSNN_NM().equals("")){
            holder.tv_companyNm.setText(FormatUtil.formatePhoneNumber(mParticipant.get(position).getRCPT_USER_HPNO()));
        }

        if(!mParticipant.get(position).getRCPT_USER_PSN_IMG().equals("") || mParticipant.get(position).getRCPT_USER_PSN_IMG() != null){
            Glide.with(mContext).load(mParticipant.get(position).getRCPT_USER_PSN_IMG()).transform(new CircleTransform(mContext)).placeholder(R.drawable.user_empty_icon).error(R.drawable.user_empty_icon).into(holder.iv_participant);
        }else{
            holder.iv_participant.setImageBitmap(null);
            holder.iv_participant.setImageResource(R.drawable.user_empty_icon);
        }

        if(mParticipant.get(position).getRCPT_USER_PSN_NM().equals("") || mParticipant.get(position).getRCPT_USER_PSN_NM()==null){
            holder.tv_devider.setVisibility(View.GONE);
        }else holder.tv_devider.setVisibility(View.VISIBLE);
        return convertView;
    }

    class ViewHolder{
        LinearLayout ll_del;
        ImageView iv_participant;
        TextView tv_name, tv_companyNm, tv_departNm, tv_devider;

        public ViewHolder(View view) {
            ll_del = (LinearLayout) view.findViewById(R.id.ll_del);
            iv_participant = (ImageView) view.findViewById(R.id.iv_participant);
            tv_name = (TextView) view.findViewById(R.id.tv_name);
            tv_companyNm = (TextView) view.findViewById(R.id.tv_companyNm);
            tv_departNm = (TextView) view.findViewById(R.id.tv_departNm);
            tv_devider = (TextView) view.findViewById(R.id.tv_divide);
        }
    }
}
