package bizplay.kosign.com.bizplay.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by Daravuth on 1/13/2017.
 */

public class TX_MYCD_MBL_L003_RES extends TxMessage {

    private TX_MYCD_MBL_L003_RES_DATA mTxKeyData; // 전문명세 필드

    public TX_MYCD_MBL_L003_RES(Object obj) throws Exception {
        mTxKeyData = new TX_MYCD_MBL_L003_RES_DATA();
        super.initRecvMessage(obj);
    }
    public TX_MYCD_MBL_L003_RES_REC getREC() throws JSONException,Exception {
        return new TX_MYCD_MBL_L003_RES_REC(getRecord(mTxKeyData.TRAN_KIND_REC));
    }

    private class TX_MYCD_MBL_L003_RES_DATA{
        private String TRAN_KIND_REC="TRAN_KIND_REC";
    }
}
